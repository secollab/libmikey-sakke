
# Arguments: 
#
#   tags         -- tags are updated on each build implicitly,
#                   specifying this target causes nothing else to be
#                   done
#   native       -- stop after building native Java6 version
#   skipj6       -- skip Java 6 target
#   skipjme      -- skip Java ME target
#   run          -- run MIDlet with Sun CLDC emulator
#   rim          -- employ Blackberry-specific APIs and generator .COD
#   rim-package  -- assume Blackberry code already built; just package (implicit for rim)
#   upload       -- upload .COD to simulator
#   sign         -- sign Blackberry .COD (implicit for upload-usb)
#   upload-usb   -- upload signed .COD via USB to real Blackberry device
# 
while [ $# -gt 0 ]; do ARG=$1; shift; case $ARG in
   tags)          ONLY_TAGS=1 ;;
   skipj6)        SKIP_JAVA6=1 ;;
   skipjme)       SKIP_JAVAME=1 ;;
   native)        ONLY_NATIVE=1 ;;
   run)           RUN_MIDLET=1 ;;
   rim)           BLACKBERRY_SPECIFIC=1 ;;
   rim-package)   BLACKBERRY_PACKAGE=1 ;;
   upload)        UPLOAD_COD=1 ;;
   sign)          SIGN_COD=1 ;;
   upload-usb)    UPLOAD_USB=1 ;;
esac; done


if [[ "$OS" = *Windows* ]]
then _sep_=';'
else _sep_=':'
fi


cd $(dirname $0)
 

function retrotranslate
{
   java -jar tools/retrotranslator/retrotranslator-transformer-1.2.9.jar "$@"
}


function compile # <target> # $APP_JARS $EXTRA_SRCDIR $PLATFORM_JARS
{
   target=$1

   # build local classpath for compilation
   declare CP="src/$target"
   [ -z "$EXTRA_SRCDIR" ] || CP="$CP$_sep_$EXTRA_SRCDIR"
   CP="$CP${_sep_}src/main${_sep_}src/test$_sep_$APP_JARS$_sep_$PLATFORM_JARS"

   # compiling is done in stages here in case the target has
   # target-specific re-implementations (which would otherwise send
   # more than one class of the same name to the compiler -- an error)
   mkdir -p $target
   javac -d $target -cp "$CP" $(find src/{main,test} -name '*.java') || return $?
   if [ -n "$EXTRA_SRCDIR" ]; then
   javac -d $target -cp "$CP" $(find $EXTRA_SRCDIR -name '*.java') || return $?
   fi
   javac -d $target -cp "$CP" $(find src/$target -name '*.java') || return $?
}


function compile_only # <target> [<outdir=target>] # $APP_JARS $PLATFORM_JARS
{
   target=$1
   outdir=${2:-$target}

   mkdir -p $outdir
   javac -d $outdir -cp "src/$target$_sep_$APP_JARS$_sep_$PLATFORM_JARS" $(find src/$target -name '*.java')
}


function clean_compile # <target> # $APP_JARS $PLATFORM_JARS
{
   target=$1

   rm -rf $target
   mkdir -p $target
   compile "$@"
}


function bcel_tool # <ToolClassName> <args...>
{
   java -classpath "tools${_sep_}tools/bcel/bcel-5.2.jar" "$@"
}


function simple_transform # <target> # $TRANSFORM_OPTS
{
   target=$1
   (cd $target && find -name '*.class' | cut -c3- | sed 's/\.class$//') > $target/classes
   bcel_tool ClassWriter "$target$_sep_$APP_JARS" $target $target/classes -u "$TRANSFORM_OPTS"
}


function retro_transform # <target> # $APP_JARS $PLATFORM_JARS
{
   retrotranslate -srcdir $1 -target 1.3 -smart -classpath "$1$_sep_$APP_JARS$_sep_$PLATFORM_JARS"
}


function retro_transform_backport_stage_and_verify # <target> <RequireClasses...> # $APP_JARS $PLATFORM_JARS $BACKPORT_OPTS
{
   target=$1
   shift

   # transform with no verification
   echo  "Transforming source allowing unresolved symbols..."
   retrotranslate -srcdir $target -target 1.3 -smart -classpath "$target$_sep_$APP_JARS$_sep_$PLATFORM_JARS" -backport "$BACKPORT_OPTS" || return $?

   # stage to explode deps (keeping the previous objects)
   echo  "Exploding dependencies from APP_JARS..."
   stage --keep-all $target $@ || return $?

   declare APP_JARS=$target.all

   # transform and verify in new target allowing for additional
   # dependencies to be brought in from the previous stage
   # this supports transformations within dependent APP_JARS
   echo  "Transforming and verifying complete class manifest..."
   retrotranslate -srcdir $target -target 1.3 -smart -verify -classpath "$target$_sep_$APP_JARS$_sep_$PLATFORM_JARS" -backport "$BACKPORT_OPTS" || return $?
   
   rm -rf $target.all || return $?

   echo "Staging final result..."
   stage $target $@
}


function stage # [--keep-all] <target> <MainClass> [<force-external> [<force-external> [...]]]
{
   keep_all=false
   if [ "$1" = "--keep-all" ]; then shift; keep_all=true; fi

   target=$1; shift

   echo "Walking dependencies..."

   bcel_tool DependencyEmitter "$target$_sep_$APP_JARS" "$PLATFORM_JARS" "$@" | sort > $target/classes

   depcheckrc=${pipestatus[1]}${PIPESTATUS[0]}
   [ $depcheckrc -eq 0 ] || return $depcheckrc

   rm -rf $target.all || return $?
   mv $target $target.all || return $?

   echo "Unpacking to $target..."

   # Javolution provides NoSuchElementException in j2me but it is
   # provided by the CLDC 1.1 and RIM platform libraries.  As part
   # of staging transform ref to the built-in version.  This will
   # not affect the 'native' build since there will be no j2me refs.

   bcel_tool ClassWriter "$target.all$_sep_$APP_JARS" $target $target.all/classes \
      'j2me.util.NoSuchElementException:java.util.NoSuchElementException'

   $keep_all || rm -rf $target.all
}


function validate_cldc # <target> # $PLATFORM_JARS
{
   target=$1

   echo "Preverifying in $target..."

   preverify -classpath "$PLATFORM_JARS" -d $target $target
}


function retro_build # <target> [<AdditionalClasses...>] # $APP_JARS $PLATFORM_JARS $BACKPORT_OPTS
{
   target=$1
   shift

   echo "Compiling to $target..."
   clean_compile $target || return $?

   echo "First pass retro to 1.3 target without API replacement..."
   retro_transform $target || return $?

   echo "Second pass retro and verification with backport..."
   retro_transform_backport_stage_and_verify $target MikeySakkeTest $@ || return $?

   echo "Preparing CLDC classes..."
   validate_cldc $target || return $?
}


function package_midlet() # <Name> <target>
{
   Name=$1
   target=$2

   echo "Creating $Name JAR..."

   cp ../icons/ms-test-48.png $target/MikeySakkeTest.png

   jar cvfm MikeySakkeTest-$Name.jar MikeySakkeTest.jad.in -C $target . || exit $?

   JAR_SIZE=$(ls -l MikeySakkeTest-$Name.jar | awk '{print $5}')

   echo "Creating $Name JAD..."

   cat   > MikeySakkeTest-$Name.jad MikeySakkeTest.jad.in
   echo >> MikeySakkeTest-$Name.jad "MIDlet-Jar-Size: $JAR_SIZE"
}


function semi_concat # <VAR> <elem> [<elem> [...]]
{
   VAR=$1; shift
   if [ $# -eq 0 ]
   then
      export $VAR=''
      return
   fi
   export $VAR=$1
   shift
   [ $# -ge 1 ] || return
   while [ $# -ge 1 ]
   do
      export $VAR="$(eval echo "\$$VAR");$1"
      shift
   done
}


for tool in tools/*.java
do
   if [ $tool -nt ${tool%.java}.class ]
   then
      echo "Making build tools..."
      javac -cp "tools${_sep_}tools/bcel/bcel-5.2.jar" tools/*.java || exit $?
   fi
done


if silence=$(ctags --version) && tip=$(git log -1 --pretty=%h .)
then
   echo "Checking whether tags need updating..."
   if ! [ -r tags -a -r tags.commit ]
   then
      OIFS="$IFS"; IFS=$'\n'; files=($(git ls-files '*.java')); IFS="$OIFS"
      echo "Creating initial tags file..."
      ctags "${files[@]}"
      echo "Tags created at $tip."
   else
      tags_commit=$(cat tags.commit)
      OIFS="$IFS"; IFS=$'\n'; files=($(git diff --name-only --relative $tags_commit -- '*.java')); IFS="$OIFS"
      if [ ${#files[@]} -gt 0 ]
      then
         echo "Updating tags for"
         for f in "${files[@]}"
         do
            echo "   $f"
            grep -v "$f" tags > tags.clear
            mv -f tags.clear tags
         done
         # the complexity here works around a not understood corruption bug
         if ! ctags -a "${files[@]}"
         then
            sed -i 1d tags
            if ctags -a "${files[@]}"
            then
               echo "Tags file fixed."
            else
               rm -f tags
               echo "Tag corruption workaround failed.  Removed."
            fi
         fi
         echo "Tags updated to $tip."
      else
         echo "No source changes from $tags_commit."
      fi
   fi
   echo $tip > tags.commit
fi


[ "$ONLY_TAGS" != 1 ] || exit 0


# default implementation selections
semi_concat TRANSFORM_OPTS \
   com.selex.math.ModInt:com.selex.math.SunModInt \
   com.selex.math.ModIntValue:com.selex.math.SunModIntValue \
   com.selex.math.ConstantBigInt:com.selex.math.SunConstantBigInt \
   com.selex.math.InplaceModInt:com.selex.math.SunInplaceModInt \


if [ "$SKIP_JAVA6" != 1 ]
then
   echo "Compiling source for Java 6..."

   PLATFORM_JARS=builtin
   APP_JARS="lib/bcprov-jdk15on-147.jar${_sep_}lib/commons-logging-1.1.1.jar${_sep_}lib/javolution-5.5.1.jar"
   clean_compile java6 || exit $?

   echo "Implementation selection..."
   simple_transform java6

   stage java6 MikeySakkeTest \
      org.apache.commons.logging.impl.LogFactoryImpl \
      org.apache.commons.logging.impl.Jdk14Logger \
      || exit $?

   echo "Creating Java6 jar..."

   jar cvfm MikeySakkeTest-Java6.jar MikeySakkeTest.jad.in -C java6 . || exit $?
fi


[ "$ONLY_NATIVE" != 1 ] || exit 0


J2ME_BIG_INTEGER=alt.math.bc.BigInteger


semi_concat BACKPORT_OPTS \
   java.lang.Object.clone:com.selex.retro.pacify.java.lang.Object.clone \
   java.util.Arrays.copyOf:com.selex.retro.adapt.java.util.Arrays.copyOf \
   java.lang.Class.getComponentType:com.selex.retro.pacify.java.lang.Class.getComponentType \
   java.lang.Class.getClassLoader:com.selex.retro.pacify.java.lang.Class.getClassLoader \
   java.lang.ClassLoader:com.selex.retro.subst.java.lang.ClassLoader \
   java.lang.Class.desiredAssertionStatus:com.selex.retro.pacify.java.lang.Class.desiredAssertionStatus \
   java.lang.AssertionError:com.selex.retro.subst.java.lang.AssertionError \
   java.security.cert.X509Certificate:com.selex.retro.subst.java.security.cert.X509Certificate \
   java.security.PrivateKey:com.selex.retro.subst.java.security.NotImplemented \
   java.lang.Integer.valueOf:com.selex.retro.adapt.ValueOf.valueOf \
   java.lang.Byte.valueOf:com.selex.retro.adapt.ValueOf.valueOf \
   java.lang.Boolean.valueOf:com.selex.retro.adapt.ValueOf.valueOf \
   java.util.TreeMap:javolution.util.FastMap \
   java.lang.Enum:com.selex.retro.subst.java.lang.Enum \
   java.lang.String.isEmpty:com.selex.retro.adapt.java.lang.String.isEmpty \
   java.math.BigInteger:$J2ME_BIG_INTEGER \
   java.nio.Buffer:j2me.nio.Buffer \
   java.nio.ByteBuffer:j2me.nio.ByteBuffer \
   java.io:j2me.io \
   java.lang:j2me.lang \
   java.util:j2me.util \
   java.security:j2me.security \
   $TRANSFORM_OPTS \


if [ "$SKIP_JAVAME" != 1 ]
then
   echo "Compiling source for Java ME..."

   PLATFORM_JARS="lib/cldcapi11.jar${_sep_}lib/midpapi20.jar"
   APP_JARS="lib/cldc_bccore_classes.zip${_sep_}lib/javolution-5.5.1-j2me.jar"
   retro_build j2me || exit $?

   package_midlet MIDlet j2me || exit $?
fi


if [ "$RUN_MIDLET" = 1 ]
then
   echo "Executing with cldc emulator..."
   emulator -Xdescriptor:MikeySakkeTest-MIDlet.jad
fi

# everything below here is Blackberry-specific

PLATFORM_JARS="lib/net_rim_api_600.jar"
APP_JARS="lib/cldc_bccore_classes.zip${_sep_}lib/javolution-5.5.1-j2me.jar"
EXTRA_SRCDIR="src/j2me"
semi_concat BACKPORT_OPTS \
   com.selex.math.ModInt:net.rim.device.api.crypto.CryptoInteger \
   com.selex.math.ModIntUtils.bitLength:net.rim.device.api.crypto.CryptoByteArrayArithmetic.getNumBits \
   com.selex.math.ModIntValue:com.selex.math.ByteArrayModIntValue \
   com.selex.math.ConstantBigInt:com.selex.math.ByteArrayConstantBigInt \
   com.selex.math.InplaceModInt:com.selex.math.ByteArrayInplaceModInt \
   $BACKPORT_OPTS

# for plain J2ME java.nio.Buffer and java.nio.ByteBuffer are not
# provided, for Blackberry they are, prevent replacing it with an
# inferior implementation...
#
BACKPORT_OPTS=${BACKPORT_OPTS//java.nio.Buffer/xava.nio.Buffer}
BACKPORT_OPTS=${BACKPORT_OPTS//java.nio.ByteBuffer/xava.nio.ByteBuffer}

# ...but, RIM does not provide allocate() or {put,get}Long
semi_concat BACKPORT_OPTS \
   java.nio.ByteBuffer.allocate:java.nio.ByteBuffer.allocateDirect \
   java.nio.ByteBuffer.putLong:com.selex.adapt.ByteBufferExtras.putLong \
   java.nio.ByteBuffer.getLong:com.selex.adapt.ByteBufferExtras.getLong \
   $BACKPORT_OPTS

if [ "$BLACKBERRY_SPECIFIC" = 1 ]
then
   echo "Custom build for Blackberry..."
   retro_build rim com.selex.mikeysakke.user.RIMPersistentKeyStore || exit $?
fi

if [ "$BLACKBERRY_SPECIFIC" = 1 -o "$BLACKBERRY_PACKAGE" = 1 ]
then

   # assume that manual byte-code manipulation has occurred if
   # repackaging specified without compilation -- so revalidate
   [ "$BLACKBERRY_SPECIFIC" = 1 ] || validate_cldc rim || exit $?

   package_midlet RIM-MIDlet rim || exit $?

   echo "Generating Blackberry package..."
   rapc import="$PLATFORM_JARS" codename=MikeySakkeTest-RIM -nowarn \
                               -midlet MikeySakkeTest-RIM-MIDlet.jad \
                                       MikeySakkeTest-RIM-MIDlet.jar || exit $?
fi

if [ "$UPLOAD_COD" = 1 ]
then
   echo "Uploading package to Blackberry simulator..."
   javaloader -u load MikeySakkeTest-RIM.cod
fi


if [ "$SIGN_COD" = 1 ] || [ "$UPLOAD_USB" = 1 ]
then
   echo "Signing Blackberry package..."
   # assume signature-tool in same path as javaloader
   SIGTOOL_PATH="$(dirname $(which javaloader))"
   java -jar "$SIGTOOL_PATH/SignatureTool.jar" -a -c MikeySakkeTest-RIM.cod
fi


if [ "$UPLOAD_USB" = 1 ]
then
   echo "Uploading package to Blackberry device..."
   javaloader -u load MikeySakkeTest-RIM.cod
fi


echo "Done."

