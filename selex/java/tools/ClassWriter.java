import org.apache.bcel.util.ClassPath;
import org.apache.bcel.util.Repository;
import org.apache.bcel.util.SyntheticRepository;
import org.apache.bcel.classfile.JavaClass;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class ClassWriter
{
   public static void main(String[] args)
   {
      if (args.length < 3 || args.length > 5)
      {
         System.err.println("Usage: ClassWriter <classpath> <targetdir> <classlistfile> [[-u] <transformations...>]");
         System.exit(20);
         return;
      }

      ClassPath classPath = new ClassPath(args[0]);
      File targetDir = new File(args[1]);
      File classListFile = new File(args[2]);

      boolean onlyWriteIfTransformed = args.length > 3 && args[3].equals("-u");

      int transformArg = 3;
      if (onlyWriteIfTransformed)
         ++transformArg;

      ReferenceTranslator translator = args.length > transformArg?
         new ReferenceTranslator(args[transformArg]) : null;

      System.setProperty("sun.boot.class.path","");
      System.setProperty("java.class.path","");

      Repository repo = SyntheticRepository.getInstance(classPath);

      try
      {
         BufferedReader in = new BufferedReader(
                             new InputStreamReader(
                             new FileInputStream(classListFile)));

         for (String line; (line = in.readLine()) != null;)
         {
            JavaClass c = repo.loadClass(line);
            
            boolean transformed = false;
            if (translator != null)
               if (translator.translate(c))
                 transformed = true;
               else if (onlyWriteIfTransformed)
                  continue;

            line = line.replace('.', '/');

            File classFile = new File(targetDir, line + ".class");
            File classDir = classFile.getParentFile();

            if (!classDir.exists())
               classDir.mkdirs();

            c.dump(classFile);

            if (transformed)
               System.out.println("Transformed '"+classFile+"'");
            else
               System.out.println("Written '"+classFile+"'");
         }
      }
      catch (Exception e)
      {
         System.err.println("Failed: " + e.toString());
         System.exit(1);
      }
   }
}

