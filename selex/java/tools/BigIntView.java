import java.math.BigInteger;

public class BigIntView
{
   static final BigInteger fromString(String s)
   {
      if (s.matches("^-?[0-9]+$"))
         return new BigInteger(s, 10);
      if (s.endsWith("h"))
         return new BigInteger(s.replace("h",""), 16);
      return new BigInteger(s, 16);
   }

   public static void main(String[] args)
   {
      if (args.length < 1)
      {
         System.err.print("\n"
                         +"Usage: BigIntView <i:asciirep>[h] [<p:asciirep>[h]]\n"
                         +"\n"
                         +"  Radix defaults to 16 unless input consists only of\n"
                         +"  digits [0-9] with optional sign in which case 10\n"
                         +"  is used.  The latter form can be forced to base 16\n"
                         +"  by suffixing with the character 'h'\n"
                         +"\n"
                         +"Example:\n"
                         +"\n"
                         +"    java -cp tools BigIntView -3 ffffffff00000001000000000000000000000000ffffffffffffffffffffffff\n"
                         +"\n"
                         +"  yields\n"
                         +"\n"
                         +"    DEC: i:            -3\n"
                         +"    HEX: i:            -3\n"
                         +"    DEC: p:            115792089210356248762697446949407573530086143415290314195533631308867097853951\n"
                         +"    HEX: p:            ffffffff00000001000000000000000000000000ffffffffffffffffffffffff\n"
                         +"    DEC: i (mod p):    115792089210356248762697446949407573530086143415290314195533631308867097853948\n"
                         +"    HEX: i (mod p):    ffffffff00000001000000000000000000000000fffffffffffffffffffffffc\n"
                         +"\n"
                         +"Parsing output:\n"
                         +"\n"
                         +"  Values are written at column 20; use 'cut -c20-' to\n"
                         +"  filter out leader.  Use 'grep HEX' or 'grep DEC' to\n"
                         +"  filter on a particular output base.\n"
                         +"\n"
                         +"    java -cp tools BigIntView -3 ffffffff00000001000000000000000000000000ffffffffffffffffffffffff | grep DEC | cut -c20-\n"
                         +"\n"
                         +"  yields\n"
                         +"\n"
                         +"    -3\n"
                         +"    115792089210356248762697446949407573530086143415290314195533631308867097853951\n"
                         +"    115792089210356248762697446949407573530086143415290314195533631308867097853948\n"
                         +"\n"
                         );
         System.exit(20);
      }
      BigInteger i = fromString(args[0]);
      BigInteger p = (args.length == 2)? fromString(args[1]) : null;
      System.out.println("DEC: i:            "+i.toString(10));
      System.out.println("HEX: i:            "+i.toString(16));
      if (p != null)
      {
         System.out.println("DEC: p:            "+p.toString(10));
         System.out.println("HEX: p:            "+p.toString(16));
         i = i.mod(p);
         System.out.println("DEC: i (mod p):    "+i.toString(10));
         System.out.println("HEX: i (mod p):    "+i.toString(16));
      }
   }
}
