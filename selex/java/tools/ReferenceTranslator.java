import org.apache.bcel.Constants;
import org.apache.bcel.classfile.Constant;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.generic.ConstantPoolGen;

import java.util.Map;
import java.util.HashMap;
import java.util.HashSet;

import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ReferenceTranslator
{
   private HashSet<String> subjects = new HashSet<String>();
   private HashMap<String,String> substitutions = new HashMap<String,String>();
   private HashMap<Pattern,String> sigPatterns = new HashMap<Pattern,String>();

   public ReferenceTranslator(String translationSpec)
   {
      for (String specEntry : translationSpec.split(";"))
      {
         String[] entry = specEntry.split(":");
         if (entry.length != 2)
         {
            System.err.println("Skipping incorrectly specified translation '"+specEntry+"'");
            continue;
         }
         subjects.add(entry[0]);
         String from = entry[0].replace('.', '/'),
                  to = entry[1].replace('.', '/');
         substitutions.put(from, to);
         sigPatterns.put(Pattern.compile("L"+from+";"), "L"+to+";");
      }
   }

   public boolean translate(JavaClass jc)
   {
      // XXX: this may break explicit anon class replacement (but
      // XXX: that's an edge case I'd hope)
      String rootClassName = jc.getClassName().replaceAll("[$].*","");

      // don't translate references within a class that
      // is itself subject to replacement.
      if (subjects.contains(rootClassName))
         return false;

      boolean transformed = false;

      ConstantPoolGen gen = new ConstantPoolGen(jc.getConstantPool().getConstantPool());

      for (int i = 1, end = gen.getSize(); i < end; ++i)
      {
         Constant c = gen.getConstant(i);
         switch (c.getTag())
         {
         case Constants.CONSTANT_Utf8:
            if (transform((ConstantUtf8) c))
               transformed = true;
            break;

         case Constants.CONSTANT_Double:
         case Constants.CONSTANT_Long:
            ++i; // additional null entry for each of these
            break;
         }
      }
      if (transformed)
         jc.setConstantPool(gen.getFinalConstantPool());
      return transformed;
   }

   private boolean transform(ConstantUtf8 c)
   {
      String s = c.getBytes();

      // early out if this is a plain class name
      // TODO: prob need to check inner classes and anons
      {
         String t = substitutions.get(s);
         if (t != null)
         {
            c.setBytes(t);
            return true;
         }
      }

      boolean transformed = false;

      // otherwise attempt each pattern replacement globally
      //
      for (Map.Entry<Pattern, String> e : sigPatterns.entrySet())
      {
         Matcher m = e.getKey().matcher(s);
         if (m.find())
         {
            String replacement = e.getValue();
            StringBuffer sb = new StringBuffer();
            m.appendReplacement(sb, replacement);
            while (m.find())
               m.appendReplacement(sb, replacement);
            m.appendTail(sb);
            transformed = true;
            s = sb.toString();
         }
      }

      if (transformed)
         c.setBytes(s);

      return transformed;
   }
}

