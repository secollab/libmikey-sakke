import org.apache.bcel.util.ClassPath;
import org.apache.bcel.util.Repository;
import org.apache.bcel.util.SyntheticRepository;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.DescendingVisitor;
import org.apache.bcel.classfile.EmptyVisitor;
import org.apache.bcel.classfile.JavaClass;

import java.util.Queue;
import java.util.LinkedList;
import java.util.HashSet;
import java.util.ArrayList;

public class DependencyEmitter extends EmptyVisitor
{
   Repository repo;
   Repository sys = null;
   Queue<String[]> todo = new LinkedList<String[]>();
   HashSet<String> done = new HashSet<String>();

   public DependencyEmitter()
   {
      repo = SyntheticRepository.getInstance();
   }

   public DependencyEmitter(String classPath)
   {
      // XXX: this needs extending for other JVMs than Sun's if they
      // XXX: are needed to be supported.
      System.setProperty("sun.boot.class.path","");
      System.setProperty("java.class.path","");

      repo = SyntheticRepository.getInstance(new ClassPath(classPath));
   }

   private final static String sun_boot_class_path = System.getProperty("sun.boot.class.path");
   private final static String java_class_path = System.getProperty("java.class.path");

   public DependencyEmitter(String classPath, String sysClassPath)
   {
      this(classPath);

      if (sysClassPath.equals("builtin"))
      {
         if (!sun_boot_class_path.isEmpty())
            sysClassPath = sun_boot_class_path;
         if (!java_class_path.isEmpty())
         {
            if (!sysClassPath.isEmpty())
               sysClassPath += ";";
            sysClassPath += java_class_path;
         }
      }

      sys = SyntheticRepository.getInstance(new ClassPath(sysClassPath));
   }

   public void addClasses(String... classNames)
   {
      for (String className : classNames)
         todo.add(new String[] {className, "Command line"});
   }

   boolean verbose = false;
   JavaClass currentClass;
   String currentPath;

   @Override
   public void visitConstantClass(ConstantClass obj)
   {
      ConstantPool cp = currentClass.getConstantPool();
      String bytes = obj.getBytes(cp);
      if (done.contains(bytes))
         return;
      todo.add(new String[] {bytes, currentPath});
      done.add(bytes);
   }

   @Override
   public void visitLocalVariable(LocalVariable obj)
   {
      String type = obj.getSignature();
      if (type.length() < 2 || type.charAt(0) != 'L')
         return;
      type = type.substring(1, type.length()-1);
      if (done.contains(type))
         return;
      todo.add(new String[] {type, currentPath});
      done.add(type);
   }

   public int emitRecursive()
   {
      int skipped = 0;
      ArrayList<String> failed = new ArrayList<String>();

      while (!todo.isEmpty())
      {
         String[] top = todo.remove();

         done.add(top[0]);

         if (top[0].startsWith("["))
         {
            if (verbose)
               System.err.println("::: Skipping '"+top[0]+"'");
            ++skipped;
            continue;
         }

         // Try system path first if provided:
         // (platform classes cannot be replaced)
         //
         try {
            if (sys != null && sys.loadClass(top[0]) != null)
            {
               if (verbose)
                  System.err.println("::: Provided by platform '"+top[0]+"'");
               ++skipped;
               continue;
            }
         } catch (ClassNotFoundException _) {}

         try
         {
            currentClass = repo.loadClass(top[0]);
            currentPath = top[1] + " :: " + top[0];
            System.out.println(top[0]);
            new DescendingVisitor(currentClass, this).visit();
         }
         catch (ClassNotFoundException _)
         {
            System.err.print("::: Could not find '"+top[0]+"'");
            System.err.println(" required by '"+top[1]+"'");
            if (verbose)
               failed.add(top[0] + " (required by '"+top[1]+"')");
            else
               failed.add(top[0]);
            continue;
         }
      }

      System.err.println("Application CLASSPATH: " + repo.getClassPath());
      if (sys != null)
      System.err.println("System CLASSPATH: " + sys.getClassPath());
      System.err.println("Skipped count = " + skipped);
      for (String f : failed)
      System.err.println("Failed: " + f);
      System.err.println("Failed count = " + failed.size());
      return failed.size();
   }

   public static void main(String[] args) throws Exception
   {
      DependencyEmitter emitter;
      boolean verbose = false;
      boolean help = false;

      ArrayList<String> env = new ArrayList<String>();
      boolean doneEnv = false;
      ArrayList<String> classes = new ArrayList<String>();

      for (String arg : args)
      {
         if (!verbose)
            if (arg.equals("-v") || arg.equals("--verbose"))  { verbose = true; continue; }
         
         if (!help)
            if (arg.equals("-h") || arg.equals("--help"))     { help = true; break; }

         if (arg.equals("--"))                                { doneEnv = true; continue; }

         if (!doneEnv && env.size() < 2)
            env.add(arg);
         else
            classes.add(arg);
      }
      if (help || classes.size() == 0)
      {
         System.err.println("Usage: DependencyEmitter [-v] [<project-classpath> [<system-classpath>]] [--] <classname> [<classname> [...]]");
         System.exit(20);
         return;
      }
      if (env.size() == 2)
         emitter = new DependencyEmitter(env.get(0), env.get(1));
      else if (env.size() == 1)
         emitter = new DependencyEmitter(env.get(0));
      else
         emitter = new DependencyEmitter();
   
      emitter.addClasses(classes.toArray(new String[classes.size()]));
      emitter.verbose = verbose;

      System.exit(emitter.emitRecursive());
   }

}

