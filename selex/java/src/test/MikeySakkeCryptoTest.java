import com.selex.mikeysakke.crypto.AlgorithmFactory;
import com.selex.mikeysakke.crypto.SigningParameterSet;
import com.selex.mikeysakke.crypto.SakkeParameterSet;
import com.selex.mikeysakke.crypto.SecureRandomGenerator;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.mikeysakke.user.RuntimeKeyStorage;

import com.selex.mikeysakke.test.MIKEYSAKKETest;

import com.selex.util.RandomGenerator;
import com.selex.util.OctetString;

import com.selex.test.ui.SimpleUI;
import com.selex.util.TimeMeasure;
import com.selex.log.DefaultLogger;


public class MikeySakkeCryptoTest extends SimpleUI
{
   // FIXME: nested midlet construction is not permitted; workaround
   public int test(String[] argv) { return static_test(argv); }
   public static int static_test(String[] argv)
   {
      com.selex.mikeysakke.crypto.ECCSI ECCSI = AlgorithmFactory.getSignatureAlgorithm("local");
      com.selex.mikeysakke.crypto.SAKKE SAKKE = AlgorithmFactory.getKeyEncapsulationAlgorithm("local");

      DefaultLogger.out.print("\n---------- Community setup: --------------------\n\n");

      String community = "aliceandbob.co.uk";

      OctetString communityPublicAuthenticationKey = new OctetString(OctetString.skipws
         ("04                                 "
         +"50D4670B DE75244F 28D2838A 0D25558A"
         +"7A72686D 4522D4C8 273FB644 2AEBFA93"
         +"DBDD3755 1AFD263B 5DFD617F 3960C65A"
         +"8C298850 FF99F203 66DCE7D4 367217F4"));

      OctetString communityPublicKey = new OctetString(OctetString.skipws
         ("04                                 "
         +"5958EF1B 1679BF09 9B3A030D F255AA6A"
         +"23C1D8F1 43D4D23F 753E69BD 27A832F3"
         +"8CB4AD53 DDEF4260 B0FE8BB4 5C4C1FF5"
         +"10EFFE30 0367A37B 61F701D9 14AEF097"
         +"24825FA0 707D61A6 DFF4FBD7 273566CD"
         +"DE352A0B 04B7C16A 78309BE6 40697DE7"
         +"47613A5F C195E8B9 F328852A 579DB8F9"
         +"9B1D0034 479EA9C5 595F47C4 B2F54FF2"
         +"                                   "
         +"1508D375 14DCF7A8 E143A605 8C09A6BF"
         +"2C9858CA 37C25806 5AE6BF75 32BC8B5B"
         +"63383866 E0753C5A C0E72709 F8445F2E"
         +"6178E065 857E0EDA 10F68206 B63505ED"
         +"87E534FB 2831FF95 7FB7DC61 9DAE6130"
         +"1EEACC2F DA3680EA 4999258A 833CEA8F"
         +"C67C6D19 487FB449 059F26CC 8AAB655A"
         +"B58B7CC7 96E24E9A 39409575 4F5F8BAE"));

      DefaultLogger.info.println("KMS id: " + community);
      DefaultLogger.info.println("KPAK:   " + communityPublicAuthenticationKey.toHexString());
      DefaultLogger.info.println("Z:      " + communityPublicKey.toHexString());


      DefaultLogger.out.print("\n---------- Alice setup: --------------------\n\n");

      final OctetString.Translation raw = OctetString.Untranslated;

      String alice_id = "2011-02\0tel:+447700900123\0";

      KeyStorage alice_keys = new RuntimeKeyStorage();

      alice_keys.StorePrivateKey(alice_id, "SSK", new OctetString(OctetString.skipws
         ("23F374AE 1F4033F3 E9DBDDAA EF20F4CF"
         +"0B86BBD5 A138A5AE 9E7E006B 34489A0D")));

      alice_keys.StorePublicKey(alice_id, "PVT", new OctetString(OctetString.skipws
         ("04                                 "
         +"758A1427 79BE89E8 29E71984 CB40EF75"
         +"8CC4AD77 5FC5B9A3 E1C8ED52 F6FA36D9"
         +"A79D2476 92F4EDA3 A6BDAB77 D6AA6474"
         +"A464AE49 34663C52 65BA7018 BA091F79")));

      // Both alice and bob share the necessary community parameters.
      //
      alice_keys.StorePublicKey(community, "KPAK", communityPublicAuthenticationKey);
      alice_keys.StorePublicKey(community, "Z", communityPublicKey);
      alice_keys.StorePublicParameter(community, "SakkeSet", "1");

      DefaultLogger.out.print("\n---- Alice validates KMS signing keys ----\n\n");

      TimeMeasure m = new TimeMeasure();
      long dt;

      m.reset();

      boolean validated =
         ECCSI.ValidateSigningKeysAndCacheHS(
               alice_id, community, alice_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.out.println("Validated:       " + validated);
      DefaultLogger.out.println("Time:            " + dt);

      DefaultLogger.info.println("SSK: " + alice_keys.GetPrivateKey(alice_id, "SSK").toHexString());
      DefaultLogger.info.println("PVT: " + alice_keys.GetPublicKey(alice_id, "PVT").toHexString());

      DefaultLogger.out.print("\n---------- Bob setup: --------------------\n\n");

      // Note that for checking against spec; bob happens to have same
      // phone number as alice.
      String bob_id = "2011-02\0tel:+447700900123\0";

      KeyStorage bob_keys = new RuntimeKeyStorage();

      bob_keys.StorePrivateKey(bob_id, "RSK", new OctetString(OctetString.skipws
         ("04                                 "
         +"93AF67E5 007BA6E6 A80DA793 DA300FA4"
         +"B52D0A74 E25E6E7B 2B3D6EE9 D18A9B5C"
         +"5023597B D82D8062 D3401956 3BA1D25C"
         +"0DC56B7B 979D74AA 50F29FBF 11CC2C93"
         +"F5DFCA61 5E609279 F6175CEA DB00B58C"
         +"6BEE1E7A 2A47C4F0 C456F052 59A6FA94"
         +"A634A40D AE1DF593 D4FECF68 8D5FC678"
         +"BE7EFC6D F3D68353 25B83B2C 6E69036B"
         +"                                   "
         +"155F0A27 241094B0 4BFB0BDF AC6C670A"
         +"65C325D3 9A069F03 659D44CA 27D3BE8D"
         +"F311172B 55416018 1CBE94A2 A783320C"
         +"ED590BC4 2644702C F371271E 496BF20F"
         +"588B78A1 BC01ECBB 6559934B DD2FB65D"
         +"2884318A 33D1A42A DF5E33CC 5800280B"
         +"28356497 F87135BA B9612A17 26042440"
         +"9AC15FEE 996B744C 33215123 5DECB0F5")));


      // Both alice and bob share the necessary community parameters.
      //
      bob_keys.StorePublicKey(community, "KPAK", communityPublicAuthenticationKey);
      bob_keys.StorePublicKey(community, "Z", communityPublicKey);
      bob_keys.StorePublicParameter(community, "SakkeSet", "1");


      DefaultLogger.out.print("\n-- Bob validates his SAKKE receiver key --\n\n");

      m.reset();

      validated = 
         SAKKE.ValidateReceiverSecretKey(
               bob_id, community, bob_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.out.println("Validated:       " + validated);
      DefaultLogger.out.println("Time:            " + dt);


      DefaultLogger.info.println("RSK: " + bob_keys.GetPrivateKey(bob_id, "RSK").toHexString());


      DefaultLogger.out.print("\n------ Alice signs message for Bob: ------\n\n");

      OctetString message = new OctetString("message\0", raw);

      m.reset();

      OctetString signature =
         ECCSI.Sign(message.raw(), 0, message.size(),
            alice_id, community, MIKEYSAKKETest.EphemeralFrom6507, alice_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.info.println("Message Text:    " + message.untranslated());
      DefaultLogger.info.println("Message Octets:  " + message.toHexString());
      DefaultLogger.info.println("ECCSI Signature: " + signature.toHexString());
      DefaultLogger.out.println("Time:            " + dt);


      DefaultLogger.out.print("\n---- Bob verifies message from Alice: ----\n\n");

      m.reset();

      boolean verified = ECCSI.Verify(message.raw(), 0, message.size(),
                                signature.raw(), 0, signature.size(),
                                alice_id, community, bob_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.out.println("Verified:        " + verified);
      DefaultLogger.out.println("Time:            " + dt);

      DefaultLogger.out.print("\n---- Alice sends shared secret to Bob: ---\n\n");

      m.reset();

      OctetString encrypted = new OctetString();
      OctetString alice_ssv =
         SAKKE.GenerateSharedSecretAndSED(
               encrypted, bob_id, community, MIKEYSAKKETest.SSVFrom6508, alice_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.out.println("Alice SSV:                " + alice_ssv.toHexString());
      DefaultLogger.info.println("SAKKE Encapsulated Data:  " + encrypted.toHexString());
      DefaultLogger.out.println("Time:                     " + dt);


      DefaultLogger.out.print("\n------- Bob extracts shared secret: ------\n\n");

      m.reset();

      OctetString bob_ssv =
         SAKKE.ExtractSharedSecret(encrypted, bob_id, community, bob_keys);

      dt = m.getDeltaTimeMillis();

      DefaultLogger.out.println("Bob SSV:                  " + bob_ssv.toHexString());
      DefaultLogger.out.println("Time:                     " + dt);


      DefaultLogger.out.print("\n----------- Check equivalence: -----------\n\n");

      DefaultLogger.out.println("Alice SSV == Bob SSV:  " + alice_ssv.equals(bob_ssv));

      return 0;
   }

   public static void main(String[] args) { System.exit(static_test(args)); }
}

