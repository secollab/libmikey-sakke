import com.selex.mikeysakke.kms.Client;
import com.selex.mikeysakke.user.KeyStorage;

import com.selex.mikeysakke.kms.DefaultClient;
import com.selex.mikeysakke.user.RuntimeKeyStorage;

import com.selex.test.ui.SimpleUI;
import com.selex.log.DefaultLogger;
import com.selex.util.PlatformUtils;

import java.util.Collection;

class HandleFetchResult implements Client.CompletionHandler
{
   final KeyStorage keys;

   public HandleFetchResult(KeyStorage keys)
   {
      this.keys = keys;
   }
   public void OnComplete(String identifier, Client.ErrorCode error, String errorText)
   {
      if (error != Client.ErrorCode.Success)
      {
         DefaultLogger.err.println("Failed to fetch keys for identifier '"
                           + identifier + "': " + error + ": " + errorText);
         return;
      }

      DefaultLogger.err.print(
            "Retrieved keys for '"+identifier+"':\n"
          + "\n"
          + "RSK:      " + keys.GetPrivateKey(identifier, "RSK").toHexString() + "\n"
          + "SSK:      " + keys.GetPrivateKey(identifier, "SSK").toHexString() + "\n"
          + "\n"
          + "PVT:      " + keys.GetPublicKey(identifier, "PVT").toHexString() + "\n"
          + "\n"
          );

      Collection<String> communities = keys.GetCommunityIdentifiers();

      DefaultLogger.out.print("Communities known to KMS:\n");

      for (String s : communities)
      {
         DefaultLogger.out.print(
              "\nCommunity: '" + s + "'\n"
            + "\n"
            + "SakkeSet: " + keys.GetPublicParameter(s, "SakkeSet") + "\n"
            + "KPAK:     " + keys.GetPublicKey(s, "KPAK").toHexString() + "\n"
            + "Z:        " + keys.GetPublicKey(s, "Z").toHexString() + "\n"
            );
      }
   }
}


public class MikeySakkeTest extends SimpleUI
{
   public MikeySakkeTest()
   {
      if (!hasConsole())
      {
         addParameter("KMS Server:Port",      UserInputType.Text,     "localhost:7070");
         addParameter("KMS Username",         UserInputType.Text,     "jim");
         addParameter("KMS Password",         UserInputType.Password, "jimpass");
         addParameter("Test month (YYYY-MM)", UserInputType.Text,     "2011-02");
         addParameter("Test user URI",        UserInputType.Text,     "tel:+447700900123");
         addParameter("Enable Crypto Test",   UserInputType.Boolean,  "true");
         addParameter("ECCSI Implementation", UserInputType.Text,     "ECCSI:local");
         addParameter("SAKKE Implementation", UserInputType.Text,     "SAKKE:fake");
         if (PlatformUtils.isRIM())
            addParameter("Server URI suffix", UserInputType.Text,     ";interface=wifi");
      }
   }

   public int test(String[] argv)
   {
      ModIntTest.main(argv);

      com.selex.mikeysakke.crypto.sakke.SakkeUtils.main(argv);

      {
         int message = MikeySakkeMessageTest.static_test(argv);
         if (message != 0)
            return message;
      }

      if (argv.length < 6 || argv[5].equals("true"))
      {
         int crypto = MikeySakkeCryptoTest.static_test(argv);
         if (crypto != 0)
            return crypto;
      }

      String kmsServer = "localhost:7070";
      String username = "jim";
      String password = "jimpass";
      String identifier = "2011-02\0tel:+447700900123\0";
      String serverUriSuffix = null;

      if (argv.length > 0)
         kmsServer = argv[0];
      if (argv.length > 1)
         username = argv[1];
      if (argv.length > 2)
         password = argv[2];
      if (argv.length > 3) // give as two args
         identifier = argv[3] + "\0" + argv[4] + "\0";
      if (argv.length > 5)
         serverUriSuffix = argv[5];

      DefaultLogger.out.println("TestOUT");
      DefaultLogger.err.println("TestERR");
      DefaultLogger.info.println("TestINFO");
      DefaultLogger.verb.println("TestVERB");

      Object o = com.selex.mikeysakke.crypto.SakkeParameterSet.SAKKE_PARAM_SET_1;
      Object p = com.selex.mikeysakke.crypto.SigningParameterSet.ECCSI_6509_PARAM_SET;

      KeyStorage keys = new RuntimeKeyStorage();
      Client kms = new DefaultClient(keys);

      kms.FetchKeyMaterial(kmsServer, Client.DontVerifySSLCertificate,
                           username, password, identifier, serverUriSuffix,
                           new HandleFetchResult(keys));
      return 0;
   }

   public static void main(String[] args) { System.exit(new MikeySakkeTest().test(args)); }
}

