
// build-time selected implementations
import com.selex.math.ModInt;
import com.selex.math.ConstantBigInt;
import com.selex.math.InplaceModInt;

// explicitly test some implementations to which we have the source
import com.selex.math.SunModInt;
import com.selex.math.SunConstantBigInt;
import com.selex.math.SunInplaceModInt;

// java-compatible implementation
import java.math.BigInteger;      // Build-time choice (or provided by platform)
//import alt.math.sun.BigInteger; // Sun
//import alt.math.bc.BigInteger;  // BouncyCastle


import com.selex.log.DefaultLogger;
import com.selex.util.OctetString;
import com.selex.util.TimeMeasure;


public class ModIntTest
{
   public static void main(String[] args)
   {
      ModInt i = new ModInt("deadbeef");
      ModInt q = new ModInt("deadbef2");
      DefaultLogger.out.println(i);
      i.increment(1, q);
      DefaultLogger.out.println(i);
      i.increment(1, q);
      DefaultLogger.out.println(i);
      i.increment(1, q);
      DefaultLogger.out.println(i);
      i.increment(1, q);
      DefaultLogger.out.println(i);

      inplaceTest();
      bitTest();

      // Continue to run the fast tests on Blackberry for metric
      // gathering, the others are disabled.
      if (com.selex.util.PlatformUtils.isRIM())
      {
         crunchTestTunedInplaceModInt();
         crunchTestTunedModInt();
      }

      /*
      crunchTestPlatformBigInteger();
      crunchTestBouncyCastleBigInteger();
      crunchTestSunBigInteger();

      crunchTestSunInplaceModInt();
      crunchTestSunModInt();
      */
   }

   static String asciiBinToHex(String asciiBin)
   {
      return asciiBinToOctetString(asciiBin).toHexString();
   }
   static OctetString asciiBinToOctetString(String asciiBin)
   {
      int bits = asciiBin.length();
      int ai = 0;

      for (final int len = bits; ai != len; ++ai, --bits)
         if (asciiBin.charAt(ai) != '0')
            break;
      if (bits == 0)
         return new OctetString();

      int bytes = (bits + 7) >> 3;
      int msbpow = (bits - 1) & 7;

      OctetString res = new OctetString(bytes);
      byte[] b = res.raw();
      int bi = 0;
      if (msbpow != 7) // special case for non-byte-aligned input
      {
         for (int i = msbpow; i >= 0; --i)
            if (asciiBin.charAt(ai++) != '0')
               b[bi] |= (byte) (1 << i);
         ++bi;
      }
      for (final int be = b.length; bi != be; ++bi)
         for (int i = 7; i >= 0; --i)
            if (asciiBin.charAt(ai++) != '0')
               b[bi] |= (byte) (1 << i);
      return res;
   }
   static String intToAsciiBin(int i)
   {
      if (i == 0)
         return "0";
      StringBuilder s = new StringBuilder();
      for (; i != 0; i >>>= 1)
         s.insert(0, (i&1)==1? '1' : '0');
      return s.toString();
   }
   static String byteArrayToAsciiBin(byte[] a)
   {
      if (a.length == 0)
         return "0";
      StringBuilder s = new StringBuilder();
      for (int i = a.length - 1; i > 0; --i)
      {
         int x = a[i] & 255;
         for (int b = 0; b != 8; ++b, x >>>= 1)
            s.insert(0, (x&1)==1? '1' : '0');
      }
      int x = a[0] & 255;
      for (; x != 0; x >>>= 1)
         s.insert(0, (x&1)==1? '1' : '0');
      return s.toString();
   }
   static void testAsciiBin()
   {
      for (String prefix : new String[] { "", "0", "00000" })
         for (int i = 0; i < 1024; ++i)
         {
            String s = prefix + intToAsciiBin(i);
            DefaultLogger.verb.print(i + " -- " + s + " ==> ");
            OctetString o = asciiBinToOctetString(s).reserve(2, OctetString.ResizeKeepRight);
            byte[] b = o.raw();
            if ((b[0]&255) == ((i>>>8)&255) && (b[1]&255) == (i&255))
               DefaultLogger.verb.println("OKAY");
            else
               DefaultLogger.err.println("Failed -- " + o.toHexString() + " -- " + intToAsciiBin(b[0]&255) + ", " + intToAsciiBin(b[1]&255));
         }
   }

   static void inplaceTest()
   {
      ConstantBigInt q = new ConstantBigInt("deadbef2");
      DefaultLogger.out.println(q);
      InplaceModInt i = new InplaceModInt("deadbeef", q);
      DefaultLogger.out.println(i);
      i.inc();
      DefaultLogger.out.println(i);
      i.inc();
      DefaultLogger.out.println(i);
      i.inc();
      DefaultLogger.out.println(i);
      i.inc();
      DefaultLogger.out.println(i);
      ConstantBigInt p1 = new ConstantBigInt("200000");
      ConstantBigInt p2 = new ConstantBigInt(2, 21);
      DefaultLogger.out.println(p1);
      DefaultLogger.out.println(p2);
      InplaceModInt i1 = new InplaceModInt("deadbeef", p1);
      DefaultLogger.out.println(i1);
      InplaceModInt i2 = new InplaceModInt("deadbeef", p2);
      DefaultLogger.out.println(i2);

      testAsciiBin();

      testInplaceXor();
   }
   static String pad(int n)
   {
      StringBuilder s = new StringBuilder();
      while (n-- > 0)
         s.append(' ');
      return s.toString();
   }
   static String padTo(int n, String s)
   {
      if (s.length() >= n)
         return s;
      return pad(n - s.length()) + s;
   }
   static void testInplaceXor()
   {
      String mod =                   "100000000000000000000000000000000000000000000000000";
      String[] bits1 = new String[] { "00101010010110011101101010101111101101010100100111", "10010101011110110101010100011011111010101011101100" };
      String[] bits2 = new String[] { "00111010101011101110101010101100001101010010011100", "01010111010101111101010110100001101000110110001010" };
      String[] bitsx = new String[] { "00010000111101110011000000000011100000000110111011", "11000010001011001000000010111010010010011101100110" };

      ConstantBigInt q1 = new ConstantBigInt(asciiBinToOctetString(mod).raw());
      ConstantBigInt q2 = q1;
      for (int l = q1.bitsInField(); l != 1; --l)
      { 
         int passes = 0;
         for (int i = 0, e = bits1.length; i != e; ++i)
         {
            String b1 = bits1[i]; // in q1
            String b2 = bits2[i].substring(bits2[i].length() - l); // in q2
            String bx = bits1[i].substring(0, bitsx[i].length() - l) + bitsx[i].substring(bitsx[i].length() - l); // in q1
            InplaceModInt i1 = new InplaceModInt(asciiBinToOctetString(b1).raw(), q1);
            InplaceModInt i2 = new InplaceModInt(asciiBinToOctetString(b2).raw(), q2);
            InplaceModInt ix = new InplaceModInt(asciiBinToOctetString(bx).raw(), q1);
            InplaceModInt t = new InplaceModInt(q1);
            if (!t.assign(i1).xor(i2).equals(ix))
            {
               DefaultLogger.err.println("XOR " + i + " failed: " + t + " != " + ix);
               DefaultLogger.err.println("     " + b1);
               DefaultLogger.err.println(" xor " + padTo(b1.length(), b2));
               DefaultLogger.err.println("  rc " + padTo(b1.length(), byteArrayToAsciiBin(t.toByteArray())));
               DefaultLogger.err.println("  != " + bx);
            }
            else
               ++passes;
         }
         java.io.PrintStream out;
         if (passes == bits1.length)
            out = DefaultLogger.info;
         else
            out = DefaultLogger.err;
         out.print("Testing XOR of integer in " + q2.bitsInField() + " bit field over field of " + q1.bitsInField() + " bits ==> ");
         out.println(passes == bits1.length? "PASSED" : "FAILED");

         q2 = new ConstantBigInt(asciiBinToOctetString(mod.substring(0, l)).raw());
      }
   }

   static class TimedTest
   {
      String name;
      TimeMeasure m = new TimeMeasure();

      void start(String name)
      {
         if (this.name != null)
            end();
         this.name = name;
         DefaultLogger.out.println("Timing " + name + "...");
         m.reset();
      }
      void end()
      {
         long dt = m.getDeltaTimeMillis();
         DefaultLogger.out.println(name + " took " + dt + "ms");
         name = null;
      }
   }

   static final TimedTest t = new TimedTest();

   static final String Q = "e85a2322fa56807579673841602492cf7bae2c24b27021853dd38ca4c1";
   static final String I = "7e10c064dd27625bd11547ef43e50089228a4f116da694d9c4cd851717";
   static final String J = "cafebabedeadbeef";
   static final String K = "0123456789abcdef0123";

   static final int loops = com.selex.util.PlatformUtils.isRIM()? 200 : 
                            com.selex.util.PlatformUtils.isCLDC()? 300 : 10000;

   static void crunchTestPlatformBigInteger()
   {
      final BigInteger q = new BigInteger(Q,16);
      BigInteger i = new BigInteger(I, 16);
      BigInteger j = new BigInteger(J, 16);
      BigInteger k = new BigInteger(K, 16);
      t.start("Platform BigInteger");
      for (int l = 0; l < loops; ++l)
      {
         i = i.multiply(j).mod(q);
         i = i.modPow(k,q);
         j = j.add(k).mod(q);
         k = k.modInverse(q);
         i = i.multiply(k).mod(q);
         k = k.multiply(i.gcd(k)).mod(q);
         k = k.multiply(j.gcd(k)).mod(q);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString(16) + "  j: " + j.toString(16) + "  k: " + k.toString(16));
   }

   static void crunchTestSunBigInteger()
   {
      final alt.math.sun.BigInteger q = new alt.math.sun.BigInteger(Q,16);
      alt.math.sun.BigInteger i = new alt.math.sun.BigInteger(I, 16);
      alt.math.sun.BigInteger j = new alt.math.sun.BigInteger(J, 16);
      alt.math.sun.BigInteger k = new alt.math.sun.BigInteger(K, 16);
      t.start("Sun BigInteger");
      for (int l = 0; l < loops; ++l)
      {
         i = i.multiply(j).mod(q);
         i = i.modPow(k,q);
         j = j.add(k).mod(q);
         k = k.modInverse(q);
         i = i.multiply(k).mod(q);
         k = k.multiply(i.gcd(k)).mod(q);
         k = k.multiply(j.gcd(k)).mod(q);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString(16) + "  j: " + j.toString(16) + "  k: " + k.toString(16));
   }

   static void crunchTestBouncyCastleBigInteger()
   {
      final alt.math.bc.BigInteger q = new alt.math.bc.BigInteger(Q,16);
      alt.math.bc.BigInteger i = new alt.math.bc.BigInteger(I, 16);
      alt.math.bc.BigInteger j = new alt.math.bc.BigInteger(J, 16);
      alt.math.bc.BigInteger k = new alt.math.bc.BigInteger(K, 16);
      t.start("BouncyCastle BigInteger");
      for (int l = 0; l < loops; ++l)
      {
         i = i.multiply(j).mod(q);
         i = i.modPow(k,q);
         j = j.add(k).mod(q);
         k = k.modInverse(q);
         i = i.multiply(k).mod(q);
         k = k.multiply(i.gcd(k)).mod(q);
         k = k.multiply(j.gcd(k)).mod(q);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString(16) + "  j: " + j.toString(16) + "  k: " + k.toString(16));
   }

   static void crunchTestSunModInt()
   {
      final SunModInt q = new SunModInt(Q);
      SunModInt i = new SunModInt(I);
      SunModInt j = new SunModInt(J);
      SunModInt k = new SunModInt(K);
      t.start("SunModInt");
      for (int l = 0; l < loops; ++l)
      {
         i = i.multiply(j,q);
         i = i.exponent(k,q);
         j = j.add(k,q);
         k = k.invert(q);
         i = i.multiply(k,q);
         k = k.multiply(i.gcd(k),q);
         k = k.multiply(j.gcd(k),q);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString() + "  j: " + j.toString() + "  k: " + k.toString());
   }

   static void crunchTestTunedModInt()
   {
      final ModInt q = new ModInt(Q);
      ModInt i = new ModInt(I);
      ModInt j = new ModInt(J);
      ModInt k = new ModInt(K);
      t.start("ModInt");
      for (int l = 0; l < loops; ++l)
      {
         i = i.multiply(j,q);
         i = i.exponent(k,q);
         j = j.add(k,q);
         k = k.invert(q);
         i = i.multiply(k,q);
         k = k.multiply(i.gcd(k),q);
         k = k.multiply(j.gcd(k),q);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString() + "  j: " + j.toString() + "  k: " + k.toString());
   }

   static void crunchTestSunInplaceModInt()
   {
      final SunConstantBigInt q = new SunConstantBigInt(Q);
      SunInplaceModInt i = new SunInplaceModInt(I,q);
      SunInplaceModInt j = new SunInplaceModInt(J,q);
      SunInplaceModInt k = new SunInplaceModInt(K,q);
      SunInplaceModInt s = new SunInplaceModInt(q);
      t.start("SunInplaceModInt");
      for (int l = 0; l < loops; ++l)
      {
         i.mul(j);
         i.exp(k);
         j.add(k);
         k.inv();
         i.mul(k);
         s.assign(i); s.gcd(k); k.mul(s);
         s.assign(j); s.gcd(k); k.mul(s);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString() + "  j: " + j.toString() + "  k: " + k.toString());
   }

   static void crunchTestTunedInplaceModInt()
   {
      final ConstantBigInt q = new ConstantBigInt(Q);
      InplaceModInt i = new InplaceModInt(I,q);
      InplaceModInt j = new InplaceModInt(J,q);
      InplaceModInt k = new InplaceModInt(K,q);
      InplaceModInt s = new InplaceModInt(q);
      t.start("Platform-specific InplaceModInt");
      for (int l = 0; l < loops; ++l)
      {
         i.mul(j);
         i.exp(k);
         j.add(k);
         k.inv();
         i.mul(k);
         s.assign(i); s.gcd(k); k.mul(s);
         s.assign(j); s.gcd(k); k.mul(s);
      }
      t.end();
      DefaultLogger.out.println("i: " + i.toString() + "  j: " + j.toString() + "  k: " + k.toString());
   }

   static void bitTest()
   {
      InplaceModInt i = new InplaceModInt(Q, new ConstantBigInt("01" + Q));
      InplaceModInt j = new InplaceModInt(I, new ConstantBigInt(Q));

      int ibits = i.bits();
      int jbits = j.bits();

      DefaultLogger.out.println("bits(i) : " + ibits);
      DefaultLogger.out.println("bits(j) : " + jbits);

      InplaceModInt si = new InplaceModInt(i);
      InplaceModInt sj = new InplaceModInt(j);

      InplaceModInt ni = new InplaceModInt(1, i.modulus());
      InplaceModInt nj = new InplaceModInt(1, i.modulus());

      for (int n = 0; n < ibits; ++n)
      {
         if (i.testBit(n))
            si.sub(ni);
         ni.mul2();
      }

      DefaultLogger.out.println("si.isZero() should be true ==> " + si.isZero());

      for (int n = 0; n < jbits; ++n)
      {
         if (j.testBit(n))
            sj.sub(nj);
         nj.mul2();
      }

      DefaultLogger.out.println("sj.isZero() should be true ==> " + sj.isZero());
   }
}

