package com.selex.net.ssl;

import java.security.cert.X509Certificate;
import java.security.SecureRandom;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLContext;

import com.selex.log.DefaultLogger;

public class CertificateUtils
{
   public static void disableHostnameVerification()
   {
      javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
         new javax.net.ssl.HostnameVerifier() {
            public boolean verify(String urlHost, SSLSession sslSession) {return true;}
         });

      com.sun.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(
         new com.sun.net.ssl.HostnameVerifier() {
            public boolean verify(String urlHost, String certHost) {return true;}
         });
   }

   public static void trustAllCertificates()
   {
      TrustManager[] trustAll = new TrustManager[]
      {
         new X509TrustManager()
         {
            public X509Certificate[] getAcceptedIssuers() { return null; }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {}
            public void checkServerTrusted(X509Certificate[] certs, String authType) {}
         }
      };
      try
      {
         SSLContext sc = SSLContext.getInstance("SSL");
         sc.init(null, trustAll, new SecureRandom());

         com.sun.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
         javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
      }
      catch (Exception e)
      {
         DefaultLogger.err.println("Failed to install all-trusting TrustManager: " + e);
      }
   }

   public static boolean isCertificateException(Exception e)
   {
      return e instanceof java.security.cert.CertificateException
          || (e.getCause() != null && e.getCause() instanceof java.security.cert.CertificateException)
          ;
   }
}

