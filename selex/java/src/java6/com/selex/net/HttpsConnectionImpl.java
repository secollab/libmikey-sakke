package com.selex.net;

import java.io.InputStream;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;

public class HttpsConnectionImpl implements HttpsConnection
{
   private HttpsURLConnection c;

   public HttpsConnectionImpl(String url) throws java.io.IOException
   {
      c = (HttpsURLConnection) new URL(url).openConnection();
   }

   // request params
   public void setRequestProperty(String property, String value)
   {
      c.setRequestProperty(property, value);
   }

   // make request and access response
   public InputStream openDataInputStream() throws java.io.IOException
   {
      return c.getInputStream();
   }

   // response meta-data
   public int getResponseCode() throws java.io.IOException
   {
      return c.getResponseCode();
   }
   public long getLength()
   {
      return c.getContentLength();
   }

   public void close() throws java.io.IOException
   {
      c.disconnect();
   }
}

