package com.selex.test.ui;

import com.selex.log.DefaultLogger;

public abstract class SimpleUI implements TestUI
{
   private static boolean instanceCreated = false;
   public SimpleUI()
   {
      if (instanceCreated)
         DefaultLogger.err.println("WARNING: Some SimpleUI implementations (e.g. MIDlet) will not support multiple instances.");
      else instanceCreated = true;
   }

   public boolean hasConsole() { return true; }

   public void addParameter(String prompt, UserInputType type, String defaultText)
   {
      throw new RuntimeException("addParameter unsupported for console tests yet.  Guard behind hasConsole() == false");
   }

   public String getUserInputString(String prompt, UserInputType type, String defaultText)
   {
      if (System.console() == null)
      {
         System.err.print("Defaulting '"+prompt+"'");
         if (type != UserInputType.Password)
            System.err.println(" to '"+defaultText+"'");
         else
            System.err.println();
         return defaultText;
      }

      System.out.print(prompt + ": ");
      if (!defaultText.isEmpty())
         System.out.print("[" + defaultText + "]: ");
      String text;
      if (type == UserInputType.Password)
         text = new String(System.console().readPassword());
      else
         text = System.console().readLine();
      if (text.isEmpty())
         return defaultText;
      return text;
   }
}

