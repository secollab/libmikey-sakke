/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYException;

/**
 * Class for handling MIKEY exceptions of uninitialized components
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYExceptionUninitialized extends MIKEYException {
	private static final long serialVersionUID = 5231494519247111012L;

	/**
	 * Class constructor
	 * 
	 * @param message The error message
	 */
	public MIKEYExceptionUninitialized(String message) {
		super(message);
	}
}