/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYException;

import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessageError;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadERR;

/**
 * Class for handling MIKEY exceptions of message content errors
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYExceptionMessageContent extends MIKEYException {
	private static final long serialVersionUID = 3746188107665599296L;

	private MIKEYMessageError errorMessageValue = null;
	private int errorCode;

	/**
	 * Class constructor
	 * 
	 * @param message The error message
	 */
	public MIKEYExceptionMessageContent(String message) {
		super(message);
	}

	/**
	 * Class constructor
	 * 
	 * @param errorMessage The error message
	 * @param error the MIKEY error code that is embedded in the message
	 */
	public MIKEYExceptionMessageContent(MIKEYMessageError errorMessage, int error) {
		super(MIKEYPayloadERR.errorCodeToText(error));
		errorMessageValue = errorMessage;
		errorCode = error;
	}

	/**
	 * Class constructor
	 * 
	 * @param errorMessage The MIKEYMessage containing an error
	 * @param msg The error message
	 */
	public MIKEYExceptionMessageContent(MIKEYMessageError errorMessage, String msg) {
		super(msg);
		errorMessageValue = errorMessage;
	}

	/**
	 * Gets the error message
	 * 
	 * @return The error message
	 */
	public MIKEYMessageError errorMessage() {
		return errorMessageValue;
	}
	
	/**
	 * Gets the error code of the embedded MIKEY error.
	 * @return The error code of the embedded MIKEY error.
	 */
	public int getErrorCode(){
		return errorCode;
	}
}
