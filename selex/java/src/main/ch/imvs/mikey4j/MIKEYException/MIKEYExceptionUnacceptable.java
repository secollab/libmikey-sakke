/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYException;

/**
 * Class for handling MIKEY exceptions of unacceptable methods
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYExceptionUnacceptable extends MIKEYException {
	private static final long serialVersionUID = -8315859522255143378L;

	/**
	 * Class constructor
	 * 
	 * @param message The error message
	 */
	public MIKEYExceptionUnacceptable(String message) {
		super(message);
	}
}
