/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYMessage;

import java.util.List;
import java.util.ArrayList;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.util.encoders.Base64;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayload;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadCERT;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadHDR;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloads;
import ch.imvs.mikey4j.util.Certificate;

/**
 * Class for handling MIKEY messages in general
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public abstract class MIKEYMessage extends MIKEYPayloads {
	private static Log log = LogFactory.getLog(MIKEYMessage.class);
	
	/** MIKEY message type Pre-Shared Key, initiator message: 0 */
	public final static int MIKEY_TYPE_PSK_INIT = 0;
	/** MIKEY message type Pre-Shared Key, response message: 1 */
	public final static int MIKEY_TYPE_PSK_RESP = 1;
	/** MIKEY message type Public Key, initiator message: 2 */
	public final static int MIKEY_TYPE_PK_INIT = 2;
	/** MIKEY message type Public Key, response message: 3 */
	public final static int MIKEY_TYPE_PK_RESP = 3;
	/** MIKEY message type Diffie-Hellman, initiator message: 4 */
	public final static int MIKEY_TYPE_DH_INIT = 4;
	/** MIKEY message type Diffie-Hellman, response message: 5 */
	public final static int MIKEY_TYPE_DH_RESP = 5;
	/** MIKEY message type Error: 6 */
	public final static int MIKEY_TYPE_ERROR = 6;
	/** MIKEY message type Diffie-Hellman Hash Message Authentication Code, initiator message: 7 */
	public final static int MIKEY_TYPE_DHHMAC_INIT = 7;
	/** MIKEY message type Diffie-Hellman Hash Message Authentication Code, response message: 8 */
	public final static int MIKEY_TYPE_DHHMAC_RESP = 8;
	/** MIKEY message type RSA reverse, initiator message: 9 */
	public final static int MIKEY_TYPE_RSA_R_INIT = 9;
	/** MIKEY message type RSA reverse, response message: 10 */
	public final static int MIKEY_TYPE_RSA_R_RESP = 10;
	/** MIKEY message type SAKKE, initiator message: 26 */
	public final static int MIKEY_TYPE_SAKKE_INIT = 26;
	/** MIKEY message type SAKKE, Crypto Session ID update message: 27 */
	public final static int MIKEY_TYPE_SAKKE_RESP = 27;
	public final static int MIKEY_TYPE_CSID_RESP = 27;

	/** MIKEY Encryption Null: 0 */
	public final static int MIKEY_ENCR_NULL = 0;
	/** MIKEY Encryption AES CM 128: 1 */
	public final static int MIKEY_ENCR_AES_CM_128 = 1;
	/** MIKEY Encryption AES KW 128: 2 */
	public final static int MIKEY_ENCR_AES_KW_128 = 2;
	/** MIKEY MAC Null: 0 */
	public final static int MIKEY_MAC_NULL = 0;
	/** MIKEY MAC Hash Message Authentication Code SHA1: 1 */
	public final static int MIKEY_MAC_HMAC_SHA1_160 = 1;
	
	/**
	 * Class constructor
	 */
	protected MIKEYMessage() {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYMessage");
		
		payloads = new ArrayList<MIKEYPayload>();
	}

	/**
	 * Parse the received Base64 encoded MIKEY message
	 * 
	 * @param b64Message The Base64 encoded MIKEY message
	 * @return The message
	 */
	public static MIKEYMessage parse(String b64Message) {
		if(log.isTraceEnabled())
			log.trace("b64Message=" + b64Message);
		
		byte[] messageData = Base64.decode(b64Message);
		if (messageData == null)
			throw new MIKEYExceptionMessageContent("Invalid Base64 input message");
		
		// parse the message and creates a new message
		return parse(messageData, messageData.length);
	
	}
	
	/**
	 * Parses the received message and creates a new message<br>
	 * <PRE>
	 * 1. Parse HDR payload
	 * 2. While not end of packet
	 *    2.1 Parse payload (choose right class) and store next payload type.
	 *    2.2 Add payload to list of all payloads in the message.
	 * <PRE>
	 * 
	 * @param message The received message to parse
	 * @param lengthLimit The length limit of the message
	 * @return An answer to the received message
	 * @throws MIKEYException
	 */
	private static MIKEYMessage parse(byte[] message, int lengthLimit) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("message=[...], lengthLimit=" + lengthLimit);
		
		// ArrayList to store the received payloads.
		ArrayList<MIKEYPayload> payloads = new ArrayList<MIKEYPayload>();

		// parses the message 
		MIKEYPayloads.parse(MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, message, lengthLimit, payloads);
		if (payloads.size() == 0)
			throw new MIKEYExceptionMessageContent("No payloads");

		// gets the header payload
		MIKEYPayloadHDR hdr = (MIKEYPayloadHDR) payloads.get(0);
		if (hdr == null)
			throw new MIKEYExceptionMessageContent("No header in the payload");

		MIKEYMessage msg = null;
		
		// creates a new MIKEY message
		switch(hdr.dataType()) {
			case MIKEY_TYPE_DH_INIT:
			case MIKEY_TYPE_DH_RESP:
				msg = new MIKEYMessageDH();
				break;
			case MIKEY_TYPE_SAKKE_INIT:
			case MIKEY_TYPE_SAKKE_RESP: // or more general name MIKEY_TYPE_CSID_RESP 
				msg = new MIKEYMessageSAKKE();
				break;
			case MIKEYMessage.MIKEY_TYPE_ERROR:
				msg = new MIKEYMessageError();
				break;
			case MIKEY_TYPE_PSK_INIT:
			case MIKEY_TYPE_PSK_RESP:
			case MIKEY_TYPE_PK_INIT:
			case MIKEY_TYPE_PK_RESP:
			case MIKEY_TYPE_DHHMAC_INIT:
			case MIKEY_TYPE_DHHMAC_RESP:
			case MIKEY_TYPE_RSA_R_INIT:
			case MIKEY_TYPE_RSA_R_RESP:
			default:
				throw new MIKEYExceptionMessageContent("Not yet implemented in Java");
		}
		msg.setRawMessageData(message);
		msg.payloads = payloads;

		return msg;
	}
	
	/**
	 * Gets the type of the MIKEY message
	 * 
	 * @return The type of the message
	 */
	protected int type() {
		log.trace("");

		MIKEYPayload hdr = extractPayload(MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, false);
		if (hdr == null)
			throw new MIKEYExceptionMessageContent("No header in the payload");

		MIKEYPayloadHDR phdr = (MIKEYPayloadHDR) hdr;

		return phdr.dataType();
	}
	
	/**
	 * In this method the initiator extracts the answer which he has got from the responder. 
	 * The public exponents of the responders is added to the KeyAgreement with which now the key can be generated. 
	 * 
	 * @param ka The key agreement
	 * @param maxOffsetSeconds The maximum number of seconds the timestamp may differ from this systems clock
	 */
	public abstract void parseResponse(KeyAgreement ka, int maxOffsetSeconds);
	
	/**
	 * Sets an Offer<br>
	 * The Responder extracts with this method the public exponents of the initiator and adds it to the KeyAgreement.
	 * 
	 * @param ka The key agreement
	 * @param maxOffsetSeconds The maximum number of seconds the timestamp may differ from this systems clock
	 */
	public abstract void setOffer(KeyAgreement ka, int maxOffsetSeconds);

	/**
	 * Builds a response<br>
	 * With this method the Responder generates the RESPONSE message for the initiator. 
	 * Own identity and own public exponent is inserted again. A signature about the whole news is provided again.
	 * 
	 * @param ka The key agreement
	 * @return The MIKEY message
	 */
	public abstract MIKEYMessage buildResponse(KeyAgreement ka);
	
	/**
	 * Authenticates (verifies the signature) the received message
	 * 
	 * @param ka The key agreement
	 * @return True, if the message can be authenticated successful, false if not
	 */
	public abstract boolean authenticate(KeyAgreement ka);
	
	/**
	 * Gets the type of the key agreement
	 * 
	 * @return The key agreement type
	 */
	public abstract KeyAgreementType keyAgreementType();

	@Override
	public void addCertificatePayloads(List<Certificate> certificateChain) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("certificateChain=" + certificateChain);
		
		if (certificateChain == null) {
			log.debug("No certificate found for CertificatePayloads");
			return;
		}

		// All certificates in the certificate chain will be added as payload
		for(Certificate cert : certificateChain){
			MIKEYPayload payload = new MIKEYPayloadCERT(MIKEYPayloadCERT.MIKEYPAYLOAD_CERT_TYPE_X509V3SIGN, cert.getCert());
			addPayload(payload);
		}
	}

	//workaround to make the method in the superclass protected (aka package-import)
	@Override
	protected void addPolicyToPayload(KeyAgreement ka) {
		super.addPolicyToPayload(ka);
	}
	
	@Override
	protected void addPayload(MIKEYPayload payload){
		super.addPayload(payload);
	}
	
	@Override
	protected void addSignaturePayload(Certificate cert, boolean addIdsAndT){
		super.addSignaturePayload(cert, addIdsAndT);
	}
}
