/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYMessage;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUnimplemented;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadERR;

/**
 * Class for handling MIKEY error messages
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYMessageError extends MIKEYMessage {
	@Override
	public boolean authenticate(KeyAgreement ka) {
		throw new MIKEYExceptionUnimplemented("authenticate not implemented in MIKEY Error Message");
	}

	@Override
	public MIKEYMessage buildResponse(KeyAgreement ka) {
		throw new MIKEYExceptionUnimplemented("buildResponse not implemented in MIKEY Error Message");
	}

	@Override
	public KeyAgreementType keyAgreementType() {
		throw new MIKEYExceptionUnimplemented("keyAgreementType not implemented in MIKEY Error Message");
	}

	@Override
	public void parseResponse(KeyAgreement ka, int maxOffsetSeconds) {
		throw new MIKEYExceptionUnimplemented("parseResponse not implemented in MIKEY Error Message");
	}

	@Override
	public void setOffer(KeyAgreement ka, int maxOffsetSeconds) {
		throw new MIKEYExceptionUnimplemented("setOffer not implemented in MIKEY Error Message");
	}
	
	public int getErrorCode(){
		MIKEYPayloadERR err = (MIKEYPayloadERR)extractPayload(MIKEYPayloadERR.MIKEYPAYLOAD_ERR_PAYLOAD_TYPE, false);
		return err.errorType();
	}
}
