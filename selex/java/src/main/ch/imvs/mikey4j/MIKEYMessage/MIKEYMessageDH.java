/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYMessage;

import java.nio.ByteBuffer;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.List;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementDH;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMap;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayload;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadDH;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadERR;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadHDR;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadRAND;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadSIGN;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadT;
import ch.imvs.mikey4j.util.Certificate;
import ch.imvs.mikey4j.util.Rand;

/**
 * Class for handling Diffie-Hellman MIKEY messages
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYMessageDH extends MIKEYMessage{
	private static Log log = LogFactory.getLog(MIKEYMessageDH.class);
	
	/**
	 * Class constructor
	 */
	public MIKEYMessageDH() {
		log.trace("constructing MIKEYMessageDH");	
	}
	
	/**
	 * Class constructor<br>
	 * The payloads will be assembled to the Diffie-Hellman MIKEY message
	 * 
	 * @param ka The Diffie-Hellman key agreement used for the MIKEY message
	 */
	public MIKEYMessageDH(KeyAgreementDH ka) {
		if(log.isTraceEnabled())
			log.trace("ka=" + ka);
		
		// generate a random Crypto Session Bundle ID
		int csbId = ka.csbId();
		if (csbId == 0){
			csbId = Rand.randomInt();
			ka.setCsbId(csbId);
		}
		
		log.debug("add Payload HDR");
		addPayload(new MIKEYPayloadHDR(MIKEYPayloadHDR.HDR_DATA_TYPE_DH_INIT, 1, MIKEYPayloadHDR.HDR_PRF_MIKEY_1, csbId, ka.nCs(), ka.getCsIdMapType(), ka.csIdMap()));
		
		log.debug("add Payload T");
		addPayload(new MIKEYPayloadT());
		
		log.debug("add Policy to Payload");
		addPolicyToPayload(ka);
		
		log.debug("add Payload RAND");
		MIKEYPayloadRAND payload = new MIKEYPayloadRAND();
		addPayload(payload);

		// keep a copy of the random value at the key agreement
		ka.setRand(payload.randData());
		
		log.debug("add Payload Cert");
		// include the list of certificates if available
		addCertificatePayloads(ka.certificateChain());
		
		log.debug("add Payload DH");
		addPayload(new MIKEYPayloadDH(ka.group(), ka.publicKey(), ka.keyValidity()));
		
		log.debug("add Signature Payload");
		addSignaturePayload(ka.certificateChain().get(0), false);
	}

	//for the responder (part 1)
	@Override
	public void setOffer(KeyAgreement kaBase, int maxOffsetSeconds) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("kaBase=" + kaBase);
		
		// convert the given key agreement
		KeyAgreementDH ka = (KeyAgreementDH) kaBase;
		
		// gets and checks the header payload
		MIKEYPayload extractedPayload = extractPayload(MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, false);
		if (extractedPayload == null)
			throw new MIKEYExceptionMessageContent("DH init message had no HDR payload");
		
		MIKEYPayloadHDR hdr = (MIKEYPayloadHDR) extractedPayload;
		if (hdr.dataType() != MIKEYPayloadHDR.HDR_DATA_TYPE_DH_INIT)
			throw new MIKEYExceptionMessageContent("Expected DH init message");

		ka.setCsbId(hdr.csbId());

		if (hdr.csIdMapType() == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID) { 
			ka.setCsIdMap(hdr.csIdMap());
			ka.setCsIdMapType(hdr.csIdMapType());
		}
		else
			throw new MIKEYExceptionMessageContent("Unknown type of Crypto Session ID map");
		
		payloads.remove(extractedPayload);
		hdr = null;
		
		// gets and checks the timestamp payload
		extractedPayload = extractPayload(MIKEYPayloadT.MIKEYPAYLOAD_T_PAYLOAD_TYPE, false);
		if (extractedPayload == null)
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);

		// check the validity of the timestamp
		if (!((MIKEYPayloadT) extractedPayload).checkOffset(maxOffsetSeconds))
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_TS, ka);
		
		payloads.remove(extractedPayload);

		addPolicyTo_ka(ka);
		
		// gets and checks the RAND payload
		extractedPayload = extractPayload(MIKEYPayloadRAND.MIKEYPAYLOAD_RAND_PAYLOAD_TYPE, false);
		if (extractedPayload == null)
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);

		ka.setRand(((MIKEYPayloadRAND) extractedPayload).randData());

		payloads.remove(extractedPayload);

		// If we haven't got the peer's certificate chain
		// (for instance during authentication of the message),
		// try to get it now
		List<X509Certificate> peerChain = ka.peerCertificateChain();
		if (peerChain == null || peerChain.get(0) == null) {
			peerChain = extractCertificateChain();

			if (peerChain != null)
				ka.setPeerCertificateChain(peerChain);
		}
		
		// gets and checks the Diffie-Hellman payload
		extractedPayload = extractPayload(MIKEYPayloadDH.MIKEYPAYLOAD_DH_PAYLOAD_TYPE, false);
		if (extractedPayload == null)
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);
		
		MIKEYPayloadDH dh = (MIKEYPayloadDH) extractedPayload;

		if (ka.group() != dh.group()){
			try{
				ka.setGroup(dh.group());
			}
			catch(MIKEYException e){
				throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_DH, ka);
			}
		}
		
		// sets the Diffie-Hellman keys
		ka.setPeerKey(dh.dhKey());
		
		// sets the validity of the Diffie-Hellman keys
		ka.setKeyValidity(dh.kv());
		
		payloads.remove(extractedPayload);
	}

	//for the responder (part 2)
	@Override
	public MIKEYMessageDH buildResponse(KeyAgreement kaBase) {
		if(log.isTraceEnabled())
			log.trace("kaBase=" + kaBase);
		
		// check the given key agreement
		KeyAgreementDH ka = (KeyAgreementDH) kaBase;

		// Build the response message
		MIKEYMessageDH result = new MIKEYMessageDH();
		
		log.debug("Responder: add Payload HDR");
		result.addPayload(
				new MIKEYPayloadHDR(	MIKEYPayloadHDR.HDR_DATA_TYPE_DH_RESP, 0, 
										MIKEYPayloadHDR.HDR_PRF_MIKEY_1, ka.csbId(),
										ka.nCs(), ka.getCsIdMapType(), 
										ka.csIdMap()));
		
		log.debug("Responder: add Payload T");
		result.addPayload(new MIKEYPayloadT());
		
		log.debug("Responder: add Payload SP");
		result.addPolicyToPayload(ka);

		log.debug("Responder: add Payload CERT");
		// Include the list of certificates if available
		result.addCertificatePayloads(ka.certificateChain());
		
		log.debug("Responder: add Payload DHr");
		result.addPayload(new MIKEYPayloadDH(
					    ka.group(),
	                    ka.publicKey(),
					    ka.keyValidity()));
		
		log.debug("Responder: add Payload DHi");
		result.addPayload(new MIKEYPayloadDH(
					    ka.group(),
					    ka.peerKey(),
					    ka.keyValidity()));
		
		log.debug("Responder: add Payload SIGN");
		result.addSignaturePayload(ka.certificateChain().get(0), false);
		
		return result;
	}

	//for the initiator (parses the response created with buildResponse)
	@Override
	public void parseResponse(KeyAgreement kaBase, int maxOffsetSeconds) {
		if(log.isTraceEnabled())
			log.trace("kaBase=" + kaBase);
		
		/** True if the Diffie-Hellman keys of the initiator are obtained */
		boolean gotDHi = false;
		
		// check the given key agreement
		KeyAgreementDH ka = (KeyAgreementDH) kaBase;
		if (ka == null)
			throw new MIKEYExceptionMessageContent("Not a DH keyagreement");
		
		// gets and checks the header payload
		MIKEYPayload extractedPayload = extractPayload(MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, true);		
		if (extractedPayload == null)
			throw new MIKEYExceptionMessageContent("DH resp message had no HDR payload");
		
		MIKEYPayloadHDR hdr = (MIKEYPayloadHDR) extractedPayload;
		if (hdr.dataType() != MIKEYPayloadHDR.HDR_DATA_TYPE_DH_RESP)
			throw new MIKEYExceptionMessageContent("Expected DH resp message: got " + hdr.dataType() + " instead of " + MIKEYPayloadHDR.HDR_DATA_TYPE_DH_RESP);
		
		// gets and sets the Crypto Session ID map
		MIKEYCsIdMap csIdMap;
		if (hdr.csIdMapType() == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID)
			csIdMap = hdr.csIdMap();
		else
			throw new MIKEYExceptionMessageContent("Unknown type of CS ID map");
		
		ka.setCsIdMap(csIdMap);
		//FIXME: from libmikey, look at the other fields!
		
		// gets and checks the timestamp payload
		extractedPayload = extractPayload(MIKEYPayloadT.MIKEYPAYLOAD_T_PAYLOAD_TYPE, true);
		if (extractedPayload == null) {
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);
		}
		else {
			if (!((MIKEYPayloadT) extractedPayload).checkOffset(maxOffsetSeconds)) {
				throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_TS, ka);
			}
		}

		addPolicyTo_ka(ka);

		// If we haven't gotten the peer's certificate chain
		// (for instance during authentication of the message),
		// try to get it now
		List<X509Certificate> peerChain = ka.peerCertificateChain();
		if (peerChain == null || peerChain.get(0) == null) {
			peerChain = extractCertificateChain();

			if (peerChain != null)
				ka.setPeerCertificateChain(peerChain);
		}
		
		// gets and checks the Diffie-Hellman payload
		extractedPayload = extractPayload(MIKEYPayloadDH.MIKEYPAYLOAD_DH_PAYLOAD_TYPE, true);
		if (extractedPayload == null) {
			throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);
		}
		else{
			MIKEYPayloadDH dh = (MIKEYPayloadDH) extractedPayload;
			// Diffie-Hellman keys (DHi) of the initiator
			if (Arrays.equals(dh.dhKey(), ka.publicKey()))
				gotDHi = true;
	
			// Diffie-Hellman keys (DHr) of the responder
			else
				ka.setPeerKey(dh.dhKey());
	
	
			extractedPayload = extractPayload(MIKEYPayloadDH.MIKEYPAYLOAD_DH_PAYLOAD_TYPE, false);
			if (extractedPayload == null) {
				throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);
			}
			else{
				dh = (MIKEYPayloadDH) extractedPayload;
				if (gotDHi)
					ka.setPeerKey(dh.dhKey());	//this one should then be DHr
				else {
					if (!Arrays.equals(dh.dhKey(), ka.publicKey())) {
						throw createErrorMessage(MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED, ka);
					}
				}
			}
		}
	}

	@Override
	public boolean authenticate(KeyAgreement kaBase) throws MIKEYException{
		if(log.isTraceEnabled())
			log.trace("kaBase=" + kaBase);
		
		// check the given key agreement
		KeyAgreementDH ka = (KeyAgreementDH) kaBase;

		// gets and checks the signature payload
		MIKEYPayload sign = lastPayload();
		if (sign.payloadType() != MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE) {
			ka.setAuthError("No signature payload found");
			return true;
		}
		
		// gets and checks the certificates of the peer
		List<X509Certificate> peerCert = ka.peerCertificateChain();
		if (peerCert == null || peerCert.size() == 0) {
			peerCert = extractCertificateChain();

			if (peerCert == null) {
				ka.setAuthError("No certificate was found");
				return true;
			}
			ka.setPeerCertificateChain(peerCert);
		}
		
		MIKEYPayloadSIGN signPl = (MIKEYPayloadSIGN) sign;

		// verifies the signature 
		Certificate c = new Certificate(peerCert.get(0));
		ByteBuffer wrap = ByteBuffer.wrap(rawMessageData());
		wrap.position(0);
		wrap.limit(rawMessageData().length - signPl.sigLength());
		return c.verifySig(wrap, signPl.sigData(), MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_RSA_PKCS);
	}

	@Override
	public KeyAgreementType keyAgreementType() {
		log.trace(KeyAgreementType.DH);
		
		return KeyAgreementType.DH;
	}
	
	private static MIKEYExceptionMessageContent createErrorMessage(int error, KeyAgreementDH ka){
		MIKEYMessageError errorMessage = new MIKEYMessageError();
		errorMessage.addPayload(
			new MIKEYPayloadHDR(MIKEYPayloadHDR.HDR_DATA_TYPE_ERROR, 0,
				MIKEYPayloadHDR.HDR_PRF_MIKEY_1, ka.csbId(),
				ka.nCs(), ka.getCsIdMapType(), 
				ka.csIdMap()));
		errorMessage.addPayload(new MIKEYPayloadERR(error));
		errorMessage.addSignaturePayload(ka.certificateChain().get(0), false);
		return new MIKEYExceptionMessageContent(errorMessage, error);
	}
}