package ch.imvs.mikey4j.MIKEYMessage;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementSAKKE;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;

import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadHDR;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadT;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadRAND;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadID;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadSP;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadSAKKE;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadSIGN;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMap;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;

import org.apache.commons.net.ntp.TimeStamp;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.mikeysakke.util.MIKEYSAKKEUtils;
import com.selex.mikeysakke.crypto.SigningParameterSet;
import com.selex.mikeysakke.crypto.ECCSI;
import com.selex.mikeysakke.crypto.SakkeParameterSet;
import com.selex.mikeysakke.crypto.SAKKE;
import com.selex.mikeysakke.test.MIKEYSAKKETest;

import com.selex.util.OctetString;
import com.selex.log.DefaultLogger;

import java.util.Collection;


public class MIKEYMessageSAKKE extends MIKEYMessage
{
	public MIKEYMessageSAKKE() // incoming
	{
	}
	public MIKEYMessageSAKKE(KeyAgreementSAKKE ka) // outgoing
	{
		final KeyStorage keys = ka.getKeyMaterial();

		int csbId = ka.csbId();

		if (csbId == 0)
		{
			csbId = ka.getRng().randomInt();
			ka.setCsbId( csbId );
		}

		MIKEYPayloadT tPayload;
		MIKEYPayloadRAND randPayload;

		// for one-way transmissions (such as PA/Tannoy/Simplex
		// intercom), no CS Id update is necessary.
		// XXX: currently this is hard-coded to false but ought to be
		// XXX: based on the requesting user's intent.
		final boolean unidirectional = false;

		//adding header payload
		addPayload(new MIKEYPayloadHDR(MIKEYPayloadHDR.HDR_DATA_TYPE_SAKKE_INIT, unidirectional?0:1, 
												 MIKEYPayloadHDR.HDR_PRF_MIKEY_1, csbId, ka.nCs(),
												 ka.getCsIdMapType(), ka.csIdMap()));

		//adding timestamp payload
		addPayload(tPayload = new MIKEYPayloadT());

		//keep a copy of the time stamp
		final TimeStamp t = tPayload.ts();
		
		//adding random payload
		addPayload(randPayload = new MIKEYPayloadRAND());

		//keep a copy of the random value
		ka.setRand(randPayload.randData());

		//derive a textual date stamp from the MIKEY time stamp
		byte[] uriPrefix = MIKEYSAKKEUtils.formatIdDateStamp6509(t);

		final OctetString.Translation raw = OctetString.Untranslated;
		final byte NUL = 0;

		final OctetString senderId = new OctetString(uriPrefix);
		senderId.concat(MIKEYSAKKEUtils.canonicalizeUri(ka.uri()), raw).concat(NUL);

		final OctetString peerId = new OctetString(uriPrefix);
		peerId.concat(MIKEYSAKKEUtils.canonicalizeUri(ka.peerUri()), raw).concat(NUL);

		Collection<String> communities = keys.GetCommunityIdentifiers();

		if (communities.isEmpty())
			throw new RuntimeException("No MIKEY-SAKKE user communities configured.");

		final boolean anonymousSender = false; // TODO: support anonymous sender via config

		// TODO: choose community ids appropriately

		final String senderCommunity = communities.iterator().next();
		final String peerCommunity = senderCommunity;

		final OctetString senderKmsId = new OctetString(senderCommunity, raw);
		senderKmsId.concat(NUL);

		final OctetString peerKmsId = new OctetString(peerCommunity, raw);
		peerKmsId.concat(NUL);

		//for now, include all identifiers
		if (!anonymousSender)
			addPayload(new MIKEYPayloadID(
					MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
					senderId.clampedArray(),
					MIKEYPayloadID.MIKEYPAYLOAD_ID_ROLE_INITIATOR));
		addPayload(new MIKEYPayloadID(
				MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
				peerId.clampedArray(),
				MIKEYPayloadID.MIKEYPAYLOAD_ID_ROLE_RESPONDER));
		if (!anonymousSender)
			addPayload(new MIKEYPayloadID(
					MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
					senderKmsId.clampedArray(),
					MIKEYPayloadID.MIKEYPAYLOAD_ID_ROLE_INITIATOR_KMS));
		addPayload(new MIKEYPayloadID(
				MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_BYTE_STRING,
				peerKmsId.clampedArray(),
				MIKEYPayloadID.MIKEYPAYLOAD_ID_ROLE_RESPONDER_KMS));

		//adding security policy
		addPolicyToPayload(ka);

		//determine sakke parameter set being used
		final String sakkeSet = keys.GetPublicParameter(peerCommunity, "SakkeSet");
		final SakkeParameterSet sakkeParams = SakkeParameterSet.SAKKE_PARAM_SET_1;
		if (!sakkeSet.equals("1"))
			throw new RuntimeException("Currently only SAKKE parameter set '1' is supported.");

		//add SAKKE payload
		addPayload(new MIKEYPayloadSAKKE(ka, sakkeParams, peerId, peerCommunity, keys));
		
		if (!anonymousSender)
		{
			int sig_len = 
				1 + 4 * SigningParameterSet.ECCSI_6509_PARAM_SET.hash.octets;

			//add SIGN payload
			MIKEYPayloadSIGN sign;
			addPayload(sign = new MIKEYPayloadSIGN(
					sig_len, MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_TYPE_ECCSI));

			boolean sign_ok = false;

			try
			{
				final byte[] data = rawMessageData();
				OctetString sigData = ka.getSignatureAlgorithm().Sign(
					  data, 0, data.length - sig_len,
					  senderId.toString(),
					  senderCommunity,
					  MIKEYSAKKETest.enable_sakke_test_vectors
						  ? MIKEYSAKKETest.EphemeralFrom6507
						  : ka.getRng(),
					  keys);

				sign.setSigData(sigData.clampedArray());

				DefaultLogger.err.println("S:sig="+new OctetString(sigData.clampedArray()).toHexString());
				sign_ok = !sigData.empty();
			}
			catch (Exception e)
			{
				DefaultLogger.err.println("Sign error: " + e);
			}

			if (sign_ok)
				DefaultLogger.out.println("ECCSI signing success.");
			else
				DefaultLogger.err.println("ECCSI signing FAILED.");

			if (!sign_ok)
				throw new MIKEYException("ECCSI signing failed.");
		}
	}

	public boolean authenticate(KeyAgreement kaBase)
	{
		KeyAgreementSAKKE ka = (KeyAgreementSAKKE) kaBase;

		MIKEYPayloadHDR hdr = (MIKEYPayloadHDR) extractPayload( MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, false );

		// SAKKE does not require a verification message.  The response,
		// parsed in parseResponse() below, is purely to update the CS Id map
		// with the SSRCs of the responder's streams.
		if (hdr.dataType() == MIKEYPayloadHDR.HDR_DATA_TYPE_SAKKE_RESP)
			return true;

		ka.setCsbId( hdr.csbId() );

		if (hdr.csIdMapType() == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID)
		{
			ka.setCsIdMap( hdr.csIdMap() );
			ka.setCsIdMapType( hdr.csIdMapType() );
		}
		else throw new MIKEYException("SAKKE crypto session id map is not SRTP" );

		MIKEYPayloadRAND rand = (MIKEYPayloadRAND) extractPayload( MIKEYPayloadRAND.MIKEYPAYLOAD_RAND_PAYLOAD_TYPE, false );
		ka.setRand( rand.randData() );

		MIKEYPayloadSIGN sign = (MIKEYPayloadSIGN) lastPayload();

		if (sign == null || sign.payloadType() != MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE)
		{
			// TODO: notify user via warning that SAKKE payload is
			// TODO: not signed by the sender.
			ka.setAuthError("Anonymous sender for MIKEY-SAKKE key agreement is currently unsupported.");
			return false;
		}

		KeyStorage keys = ka.getKeyMaterial();
		
		MIKEYPayloadT tpl = (MIKEYPayloadT) extractPayload( MIKEYPayloadT.MIKEYPAYLOAD_T_PAYLOAD_TYPE, false );

		if (tpl == null)
			throw new MIKEYException("MIKEY-SAKKE message contains no Timestamp payload.");

		final TimeStamp t = tpl.ts();

		//derive a textual date stamp from the MIKEY time stamp
		final OctetString uriPrefix = new OctetString(MIKEYSAKKEUtils.formatIdDateStamp6509(t));

		final OctetString.Translation raw = OctetString.Untranslated;
		final byte NUL = 0;

		final OctetString responderId = new OctetString(uriPrefix);
		responderId.concat(MIKEYSAKKEUtils.canonicalizeUri(ka.uri()), raw).concat(NUL);

		final OctetString senderId = new OctetString(uriPrefix);
		senderId.concat(MIKEYSAKKEUtils.canonicalizeUri(ka.peerUri()), raw).concat(NUL);

		Collection<String> communities = keys.GetCommunityIdentifiers();

		if (communities.isEmpty())
		{
			ka.setAuthError("No MIKEY-SAKKE user communities are known.");
			return false;
		}

		// TODO: choose community ids appropriately

		final String responderCommunity = communities.iterator().next();
		final String senderCommunity = responderCommunity;

		// TODO: process any IDR* payloads

		boolean verify_ok = false;

		try
		{
			final byte[] data = rawMessageData();

			verify_ok = ka.getSignatureAlgorithm().Verify(
					 data, 0, data.length - sign.sigLength(),
					 sign.sigData(), 0, sign.sigLength(),
					 senderId.toString(),
					 senderCommunity,
					 keys);
		}
		catch (Exception e)
		{
			DefaultLogger.err.println("Verify error: " + e);
		}

		if (verify_ok)
			DefaultLogger.out.println("ECCSI signature verified.");
		else
			DefaultLogger.err.println("ECCSI signature verification FAILED.");

		if (!verify_ok)
		{
			ka.setAuthError("MIKEY-SAKKE message signature verification failed.");
			return false;
		}

		sign = null;

		MIKEYPayloadSAKKE sakke = (MIKEYPayloadSAKKE) extractPayload(MIKEYPayloadSAKKE.MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE, false);

		if (sakke == null)
		{
			ka.setAuthError("SAKKE payload not found in MIKEY-SAKKE message.");
			return false;
		}

		OctetString SSV = null;

		try
		{
			SSV = ka.getKeyEncapsulationAlgorithm()
					  .ExtractSharedSecret(sakke.SED,
												  responderId.toString(),
												  responderCommunity,
												  keys);

			if (!SSV.empty())
				DefaultLogger.out.println("SAKKE encapsulated data decrypted; SSV = " + SSV.toHexString());
			else
				DefaultLogger.err.println("SAKKE encapsulated data could not be decrypted.");
		}
		catch (Exception e)
		{
			DefaultLogger.err.println("ExtractSharedSecret error: " + e);
		}


		if (SSV == null || SSV.empty())
		{
			ka.setAuthError("Failed to extract TGK from SAKKE payload.");
			return false;
		}

		ka.setSSV(SSV);

		return true;
	}

	public void setOffer(KeyAgreement ka, int maxOffsetSeconds)
	{
		addPolicyTo_ka(ka);
	}

	public MIKEYMessage buildResponse(KeyAgreement ka)
	{
		final int csbId = ka.csbId();
		if (csbId == 0)
			throw new MIKEYException("SAKKE response requires that CSB Id is initialized");

		MIKEYMessage result = new MIKEYMessageSAKKE();

		result.addPayload(new MIKEYPayloadHDR(
					MIKEYPayloadHDR.HDR_DATA_TYPE_SAKKE_RESP, 0, 
					MIKEYPayloadHDR.HDR_PRF_MIKEY_1, csbId, ka.nCs(),
					ka.getCsIdMapType(), ka.csIdMap()));

		return result;
	}

	public void parseResponse(KeyAgreement ka, int maxOffsetSeconds)
	{
		MIKEYPayloadHDR hdr = (MIKEYPayloadHDR) extractPayload( MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE, false );

		if (hdr.csbId() != ka.csbId())
			throw new MIKEYException("SAKKE response header must agree with initiator on CSB Id");

		//???ka.setnCs( hdr->nCs() );

		if (hdr.csIdMapType() == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID)
		{
			ka.setCsIdMap( hdr.csIdMap() );
			ka.setCsIdMapType( hdr.csIdMapType() );
		}
		else throw new MIKEYException("SAKKE crypto session id map is not SRTP" );
	}

	public KeyAgreementType keyAgreementType()
	{
		return KeyAgreementType.SAKKE;
	}
}

