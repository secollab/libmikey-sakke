/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.io.ByteArrayInputStream;
import java.nio.ByteBuffer;
import java.security.cert.*;
import java.util.*;


import org.apache.commons.net.ntp.TimeStamp;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.util.encoders.Base64;

import ch.imvs.mikey4j.MIKEYPolicyParam;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementPolicyEntry;
import ch.imvs.mikey4j.MIKEYException.*;
import ch.imvs.mikey4j.util.*;
import ch.imvs.mikey4j.util.Certificate;


/**
 * Class for handling MIKEY payloads in general
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloads {
	/** Guessed signature length (the signature calculation will be factor two faster if this guess is correct (128 bytes == 1024 bits)) */
	private final static int GUESSED_SIGNATURE_LENGTH = 128;
	
	private static Log log = LogFactory.getLog(MIKEYPayloads.class);
	
	/** ArrayList which contains all the payloads */
	protected ArrayList<MIKEYPayload> payloads = new ArrayList<MIKEYPayload>();
	/** True if the payloads are compiled */
	private boolean compiled;
	/** Raw data of the payloads */
	private byte[] rawData;
	
	/** If true, the vector payloads must be cleared (remove all containing entries) */
	protected boolean first = true;
	
	/**
	 * Class constructor
	 */
	protected MIKEYPayloads() {
		compiled = false;
		rawData = null;
	}
	
	/**
	 * Parses the received message
	 * 
	 * @param firstPayloadType The type of the first payload in the received message
	 * @param message The message data in a byte array
	 * @param lengthLimit The length limit of the message
	 * @param payloads The vector payloads, where the parsed payloads will be stored
	 */
	protected static void parse(int firstPayloadType, byte[] message, int lengthLimit, ArrayList<MIKEYPayload> payloads){
		if(log.isTraceEnabled())
			log.trace("firstPayloadType=" + firstPayloadType + ", message=[...], lengthLimit=" + lengthLimit + ", payloads=" + payloads);
		 
		int offset = 0;
		int msgpos = offset;
		int limit = lengthLimit;
		
		// gets the header payload
		MIKEYPayload hdr = parsePayload(firstPayloadType, message, offset, limit);
		payloads.add(hdr);
		
		limit -=  (int)(hdr.end() - msgpos);
		msgpos = hdr.end();
		
		int nextPayloadType = hdr.nextPayloadType();
		
		// gets all following payloads
		while(!(msgpos >= offset + lengthLimit) && nextPayloadType != MIKEYPayload.MIKEYPAYLOAD_LAST_PAYLOAD) {
			MIKEYPayload payload = parsePayload(nextPayloadType, message, msgpos, limit);
			nextPayloadType = payload.nextPayloadType();	
			payloads.add(payload);
			assert(payload.end() - msgpos == payload.length());
			limit -= payload.end() - msgpos;
			msgpos = payload.end();
		}
		if (!(msgpos == lengthLimit && nextPayloadType==MIKEYPayload.MIKEYPAYLOAD_LAST_PAYLOAD))
			throw new MIKEYExceptionMessageLengthException("The length " + msgpos + " of the message did not match the total length " + lengthLimit + " of payloads.");
	}
	
	/**
	 * Parses a given payload
	 * 
	 * @param payloadType The type of the payload
	 * @param raw_data The complete raw data of the message
	 * @param offset The index, at which the payload begins in the raw data
	 * @param limit The length limit of the payload
	 * @return The parsed payload
	 * @throws MIKEYException
	 */
	private static MIKEYPayload parsePayload(int payloadType, byte[] raw_data, int offset, int limit) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("payloadType=" + payloadType + ", raw_data=[...], offset=" + offset + ", limit=" + limit);
		
		MIKEYPayload payload = null;
		switch (payloadType) {
			case MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE:
				payload = new MIKEYPayloadHDR(raw_data, offset, limit);
				break;
			case MIKEYPayloadDH.MIKEYPAYLOAD_DH_PAYLOAD_TYPE:
				payload = new MIKEYPayloadDH(raw_data, offset, limit);
				break;
			case MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE:
				payload = new MIKEYPayloadSIGN(raw_data, offset, limit);
				break;
			case MIKEYPayloadT.MIKEYPAYLOAD_T_PAYLOAD_TYPE:
				payload = new MIKEYPayloadT(raw_data, offset, limit);
				break;
			case MIKEYPayloadID.MIKEYPAYLOAD_ID_PAYLOAD_TYPE:
				payload = new MIKEYPayloadID(raw_data, offset, limit);
				break;
			case MIKEYPayloadID.MIKEYPAYLOAD_IDR_PAYLOAD_TYPE:
				payload = new MIKEYPayloadID(raw_data, offset, limit, /*expectIDR=*/true);
				break;
			case MIKEYPayloadCERT.MIKEYPAYLOAD_CERT_PAYLOAD_TYPE:
				payload = new MIKEYPayloadCERT(raw_data, offset, limit);
				break;
			case MIKEYPayloadSP.MIKEYPAYLOAD_SP_PAYLOAD_TYPE:
				payload = new MIKEYPayloadSP(raw_data, offset, limit);
				break;
			case MIKEYPayloadRAND.MIKEYPAYLOAD_RAND_PAYLOAD_TYPE:
				payload = new MIKEYPayloadRAND(raw_data, offset, limit);
				break;
			case MIKEYPayloadERR.MIKEYPAYLOAD_ERR_PAYLOAD_TYPE:
				payload = new MIKEYPayloadERR(raw_data, offset, limit);
				break;
			case MIKEYPayloadKeyData.MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE:
				throw new MIKEYExceptionUnimplemented("MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE not yet supported");
//				payload = new MIKEYPayloadKeyData(raw_data, offset, limit);
//				break;
			case MIKEYPayloadGeneralExtensions.MIKEYPAYLOAD_GENERALEXTENSIONS_PAYLOAD_TYPE:
				payload = new MIKEYPayloadGeneralExtensions(raw_data, offset, limit);
				break;
			case MIKEYPayloadSAKKE.MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE:
				payload = new MIKEYPayloadSAKKE(raw_data, offset, limit);
				break;
			case MIKEYPayload.MIKEYPAYLOAD_LAST_PAYLOAD:
				break;
			default:
				throw new MIKEYExceptionMessageContent("Payload of unrecognized type: " +  payloadType);
		}
	
		return payload;
	}

	/**
	 * Add a payload to the vector payloads
	 * 
	 * @param payload The payload to be added
	 */
	protected void addPayload(MIKEYPayload payload) {
		if(log.isTraceEnabled())
			log.trace("payload=" + payload);
		
		compiled = false;
		
		// if the first payload will be added, it is necessary to ensure that the vector is empty
		if (first) {
			payloads.clear();
			first = false;
		}
		// Put the nextPayloadType in the previous payload		
		if (payload.payloadType() != MIKEYPayloadHDR.MIKEYPAYLOAD_HDR_PAYLOAD_TYPE) {
			payloads.get(payloads.size() - 1).setNextPayloadType(payload.payloadType());
		}
		payloads.add(payload);
	}
	
	/**
	 * Build the signature data
	 * 
	 * @param sigLength The length of the signature
	 * @param useIdsT True if the ID payloads are used
	 * @return The data to be signed
	 * @throws MIKEYException
	 */
	private ArrayList<Byte> buildSignData(int sigLength, boolean useIdsT) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("sigLength=" + sigLength + ", useIdsT=" + useIdsT);
		
		ArrayList<Byte> signData = new ArrayList<Byte>();
		
		// gets the raw data of the MIKEY message, but ignore the signature field RFC Sec. 5.2
		byte[] value = rawMessageData();
		for(int i = 0; i < value.length - sigLength; i++)
			signData.add(value[i]);

		if (useIdsT) {
			MIKEYPayloadT plT = (MIKEYPayloadT)extractPayload(MIKEYPayloadT.MIKEYPAYLOAD_T_PAYLOAD_TYPE, false);
			
			signData.addAll(extractIdVec(0)); //IDi = ID Initiator
			signData.addAll(extractIdVec(1)); //IDr = ID Responder
			signData.addAll(tsToVec(plT.ts()));
		}

		return signData;
	}
	
	/**
	 * Puts the timestamp into a vector of byte array
	 * 
	 * @param ts
	 * @return
	 */
	private ArrayList<Byte> tsToVec(TimeStamp ts) {
		if(log.isTraceEnabled())
			log.trace("ts=" + ts);
		
		ArrayList<Byte> vec = new ArrayList<Byte>(8);
		ByteBuffer b = ByteBuffer.allocate(8);
		b.putLong(ts.ntpValue());
		b.position(0);
		for(int i = 0; i < 8; i++)
			vec.add(b.get());

		return vec;
	}
	
	/**
	 * Adds the signature payload to the MIKEY message
	 * 
	 * @param cert The X.509 certificate, which is to be used to sign the data
	 * @param addIdsAndT True if the ID payloads are used
	 */
	protected void addSignaturePayload(Certificate cert, boolean addIdsAndT) {
		if(log.isTraceEnabled())
			log.trace("cert=" + cert + ", addIdsAndT=" + addIdsAndT);
		
		int signatureLength=128;
		byte[] signature = new byte[signatureLength];
		
		MIKEYPayloadSIGN sign;
		ArrayList<Byte> signData;
		
		// set the previous nextPayloadType to signature
		MIKEYPayload last = lastPayload();
		last.setNextPayloadType(MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE);
		
		//The SIGN payload constructor can not take the signature as
		//parameter. This is because it can not be computed before
		//the SIGN payload has been added to the MIKEY message (the next
		//payload field in the payload before SIGN is not set).
		
		//We guess that the length of the signature is 1024 bits. We then
		//calculate the signature, and if it turns out that it was not
		//1024, we have to re-do the signature calculation with the correct length.
		
		//Double-signatures would be avoided if the Certificate had a method to find out the length of the signature.
		addPayload(sign = new MIKEYPayloadSIGN(GUESSED_SIGNATURE_LENGTH, MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_TYPE_RSA_PKCS));
		signData = buildSignData(GUESSED_SIGNATURE_LENGTH, addIdsAndT);
		
		signature = cert.signData(signData, MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_RSA_PKCS);

		// if the length field in the signature payload was wrong, the signature has to be redo
		if (signatureLength != GUESSED_SIGNATURE_LENGTH) {
			signatureLength = signature.length;
			((MIKEYPayloadSIGN) payloads.get(payloads.indexOf(sign))).setSigData(signature);
			signData = buildSignData(signatureLength, addIdsAndT);

			signature = cert.signData(signData, MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_RSA_PKCS);
		}

		// write the signature to the payloads
		((MIKEYPayloadSIGN) payloads.get(payloads.indexOf(sign))).setSigData(signature);
		compiled = false;
	}
	
	/**
	 * Compiles the assembled payloads<br>
	 * The payloads are written in one single byte array
	 * 
	 * @throws MIKEYException
	 */
	private void compile() throws MIKEYException {
		log.trace("");
		
		if (compiled)
			throw new MIKEYExceptionMessageContent("BUG: trying to compile already compiled message.");
		
		rawData = new byte[rawMessageLength()];
		int pos = 0;
		for (Iterator<MIKEYPayload> iter = payloads.iterator(); iter.hasNext();) {
			MIKEYPayload mp = iter.next();
			int len = mp.length();
			mp.writeData(rawData, pos, len);
			pos += len;
		}	
	}
	
	/**
	 * Gets the raw data of the message
	 * 
	 * @return The compiled raw data of the payloads
	 */
	protected byte[] rawMessageData(){
		log.trace("");
		
		if (!compiled)
			compile();
		
		return rawData;
	}
	
	/**
	 * Gets the length of the raw data of the message
	 * 
	 * @return The length of the raw data of the message
	 * @throws MIKEYExceptionUnimplemented
	 */
	private int rawMessageLength() throws MIKEYExceptionUnimplemented {
		int length = 0;
		for(MIKEYPayload payload : payloads){
			length += payload.length();
		}
		log.trace(length);
		
		return length;
	}

	/**
	 * Sets the raw data of the message
	 * 
	 * @param data The data to be set to the raw data
	 */
	protected void setRawMessageData(byte[] data) {
		log.trace("data=[...]");
		
		rawData = new byte[data.length];
		
		System.arraycopy(data, 0, rawData, 0, data.length);
		compiled = true;
	}
	
	/**
	 * Gets the debug information
	 * 
	 * @return the debug information
	 * @throws MIKEYException 
	 */
	@Override
	public String toString() throws MIKEYException {
		log.trace("");
		
		String ret="";
		
		for (Iterator<MIKEYPayload> iter = payloads.listIterator(); iter.hasNext();) {
			MIKEYPayload payload = iter.next();
			ret += "\n" + payload;
		}
		
		return ret;
	}
	
	/**
	 * Gets the first payload of the vector payloads
	 * 
	 * @return The first payload
	 */
	protected Iterator<MIKEYPayload> firstPayload() {
		log.trace("");
		
		return payloads.listIterator();
	}
	
	/**
	 * Gets the last payload of the vector payloads
	 * 
	 * @return The last payload
	 */
	protected MIKEYPayload lastPayload() {
		log.trace("");
		
		return payloads.get(payloads.size()-1);
	}
	
	/**
	 * Gets the payload of a given type
	 * 
	 * @param payloadType The type of the payload to be returned
	 * @param remove True if the getted payload will be removed from the vector payloads, false if it has not to be removed
	 * @return The extracted payload or null if no payload of the desired type exists
	 */
	protected MIKEYPayload extractPayload(int payloadType, boolean remove) {
		if(log.isTraceEnabled())
			log.trace("payloadType=" + payloadType + ", remove=" + remove);
		
		for (Iterator<MIKEYPayload> iter = payloads.listIterator(); iter.hasNext();) {
			MIKEYPayload payload = iter.next();
			if (payload.payloadType() == payloadType) {
				if (remove){
					MIKEYPayload result = payloads.remove(payloads.indexOf(payload));
					log.trace("Payload removed " + result);
				}
				return payload;
			}
		}
		return null;
	}

	/**
	 * Add policy of the key agreement to the Security Policy payload
	 * 
	 * @param ka The key agreement
	 */
	protected void addPolicyToPayload(KeyAgreement ka) {
		if(log.isTraceEnabled())
			log.trace("ka=" + ka);
		
		Map<Integer, MIKEYPayloadSP> existingSPpayloads = new TreeMap<Integer, MIKEYPayloadSP>();
		for(KeyAgreementPolicyEntry pt : ka.getPolicy()){
			int comp = (pt.policy_No) << 8 | (pt.prot_type);
			MIKEYPayloadSP entry = existingSPpayloads.get(comp);
			if(entry == null){
				MIKEYPayloadSP psp = new MIKEYPayloadSP(pt.policy_No, pt.prot_type);
				psp.addMIKEYPolicyParam(pt.policy_type, pt.value);
				existingSPpayloads.put(comp, psp);
				addPayload(psp);
			}
			else
				entry.addMIKEYPolicyParam(pt.policy_type, pt.value);
		}
	}
	
	/**
	 * Add policies to the key agreement
	 * 
	 * @param ka The key agreement to which the policies will be added
	 */
	protected void addPolicyTo_ka(KeyAgreement ka) {
		if(log.isTraceEnabled())
			log.trace("ka=" + ka);
		
		int policy_i, policy_j;
		while (true) {
			MIKEYPayload i = extractPayload(MIKEYPayloadSP.MIKEYPAYLOAD_SP_PAYLOAD_TYPE, false);
			MIKEYPayloadSP psp = (MIKEYPayloadSP)i;
			
			if (i == null)
				break;
			
			policy_i = 0;
			policy_j = 0;
			while (policy_i < psp.noOfPolicyParam()) {
				MIKEYPolicyParam pParam;
				if ((pParam = psp.getParameterType(policy_j++)) != null) {
					ka.setPolicyParamType(psp.policy_no, psp.prot_type, pParam.type, pParam.value);
					policy_i++;
				}	
			}
			payloads.remove(i);
		}
	}
	
	/**
	 * Add certificates to the payloads
	 * 
	 * @param certChain The certificate chain which contains the available certificates
	 */
	protected void addCertificatePayloads(List<Certificate> certChain) {
		if(log.isTraceEnabled())
			log.trace("certChain=" + certChain);
		
		if (certChain == null) {
			log.error("No Certificates");
			return;
		}

		// adds all available certificates
		for(Certificate cert : certChain){
			MIKEYPayload payload = new MIKEYPayloadCERT(MIKEYPayloadCERT.MIKEYPAYLOAD_CERT_TYPE_X509V3SIGN, cert.getCert());
			addPayload(payload);
		}
	}
	
//	/**
//	 * Verifies the signature
//	 * 
//	 * @param x509cert The X.509 certificate, which is to be used to sign the data
//	 * @param addIdsAndT True if the ID payloads are used (@see {@link libmikey.MIKEYPayload.MIKEYPayloadID} implemented, but not used and tested)
//	 * @return True if the verification process is correctly done and the signature is correct, false if not 
//	 * @throws MIKEYException
//	 */
//	protected boolean verifySignature(X509Certificate x509cert, boolean addIdsAndT) throws MIKEYException {
//		if (log.isDebugEnabled())
//			log.trace("x509cert=" + x509cert + ", addIdsAndT=" + addIdsAndT);
//
//		// gets and checks the signature payload
//		MIKEYPayload payload = extractPayload(MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE, false);
//		if (payload == null)
//			throw new MIKEYExceptionMessageContent("No SIGN payload");
//
//		MIKEYPayloadSIGN sig = (MIKEYPayloadSIGN) payload;
//		ArrayList<Byte> signData = buildSignData(sig.sigLength(), addIdsAndT);
//		Certificate c = new Certificate(x509cert);
//		return c.verifySig(signData, sig.sigData(), MIKEYPayloadSIGN.MIKEYPAYLOAD_SIGN_RSA_PKCS);
//	}

	/**
	 * Creates an ID payload from the given ID and add it to the payloads
	 * 
	 * @param theId The ID to form a new ID payload
	 */
	public void addId(String theId) {
		if(log.isTraceEnabled())
			log.trace("theId=" + theId);
		
	 	int type = MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_URI;

		// check if the id is an Network Access Identifier (NAI)
		if (theId.substring(0, 4) == "nai:") {
			type = MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_NAI;
			theId = theId.substring(4);
		}

		MIKEYPayloadID initId = new MIKEYPayloadID(type, theId.getBytes());
		addPayload(initId);
	}
	
	/**
	 * Gets the ID payload at a given index
	 * 
	 * @param index The index of the desired ID payload
	 * @return The ID payload or null if not found
	 */
	private MIKEYPayloadID extractId(int index) {
		if(log.isTraceEnabled())
			log.trace("index=" + index);
		
		MIKEYPayloadID id = null;		
		int j=0;
		
		for (Iterator<MIKEYPayload> iter = firstPayload(); iter.hasNext();) {
			MIKEYPayload payload = iter.next();

			if (payload.payloadType() == MIKEYPayloadID.MIKEYPAYLOAD_ID_PAYLOAD_TYPE) {
				if (j == index) {
					id = (MIKEYPayloadID) payload;
					break;
				}
				j++;
			}
		}
		return id;
	}
	
	/**
	 * Get the ID as a string (not the complete payload) at a given index
	 * 
	 * @param index The index of the desired ID
	 * @return The ID payload or "" if not available
	 */
	public String extractIdStr(int index) {
		if(log.isTraceEnabled())
			log.trace("index=" + index);
		
		MIKEYPayloadID id = extractId(index);

		if (id == null) return "";
		
		String idData = Conversion.byteArrayToString(id.idData());
		String idStr;

		switch(id.idType()) {
			case MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_NAI:
				idStr = "nai:" + idData;
				break;
			case MIKEYPayloadID.MIKEYPAYLOAD_ID_TYPE_URI:
				idStr = idData;
				break;
			default:
				return "";
		}

		return idStr;
	
	}
	
	/**
	 * Gets the ID payload at a given index
	 * 
	 * @param index The index of the desired ID payload
	 * @return The data of the desired ID payload as a vector
	 */
	private ArrayList<Byte> extractIdVec(int index) {
		if(log.isTraceEnabled())
			log.trace("index=" + index);
		
		MIKEYPayloadID id = extractId(index);
		ArrayList<Byte> result = new ArrayList<Byte>();
	
		if (id == null)
			return null;
		
		for(byte b : id.idData())
			result.add(b);
		return result;
	}

	/**
	 * Gets all certificates transmitted in CERT payloads as part of the MIKEY Message.
	 * 
	 * @return The certificate chain
	 */
	protected List<X509Certificate> extractCertificateChain() {
		log.trace("");
		
		CertificateFactory certificateFactory;
		try {
			certificateFactory = CertificateFactory.getInstance("X.509");
		}
		catch (CertificateException e) {
			log.error("could not get a certificate factory", e);
			throw new RuntimeException(e.toString());
		}

		List<X509Certificate> peerChain = new ArrayList<X509Certificate>();
		for (Iterator<MIKEYPayload> iter = firstPayload(); iter.hasNext();) {
			MIKEYPayload payload = iter.next();
			
			if (payload.payloadType() != MIKEYPayloadCERT.MIKEYPAYLOAD_CERT_PAYLOAD_TYPE)
				continue;

			MIKEYPayloadCERT certPayload = (MIKEYPayloadCERT) payload;
			ByteArrayInputStream bais = new ByteArrayInputStream(certPayload.certData());
			try {
				X509Certificate peerCert = (X509Certificate) certificateFactory.generateCertificate(bais);
				peerChain.add(peerCert);
			}
			catch (CertificateException e) {
				log.error("could not parse a certificate", e);
				throw new RuntimeException(e.toString());
			}
		}

		return peerChain;
	}
	
	/**
	 * Encodes the raw data with Base64
	 * 
	 * @return The encoded data as a string
	 */
	public String base64_encode() {
		log.trace("");
		
		//don't access the array directly to ensure it gets compiled if necessary
		return new String(Base64.encode(rawMessageData()));
	}
}
