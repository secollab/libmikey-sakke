/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.nio.ByteBuffer;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMap;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMapSrtp;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;



/**
 * Class for handling the MIKEY payload HDR (Header)<br>
 * The Common Header payload MUST always be present as the first payload 
 * in each message.  The Common Header includes a general description of 
 * the exchange message.
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  version      !  data type    ! next payload  !V! PRF func    !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !                         CSB ID                                !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! #CS           ! CS ID map type! CS ID map info                ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadHDR extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadHDR.class);
	
	/** Header data type Pre-Shared Key, initiator: 0*/
	public static final int HDR_DATA_TYPE_PSK_INIT = 0;
	/** Header data type Pre-Shared Key, responder: 1*/
	public static final int HDR_DATA_TYPE_PSK_RESP = 1;
	/** Header data type Public Key, initiator: 2*/
	public static final int HDR_DATA_TYPE_PK_INIT = 2;
	/** Header data type Public Key, responder: 3*/
	public static final int HDR_DATA_TYPE_PK_RESP = 3;
	/** Header data type Diffie-Hellman, initiator: 4*/
	public static final int HDR_DATA_TYPE_DH_INIT = 4;
	/** Header data type Diffie-Hellman, responder: 5*/
	public static final int HDR_DATA_TYPE_DH_RESP = 5;
	/** Header data type Error: 6*/
	public static final int HDR_DATA_TYPE_ERROR = 6;
	/** Header data type Diffie-Hellman Hash Message Authentication Code, initiator: 7*/
	public static final int HDR_DATA_TYPE_DHHMAC_INIT = 7;
	/** Header data type Diffie-Hellman Hash Message Authentication Code, responder: 8*/
	public static final int HDR_DATA_TYPE_DHHMAC_RESP = 8;
	/** Header data type RSA reverse, initiator: 9*/
	public static final int HDR_DATA_TYPE_RSA_R_INIT = 9;
	/** Header data type RSA reverse, responder: 10*/
	public static final int HDR_DATA_TYPE_RSA_R_RESP = 10;
	/** Header data type SAKKE, initiator: 26 */
	public final static int HDR_DATA_TYPE_SAKKE_INIT = 26;
	/** Header data type SAKKE, Crypto Session ID update message: 27 */
	public final static int HDR_DATA_TYPE_SAKKE_RESP = 27;
	public final static int HDR_DATA_TYPE_CSID_RESP = 27;
	/** Header Pseudo-Random Function MIKEY 1: 0*/
	public static final int HDR_PRF_MIKEY_1 = 0;
	/** Header Pseudo-Random Function MIKEY 256: 1*/
	public static final int HDR_PRF_MIKEY_256 = 1;
	/** Header Pseudo-Random Function MIKEY 384: 2*/
	public static final int HDR_PRF_MIKEY_384 = 2;
	/** Header Pseudo-Random Function MIKEY 512: 3*/
	public static final int HDR_PRF_MIKEY_512 = 3;
	/** MIKEY payload HDR Payload Type: -1 */
	public static final int MIKEYPAYLOAD_HDR_PAYLOAD_TYPE = -1;
	
	/** Version of MIKEY, refers to RFC 3830 August 2004 */
	private int version;
	/** Describes the type of message (e.g., public-key transport message, verification message, error message). */
	private int dataTypeValue; 
	/** Flag to indicate whether a verification message is expected or not (this only has meaning when it is set by the
	 * Initiator).  The V flag SHALL be ignored by the receiver in the DH method (as the response is MANDATORY).
	 */
	private int vValue;
	/** indicates the PRF function that has been/will be used for key derivation. */
	private int prfFunc;
	/** Crypto Session Bundle ID */
	private int csbIdValue;
	/** Number of Crypto Sessions */
	private int nCsValue;
	/** Specifies the method of uniquely mapping Crypto Sessions to the security protocol sessions. */
	private int csIdMapTypeValue;
	/** Identifies the crypto session(s) for which the SA should be created.  The currently defined map type is the SRTP-ID */
	private MIKEYCsIdMapSrtp csIdMapPtr;
	
	/**
	 * Class constructor
	 * 
	 * @param dataType Type of the data
	 * @param v Flag if a verification flag is expected
	 * @param prfFuncType The type of the Pseudo-Random Function (PRF)
	 * @param csbId The Crypto Session Bundle id
	 * @param nCs The number of Crypto Sessions
	 * @param mapType The type of mapping
	 * @param map The map
	 */
	public MIKEYPayloadHDR(int dataType, int v, int prfFuncType, int csbId, int nCs, int mapType, MIKEYCsIdMap map) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadHDR with dataType=" + dataType + ", v=" + v + ", prfFuncType=" + prfFuncType + ", csbId=" + csbId + ", nCs=" + nCs + ", mapType=" + mapType + ", map=" + map);
		
		payloadTypeValue = MIKEYPAYLOAD_HDR_PAYLOAD_TYPE;
		version = 1;
		dataTypeValue = dataType;
		vValue = v;
		nCsValue = nCs;
		prfFunc = prfFuncType;
		csbIdValue = csbId;
		
		if (mapType == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID) {
			log.trace("MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID mapType=" + mapType + ", map=" + map);

			csIdMapTypeValue = mapType;
			csIdMapPtr = (MIKEYCsIdMapSrtp) map;
		}
		else
			throw new MIKEYExceptionMessageContent("Unknown CS ID map type");

		if (csIdMapPtr == null)
			throw new MIKEYExceptionMessageContent("Missing CS ID map");
	}

	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadHDR(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadHDR with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		payloadTypeValue = MIKEYPAYLOAD_HDR_PAYLOAD_TYPE;
		
		if (lengthLimit < 10)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a HDR Payload");
		
		ByteBuffer reader = ByteBuffer.wrap(raw_data);
		reader.position(offset);

											//size offset+
		version = reader.get();				//1    0
		dataTypeValue = reader.get();		//1    1
		setNextPayloadType(reader.get());	//1    2
		byte vAndPrf = reader.get();		//1    3
		vValue = (vAndPrf >> 7) & 0x1;
		prfFunc = vAndPrf & 0x7F;
		csbIdValue = reader.getInt();		//4    7
		nCsValue = reader.get();			//1    8
		csIdMapTypeValue = reader.get();	//1    9
		
		if (csIdMapTypeValue == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID) {
			if (lengthLimit < (10 + nCsValue * 9))
				throw new MIKEYExceptionMessageLengthException("Given data is too short to form any HDR Payload");

			log.trace("MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID");
			
			csIdMapPtr = new MIKEYCsIdMapSrtp(raw_data, 10, 9 * nCsValue);
			endPtr = startPtr + 10 + (9 * nCsValue);
		}
		else {
			csIdMapPtr = null;
			throw new MIKEYExceptionMessageContent("Unknown type of CS_ID_map");
		}
	}

	@Override
	public int length() {
		log.trace(10 + csIdMapPtr.length());
		
		return 10 + csIdMapPtr.length();
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(version);
		raw_data[offset + 1] = Conversion.write8bitNBO(dataTypeValue);
		raw_data[offset + 2] = Conversion.write8bitNBO(nextPayloadType());
		raw_data[offset + 3] = (byte) ((vValue & 0x1) << 7 | (prfFunc & 0x7F));
		raw_data[offset + 4] = (byte) ((csbIdValue & 0xFF000000) >> 24);
		raw_data[offset + 5] = (byte) ((csbIdValue & 0xFF0000) >> 16);
		raw_data[offset + 6] = (byte) ((csbIdValue & 0xFF00) >> 8);
		raw_data[offset + 7] = (byte) (csbIdValue & 0xFF);
		raw_data[offset + 8] = Conversion.write8bitNBO(nCsValue);
		raw_data[offset + 9] = Conversion.write8bitNBO(csIdMapTypeValue);
		
		csIdMapPtr.writeData(raw_data, offset + 10, csIdMapPtr.length());
	}

	@Override
	public String toString(){
		String ret= "MIKEYPayloadHDR: version=<" + version + "> datatype=";
		switch(dataTypeValue) {
			case  HDR_DATA_TYPE_PSK_INIT: 
				ret=ret+"<Pre-shared>";
				break;
			case HDR_DATA_TYPE_PSK_RESP:
				ret=ret+"<PS ver msg>";
				break;
			case HDR_DATA_TYPE_PK_INIT:
				ret=ret+"<Public key>";
				break;
			case HDR_DATA_TYPE_PK_RESP:
				ret=ret+"<PK ver msg>";
				break;
			case  HDR_DATA_TYPE_DH_INIT:
				ret=ret+"<D-H init>";
				break;
			case HDR_DATA_TYPE_DH_RESP:
				ret=ret+"<D-H resp>";
				break;
			case HDR_DATA_TYPE_DHHMAC_INIT:
				ret=ret+"<DHMAC init>";
				break;
			case HDR_DATA_TYPE_DHHMAC_RESP:
				ret=ret+"<DHMAC resp>";
				break;
			case HDR_DATA_TYPE_RSA_R_INIT:
				ret=ret+"<RSA-R I_MSG>";
				break;
			case HDR_DATA_TYPE_RSA_R_RESP:
				ret=ret+"<RSA-R R_MSG>";
				break;
			case HDR_DATA_TYPE_ERROR:
				ret=ret+"<Error>";
				break;
		}

		ret += " next_payload=" + nextPayloadType();
		ret += " V=" + vValue;
		ret += " PRF_func=";
		switch(prfFunc) {
			case HDR_PRF_MIKEY_1:
				ret += "<MIKEY-1>";
				break;
			case  HDR_PRF_MIKEY_256:
				ret += "<MIKEY-256>";
				break;
			case HDR_PRF_MIKEY_384:
				ret += "<MIKEY-384>";
				break;
			case  HDR_PRF_MIKEY_512:
				ret += "<MIKEY-512>";
				break;
		}
		
		ret += " CSB_id=<" + csbIdValue + ">";
		ret += " #CS=<" + nCsValue;
		ret += " CS ID map type=";
		if (csIdMapTypeValue == MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID)
			ret += "<SRTP-ID>";
		else
			ret += "<unknown (" + csIdMapTypeValue + ")>";

		if (csIdMapPtr != null) {
			ret += "\n" + csIdMapPtr;
		}
		
		return ret;
	}
	
	/**
	 * Gets the type of the message
	 * 
	 * @return The type of the message
	 */
	public int dataType() {
		log.trace(dataTypeValue);
		
		return dataTypeValue;
	}
	
	/**
	 * Gets the number of Crypto Sessions
	 * 
	 * @return The Number of Crypto Sessions
	 */
	public int nCs() {
		log.trace(nCsValue);
		
		return nCsValue;
	}
	
	/**
	 * Gets the Crypto Session ID
	 * 
	 * @return The Crypto Session ID
	 */
	public int csbId() {
		log.trace(csbIdValue);
		
		return csbIdValue;
	}
	
	/**
	 * Gets the type of the Crypto Session ID mapping
	 * 
	 * @return The Type of map
	 */
	public int csIdMapType() {
		log.trace(csIdMapTypeValue);
		
		return csIdMapTypeValue;
	}
	
	/**
	 * Gets the map of the Crypto Session ID  
	 * 
	 * @return The map
	 */
	public MIKEYCsIdMap csIdMap() {
		if(log.isTraceEnabled())
			log.trace(csIdMapPtr);
		
		return csIdMapPtr;
	}
	
	/**
	 * Gets the version number of MIKEY.
	 * 
	 * @return the version number of MIKEY
	 */
	int version() {
		return version;
	}
	
	/**
	 * Gets the flag to indicate whether a verification message is expected or not.
	 * 
	 * @return 0 if no response expected, otherwise 1
	 */
	int vValue() {
		return vValue;
	}
	
	/**
	 * Gets the PRF function that has been/will be used for key derivation.
	 * 
	 * @return the PRF function that has been/will be used for key derivation
	 */
	int pfrFunc() {
		return prfFunc;
	}
}
