/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUninitialized;


/**
 * Class for handling MIKEY payloads in general<br>
 * <PRE>
 * RFC 3830: http://tools.ietf.org/html/rfc3830
 * 
 * Next payload  | Value | Section
 * -------------------------------
 * Last payload  |     0 | -
 * KEMAC         |     1 | 6.2
 * PKE           |     2 | 6.3
 * DH            |     3 | 6.4
 * SIGN          |     4 | 6.5
 * T             |     5 | 6.6
 * ID            |     6 | 6.7
 * CERT          |     7 | 6.7
 * CHASH         |     8 | 6.8
 * V             |     9 | 6.9
 * SP            |    10 | 6.10
 * RAND          |    11 | 6.11
 * ERR           |    12 | 6.12
 * Key data      |    20 | 6.13
 * General Ext.  |    21 | 6.15
 * SAKKE         |    26 | RFC 6509
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public abstract class MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayload.class);
	
	/** MIKEY payload Last Payload Type: 9 */
	public final static int MIKEYPAYLOAD_LAST_PAYLOAD = 0;
	
	/** The type of the next payload */
	protected int nextPayloadTypeValue;
	/** The current type of the payload */
	protected int payloadTypeValue;	
	/** Points to the first position within this payload */
	protected int startPtr;
	/** Points to the last position after this payload */
	protected int endPtr;
	/** True if the raw packet is valid, false if not */
	protected boolean rawPacketValid;
	
	/**
	 * Class constructor
	 */
	public MIKEYPayload() {
		log.trace("constructing MIKEYPayload");
		
		rawPacketValid = false;
		startPtr = 0;
		endPtr = 0;
		nextPayloadTypeValue = 0;
	}
	
	/**
	 * Class constructor
	 * 
	 * @param offset The index, at which position to start
	 */
	public MIKEYPayload(int offset) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayload with offset=" + offset);
		
		this.rawPacketValid = true;
		this.startPtr = offset;
		this.endPtr = 0;
		this.nextPayloadTypeValue = 0;
	}
	
	/**
	 * Gets the type of the payload
	 * 
	 * @return The payload type
	 */
	public int payloadType() {
		return payloadTypeValue;
	}

	/**
	 * Sets the type of the next payload
	 * 
	 * @param payloadType The type of the payload to be set
	 */
	public void setNextPayloadType(int payloadType) {
		nextPayloadTypeValue = payloadType;		
	}
	
	/**
	 * Gets the type of the next payload
	 * 
	 * @return The payload type
	 * @throws MIKEYExceptionUninitialized when nextPayloadType cannot be retrieved. 
	 */
	public int nextPayloadType() throws MIKEYExceptionUninitialized {
		if (nextPayloadTypeValue == -1)
			throw new MIKEYExceptionUninitialized("Next payload attribute in payload was not initialized.");
		return nextPayloadTypeValue;
	}

	/**
	 * Writes the data of the key validity to the MIKEY message
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The position to add the data
	 * @param expectedLength Expected length of the MIKEY message
	 * @throws MIKEYException when a failure occurs during the writing of the payload.
	 */
	public abstract void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException;

	/**
	 * Gets the last position after this payload
	 * 
	 * @return The last position
	 */
	public int end() {
		return endPtr;
	}
	
	/**
	 * Gets the length of the payload
	 * 
	 * @return The length of the payload
	 */
	public abstract int length();
}
