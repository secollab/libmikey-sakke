/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.KeyValidity.KeyValidity;
import ch.imvs.mikey4j.KeyValidity.KeyValidityInterval;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;


/**
 * Class for handling the MIKEY payload DH (Diffie-Hellman)<br>
 *  The DH data payload carries the DH-value and indicates the DH-group 
 *  used.  Notice that in this sub-section, "MANDATORY" is conditioned 
 *  upon DH being supported.
 * <PRE>
 * 	                    1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  Next Payload ! DH-Group      !  DH-value                     ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Reserv! KV    ! KV data (optional)                            ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadDH extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadDH.class);
	
	/** MIKEY Payload Diffie-Hellman Payload Type: 3 */
	public final static int MIKEYPAYLOAD_DH_PAYLOAD_TYPE = 3;
	/** MIKEY Payload Diffie-Hellman Group 5: 0 */
	public final static int MIKEYPAYLOAD_DH_GROUP5 = 0;
	/** MIKEY Payload Diffie-Hellman Group 1: 1 */
	public final static int MIKEYPAYLOAD_DH_GROUP1 = 1;
	/** MIKEY Payload Diffie-Hellman Group 2: 2 */
	public final static int MIKEYPAYLOAD_DH_GROUP2 = 2;
	
	/** MIKEY Payload Diffie-Hellman Group 1 Length: 96 Bytes */
	public final static int MIKEYPAYLOAD_DH_GROUP1_Length = 96;
	/** MIKEY Payload Diffie-Hellman Group 2 Length: 128 Bytes */
	public final static int MIKEYPAYLOAD_DH_GROUP2_Length = 128;
	/** MIKEY Payload Diffie-Hellman Group 5 Length: 192 Bytes */
	public final static int MIKEYPAYLOAD_DH_GROUP5_Length = 192;
	
	/** The Diffie-Hellman Group */
	private int dhGroup;
	/** The value of the Diffie-Hellman Public Key */
	private byte[] dhKeyPtr;
	/** The key validity value */
	private KeyValidity kvPtr;
	
	/**
	 * Class constructor
	 * 
	 * @param dhGroup The Diffie-Hellman group
	 * @param dhKeyPtr The Diffie-Hellman Public Key
	 * @param kv The key validity
	 */
	public MIKEYPayloadDH(int dhGroup, byte[] dhKeyPtr, KeyValidity kv) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadDH with dhGroup=" + dhGroup + ", dhKeyPtr=[...], kv=" + kv);
		
		this.payloadTypeValue = MIKEYPAYLOAD_DH_PAYLOAD_TYPE;
		this.dhGroup = dhGroup;
		
		int dhKeyLengthValue;
		switch (dhGroup) {
			case MIKEYPAYLOAD_DH_GROUP5:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP5_Length;
				break;
			case MIKEYPAYLOAD_DH_GROUP1:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP1_Length;
				break;
			case MIKEYPAYLOAD_DH_GROUP2:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP2_Length;
				break;
			default:
				throw new MIKEYExceptionMessageContent("Unknown DH group");
		}
		if(dhKeyPtr.length != dhKeyLengthValue)
			throw new MIKEYExceptionMessageLengthException("passed DH key is to short for specified group");

		this.dhKeyPtr = new byte[dhKeyLengthValue];
		System.arraycopy(dhKeyPtr, 0, this.dhKeyPtr, 0, dhKeyLengthValue);

		this.kvPtr = kv;
	}

	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadDH(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadDH with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		this.payloadTypeValue = MIKEYPAYLOAD_DH_PAYLOAD_TYPE;
		
		if (lengthLimit < 3)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a DH Payload");
		
		setNextPayloadType(Conversion.read8bitNBO(raw_data[offset + 0]));
		
		dhGroup = Conversion.read8bitNBO(raw_data[offset + 1]);
		int dhKeyLengthValue;
		switch (dhGroup) {
			case MIKEYPAYLOAD_DH_GROUP5:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP5_Length;
				break;
			case MIKEYPAYLOAD_DH_GROUP1:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP1_Length;
				break;
			case MIKEYPAYLOAD_DH_GROUP2:
				dhKeyLengthValue = MIKEYPAYLOAD_DH_GROUP2_Length;
				break;
			default:
				throw new MIKEYExceptionMessageContent("Unknown DH group: " + dhGroup);
		}
		if(log.isDebugEnabled()){
			log.debug("DH recipient, offset " + offset + " " + dhKeyLengthValue);
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 0]));
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 1]));
		}

		if (lengthLimit < 3 + dhKeyLengthValue)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a DH Payload");
		
		dhKeyPtr = new byte[dhKeyLengthValue];
		System.arraycopy(raw_data, offset + 2, dhKeyPtr, 0, dhKeyLengthValue);
		
		int kvType = raw_data[offset + 2+dhKeyLengthValue] & 0x0F;
		switch (kvType) {
			case KeyValidity.KEYVALIDITY_NULL:
				kvPtr = new KeyValidity();
				break;
//			case KeyValidity.KEYVALIDITY_SPI:
//				break;
			case KeyValidity.KEYVALIDITY_INTERVAL:
				kvPtr = new KeyValidityInterval(raw_data, offset + 3 + dhKeyLengthValue, lengthLimit - 3 - dhKeyLengthValue);
				break;
			default:
				throw new MIKEYExceptionMessageContent("Unknown DH key validity type: " + kvType);
		}
		if (lengthLimit < (3 + dhKeyLengthValue + kvPtr.length()))
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a DH Payload");
		
		endPtr = startPtr + 3 + dhKeyLengthValue + kvPtr.length();
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);

		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadType());
		raw_data[offset + 1] = Conversion.write8bitNBO(dhGroup);
		System.arraycopy(dhKeyPtr, 0, raw_data, offset + 2, dhKeyPtr.length);
		raw_data[offset + 2 + dhKeyPtr.length] = (byte) (0x0F & kvPtr.type());
		kvPtr.writeData(raw_data, offset + 2 + dhKeyPtr.length + 1,  kvPtr.length());
		
		if(log.isDebugEnabled()){
			log.debug("DH sender, offset " + offset + " " + dhKeyPtr.length + " " + dhGroup);
			log.debug(nextPayloadType());
			log.debug(dhGroup);
		}
	}

	@Override
	public int length() {
		log.trace(3 + kvPtr.length() + dhKeyPtr.length);
		
		return 3 + kvPtr.length() + dhKeyPtr.length;
	}

	@Override
	public String toString() {
		log.trace("");
		
		return "MIKEYPayloadDH: " + 
		"nextPayloadType=<" + nextPayloadTypeValue + 
		"> dhGroup=<" +  dhGroup + 
		"> dhKeyPtr=<" + Conversion.bintohex(dhKeyPtr) + 
		">" + kvPtr;
	}
	
	/**
	 * Gets the group of the Diffie-Hellman
	 * 
	 * @return The group
	 */
	public int group() {
		return dhGroup;
	}
	
	/**
	 * Gets the the Diffie-Hellman Public Key
	 * 
	 * @return The Public Key
	 */
	public byte[] dhKey() {
		return dhKeyPtr;
	}
	
	/**
	 * Gets the length of the Diffie-Hellman Public Key
	 * 
	 * @return The length of the Public Key
	 */
	public int dhKeyLength() {
		return dhKeyPtr.length;
	}
	
	/**
	 * Gets the key validity
	 * 
	 * @return The key validity
	 */
	public KeyValidity kv() {
		return kvPtr;
	}
}
