package ch.imvs.mikey4j.MIKEYPayload;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementSAKKE;
import ch.imvs.mikey4j.util.Conversion;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.mikeysakke.crypto.SakkeParameterSet;
import com.selex.mikeysakke.crypto.SAKKE;
import com.selex.mikeysakke.user.SakkeIdentifierScheme;
import com.selex.mikeysakke.util.MIKEYSAKKEUtils;
import com.selex.mikeysakke.test.MIKEYSAKKETest;

import com.selex.util.OctetString;
import com.selex.log.DefaultLogger;


public class MIKEYPayloadSAKKE extends MIKEYPayload
{
   public static final int MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE = 26;

   public OctetString SED = new OctetString();
   public int iana_sakke_params_value;
   public int id_scheme;

   public MIKEYPayloadSAKKE(KeyAgreementSAKKE ka, SakkeParameterSet params, OctetString peerId, String peerCommunity, KeyStorage keys)
   {
      iana_sakke_params_value = params.iana_sakke_params_value;
      id_scheme = MIKEYSAKKEUtils.fromSakkeIdentifier(peerId.toString()).code;

      payloadTypeValue = MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE;
      
      final OctetString SSV =
         ka.getKeyEncapsulationAlgorithm().GenerateSharedSecretAndSED(
               SED, peerId.toString(), peerCommunity, 
               MIKEYSAKKETest.enable_sakke_test_vectors
                  ? MIKEYSAKKETest.SSVFrom6508
                  : ka.getRng(),
               keys);

      ka.setSSV(SSV);

      DefaultLogger.out.println("Created SAKKE payload with SED = " + SED.toHexString() + " and SSV = " + SSV.toHexString());
   }

   MIKEYPayloadSAKKE(byte[] payload, int offset, int lengthLimit)
   {
      super(offset);

      payloadTypeValue = MIKEYPAYLOAD_SAKKE_PAYLOAD_TYPE;

      if (lengthLimit < 5)
         throw new MIKEYException("Insufficient data in SAKKE payload");

      setNextPayloadType(Conversion.read8bitNBO(payload[offset++]));
      iana_sakke_params_value = Conversion.read8bitNBO(payload[offset++]);
      id_scheme = Conversion.read8bitNBO(payload[offset++]);

      int SED_len = (Conversion.read8bitNBO(payload[offset]) << 8)
                  | (Conversion.read8bitNBO(payload[offset+1]));
      offset += 2;
      SED.assign(SED_len, payload, offset);

      endPtr = offset + SED_len;

      DefaultLogger.out.println("Read SAKKE payload with SED = " + SED.toHexString());
   }

   public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException
   {
      assert( expectedLength == length() );

      raw_data[offset++] = Conversion.write8bitNBO(nextPayloadType());
      raw_data[offset++] = Conversion.write8bitNBO(iana_sakke_params_value);
      raw_data[offset++] = Conversion.write8bitNBO(id_scheme);

      final int SED_len = SED.size();

      raw_data[offset++] = Conversion.write8bitNBO(SED_len >>> 8);
      raw_data[offset++] = Conversion.write8bitNBO(SED_len);

      System.arraycopy(SED.raw(), 0, raw_data, offset, SED_len);
   }

   public int length()
   {
      return 5 + SED.size();
   }
}
