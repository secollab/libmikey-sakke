/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.nio.ByteBuffer;
import java.security.cert.CertificateEncodingException;
import java.security.cert.X509Certificate;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;


/**
 * Class for handling the MIKEY payload CERT (Certificate)<br>
 * The certificate payload contains an indicator of the certificate 
 * provided as well as the certificate data.  If a certificate chain is 
 * to be provided, each certificate in the chain should be included in a 
 * separate CERT payload.<br>
 * The CERT payload has the same structure as the ID payload (@see {@link ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadID}), but they are two completely different payloads.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  Next Payload ! ID/Cert Type  ! ID/Cert len                   !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !                       ID/Certificate Data                     ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 * @see ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadKeyData
 */
public class MIKEYPayloadCERT extends MIKEYPayload{
	private static Log log = LogFactory.getLog(MIKEYPayloadCERT.class);
	
	/** MIKEY payload Certificate Payload Type: 7 */
	public final static int MIKEYPAYLOAD_CERT_PAYLOAD_TYPE = 7;
	/** MIKEY payload Certificate Type X.509 V3: 0 */
	public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3 = 0;
	/** MIKEY payload Certificate Type X.509 V3 URL: 1 */
	public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3URL = 1;
	/** MIKEY payload Certificate Type X.509 V3 Signature: 2 */
	public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3SIGN = 2;
	/** MIKEY payload Certificate Type X.509 V3 Encryption: 3 */
	public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3ENCR = 3;
	
	/** Specifies the certificate type used */
	private int certtype; 
	/** The length of the ID or Certificate field (in bytes) */
	private int certLengthValue;
	/** The ID or Certificate data.  The X.509 [X.509] certificates are included as a bytes string using DER encoding as specified in X.509 */
	private byte[] certDataPtr;
	
	/**
	 * Class constructor
	 * 
	 * @param type The type of the certificate
	 * @param length The length of the certificate
	 * @param data The certificate as byte array
	 */
	public MIKEYPayloadCERT(int type, int length, byte[] data) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadCERT with type=" + type + ", length=" + length + ", data=[...]");
		
		payloadTypeValue = MIKEYPAYLOAD_CERT_PAYLOAD_TYPE;
		this.certtype = type;
		certLengthValue = length;
		certDataPtr = data;
	}
	
	/**
	 * Class constructor
	 * 
	 * @param type The type of the certificate
	 * @param cert The X.509 certificate
	 */
	public MIKEYPayloadCERT(int type, X509Certificate cert) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadCERT with type=" + type + ", cert=" + cert);
		
		payloadTypeValue = MIKEYPAYLOAD_CERT_PAYLOAD_TYPE;
		this.certtype = type;
		try{
			certLengthValue = cert.getEncoded().length;
			certDataPtr = cert.getEncoded();
		}
		catch(CertificateEncodingException e){
			throw new RuntimeException(e);
		}
	}

	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadCERT(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadCERT with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		payloadTypeValue = MIKEYPAYLOAD_CERT_PAYLOAD_TYPE;
		if (lengthLimit < 4)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a CERT Payload");
	    
		ByteBuffer reader = ByteBuffer.wrap(raw_data);
		reader.position(offset);
		
		setNextPayloadType(Conversion.readUByteAsInt(reader.get()));
		certtype = Conversion.readUByteAsInt(reader.get());
		certLengthValue = Conversion.readUShortAsInt(reader.getShort());
		
		if (lengthLimit < 4 + certLengthValue)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a CERT Payload");
		
		if(log.isDebugEnabled()){
		    log.debug("CERT recipient, offset " + offset);
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 0]));
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 1]));
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 2]));
		    log.debug(Conversion.read8bitNBO(raw_data[offset + 3]));
		}
	    
		certDataPtr = new byte[certLengthValue];
		System.arraycopy(raw_data, offset + 4, certDataPtr, 0, certLengthValue);
		
		endPtr = startPtr + 4 + certLengthValue;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		ByteBuffer writer = ByteBuffer.wrap(raw_data);
		writer.position(offset);
		
		writer.put((byte) nextPayloadType());
		writer.put((byte) certtype);
		writer.putShort((short) certLengthValue);
		System.arraycopy(certDataPtr, 0, raw_data, offset + 4, certLengthValue);
		
		if(log.isDebugEnabled()){
			log.debug("CERT sender, offset " + offset);
			log.debug(nextPayloadType());
			log.debug(certtype);
			log.debug(((certLengthValue & 0xFF00) >> 8));
			log.debug(certLengthValue & 0xFF);
		}
	}

	@Override
	public int length() {
		return 4 + certLengthValue;
	}

	@Override
	public String toString() throws MIKEYException {
		return "MIKEYPayloadCERT: nextPayloadType=<"
		+ nextPayloadType() +
		"> type=<" + certtype +
		"> length=<" +  certLengthValue +
		"> data=<" + Conversion.bintohex(certDataPtr)+ ">";
	}
	
	/**
	 * Gets the certificate
	 * 
	 * @return The certificate as a byte array
	 */
	public byte[] certData() {
		return certDataPtr;
	}
	
	/**
	 * Gets the length of the certificate
	 * 
	 * @return The length of the certificate
	 */
	public int certLength() {
		return certLengthValue;
	}
	
	/**
	 * Gets the certificate type used.
	 * 
	 * @return the certificate type used
	 */
	int certType() {
		return certtype;
	}
}
