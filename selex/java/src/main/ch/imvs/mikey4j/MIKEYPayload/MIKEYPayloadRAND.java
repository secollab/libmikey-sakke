/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.math.BigInteger;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUninitialized;
import ch.imvs.mikey4j.util.Conversion;
import ch.imvs.mikey4j.util.Rand;



/**
 * Class for handling the MIKEY payload RAND<br>
 * The RAND payload consists of a (pseudo-)random bit-string.  The RAND
 * MUST be independently generated per CSB (note that if the CSB has
 * several members, the Initiator MUST use the same RAND for all the
 * members). For randomness recommendations for security, see http://tools.ietf.org/html/rfc1750
 * 
 * <PRE>
 * 	                    1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Next payload  ! RAND len      ! RAND                          ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadRAND extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadRAND.class);
	
	/** MIKEY payload RAND Payload Type: 11 */
	public final static int MIKEYPAYLOAD_RAND_PAYLOAD_TYPE = 11;
	
	private byte[] randDataPtr;
	
	/**
	 * Class constructor
	 */
	public MIKEYPayloadRAND() {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadRAND");
		
		payloadTypeValue = MIKEYPAYLOAD_RAND_PAYLOAD_TYPE;
		int randLengthValue = 16;
		randDataPtr = Rand.randomByte(randLengthValue);
	}

	/**
	 * Class constructor
	 * 
	 * @param randDataPtr The RAND data
	 */
	public MIKEYPayloadRAND(byte[] randDataPtr) {
		log.trace("constructing MIKEYPayloadRAND with randDataPtr=[...]");
		
		payloadTypeValue = MIKEYPAYLOAD_RAND_PAYLOAD_TYPE;
		this.randDataPtr = new byte[randDataPtr.length];
		System.arraycopy(randDataPtr, 0, this.randDataPtr, 0, randDataPtr.length);
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadRAND(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadRAND with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		payloadTypeValue = MIKEYPAYLOAD_RAND_PAYLOAD_TYPE;
		if (lengthLimit < 2)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a RAND Payload");
		
		setNextPayloadType(Conversion.read8bitNBO(raw_data[offset + 0]));
		int randLengthValue = Conversion.read8bitNBO(raw_data[offset + 1]);
		
		if (lengthLimit < 2 + randLengthValue)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a RAND Payload");
		
		randDataPtr = new byte[randLengthValue];
		System.arraycopy(raw_data, offset + 2, randDataPtr, 0, randLengthValue);
		
		if(log.isDebugEnabled()){
			log.debug("RAND recipient, offset " + offset);
			log.debug(Conversion.read8bitNBO(raw_data[offset + 0]));
			log.debug(Conversion.read8bitNBO(raw_data[offset + 1]));
			log.debug(new BigInteger(randDataPtr));
		}
		
		endPtr = startPtr + 2 + randLengthValue;
	}

	@Override
	public int length() {
		return 2 + randDataPtr.length;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedlength) throws MIKEYExceptionUninitialized {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedlength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadType());
		raw_data[offset + 1] = Conversion.write8bitNBO(randDataPtr.length);
		for (int i=0; i<randDataPtr.length; i++) raw_data[offset + 2 + i] = randDataPtr[i];
		
		if(log.isDebugEnabled()){
			log.debug("RAND sender, offset " + offset);
			log.debug(Conversion.write8bitNBO(nextPayloadType()));
			log.debug(Conversion.write8bitNBO(randDataPtr.length));
			log.debug(new BigInteger(randDataPtr));
		}
	}

	@Override
	public String toString() {
		return "MIKEYPayloadRAND: nextPayloadType=<" + nextPayloadType() 
			+ "> randLengthValue=<" + 4 
			+ "> randDataPtr=<" + randDataPtr + ">";
	}
	
	/**
	 * Gets the RAND data
	 * 
	 * @return The RAND data
	 */
	public byte[] randData() {
		return randDataPtr;
	}
}