/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.util.Conversion;



/**
 * Class for handling the MIKEY payload General Extensions (Error)<br>
 * The General extensions payload is included to allow possible 
 * extensions to MIKEY without the need for defining a completely new 
 * payload each time.  This payload can be used in any MIKEY message and 
 * is part of the authenticated/signed data part.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Next payload  ! Type          ! Length                        !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Data                                                          ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadGeneralExtensions extends MIKEYPayload{
	private static Log log = LogFactory.getLog(MIKEYPayloadGeneralExtensions.class);
	
	/** MIKEY Payload General Extensions Payload Type: 21 */
	public final static int MIKEYPAYLOAD_GENERALEXTENSIONS_PAYLOAD_TYPE = 21;
	
	/** Type of extension */
	private int type;
	/** Length of extension */
	private int length;
	/** Data of extension */
	private byte[] data;

	/**
	 * Class constructor
	 * 
	 * @param type Type of the extension
	 * @param length Length of the extension
	 * @param data Data of the extension
	 */
	public MIKEYPayloadGeneralExtensions(int type, int length, byte[] data) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadGeneralExtensions wtih type=" + type + ", length=" + length + ", data=[...]");
		
		payloadTypeValue = MIKEYPAYLOAD_GENERALEXTENSIONS_PAYLOAD_TYPE;
		this.type = type;
		this.length = length;
		data = new byte[length];
		for(int i=4; i<length; i++)
			data[i] = data[i];
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadGeneralExtensions(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		//if(log.isTraceEnabled())
		//	log.trace("Call: " + new Exception().getStackTrace()[0].getMethodName() + "\n\tbyte[] raw_data, int offset, int lengthLimit");

		payloadTypeValue = MIKEYPAYLOAD_GENERALEXTENSIONS_PAYLOAD_TYPE;
		nextPayloadTypeValue = raw_data[offset + 0];
		type = raw_data[offset + 1];
		length = raw_data[offset + 2] << 8 | raw_data[offset + 3];
		data = new byte[lengthLimit - 4];
		for(int i=4; i<lengthLimit; i++)
			data[i] = raw_data[offset + i];
		
		endPtr = startPtr + length + 4;
	}

	@Override
	public int length() {
		log.trace(length + 4);
		
		return length + 4;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadTypeValue);
		raw_data[offset + 1] = Conversion.write8bitNBO(type);
		raw_data[offset + 2] = (byte) ((length & 0xFF00) >> 8);
		raw_data[offset + 3] = (byte) (length & 0xFF);
		for(int i= offset + 4; i< expectedLength; i++)
			raw_data[i] = data[i-4];
	}

	@Override
	public String toString(){
		log.trace("");
		return "MIKEYPayloadGeneralExtension: nextPayloadType=<" +
			nextPayloadTypeValue + "> T=<" + type + ">, L=<" + length + ">, V=<" + Conversion.bintohex(data) + ">";
	}
}
