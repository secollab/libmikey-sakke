/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.nio.ByteBuffer;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;


import org.apache.commons.net.ntp.TimeStamp;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;

/**
 * Class for handling the MIKEY payload T (Timestamp)<br>
 * The timestamp payload carries the timestamp information.
 * 
 * <PRE>
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Next Payload  !   TS type     ! TS value                      ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
*/
public class MIKEYPayloadT extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadT.class);

	/** MIKEY Payload Timestamp Payload Type: 5 */
	public final static int MIKEYPAYLOAD_T_PAYLOAD_TYPE = 5;

	/** Timestamp Type Network Time Protocol (NTP) Universal Time Coordinated (UTC): 0 */
	final static int T_TYPE_NTP_UTC = 0;
	/** Timestamp Type Network Time Protocol (NTP): 0 */
	final static int T_TYPE_NTP = 1;
	/** Timestamp Type Counter: 2 */
	final static int T_TYPE_COUNTER = 2;

	/** The timestamp value of the specified TS type (in milliseconds) */
	private TimeStamp tsValue;
	/** Specifies the timestamp type used */
	private int tsTypeValue;

	private final int getTimeZoneOffset()
	{
		Calendar now = Calendar.getInstance();
		return TimeZone.getDefault().getOffset(1, now.get(Calendar.YEAR),
																now.get(Calendar.MONTH),
																now.get(Calendar.DAY_OF_MONTH),
																now.get(Calendar.DAY_OF_WEEK),
																now.get(Calendar.MILLISECOND));
	}

	/**
	 * Class constructor
	 */
	public MIKEYPayloadT() {
		log.trace("constructing MIKEYPayloadT");

		payloadTypeValue = MIKEYPAYLOAD_T_PAYLOAD_TYPE;
		tsTypeValue = T_TYPE_NTP_UTC;
		long timeJava = new Date().getTime() - getTimeZoneOffset();
		tsValue = new TimeStamp(new Date(timeJava));
	}

	/**
	 * Class constructor
	 * 
	 * @param type The type of the timestamp
	 * @param value The timestamp in milliseconds
	 */
	public MIKEYPayloadT(int type, TimeStamp value) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadT with type=" + type + ", value=" + value);

		payloadTypeValue = MIKEYPAYLOAD_T_PAYLOAD_TYPE;
		tsTypeValue = type;
		tsValue = value;
	}

	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadT(byte[] raw_data, int offset, int lengthLimit){
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadT with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);

		if (lengthLimit < 2)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a T Payload");

		payloadTypeValue = MIKEYPAYLOAD_T_PAYLOAD_TYPE;

		ByteBuffer reader = ByteBuffer.wrap(raw_data);
		reader.position(offset);

		setNextPayloadType(reader.get());
		tsTypeValue = reader.get();

		int tsLength;
		switch(tsTypeValue) {
			case 0:
			case 1:
				tsLength = 8;
				break;
			case 2:
				tsLength = 4;
				break;
			default:
				throw new MIKEYExceptionMessageContent("Unknown type of time stamp");
		
		}
		if (lengthLimit < 2 + tsLength)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a T Payload");

		switch(tsLength) {
			case 8:
				tsValue = new TimeStamp(reader.getLong());
				break;
			case 4:
				//RFC Sec. 6.6: COUNTER SHALL be padded (with leading zeros) to a 64-bit
			    //value when used as input for the default PRF.
				//We do that here.
				tsValue = new TimeStamp(reader.getInt());
		}

		endPtr = startPtr + 2 + tsLength;
	}

	@Override
	public int length() {
		if(log.isTraceEnabled())
			log.trace(2 + ((tsTypeValue == T_TYPE_COUNTER)?4:8));

		return 2 + ((tsTypeValue == T_TYPE_COUNTER)?4:8);
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);

		ByteBuffer writer = ByteBuffer.wrap(raw_data);
		writer.position(offset);
		writer.put(Conversion.write8bitNBO(nextPayloadType()));
		writer.put(Conversion.write8bitNBO(tsTypeValue));

		switch(tsTypeValue) {
			case T_TYPE_NTP_UTC:
			case T_TYPE_NTP:
				writer.putLong(tsValue.ntpValue());
				break;
			case T_TYPE_COUNTER:
				writer.putInt((int)(tsValue.ntpValue() & 0xFFFFFFFF));
		}
	}

	@Override
	public String toString(){
		log.trace("");

		return 	"MIKEYPayloadT: next_payload=<" + 
				nextPayloadType() +
				"> tsValue type=<" + tsTypeValue + 
				"> tsValue_value=<" + tsValue + ">";
	}

	/**
	 * Checks the offset of the timestamp
	 * 
	 * @param max The offset (in seconds) the timestamp may maximally have 
	 * @return true if the timestamp is in valid range, false if not
	 */
	public boolean checkOffset(int max){
		log.trace("max=" + max);

		switch(tsTypeValue) {
			case T_TYPE_NTP_UTC:{
				Calendar thisTs = Calendar.getInstance();
				thisTs.setTime(tsValue.getDate());

				int timeZoneOffset = getTimeZoneOffset();
				Calendar nowForward = Calendar.getInstance();
				nowForward.setTimeInMillis(nowForward.getTimeInMillis() - timeZoneOffset + 1000*max);
				Calendar nowBackward = Calendar.getInstance();
				nowBackward.setTimeInMillis(nowBackward.getTimeInMillis() - timeZoneOffset - 1000*max);
				return thisTs.after(nowBackward) && thisTs.before(nowForward);
			}
			case T_TYPE_NTP:{
				Calendar thisTs = Calendar.getInstance();
				thisTs.setTime(tsValue.getDate());

				Calendar nowForward = Calendar.getInstance();
				nowForward.setTimeInMillis(nowForward.getTimeInMillis() + 1000*max);
				Calendar nowBackward = Calendar.getInstance();
				nowBackward.setTimeInMillis(nowBackward.getTimeInMillis() - 1000*max);
				return thisTs.after(nowBackward) && thisTs.before(nowForward);
			}
			case T_TYPE_COUNTER:
				throw new MIKEYException("Cannot compute a time offset with a counter ts");
			default:
				throw new MIKEYExceptionMessageContent("Unknown type of time stamp in T payload");
		}
	}

	/**
	 * Gets the timestamp value
	 * 
	 * @return The timestamp
	 */
	public TimeStamp ts() {
		return tsValue;
	}

	/**
	 * Gets the type of the timestamp value
	 * 
	 * @return The timestamp type
	 */
	public int getType(){
		return tsTypeValue;
	}
}
