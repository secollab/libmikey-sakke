/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;

/**
 * Class for handling the MIKEY payload SIGN (Signature)<br>
 * The Signature payload carries the signature and its related data. 
 * The signature payload is always the last payload in the PK transport 
 * and DH exchange messages.  The signature algorithm used is implicit 
 * from the certificate/public key used.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! S type| Signature len         ! Signature                     ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
*/
public class MIKEYPayloadSIGN extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadSIGN.class);

	/** MIKEY payload SIGN Payload Type: 4 */
	public final static int MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE = 4;
	/** MIKEY payload SIGN type RSA PKCS: 0 */
	public final static int MIKEYPAYLOAD_SIGN_TYPE_RSA_PKCS = 0;
	/** MIKEY payload SIGN type RSA PSS: 1 */
	public final static int MIKEYPAYLOAD_SIGN_TYPE_RSA_PSS = 1;
	/** MIKEY payload SIGN type ECCSI (RFC 6509): 2 */
	public final static int MIKEYPAYLOAD_SIGN_TYPE_ECCSI = 2;
	/** MIKEY payload SIGN RSA PKCS Algorithm*/
	public static final String MIKEYPAYLOAD_SIGN_RSA_PKCS = "SHA1withRSA";
	/** MIKEY payload SIGN RSA PSS Algorithm*/
	public static final String MIKEYPAYLOAD_SIGN_RSA_PSS = "SHA1withRSA/PSS";

	private int sigTypeValue;
	private byte[] sigDataPtr;

	/**
	 * Class constructor
	 * 
	 * @param sigLengthValue The desired signature length
	 * @param type The desired type of signature
	 */
	public MIKEYPayloadSIGN(int sigLengthValue, int type) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadSIGN with sigLengthValue=" + sigLengthValue + ", type=" + type);
		
		this.payloadTypeValue = MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE;
		setNextPayloadType(0); // Since no other payload will set this value
		this.sigDataPtr = new byte[sigLengthValue];
		this.sigTypeValue = type;
	}

	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadSIGN(byte[] raw_data, int offset, int lengthLimit){
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadSIGN with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);

		if (lengthLimit < 2)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a SIGN Payload");

		this.payloadTypeValue = MIKEYPAYLOAD_SIGN_PAYLOAD_TYPE;
		setNextPayloadType(0); // Always last payload
		sigTypeValue = (raw_data[offset + 0] >> 4) & 0x0F;
		int sigLengthValue = ((int)(raw_data[offset + 0] & 0x0F)) << 8 | Conversion.read8bitNBO(raw_data[offset + 1]);

		if (lengthLimit < 2 + sigLengthValue)
			throw new MIKEYExceptionMessageLengthException("Invalid SIGN Payload: signature length vale does not match to signature length");

		sigDataPtr = new byte[ sigLengthValue ];
		System.arraycopy(raw_data, offset + 2, sigDataPtr, 0, sigLengthValue);

		if(log.isDebugEnabled()){
			log.debug("SIGN recipient, offset " + offset);
			log.debug(sigTypeValue);
			log.debug(sigLengthValue);
			String debug = "";
			for (int i = 0; i < sigLengthValue; i++)
				debug += ((int) sigDataPtr[i] + " ");
			log.debug(debug);
		}
		endPtr = startPtr + 2 + sigLengthValue;
	}

	@Override
	public int length() {
		log.trace(2 + sigDataPtr.length);
		
		return 2 + sigDataPtr.length;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = (byte) (((sigDataPtr.length & 0x0F00) >> 8) | ((sigTypeValue << 4) & 0xF0)) ;
		raw_data[offset + 1] = Conversion.write8bitNBO(sigDataPtr.length);
		
		if(log.isDebugEnabled()){
			log.debug("SIGN sender, offset " + offset + " " + (2 + sigDataPtr.length));
			log.debug(sigTypeValue);
			log.debug(sigDataPtr.length);
		}
		System.arraycopy(sigDataPtr, 0, raw_data, offset + 2, sigDataPtr.length);
	}

	@Override
	public String toString() {
		return "MIKEYPayloadSIGN: type=<" + sigTypeValue + "> length=<" + sigDataPtr.length + "> signature=<" + Conversion.bintohex(sigDataPtr) + ">";
	}

	/**
	 * Gets the length of the signature
	 * 
	 * @return The length of the signature
	 */
	public int sigLength() {
		return sigDataPtr.length;
	}

	/**
	 * Gets the data to be signed
	 * 
	 * @return The data to be signed
	 */
	public byte[] sigData() {
		log.trace(sigDataPtr);
		
		return sigDataPtr;
	}

	/**
	 * Gets the signature type
	 * 
	 * @return The signature type
	 */
	public int sigType() {
		log.trace(sigTypeValue);
		
		return sigTypeValue;
	}

	/**
	 * Sets the signature of the data in the other payloads.
	 * 
	 * @param data The signature
	 */
	public void setSigData(byte[] data) {
		if (this.sigDataPtr != null)
			this.sigDataPtr = null;

		this.sigDataPtr = new byte[data.length];
		System.arraycopy(data, 0, this.sigDataPtr, 0, data.length);
	}
}
