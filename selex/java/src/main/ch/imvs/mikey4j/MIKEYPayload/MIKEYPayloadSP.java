/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;

import java.util.Iterator;
import java.util.ArrayList;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYPolicyParam;
import ch.imvs.mikey4j.util.Conversion;

/**
 * Class for handling the security payload<br>
 * The Security Policy payload defines a set of policies that apply to a specific security protocol.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Next payload  ! Policy no     ! Prot type     ! Policy param  ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ~ length (cont) ! Policy param                                  ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * The Policy param part is built up by a set of Type/Length/Value fields.  For each security protocol, a set of possible types/values that can be negotiated is defined.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Type          ! Length        ! Value                         ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadSP extends MIKEYPayload {
	private static Log log = LogFactory.getLog(MIKEYPayloadSP.class);
	
	/** MIKEY payload SP Payload Type: 10 */
	public final static int MIKEYPAYLOAD_SP_PAYLOAD_TYPE = 10;
	
	/** MIKEY Protokoll using SRTP: 0 */
	public final static int MIKEY_PROTO_SRTP = 0;
	/** MIKEY Protokoll using IPSEC4: 7 */
	public final static int MIKEY_PROTO_IPSEC4 = 7;
	
	/** MIKEY SRTP Encryption Algorithm, null: 0 */
	public final static int MIKEY_SRTP_EALG_NULL = 0;
	/** MIKEY SRTP Encryption Algorithm, AES Counter Mode: 1 */
	public final static int MIKEY_SRTP_EALG_AESCM = 1;
	/** MIKEY SRTP Encryption Algorithm, AES F8: 2 */
	public final static int MIKEY_SRTP_EALG_AESF8 = 2;
	
	/** MIKEY SRTP Authentication Algorithm, null: 0 */
	public final static int MIKEY_SRTP_AALG_NULL = 0;
	/** MIKEY SRTP Authentication Algorithm, SHA1 width HMAC: 1 */
	public final static int MIKEY_SRTP_AALG_SHA1HMAC = 1;
	
	/** MIKEY SRTP Pseudo-Random Function (PRF), AES Counter Mode: 0 */
	public final static int MIKEY_SRTP_PRF_AESCM = 0;
	
	/** MIKEY SRTP/FEC (forward error correction (FEC)): 0 */
	public final static int MIKEY_FEC_ORDER_FEC_SRTP = 0;
	
	/** MIKEY SRTP Encryption Algorithm: 0 */
	public final static int MIKEY_SRTP_EALG = 0;
	/** MIKEY SRTP Session Encr. key length: 1 */
	public final static int MIKEY_SRTP_EKEYL = 1;
	/** MIKEY SRTP Authentication algorithm: 2 */
	public final static int MIKEY_SRTP_AALG = 2;
	/** MIKEY SRTP Session Auth. key length: 3 */
	public final static int MIKEY_SRTP_AKEYL = 3;
	/** MIKEY SRTP Session Salt key length: 4 */
	public final static int MIKEY_SRTP_SALTKEYL = 4;
	/** MIKEY SRTP SRTP Pseudo Random Function: 5 */
	public final static int MIKEY_SRTP_PRF = 5;
	/** MIKEY SRTP Key derivation rate: 6 */
	public final static int MIKEY_SRTP_KEY_DERRATE = 6;
	/** MIKEY SRTP SRTP encryption off/on: 7 */
	public final static int MIKEY_SRTP_ENCR_ON_OFF = 7;
	/** MIKEY SRTCP SRTCP encryption off/on: 8 */
	public final static int MIKEY_SRTCP_ENCR_ON_OFF = 8;
	/** MIKEY SRTP sender's FEC order: 9 */
	public final static int MIKEY_SRTP_FEC_ORDER = 9;
	/** MIKEY SRTP SRTP authentication off/on: 10 */
	public final static int MIKEY_SRTP_AUTH_ON_OFF = 10;
	/** MIKEY SRTP Authentication tag length: 11 */
	public final static int MIKEY_SRTP_AUTH_TAGL = 11;
	/** MIKEY SRTP SRTP prefix length: 12 */
	public final static int MIKEY_SRTP_PREFIX = 12;
	
	/** Each security policy payload must be given a distinct number for the current MIKEY session by the local peer.
	 * This number is used to map a crypto session to a specific policy */
	public int policy_no;
	
	/** Defines the security protocol. */
	public int prot_type;
	
	/** A vector containing the policy parameters */
	private ArrayList<MIKEYPolicyParam> param = new ArrayList<MIKEYPolicyParam>();
	/** The number of policy parameters in the vector */
	private int policy_param_length;
	
	/**
	 * Class constructor
	 * 
	 * @param policy_no The Policy Number
	 * @param prot_type The Policy type
	 */
	public MIKEYPayloadSP(int policy_no, int prot_type) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadSP with policy_no=" + policy_no + ", prot_type=" + prot_type);
		
		payloadTypeValue = MIKEYPAYLOAD_SP_PAYLOAD_TYPE;
		policy_param_length = 0;
		this.policy_no = policy_no;
		this.prot_type = prot_type;
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadSP(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadSP with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		payloadTypeValue = MIKEYPAYLOAD_SP_PAYLOAD_TYPE;
		policy_param_length = 0;
		nextPayloadTypeValue = raw_data[offset + 0]; 
		policy_no = raw_data[offset + 1];
		prot_type = raw_data[offset + 2];
		int i = 5; //size of the payload without the policy param TLVs
		int j = ((int)raw_data[offset + 3] << 8 | (int)raw_data[offset + 4]) + 5;
		endPtr = startPtr + j;
		
		while (i < j) {
			int tlvType         = raw_data[offset + (i+0)];
			int tlvLength       = raw_data[offset + (i+1)];
			int tlvDataStartPos =          offset + (i+2);

			byte[] value = new byte[tlvLength];
			System.arraycopy(raw_data, tlvDataStartPos, value, 0, value.length);
			
			addMIKEYPolicyParam(tlvType, value);
			i += 1 + 1 + tlvLength; //skip T, L and the Value (tlvLength)
		}
	}
	
	/**
	 * Add a Policy Parameter
	 * 
	 * @param type The type
	 * @param value The value
	 */
	public void addMIKEYPolicyParam(int type, byte[] value) {
		if(log.isTraceEnabled())
			log.trace("type=" + type + ", value=[...]");
		
		if (getParameterType(type) != null)
			deleteMIKEYPolicyParam(type);
		
		param.add(new MIKEYPolicyParam(type, value));
		policy_param_length = policy_param_length + value.length + 2;
	}
	
	/**
	 * Gets the Policy Parameter from a specific type
	 * 
	 * @param type The type of the desirec Policy Parameter
	 * @return The Policy Parameter
	 */
	public MIKEYPolicyParam getParameterType(int type) {
		log.trace("type=" + type);
		
		for(MIKEYPolicyParam pp : param){
			if (pp.type == type)
				return pp;
		}
		
		return null;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadTypeValue);
		raw_data[offset + 1] = Conversion.write8bitNBO(policy_no);
		raw_data[offset + 2] = Conversion.write8bitNBO(prot_type);
		raw_data[offset + 3] = (byte) ((policy_param_length & 0xFF00) >> 8);
		raw_data[offset + 4] = (byte) (policy_param_length & 0xFF);
		
		//Add policy params
		int j=5,k;
		for (Iterator<MIKEYPolicyParam> iter = param.iterator(); iter.hasNext();) {
			MIKEYPolicyParam pp = iter.next();
			raw_data[offset + j++] = Conversion.write8bitNBO(pp.type);
			raw_data[offset + j++] = Conversion.write8bitNBO(pp.value.length);
			
			for (k=0; k < pp.value.length; k++)
				raw_data[offset + j++] = Conversion.write8bitNBO(pp.value[k]);
		}
	}

	@Override
	public int length() {
		log.trace(5 + policy_param_length);
		
		return 5 + policy_param_length;
	}
	
	/**
	 * Gets the number of policy parameter entries 
	 * 
	 * @return The number of policy parameters
	 */
	public int noOfPolicyParam() {
		log.trace(param.size());
		
		return param.size();
	}
	
	/**
	 * Delete a MIKEY Parameter from a specific type
	 * 
	 * @param type The type of MIKEY Parameter to be delete
	 */
	public void deleteMIKEYPolicyParam(int type) {
		log.trace("type=" + type);
		
		for (Iterator<MIKEYPolicyParam> iter = param.iterator(); iter.hasNext();) {
			MIKEYPolicyParam pp = iter.next();
			if (pp.type == type) {
				policy_param_length = policy_param_length - pp.value.length -2;
				param.remove(param.indexOf(pp));
			}
		}
	}

	@Override
	public String toString() {
		StringBuilder ret = new StringBuilder();
		ret.append("MIKEYPayloadSP: next_payload<" + nextPayloadTypeValue + "> ");

		ret.append("policyNo: <" + policy_no + "> ");
		ret.append("protType: <" + prot_type + ">\n");
		
		for (Iterator<MIKEYPolicyParam> iter = param.iterator(); iter.hasNext();) {
			MIKEYPolicyParam pp = iter.next();
			ret.append("type: <" + pp.type + "> ");
			ret.append("value: <" + Conversion.bintohex(pp.value) + ">\n");
		}
		
		return ret.toString();
	}
}
