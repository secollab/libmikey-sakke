/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.KeyValidity.KeyValidity;
import ch.imvs.mikey4j.KeyValidity.KeyValidityInterval;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;



/**
 * Class for handling the MIKEY payload KEMAC<br>This class is implemented, but not used and not tested !!!<br>
 *  The Key data transport payload contains encrypted Key data subpayloads. 
 *  It may contain one or more Key data payloads, each including, for example, a TGK. 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  Next Payload ! Type  ! KV    ! Key data len                  !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !                         Key data                              ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Salt len (optional)           ! Salt data (optional)          ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !                        KV data (optional)                     ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadKeyData extends MIKEYPayload{
	private static Log log = LogFactory.getLog(MIKEYPayloadKeyData.class);
	
	/** MIKEY payload KEMAC Payload Type: 20 */
	public final static int MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE = 20;
	/** KEMAC data type TEK Generation KEY (TGK): 0 */
	public final static int KEYDATA_TYPE_TGK = 0;
	/** KEMAC data type TEK Generation KEY (TGK) and Salt: 1 */
	public final static int KEYDATA_TYPE_TGK_SALT = 1;
	/** KEMAC data type Transport Encryption KEY (TEK): 2 */
	public final static int KEYDATA_TYPE_TEK = 2;
	/** KEMAC data type Transport Encryption KEY (TEK) and Salt: 3 */
	public final static int KEYDATA_TYPE_TEK_SALT = 3;
	
	private int typeValue;
	private byte[] keyDataPtr;
	private int keyDataLengthValue;
	private byte[] saltDataPtr;
	private int saltDataLengthValue;
	private KeyValidity kvPtr;
	
	/**
	 * Class constructor
	 * 
	 * @param type Type of the Key Data
	 * @param keyDataPtr The Key Data
	 * @param keyDataLengthValue The length of the Key Data
	 * @param kvPtr The Key Validity Data
	 */
	public MIKEYPayloadKeyData(int type, byte[] keyDataPtr, int keyDataLengthValue, KeyValidity kvPtr) {
		if(log.isTraceEnabled())
			log.trace("type=" + type + ", keyDataPtr=[...]" + ", keyDataLengthValue" + keyDataLengthValue + ", kvPtr=" + kvPtr);
		
		this.payloadTypeValue = MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE;
		if ((type == KEYDATA_TYPE_TGK_SALT) || (type == KEYDATA_TYPE_TEK_SALT))
			throw new MIKEYException("This type of KeyData Payload requires a salt");

		this.typeValue = type;
		this.keyDataPtr = new byte[keyDataLengthValue];
		this.keyDataLengthValue = keyDataLengthValue;
		System.arraycopy(keyDataPtr, 0, this.keyDataPtr, 0, keyDataLengthValue);
		this.kvPtr = kvPtr;
		this.saltDataLengthValue = 0;
		this.saltDataPtr = null;
	}
	
	/**
	 * Class constructor
	 * 
	 * @param type Type of the Key Data
	 * @param keyDataPtr The Key Data
	 * @param keyDataLengthValue The length of the Key Data
	 * @param saltDataPtr The Salt data
	 * @param saltDataLengthValue The length of the Salt Data
	 * @param kvPtr The Key Validity Data
	 */
	public MIKEYPayloadKeyData(int type, byte[] keyDataPtr, int keyDataLengthValue, byte[] saltDataPtr, int saltDataLengthValue, KeyValidity kvPtr) {
		if(log.isTraceEnabled())
			log.trace("type=" + type + ", keyDataPtr=[...], keyDataLengthValue=" + keyDataLengthValue + ", saltDataPtr=[...], saltDataLengthValue=" + saltDataLengthValue + ", kvPtr=" + kvPtr);

		this.payloadTypeValue = MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE;
		this.typeValue = type;
		this.keyDataPtr = new byte[keyDataLengthValue];
		this.keyDataLengthValue = keyDataLengthValue;
		System.arraycopy(keyDataPtr, 0, this.keyDataPtr, 0, keyDataLengthValue);
		this.saltDataPtr = new byte[saltDataLengthValue];
		this.saltDataLengthValue = saltDataLengthValue;
		System.arraycopy(saltDataPtr, 0, this.saltDataPtr, 0, saltDataLengthValue);
		this.kvPtr = kvPtr;
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadKeyData(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		int lengthWoKvPtr;
		this.payloadTypeValue = MIKEYPAYLOAD_KEYDATA_PAYLOAD_TYPE;
		if (lengthLimit < 4)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyData Payload");
			
		setNextPayloadType(raw_data[offset + 0]);
		typeValue = (raw_data[offset + 1] >> 4) & 0x0F;
		int kvPtrType = (raw_data[offset + 1]) & 0x0F;
		keyDataLengthValue = ((int)raw_data[offset + 2]) << 8 | ((int)raw_data[offset + 3]);
		
		if ((typeValue == KEYDATA_TYPE_TGK_SALT) || (typeValue == KEYDATA_TYPE_TEK_SALT)) {
			if (lengthLimit < 6 + keyDataLengthValue)
				throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyData Payload");
			
			keyDataPtr = new byte[ keyDataLengthValue ];
			System.arraycopy(raw_data, offset + 4, keyDataPtr, 0, keyDataLengthValue);
			
			saltDataLengthValue = ((int)raw_data[offset + 2 + keyDataLengthValue]) << 8 | ((int)raw_data[offset + 3 + keyDataLengthValue]);
			
			if (lengthLimit < 6 + keyDataLengthValue + saltDataLengthValue) throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyData Payload");
			
			saltDataPtr = new byte[ saltDataLengthValue ];
			System.arraycopy(raw_data, offset + 4 + keyDataLengthValue, saltDataPtr, 0, saltDataLengthValue);
			
			lengthWoKvPtr = keyDataLengthValue + saltDataLengthValue + 6;
		}
		else {
			if (lengthLimit < 4 + keyDataLengthValue)
				throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyData Payload");
			
			keyDataPtr = new byte[ keyDataLengthValue ];
			System.arraycopy(raw_data, offset + 4, keyDataPtr, 0, keyDataLengthValue);
			saltDataLengthValue = 0;
			saltDataPtr = null;
			lengthWoKvPtr = keyDataLengthValue + offset + 4;
		}
	
		switch(kvPtrType) {
			case KeyValidity.KEYVALIDITY_NULL:
				kvPtr = new KeyValidity();
                break;
            case KeyValidity.KEYVALIDITY_INTERVAL:
                kvPtr = new KeyValidityInterval(raw_data, lengthWoKvPtr, lengthLimit - lengthWoKvPtr);
                break;
            default:
            	throw new MIKEYExceptionMessageContent("Unknown key validity type");
        }
	
		endPtr = startPtr + length();
	}

	@Override
	public int length() {
		log.trace("");
		
		return keyDataLengthValue + saltDataLengthValue + kvPtr.length() +
		  (((  typeValue == KEYDATA_TYPE_TGK_SALT) 
		    || (typeValue == KEYDATA_TYPE_TEK_SALT))?
		  6:4);
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadType());
		raw_data[offset + 1] = (byte) (((typeValue & 0x0F ) << 4) | (kvPtr.type() & 0x0F));
		raw_data[offset + 2] = (byte) ((keyDataLengthValue >> 8) & 0xFF);
		raw_data[offset + 3] = (byte) ((keyDataLengthValue     ) & 0xFF);
		
		System.arraycopy(keyDataPtr, 0, raw_data, 4, keyDataLengthValue);
		
		if ((typeValue == KEYDATA_TYPE_TGK_SALT) || (typeValue == KEYDATA_TYPE_TEK_SALT)) {
			raw_data[offset + 4 + keyDataLengthValue] = (byte) ((saltDataLengthValue >> 8) & 0xFF);
			raw_data[offset + 5 + keyDataLengthValue] = (byte) ((saltDataLengthValue     ) & 0xFF);
			
			System.arraycopy(saltDataPtr, 0, raw_data, offset + 6 + keyDataLengthValue, saltDataLengthValue);
			
			kvPtr.writeData(raw_data, offset + 6 + keyDataLengthValue + saltDataLengthValue, kvPtr.length());
		}
		else kvPtr.writeData(raw_data, offset + 4 + keyDataLengthValue, kvPtr.length());
	}

	@Override
	public String toString(){
		log.trace("");
		
		return "MIKEYPayloadKeyData:" + 
			" nextPayloadType=<" + nextPayloadType() +
			"> type=<" + typeValue + 
			"> keyDataPtr=<" + Conversion.bintohex(keyDataPtr) +
			"> saltDataPtr=<" + Conversion.bintohex(saltDataPtr) +
			"> kvPtr_data=<" + kvPtr + ">";
	}

	/**
	 * Gets the type of the Key DAta
	 * 
	 * @return The type of the Key Data
	 */
	public int type() {
		log.trace(typeValue);
		
		return typeValue;
	}

	/**
	 * Gets the Key Data
	 * 
	 * @return The Key Data
	 */
	public byte[] keyData() {
		log.trace("");
		
		return keyDataPtr;
	}

	/**
	 * Gets the length of the Key Data
	 * 
	 * @return The length of the Key data 
	 */
	public int keyDataLength() {
		log.trace(keyDataLengthValue);
		
		return keyDataLengthValue;
	}
	
	/**
	 * Gets the Salt
	 * 
	 * @return The Salt
	 */
	public byte[] saltData() {
		log.trace("");
		
		return saltDataPtr;
	}

	/**
	 * Gets the length of the Salt data
	 * 
	 * @return The length of the Salt
	 */
	public int saltDataLength() {
		log.trace(saltDataLengthValue);
		
		return saltDataLengthValue;
	}
	
	/**
	 * Gets the Key Validity
	 * 
	 * @return The Key Validity
	 */
	public KeyValidity kv() {
		if(log.isTraceEnabled())
			log.trace(kvPtr);
		
		return kvPtr;
	}
}
