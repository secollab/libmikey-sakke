/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import java.nio.ByteBuffer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;



/**
 * Class for handling the MIKEY payload ID (Identity)<br>This class is implemented, but not used and not tested!!!<br>
 * The ID payload carries a uniquely defined identifier. 
 * The ID payload has the same structure as the CERT payload ({@link ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadCERT}), but they are two completely different payloads.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  Next Payload ! ID/Cert Type  ! ID/Cert len                   !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !                       ID/Certificate Data                     ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadID extends MIKEYPayload{
	private static Log log = LogFactory.getLog(MIKEYPayloadID.class);
	
	private final static int STABLE_LENGTH = 4;
	
	/** MIKEY payload ID Payload Type: 6 */
	public final static int MIKEYPAYLOAD_ID_PAYLOAD_TYPE = 6;
	/** MIKEY payload IDR Payload Type (RFC 6043): 14 */
	public final static int MIKEYPAYLOAD_IDR_PAYLOAD_TYPE = 14;

	/** MIKEY payload ID Type Network Access Identifier (NAI): 0 */
	public static final int MIKEYPAYLOAD_ID_TYPE_NAI = 0;
	/** MIKEY payload ID Type Uniform Resource Identifier (URI): 1 */
	public static final int MIKEYPAYLOAD_ID_TYPE_URI = 1;
	/** MIKEY TICKET (RFC 6043) payload ID Type Byte String: 2 */
	public static final int MIKEYPAYLOAD_ID_TYPE_BYTE_STRING = 2;
	
	/** MIKEY TICKET ID Roles (RFC 6043) */
	public static final int MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED = 0;
	public static final int MIKEYPAYLOAD_ID_ROLE_INITIATOR = 1;
	public static final int MIKEYPAYLOAD_ID_ROLE_RESPONDER = 2;
	public static final int MIKEYPAYLOAD_ID_ROLE_KMS = 3;
	public static final int MIKEYPAYLOAD_ID_ROLE_PRE_SHARED_KEY = 4;
	public static final int MIKEYPAYLOAD_ID_ROLE_APPLICATION = 5;
	
	/** MIKEY SAKKE ID Roles (RFC 6509) */
	public static final int MIKEYPAYLOAD_ID_ROLE_INITIATOR_KMS = 6;
	public static final int MIKEYPAYLOAD_ID_ROLE_RESPONDER_KMS = 7;
	
	private int idLengthValue;
	private byte[] idDataPtr;
	private int idTypeValue;
	private int idRoleValue;
	
	/**
	 * Class constructor
	 * 
	 * @param type The Type of ID payload
	 * @param data The data of payload
	 */
	public MIKEYPayloadID(int type, byte[] data) {
		this(type, data, MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED);
	}
	public MIKEYPayloadID(int type, byte[] data, int role) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadID with type=" + type + ", data=" + data);

      if(role == MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED)
         this.payloadTypeValue = MIKEYPAYLOAD_ID_PAYLOAD_TYPE;
      else
         this.payloadTypeValue = MIKEYPAYLOAD_IDR_PAYLOAD_TYPE;
		this.idTypeValue = type;
		this.idRoleValue = role;
		this.idLengthValue = data.length;
		this.idDataPtr = new byte[data.length];
		System.arraycopy(data, 0, this.idDataPtr, 0, data.length);
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadID(byte[] raw_data, int offset, int lengthLimit) {
		this(raw_data, offset, lengthLimit, false);
	}
	public MIKEYPayloadID(byte[] raw_data, int offset, int lengthLimit, boolean expectIDR) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadID with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		final int minLength = STABLE_LENGTH + (expectIDR?1:0);
		if (lengthLimit < minLength)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a ID/IDR Payload");
		
		ByteBuffer reader = ByteBuffer.wrap(raw_data);
		reader.position(offset);
		
      if (expectIDR)
         this.payloadTypeValue = MIKEYPAYLOAD_IDR_PAYLOAD_TYPE;
      else
         this.payloadTypeValue = MIKEYPAYLOAD_ID_PAYLOAD_TYPE;
		setNextPayloadType(Conversion.readUByteAsInt(reader.get()));
		if (expectIDR)
			idRoleValue = Conversion.readUByteAsInt(reader.get());
		else
			idRoleValue = MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED;
		idTypeValue = Conversion.readUByteAsInt(reader.get());
		idLengthValue = Conversion.readUShortAsInt(reader.getShort());
		
		if (lengthLimit < minLength + idLengthValue)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a ID/IDR Payload");

		idDataPtr = new byte[idLengthValue];
		System.arraycopy(raw_data, offset + minLength, idDataPtr, 0, idLengthValue);
		endPtr = startPtr + minLength + idLengthValue;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		ByteBuffer writer = ByteBuffer.wrap(raw_data);
		writer.position(offset);
		
		writer.put((byte) nextPayloadType());
		if (idRoleValue != MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED)
			writer.put((byte) idRoleValue);
		writer.put((byte) idTypeValue);
		writer.putShort((short) idLengthValue);
		System.arraycopy(idDataPtr, 0, raw_data, writer.position(), idLengthValue);	//!!!!
	}

	@Override
	public int length() {
		return STABLE_LENGTH + (idRoleValue != MIKEYPAYLOAD_ID_ROLE_UNSPECIFIED?1:0) + idLengthValue;
	}

	@Override
	public String toString() throws MIKEYException {
		return 	"MIKEYPayloadID: nextPayloadType=<" + nextPayloadType() +
				"> type=<" + idTypeValue +
				"> role=<" + idRoleValue +
				"> length=<" + idLengthValue +
				"> data=<" + Conversion.bintohex(idDataPtr) + ">";
	}
	
	/**
	 * Gets the type of the ID payload
	 * 
	 * @return The type of the ID payload
	 */
	public int idType() {
		return idTypeValue;
	}
	public int idRole() {
		return idRoleValue;
	}
	
	/**
	 * Gets the length of the ID payload data
	 * 
	 * @return The length of the ID payload data
	 */
	public int idLength() {
		return idLengthValue;
	}
	
	/**
	 * Gets the data of the ID payload
	 * 
	 * @return The data of the ID payload
	 */
	public byte[] idData() {
		return idDataPtr;
	}
}
