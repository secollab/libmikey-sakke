/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYPayload;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;



/**
 * Class for handling the MIKEY payload ERR (Error)<br>
 * The Error payload is used to specify the error(s) that may have occurred.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * !  Next Payload ! Error no      !           Reserved            !
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPayloadERR extends MIKEYPayload{
	private static Log log = LogFactory.getLog(MIKEYPayloadERR.class);
	
	public final static int MIKEY_ERR_TYPE_AUTH_FAILURE  =  0; //Authentication failure
	public final static int MIKEY_ERR_TYPE_INVALID_TS    =  1; //Invalid timestamp
	public final static int MIKEY_ERR_TYPE_INVALID_PRF   =  2; //PRF function not supported
	public final static int MIKEY_ERR_TYPE_INVALID_MAC   =  3; //MAC algorithm not supported
	public final static int MIKEY_ERR_TYPE_INVALID_EA    =  4; //Encryption algorithm not supported
	public final static int MIKEY_ERR_TYPE_INVALID_HA    =  5; //Hash function not supported
	public final static int MIKEY_ERR_TYPE_INVALID_DH    =  6; //DH group not supported
	public final static int MIKEY_ERR_TYPE_INVALID_ID    =  7; //ID not supported
	public final static int MIKEY_ERR_TYPE_INVALID_CERT  =  8; //Certificate not supported
	public final static int MIKEY_ERR_TYPE_INVALID_SP    =  9; //SP type not supported
	public final static int MIKEY_ERR_TYPE_INVALID_SPpar = 10; //SP parameters not supported
	public final static int MIKEY_ERR_TYPE_INVALID_DT    = 11; //not supported Data type
	public final static int MIKEY_ERR_TYPE_UNSPECIFIED   = 12; //an unspecified error occurred
	
	public static String errorCodeToText(int error){
		switch(error){
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_AUTH_FAILURE:
				return "Authentication failure";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_TS:
				return "Invalid timestamp";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_PRF:
				return "PRF function not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_MAC:
				return "MAC algorithm not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_EA:
				return "Encryption algorithm not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_HA:
				return "Hash function not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_DH:
				return "DH group not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_ID:
				return "ID not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_CERT:
				return "Certificate not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_SP:
				return "SP type not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_SPpar:
				return "SP parameters not supported";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_INVALID_DT:
				return "not supported Data type";
			case MIKEYPayloadERR.MIKEY_ERR_TYPE_UNSPECIFIED:
				return "an unspecified error occurred";
			default:
				return "unknown internal error occurred";
		}
	}
	
	/** The length of the raw data (next payload type, erro type value, value 0, value 0 */
	private int raw_data_length = 4;

	/** MIKEY Payload Error Payload Type: 12 */
	public final static int MIKEYPAYLOAD_ERR_PAYLOAD_TYPE = 12;
	
	private int errTypeValue;

	/**
	 * Class constructor
	 * 
	 * @param errType The type of the error
	 */
	public MIKEYPayloadERR(int errType) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPayloadERR with errType=" + errType);
		
		this.payloadTypeValue = MIKEYPAYLOAD_ERR_PAYLOAD_TYPE;
		this.errTypeValue = errType;
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data The byte array of the MIKEY message
	 * @param offset The index, at which position the data of this payload begins
	 * @param lengthLimit The length limit of the payload
	 */
	public MIKEYPayloadERR(byte[] raw_data, int offset, int lengthLimit) {
		super(offset);

		if(log.isTraceEnabled())
			log.trace("constructing  with raw_data=[...], offset=" + offset + ", lengthLimit=" + lengthLimit);
		
		this.payloadTypeValue = MIKEYPAYLOAD_ERR_PAYLOAD_TYPE;
		if (lengthLimit < raw_data_length)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a ERR Payload");

		setNextPayloadType(Conversion.read8bitNBO(raw_data[offset + 0]));
		this.errTypeValue = Conversion.read8bitNBO(raw_data[offset + 1]);
		endPtr = startPtr + raw_data_length;
		if(log.isDebugEnabled()){
			log.debug("ERR recipient, offset " + offset);
			log.debug(Conversion.read8bitNBO(raw_data[offset + 0]));
			log.debug(Conversion.read8bitNBO(raw_data[offset + 1]));
			log.debug(raw_data[offset + 2]);
			log.debug(raw_data[offset + 3]);
		}
	}

	@Override
	public int length() {
		return raw_data_length;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset + 0] = Conversion.write8bitNBO(nextPayloadType());
		raw_data[offset + 1] = Conversion.write8bitNBO(errTypeValue);
		raw_data[offset + 2] = 0;
		raw_data[offset + 3] = 0;
		if(log.isDebugEnabled()){
			log.debug("ERR sender, offset " + offset);
			log.debug(nextPayloadType());
			log.debug(errTypeValue);
			log.debug(0);
			log.debug(0);
		}
	}

	@Override
	public String toString() {
		return "MIKEYPayloadERR: nextPayloadType=<" +
			nextPayloadTypeValue + "> err_type=<" +
			errTypeValue + ">";
	}
	
	/**
	 * Gets the type of the error
	 * 
	 * @return The error type
	 */
	public int errorType() {
		return errTypeValue;
	}
}
