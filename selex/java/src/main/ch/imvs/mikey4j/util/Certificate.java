/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.util;

import java.io.File;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Utility class to simplify signature creation and validation.
 * 
 * @author Ingo Bauersachs
 */
public class Certificate {
	// Subject Alternative Name (SAN) according to RFC 3280
	// http://tools.ietf.org/html/rfc3280#section-4.2.1.7
	public final static int SAN_OTHERNAME = 0;
	public final static int SAN_RFC822NAME = 1;
	public final static int SAN_DNSNAME = 2;
	public final static int SAN_X400ADDRESS = 3;
	public final static int SAN_DIRECTORYNAME = 4;
	public final static int SAN_EDIPARTYNAME = 5;
	public final static int SAN_URI = 6;
	public final static int SAN_IPADDRESS = 7;
	public final static int SAN_REGISTEREDID = 8;

	private static Log log = LogFactory.getLog(Certificate.class);
	private X509Certificate cert = null;
	private PrivateKey privateKey = null;
//	private final static char[] DEFAULT_PASSWORD = " ".toCharArray();

	/**
	 * Creates a wrapper for signature verification and SAN extraction only.
	 * @param cert The certificate to operate on.
	 */
	public Certificate(X509Certificate cert){
		this(cert, null);
	}

	/**
	 * Creates a wrapper with signing possibilities
	 * @param cert The certificate to operate on.
	 * @param privateKey The private key belonging to <code>cert</code>
	 */
	public Certificate(X509Certificate cert, PrivateKey privateKey){
		this.cert = cert;
		this.privateKey = privateKey;
	}

	/**
	 * Creates a wrapper out of a PKCS#12 encoded certificate file. If the file
	 * contains the private key it is loaded as well. The key may not be
	 * protected by a password. If the file contains more than one certificate, the
	 * first available is used.
	 * 
	 * @param file The PKCS#12 certificate file to read.
	 * @param password The password for the PKCS#12 certificate. 
	 * @return A Certificate instance containing the read certificate and possibly the associated private key.
	 */
	public static Certificate fromPKCS12File(String file, String password){
		return fromPKCS12File(file, null, password.toCharArray());
	}
	
	/**
	 * Creates a wrapper out of a PKCS#12 encoded certificate file. If the file
	 * contains the private key it is loaded as well. If the file contains more
	 * than one certificate, the first available is used.
	 * 
	 * @param file The PKCS#12 certificate file to read.
	 * @param alias The alias to use if more than one certificate is found. If this parameter is null, the first available is used.
	 * @param password The password to read the private key.
	 * @return A Certificate instance containing the read certificate and possibly the associated private key.
	 */
	public static Certificate fromPKCS12File(String file, String alias, char[] password){
		try {
			KeyStore ks = KeyStore.getInstance("PKCS12");
			FileInputStream fis = new FileInputStream(new File(file));
			ks.load(fis, password);
			if(alias == null)
				alias = ks.aliases().nextElement();
			X509Certificate cert = (X509Certificate)ks.getCertificate(alias);
			PrivateKey privateKey = (PrivateKey)ks.getKey(alias, password);
			return new Certificate(cert, privateKey);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
	
	/**
	 * Signs byte data.
	 * 
	 * @param data		The data to be signed.
	 * @param hashtype	The hash type to sign.
	 * @return			The signed data.
	 */
	public byte[] signData(byte[] data, String hashtype) {
		if(log.isTraceEnabled())
			log.trace("data=[...], hashtype=" + hashtype);

		if(privateKey == null)
			throw new NullPointerException("private key not initialized");

		Signature signer;
		try {
			signer = Signature.getInstance(hashtype);
			signer.initSign(privateKey);
			signer.update(data);
			return signer.sign();
		}
		catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Signs byte data.
	 * 
	 * @param data		The data to be signed.
	 * @param hashtype	The hash type to sign.
	 * @return			The signed data.
	 */
	public byte[] signData(ArrayList<Byte> data, String hashtype) {
		if(log.isTraceEnabled())
			log.trace("data=[...], hashtype=" + hashtype);

		if(privateKey == null)
			throw new NullPointerException("private key not initialized");

		Signature signer;
		try {
			signer = Signature.getInstance(hashtype);
			signer.initSign(privateKey);
			for(byte b : data)
				signer.update(b);
			return signer.sign();
		}
		catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e);
		}
	}
	
	/**
	 * Verify signed data.
	 * 
	 * @param data		The signed data.
	 * @param sig		The signature.
	 * @param hashtype	The hash type.
	 * @return			True if the verification process is correctly done and the signature is correct, false if not.
	 */
	public boolean verifySig(byte[] data, byte[] sig, String hashtype) {
		return verifySig(ByteBuffer.wrap(data), sig, hashtype);
	}

	/**
	 * Verify signed data.
	 * 
	 * @param data		The signed data.
	 * @param sig		The signature.
	 * @param hashtype	The hash type.
	 * @return			True if the verification process is correctly done and the signature is correct, false if not.
	 */
	public boolean verifySig(ByteBuffer data, byte[] sig, String hashtype) {
		if(log.isTraceEnabled())
			log.trace("data=[...], sig=[...], hashtype=" + hashtype);
		
		Signature signer;
		try {
			signer = Signature.getInstance(hashtype);
			signer.initVerify(cert.getPublicKey());
			signer.update(data);
			return signer.verify(sig);
		}
		catch (NoSuchAlgorithmException e) { log.error(e); }
		catch (InvalidKeyException e) { log.error(e); }
		catch (SignatureException e) { log.error(e); }
		return false;
	}
	
	/**
	 * Returns the certificate of the used object.
	 * 
	 * @return X509 certificate.
	 */
	public X509Certificate getCert() {
		return cert;
	}
	
	/**
	 * Gets the SAN (Subject Alternative Name) of the specified type.
	 * 
	 * @param altNameType The type to be returned
	 * @return SAN of the type
	 * 
	 * <PRE>
	 * GeneralName ::= CHOICE {
				otherName                       [0]     OtherName,
				rfc822Name                      [1]     IA5String,			E-Mail-Address
				dNSName                         [2]     IA5String,
				x400Address                     [3]     ORAddress,
				directoryName                   [4]     Name,
				ediPartyName                    [5]     EDIPartyName,
				uniformResourceIdentifier       [6]     IA5String,
				iPAddress                       [7]     OCTET STRING,
				registeredID                    [8]     OBJECT IDENTIFIER }
	 * <PRE>
	 */
	public String getSubAltName(int altNameType) {
		log.trace("altNameType=" + altNameType);

		Collection<List<?>> altNames = null;
		try {
			altNames = cert.getSubjectAlternativeNames();
		}
		catch (CertificateParsingException e) {
			log.error(e);
			return null;
		}
		
		for(List<?> item : altNames){
			if(item.contains(altNameType)){
				Integer type = (Integer)item.get(0);
				if (type.intValue() == altNameType)
					return (String)item.get(1);
			}
		}
		return null;
	}
}
