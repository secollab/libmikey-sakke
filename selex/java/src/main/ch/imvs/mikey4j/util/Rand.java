/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.util;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * Utility class for creating random values using SecureRandom with SHA1PRNG
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class Rand {
	private static SecureRandom random;
	
	private Rand(){}
	
	static{
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		}
		catch (NoSuchAlgorithmException e) {
			random = new SecureRandom();
		}
	}
	
	/**
	 * Generates a random value of desired size
	 * 
	 * @param size The size of the random value in bytes
	 * @return Random value as a byte array
	 */
	public static byte[] randomByte(int size) {
		byte[] serno = new byte[size];
		random.nextBytes(serno);
		return serno;
	}
	
	/**
	 * Generates a random integer value
	 * 
	 * @return Random integer
	 */
	public static int randomInt() {
		return Math.abs(random.nextInt());
	}
}
