/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.util;

/**
 * Class for handling conversions.
 * 
 * @author Markus Zeiter
 */
public class Conversion {

	private Conversion(){}
	
	/**
	 * Converts a binary byte array to a hex string
	 * 
	 * @param data The binary data
	 * @return The data as hex string
	 */
	public static String bintohex(byte[] data) {
		final String HEXES = "0123456789ABCDEF";
		final StringBuilder hex = new StringBuilder(2 * data.length);
		for (final byte b : data) {
			hex.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
		}
		return hex.toString();
	}
	
	/**
	 * 
	 * Converts a byte array to a String
	 * 
	 * @param data The byte array to be converted
	 * @return The data as a String
	 */
	public static String byteArrayToString(byte[] data) {
      try {
         return new String(data, "ISO8859-1");
      } catch (java.io.UnsupportedEncodingException e) {
         throw new RuntimeException("ISO8859-1 unsupported for raw string decode.");
      }
	}
	
	/**
	 * Convert an integer to 8 bit Network Byte Order (NBO) byte array
	 * 
	 * @param data The data as integer
	 * @return The data as byte array
	 */
	public static byte write8bitNBO(int data) {
		return (byte) (data & 0xFF);
	}
	
	/**
	 * Converts a 8 bit Network Byte Order (NBO) byte array to integer
	 * 
	 * @param data The data as byte array
	 * @return The data as integer
	 */
	public static int read8bitNBO(byte data) {
		return (0xFF & ((int)data));
	}
	
	/**
	 * Converts an unsigned short value into an integer.
	 * 
	 * @param s the unsigned short value
	 * @return the short value as integer
	 */
	public static int readUShortAsInt(short s) {
		return (((int) s) & 0xFFFF);
	}
	
	/**
	 * Converts an unsigned byte value into an integer.
	 * 
	 * @param b the unsigned byte value
	 * @return the byte value as integer
	 */
	public static int readUByteAsInt(byte b) {
		return (((int) b) & 0xFF);
	}
}
