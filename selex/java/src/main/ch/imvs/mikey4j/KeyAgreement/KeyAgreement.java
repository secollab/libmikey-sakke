/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyAgreement;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.crypto.Mac;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.macs.HMac;
import org.bouncycastle.crypto.params.KeyParameter;

import ch.imvs.mikey4j.KeyValidity.KeyValidity;
import ch.imvs.mikey4j.KeyValidity.KeyValidityInterval;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMap;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMapSrtp;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUnimplemented;
import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadSP;
import ch.imvs.mikey4j.util.Conversion;
import ch.imvs.mikey4j.util.Rand;



/**
 * Class for handling a general key agreement
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public abstract class KeyAgreement {	
	private static Log log = LogFactory.getLog(KeyAgreement.class);

	/** Security policies will be added to this list */
	public List<KeyAgreementPolicyEntry> policy = new ArrayList<KeyAgreementPolicyEntry>();
	
	/** Key Derivation Transport Encryption Key (TEK): 0 */
	public static final int KEY_DERIV_TEK = 0;
	/** Key Derivation Salt: 1 */
	public static final int KEY_DERIV_SALT = 1;
	/** Key Derivation Transport Encryption: 2 */
	public static final int KEY_DERIV_TRANS_ENCR = 2;
	/** Key Derivation Transport Salt: 3 */
	public static final int KEY_DERIV_TRANS_SALT = 3;
	/** Key Derivation Transport Authentication: 4 */
	public static final int KEY_DERIV_TRANS_AUTH = 4;
	/** Key Derivation Encryption: 5 */
	public static final int KEY_DERIV_ENCR = 5;
	/** Key Derivation Authentication: 6 */
	public static final int KEY_DERIV_AUTH = 6;
	
	/** SRTP Policy default according to RFC3830, Sec. 6.10.1 */
	public static byte SRTP_DEFAULT_POLICY_VALUES[] = {
		MIKEYPayloadSP.MIKEY_SRTP_EALG_AESCM,		// 0	Encryption algorithm			MIKEY_SRTP_EALG
		16,											// 1	Session Encr. key length		MIKEY_SRTP_EKEYL
		MIKEYPayloadSP.MIKEY_SRTP_AALG_SHA1HMAC,	// 2	Authentication algorithm		MIKEY_SRTP_AALG
		20,											// 3	Session Auth. key length		MIKEY_SRTP_AKEYL
		14,											// 4	Session Salt key length			MIKEY_SRTP_SALTKEYL
		MIKEYPayloadSP.MIKEY_SRTP_PRF_AESCM,		// 5	SRTP Pseudo Random Function		MIKEY_SRTP_PRF
		0,											// 6	Key derivation rate				MIKEY_SRTP_KEY_DERRATE
		1,											// 7	SRTP encryption off/on			MIKEY_SRTP_ENCR_ON_OFF
		1,											// 8	SRTCP encryption off/on			MIKEY_SRTCP_ENCR_ON_OFF
		MIKEYPayloadSP.MIKEY_FEC_ORDER_FEC_SRTP,	// 9	sender's FEC order				MIKEY_SRTP_FEC_ORDER
		1,											//10	SRTP authentication off/on		MIKEY_SRTP_AUTH_ON_OFF
		10,											//11	Authentication tag length		MIKEY_SRTP_AUTH_TAGL
		0											//12	SRTP prefix length				MIKEY_SRTP_PREFIX
	};

	private String peerUriValue;
	
	/** Specifies the method of uniquely mapping Crypto Sessions to the security protocol sessions. */
	private int CsIdMapType;

	/**
	 * Crypto Session Bundle (CSB): 
	 * collection of one or more Crypto Sessions, which can have common TGKs 
	 * and security parameters.
	 */
	private int csbIdValue;
	
	/** indicates the number of Crypto Sessions that will be handled within the CBS.*/
	private String authErrorValue = null;
	private String uriValue;
	private KeyValidity kvPtr = null;
	private Mac mac;
	
	private byte[] tgkPtr;
	private byte[] randPtr;
	private int tgkLengthValue;
	private int randLengthValue;
	private MIKEYCsIdMap csIdMapPtr;	
	
	/**
	 * Class constructor
	 */
	public KeyAgreement() {
		log.trace("constructing");
		
		tgkPtr = null;
		tgkLengthValue = 0;
		randPtr = null;
		randLengthValue = 0;
		csbIdValue = 0;
		csIdMapPtr = new MIKEYCsIdMapSrtp();
		kvPtr = new KeyValidity();
		mac = new HMac(new SHA1Digest());
	}
	
	/**
	 * Gets a language-independent name for the KeyAgreement.
	 * @return Language-independent name to identify the instance.
	 */
	public abstract String getFriendlyName();

	/**
	 * Gets the type of the key agreement
	 * 
	 * @return type of the key agreement
	 */
	public abstract KeyAgreementType type();

	/**
	 * Generate a MIKEY message
	 * 
	 * @return generated MIKEY message
	 */
	public abstract MIKEYMessage createMessage();
		
	/**
	 * Gets the byte array of the TGK (TEK Generation Key)
	 * 
	 * @return value of the TGK
	 */
	public byte[] tgk() {
		log.trace("");
		
		return tgkPtr;
	}
	
	/**
	 * Gets the byte array of the random data (RAND)
	 * 
	 * @return value of the random data
	 */
	public byte[] rand() {
		log.trace("");
		
		return randPtr;
	}
	
	/**
	 * Gets the key validity object.
	 * 
	 * @return key validity
	 * @see ch.imvs.mikey4j.KeyValidity.KeyValidity
	 */
	public KeyValidity keyValidity() {
		if(log.isTraceEnabled())
			log.trace(kvPtr);
		
		return kvPtr;
	}
	
	/**
	 * Sets the key validity.
	 * 
	 * @param kv key validity to be set
	 * @see ch.imvs.mikey4j.KeyValidity.KeyValidity
	 */
	public void setKeyValidity(KeyValidity kv) {
		if(log.isTraceEnabled())
			log.trace("kv=" + kv);
		
		kvPtr = null;
		
		switch (kv.type()) {
			case KeyValidity.KEYVALIDITY_NULL:
				kvPtr = new KeyValidity();
				break;
			case KeyValidity.KEYVALIDITY_INTERVAL:
				kvPtr = new KeyValidityInterval((KeyValidityInterval) kv);
				break;
			default:
				return;
		}
	}
	
	/**
	 * Sets the random data (RAND).
	 * 
	 * @param rand random data to be set
	 */
	public void setRand(byte[] rand) {
		log.trace("rand=[...]");
		
		this.randLengthValue = rand.length;
		randPtr = new byte[ randLengthValue ];
		System.arraycopy(rand, 0, randPtr, 0, randLengthValue);
	}

	private final int PRF_KEY_CHUNK_LENGTH = 32;
	private final int SHA_DIGEST_SIZE = 20;
	
	/**
	 * Calculates a pseudo-random value, using a PRF (Pseudo-Random Function), on the basis of a input key and a label
	 * 
	 * @param inkeyBuffer The input key to the derivation function
	 * @param label A specific label, dependent on the type of the key to be derived, the RAND, and the session IDs
	 * @param outkeyLength Desired length in bytes of the output key.
	 * @return output key
	 */
	public byte[] prf(byte[] inkeyBuffer, byte[] label, int outkeyLength){
		if(log.isTraceEnabled())
			log.trace("inkey=[...], label=[...], outkeyLength=" + outkeyLength);
		
		int n = (inkeyBuffer.length + PRF_KEY_CHUNK_LENGTH - 1) / PRF_KEY_CHUNK_LENGTH;
		int m = (outkeyLength + SHA_DIGEST_SIZE - 1) / SHA_DIGEST_SIZE;
		ByteBuffer inkey = ByteBuffer.wrap(inkeyBuffer);
		ByteBuffer output = ByteBuffer.allocate(m * SHA_DIGEST_SIZE);
		byte[] outkey = new byte[outkeyLength];
		for(int i = 0; i < n - 1; i++){
			inkey.position(i * PRF_KEY_CHUNK_LENGTH);
			inkey.limit(inkey.position() + PRF_KEY_CHUNK_LENGTH);
			output.position(0);
			p(inkey.slice(), label, m, output);
			for(int j = 0; j < outkeyLength; j++){
				outkey[j] ^= output.get(j);
			}
		}

		//last step
		//TODO: this is probably wrong in libmikey! the last part of the key is never used if inkey%chunk==0
		int remainder = inkeyBuffer.length % PRF_KEY_CHUNK_LENGTH;
		inkey.position((n-1)*PRF_KEY_CHUNK_LENGTH);
		inkey.limit(inkey.position() + ((remainder == 0) ? PRF_KEY_CHUNK_LENGTH : remainder));
		output.position(0);
		p(inkey.slice(), label, m, output);
		for(int j = 0; j < outkeyLength; j++){
			outkey[j] ^= output.get(j);
		}
		
		return outkey;
	}
	
	private void p(ByteBuffer s, byte[] label, int m, ByteBuffer output){
		KeyParameter sk;
		if (s.hasArray())
			sk = new KeyParameter(s.array(), s.arrayOffset(), s.limit());
		else {
			byte[] s_bytes = new byte[s.limit()];
			s.get(s_bytes);
			sk = new KeyParameter(s_bytes, 0, s_bytes.length);
		}
		mac.init(sk);

		final int macLen = mac.getMacSize();
		final byte[] prevA = new byte[macLen];
		mac.update(label, 0, label.length);
		mac.doFinal(prevA, 0);

		final boolean inplace = output.hasArray();
		final byte[] temp = inplace? null : new byte[macLen];

		for (;;) {
			mac.update(prevA, 0, macLen);
			mac.update(label, 0, label.length);
			int remaining = output.remaining();
			if (remaining < macLen) {
				mac.doFinal(prevA, 0);
				output.put(prevA, 0, remaining);
				break;
			}
			if (inplace) {
				mac.doFinal(output.array(), output.arrayOffset());
				output.position(output.position() + macLen);
			}
			else {
				mac.doFinal(temp, 0);
				output.put(temp, 0, macLen);
			}
			
			if (--m <= 0)
				break;
			
			mac.update(prevA, 0, macLen);
			mac.doFinal(prevA, 0);
		}
	}

	/**
	 * Derives a key using a PRF (Pseudo-Random Function)
	 * 
	 * @param csId Crypto Session ID
	 * @param csbIdValue Crypto Session Bundle ID
	 * @param inkey input key
	 * @param keyLength Desired length in bytes of the output key.
	 * @param type Type to calculate key
	 * @return derived key
	 */
	public byte[] keyDeriv(int csId, int csbIdValue, byte[] inkey, int keyLength, int type) {
		if(log.isTraceEnabled())
			log.trace("csId=" + csId + ", csbIdValue=" + csbIdValue + ", inkey=[...], keyLength=" + keyLength + ", type=" + type);
		
		byte[] label = new byte[4+4+1+randLengthValue];
		
		switch(type) {
			case KEY_DERIV_SALT:
				label[0] = Conversion.write8bitNBO(0x39);
				label[1] = Conversion.write8bitNBO(0xA2);
				label[2] = Conversion.write8bitNBO(0xC1);
				label[3] = Conversion.write8bitNBO(0x4B);
				break;
			case KEY_DERIV_TEK:
				label[0] = Conversion.write8bitNBO(0x2A);
				label[1] = Conversion.write8bitNBO(0xD0);
				label[2] = Conversion.write8bitNBO(0x1C);
				label[3] = Conversion.write8bitNBO(0x64);
				break;
			case KEY_DERIV_TRANS_ENCR:
				label[0] = Conversion.write8bitNBO(0x15);
				label[1] = Conversion.write8bitNBO(0x05);
				label[2] = Conversion.write8bitNBO(0x33);
				label[3] = Conversion.write8bitNBO(0xE1);
				break;
			case KEY_DERIV_TRANS_SALT:
				label[0] = Conversion.write8bitNBO(0x29);
				label[1] = Conversion.write8bitNBO(0xB8);
				label[2] = Conversion.write8bitNBO(0x89);
				label[3] = Conversion.write8bitNBO(0x16);
				break;
			case KEY_DERIV_TRANS_AUTH:
				label[0] = Conversion.write8bitNBO(0x2D);
				label[1] = Conversion.write8bitNBO(0x22);
				label[2] = Conversion.write8bitNBO(0xAC);
				label[3] = Conversion.write8bitNBO(0x75);
				break;
			case KEY_DERIV_ENCR:
				label[0] = Conversion.write8bitNBO(0x15);
				label[1] = Conversion.write8bitNBO(0x79);
				label[2] = Conversion.write8bitNBO(0x8C);
				label[3] = Conversion.write8bitNBO(0xEF);  
				break;
			case KEY_DERIV_AUTH:
				label[0] = Conversion.write8bitNBO(0x1B);
				label[1] = Conversion.write8bitNBO(0x5C);
				label[2] = Conversion.write8bitNBO(0x79);
				label[3] = Conversion.write8bitNBO(0x73);
				break;
		}
		label[4] = Conversion.write8bitNBO(csId);
		
		label[5] = (byte) ((csbIdValue>>24) & 0xFF);
		label[6] = (byte) ((csbIdValue>>16) & 0xFF);
		label[7] = (byte) ((csbIdValue>>8) & 0xFF);
		label[8] = (byte) (csbIdValue & 0xFF);
		System.arraycopy(randPtr, 0, label, 9, randLengthValue);
		
		try {
			return prf(inkey, label, keyLength);
		}
		catch (Exception e) {
			log.error(e);
			throw new RuntimeException(e.toString());
		}
	}
	
	/**
	 * Generates a TEK (Transport Encryption Key)
	 * 
	 * @param csId Crypto Session ID
	 * @param tekLength Desired length in bytes of TEK
	 * @return generated TEK
	 */
	public byte[] genTek(int csId, int tekLength) {
		if(log.isTraceEnabled())
			log.trace("csId=" + csId + ", tekLength=" + tekLength);

		byte[] key = keyDeriv(csId, csbIdValue, tgkPtr, tekLength, KEY_DERIV_TEK);
		if(log.isTraceEnabled())
			log.trace("TEK for csId " + csId + "=" + Conversion.bintohex(key));
		return key;
	}
	
	/**
	 * Generates a Salt
	 * 
	 * @param csId Crypto Session ID
	 * @param saltLength Desired length in bytes of the Salt
	 * @return generated Salt
	 */
	public byte[] genSalt(int csId, int saltLength) {
		if(log.isTraceEnabled())
			log.trace("csId=" + csId + ", saltLength=" + saltLength);
		
		byte[] key = keyDeriv(csId, csbIdValue, tgkPtr, saltLength, KEY_DERIV_SALT);
		if(log.isTraceEnabled())
			log.trace("Salt for csId " + csId + "=" + Conversion.bintohex(key));
		return key;
	}
	
	/**
	 * Generates a Encryption Key
	 * 
	 * @param csId Crypto Session ID
	 * @param e_keylen Desired length in bytes of the Encryption Key
	 * @return generated Encryption Key
	 */
	public byte[] genEncr(int csId, int e_keylen) {
		if(log.isTraceEnabled())
			log.trace("csId=" + csId + ", e_keylen=" + e_keylen);
		
		byte[] key = keyDeriv(csId, csbIdValue, tgkPtr, e_keylen, KEY_DERIV_ENCR);
		return key;
	}
	
	/**
	 * Generates a Authentication Key
	 * 
	 * @param csId Crypto Session ID
	 * @param a_keylen Desired length in bytes of the Authentication Key
	 * @return generated Authentication Key
	 */
	public byte[] genAuth(int csId, int a_keylen) {
		if(log.isTraceEnabled())
			log.trace("csId=" + csId + ", a_keylen=" + a_keylen);
		
		return keyDeriv(csId, csbIdValue, tgkPtr, a_keylen, KEY_DERIV_AUTH);
	}
	
	/**
	 * Gets the Crypto Session ID
	 * 
	 * @return Crypto Session ID
	 */
	public int csbId() {
		log.trace(csbIdValue);
		
		return csbIdValue;
	}
	
	/**
	 * Sets the Crypto Session Bundle ID
	 * 
	 * @param csbid Crypto Session Bundle ID to be set
	 */
	public void setCsbId(int csbid) {
		log.trace("csbid=" + csbid);
		
		csbIdValue = csbid;
	}
	
	/**
	 * Sets the TGK (TEK Generation Key)
	 * 
	 * @param tgk The TGK to be set
	 */
	protected void setTgk(byte[] tgk) {
		if(log.isTraceEnabled())
			log.trace("tgk=[...]");
		
		if (tgk != null){
			tgkLengthValue = tgk.length;
			tgkPtr = tgk;
		}
		else{
			tgkLengthValue = 16;
			tgkPtr = Rand.randomByte(tgkLengthValue);
		}
	}
	
	/**
	 * Gets the Authentication error
	 * 
	 * @return Authentication error message
	 */
	public String authError() {
		log.trace(authErrorValue);
		
		return authErrorValue;
	}
	
	/**
	 * Sets the Authentication error
	 * 
	 * @param error Error message
	 */
	public void setAuthError(String error) {
		log.error(error);
		authErrorValue = error;
	}
	
	/**
	 * Sets the Authentication error
	 * 
	 * @param error Exception
	 */
	public void setAuthError(Exception error) {
		log.error(error);
		authErrorValue = error.getMessage();
	}
	
	/**
	 * Gets our own URI
	 * 
	 * @return Our own URI
	 */
	public String uri() {
		log.trace(uriValue);
		
		return uriValue;
	}
	
	/**
	 * Sets our own URI
	 * 
	 * @param theUri The URI to be set as our own
	 */
	public void setUri(String theUri) {
		if(log.isTraceEnabled())
			log.trace("theUri=" + theUri);
		
		uriValue = theUri;
	}
	
	/**
	 * Gets the URI of the counterpart
	 * 
	 * @return The URI
	 */
	public String peerUri() {
		log.trace(peerUriValue);
		
		return peerUriValue;
	}
	
	/**
	 * Sets the URI of the counterpart
	 * 
	 * @param thePeerUri The URI of to be set
	 */
	public void setPeerUri(String thePeerUri) {
		log.trace("thePeerUri=" + thePeerUri);
		
		peerUriValue = thePeerUri;
	}
	
	/**
	 * Gets the map of Crypto Session ID
	 * 
	 * @return map
	 */
	public MIKEYCsIdMap csIdMap() {
		if(log.isTraceEnabled())
			log.trace(csIdMapPtr);
		
		return csIdMapPtr;
	}
	
	/**
	 * Sets the map of Crypto Session ID
	 * 
	 * @param idMap The map to be set
	 */
	public void setCsIdMap(MIKEYCsIdMap idMap) {
		if(log.isTraceEnabled())
			log.trace(idMap);
		
		csIdMapPtr = idMap;
	}
	
	/**
	 * Gets the number of Crypto Sessions
	 * 
	 * @return number of Crypto Sessions
	 */
	public int nCs() {
		return csIdMapPtr.getNoCs();
	}
	
	/**
	 * Sets the type of the Crypto Session ID map
	 * 
	 * @param type type of the Crypto Session ID map
	 */
	public void setCsIdMapType(int type) {
		if(log.isTraceEnabled())
			log.trace("type=" + type);
		
		CsIdMapType = type;
	}
	
	/**
	 * Gets the type of the Crypto Session ID map
	 * 
	 * @return type of the Crypto Session ID map
	 */
	public int getCsIdMapType() {
		log.trace(CsIdMapType);
		
		return CsIdMapType;
	}
	
	/**
	 * Gets the crypto session mapping.
	 * @param <T> Type of the crypto session to return
	 * @return the crypto session mapping.
	 */
	@SuppressWarnings("unchecked")
	public <T> T getCsIdMap(){
		return (T)csIdMapPtr;
	}
	
	/**
	 * Gets the policy type from the parameters
	 * 
	 * @param policy_No number of the policy
	 * @param prot_type type of the security protocol
	 * @param policy_type type of the policy
	 * @return policy type
	 */
	public KeyAgreementPolicyEntry getPolicyParamType(int policy_No, int prot_type, int policy_type) {
		if(log.isTraceEnabled())
			log.trace("policy_No=" + policy_No + ", prot_type=" + prot_type + ", policy_type=" + policy_type);
		
		for(KeyAgreementPolicyEntry pt : policy){
			if (pt.policy_No == policy_No && pt.prot_type == prot_type && pt.policy_type == policy_type)
				return pt;
		}
		return null;
	}
	
	/**
	 * Gets the policy type value from the parameters
	 * 
	 * @param policy_No No number of the policy
	 * @param prot_type type of the security protocol
	 * @param policy_type type of the policy
	 * @return value of the 
	 */
	public int getPolicyParamTypeValueOrDefault(int policy_No, int prot_type, int policy_type) {
		KeyAgreementPolicyEntry entry = getPolicyParamType(policy_No, prot_type, policy_type);
		if(entry != null)
			return entry.value[0];

		switch(prot_type) {
			case MIKEYPayloadSP.MIKEY_PROTO_SRTP:
				if (policy_type < SRTP_DEFAULT_POLICY_VALUES.length)
					return SRTP_DEFAULT_POLICY_VALUES[policy_type];
				log.error("MIKEY_PROTO_SRTP type out of range " + policy_type);
				break;
			default:
				break;
		}
		return 0;
	}
	
	/**
	 * Sets a policy parameter to the value of a given type
	 * 
	 * @param policy_No number of the policy 
	 * @param prot_type type of the security protocol
	 * @param policy_type type of the policy
	 * @param value the value to be set
	 */
	public void setPolicyParamType(int policy_No, int prot_type, int policy_type, byte[] value) {
		if(log.isTraceEnabled())
			log.trace("policy_No=" + policy_No + ", prot_type=" + prot_type + ", policy_type=" + policy_type + ", value=[...]");
		
		KeyAgreementPolicyEntry pol;
		if ((pol = getPolicyParamType(policy_No, prot_type, policy_type)) == null)
			policy.add(new KeyAgreementPolicyEntry(policy_No, prot_type, policy_type, value));
		else {
			policy.remove(pol);
			pol = null;
			policy.add(new KeyAgreementPolicyEntry(policy_No, prot_type, policy_type, value));
		}
	}

	/**
	 * Sets the default policy with the specified algorithms
	 * 
	 * @param prot_type type of the security protocol
	 * @return number of policies
	 */
	public int setDefaultPolicy(int prot_type) {
		if (log.isDebugEnabled())
			log.trace("prot_type=" + prot_type);

		int policyNo = 0;

		if (policy != null) {
			for (Iterator<KeyAgreementPolicyEntry> iter = policy.listIterator(); iter.hasNext();) {
				KeyAgreementPolicyEntry pt = iter.next();
				if (pt.policy_No == policyNo) {
					iter = policy.listIterator();
					policyNo++;
				}
			}
		}

		switch (prot_type) {
			case MIKEYPayloadSP.MIKEY_PROTO_SRTP:
				for (int i = 0; i < SRTP_DEFAULT_POLICY_VALUES.length; i++) {
					policy.add(new KeyAgreementPolicyEntry(policyNo, prot_type, i, new byte[] { SRTP_DEFAULT_POLICY_VALUES[i] }));
				}
				break;
			case MIKEYPayloadSP.MIKEY_PROTO_IPSEC4:
				throw new MIKEYExceptionUnimplemented("Not implemented yet in Java");
		}
		return policyNo;
	}

	/**
	 * Gets the policy
	 * 
	 * @return policy
	 */
	public List<KeyAgreementPolicyEntry> getPolicy() {
		log.trace("");
		
		return policy;
	}
}
