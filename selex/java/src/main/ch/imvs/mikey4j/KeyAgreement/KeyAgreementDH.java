/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyAgreement;

import java.math.BigInteger;
import java.security.*;
import java.security.cert.*;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

import javax.crypto.interfaces.DHPrivateKey;
import javax.crypto.interfaces.DHPublicKey;
import javax.crypto.spec.DHPrivateKeySpec;
import javax.crypto.spec.DHPublicKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.generators.DHKeyPairGenerator;
import org.bouncycastle.crypto.params.DHKeyGenerationParameters;
import org.bouncycastle.crypto.params.DHParameters;
import org.bouncycastle.crypto.params.DHPrivateKeyParameters;
import org.bouncycastle.crypto.params.DHPublicKeyParameters;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessageDH;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadDH;
import ch.imvs.mikey4j.util.Certificate;


/**
 * Class for handling a Diffie-Hellman key agreement.<br><br>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class KeyAgreementDH extends KeyAgreement {
	private static Log log = LogFactory.getLog(KeyAgreementDH.class);
	
	/** The certificate chain, also known as the certification path, is a list of certificates used to authenticate an entity. */
	private List<Certificate> signingCertificate;
	private List<X509Certificate> peerCertChainPtr;

	/** Prime modulus of the OAKLEY group 5 */
	private static BigInteger pGROUP_5 = new BigInteger("FFFFFFFFFFFFFFFFC90FDAA22168C234C4C6628B80DC1CD129024E088A67CC74020BBEA63B139B22514A08798E3404DDEF9519B3CD3A431B302B0A6DF25F14374FE1356D6D51C245E485B576625E7EC6F44C42E9A637ED6B0BFF5CB6F406B7EDEE386BFB5A899FA5AE9F24117C4B1FE649286651ECE45B3DC2007CB8A163BF0598DA48361C55D39A69163FA8FD24CF5F83655D23DCA3AD961C62F356208552BB9ED529077096966D670C354E4ABC9804F1746C08CA237327FFFFFFFFFFFFFFFF", 16);
	/** Generator of the OAKLEY group 5 */
	private static BigInteger gGROUP_5 = BigInteger.valueOf(2L);
	
	private byte[] peerKey;
	private KeyFactory keyFactory;
	private javax.crypto.KeyAgreement dhKeyAgreement;
	private byte[] publicKeyPtr;
	private int dhgroup;
	
	
	/**
	 * Class constructor
	 * 
	 * @param signingCertificate certificate chain 
	 */
	public KeyAgreementDH(List<Certificate> signingCertificate) {
		if(log.isTraceEnabled())
			log.trace("certChainPtr=" + signingCertificate);

		this.signingCertificate = signingCertificate;
	}
	
	@Override
	public String getFriendlyName() {
		return "DH";
	}
	
	/**
	 * Gets the type of the key agreement
	 * 
	 * @return type of the key agreement
	 */
	@Override
	public KeyAgreementType type() {
		log.trace("KEY_AGREEMENT_TYPE_DH");
		
		return KeyAgreementType.DH;
	}
	
	/**
	 * Generate a MIKEY message
	 * 
	 * @return generated MIKEY message
	 * @throws MIKEYException
	 * @throws CertificateEncodingException
	 * @throws Exception
	 */
	@Override
	public MIKEYMessageDH createMessage() throws MIKEYException {
		return new MIKEYMessageDH(this);
	}
	
	/**
	 * Gets the Diffie-Hellman group
	 * 
	 * @return the group or -1 if the public key is null
	 */
	public int group() {
		if (publicKeyPtr == null) {
			return -1;
		}
		
		return dhgroup;
	}
	
	/**
	 * Sets the Diffie-Hellman group
	 * 
	 * @param groupValue the group to be set
	 */
	public void setGroup(int groupValue) {
		log.trace("groupValue=" + groupValue);
		
		dhgroup = groupValue;
		DHKeyGenerationParameters dhparams;
		switch(groupValue){
			case MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP1:
			case MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP2:
				//TODO support all DH groups
				throw new MIKEYException("DH group not yet supported in Java");
			case MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5:
				dhparams = new DHKeyGenerationParameters(
						new SecureRandom(), 
						new DHParameters(pGROUP_5, gGROUP_5));
				break;
			default:
				throw new MIKEYException("unknown DH group");
		}
		try {
			DHKeyPairGenerator keyGen = new DHKeyPairGenerator();
			keyGen.init(dhparams);
			
			AsymmetricCipherKeyPair bcKeyPair = keyGen.generateKeyPair(); //CPU intense!
			
			KeyPair keyPair = convert(bcKeyPair);
			keyFactory = KeyFactory.getInstance("DH");
			dhKeyAgreement = javax.crypto.KeyAgreement.getInstance("DH");
			dhKeyAgreement.init(keyPair.getPrivate());
			
			publicKeyPtr = ((DHPublicKey) keyPair.getPublic()).getY().toByteArray();
	        makePublicKeyUnsigned();
		}
		catch (NoSuchAlgorithmException e) { setAuthError(e); }
		catch (InvalidKeyException e) { setAuthError(e); }
	}
	
	/**
	 * Converts bouncycastle KeyPair to java KeyPair.
	 * 
	 * @param pair 
	 * @return
	 */
	private KeyPair convert(AsymmetricCipherKeyPair pair) {
		DHPublicKeyParameters pub = (DHPublicKeyParameters) pair.getPublic();
        DHPrivateKeyParameters priv = (DHPrivateKeyParameters) pair.getPrivate();
        
        try{
			KeyFactory keyFac = KeyFactory.getInstance("DH");

			DHParameters dhPubParams = pub.getParameters();
			DHPublicKeySpec pubKeySpecs = new DHPublicKeySpec(pub.getY(),
					dhPubParams.getP(), dhPubParams.getG());
			DHPublicKey pubKey = (DHPublicKey) keyFac
					.generatePublic(pubKeySpecs);

			DHParameters dhPrivParams = priv.getParameters();
			DHPrivateKeySpec privKeySpecs = new DHPrivateKeySpec(priv.getX(),
					dhPrivParams.getP(), dhPrivParams.getG());
			DHPrivateKey privKey = (DHPrivateKey) keyFac
					.generatePrivate(privKeySpecs);

            return new KeyPair(pubKey, privKey);
        }
        catch (Exception e) {
        	throw new RuntimeException("Could not convert key."); 
        }
	}

	private void makePublicKeyUnsigned() {
		//strip the first byte that indicates a "positive" bigint
		if(publicKeyPtr.length == MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5_Length + 1){
			assert(publicKeyPtr[0] == 0);
			byte[] temp = new byte[MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5_Length];
			System.arraycopy(publicKeyPtr, 1, temp, 0, MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5_Length);
			publicKeyPtr = temp;
		}
		else if(publicKeyPtr.length !=  MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5_Length){
			throw new MIKEYException("invalid public DH key generated");
		}
		
		assert(publicKeyPtr.length == MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5_Length);
	}
	
	/**
	 * Sets the peer key
	 * 
	 * @param peerKeyPtr the peer key to be set
	 */
	public void setPeerKey(byte[] peerKeyPtr) {
		peerKey = peerKeyPtr;
	}
	
	/**
	 * Gets the public key
	 * 
	 * @return the public key
	 */
	public byte[] publicKey() {
		log.trace("");
		
		return publicKeyPtr;
	}
	
	/**
	 * Computes the TEK Generation Key (TGK)
	 */
	public void computeTgk() {
		log.trace("");
		
		try {
			dhKeyAgreement.doPhase(preparePublicKeyForDH(), true);
			setTgk(dhKeyAgreement.generateSecret());
		}
		catch(Exception e){
			setAuthError(e);
			throw new RuntimeException(e);
		}
	}

	private PublicKey preparePublicKeyForDH() throws InvalidKeySpecException {
		return keyFactory.generatePublic(new DHPublicKeySpec(new BigInteger(1, peerKey), pGROUP_5, gGROUP_5));
	}
	
	/**
	 * Gets the peer key
	 * 
	 * @return the peer key
	 */
	public byte[] peerKey() {
		log.trace("");
		
		return peerKey;
	}

	/**
	 * Gets the certificate chain
	 * 
	 * @return certificate chain
	 */
	public List<Certificate> certificateChain() {
		if(log.isTraceEnabled())
			log.trace(signingCertificate);
		
		return signingCertificate;
	}
	
	/**
	 * Gets the certificate chain of the counterpart
	 * 
	 * @return certificate chain of the counterpart
	 */
	public List<X509Certificate> peerCertificateChain() {
		if(log.isTraceEnabled())
			log.trace(peerCertChainPtr);
		
		return peerCertChainPtr;
	}
	
	/**
	 * Sets the certificate chain of the counterpart
	 * 
	 * @param peerChain certificate chain to be set
	 */
	public void setPeerCertificateChain(List<X509Certificate> peerChain) {
		if(log.isTraceEnabled())
			log.trace("peerChain=" + peerChain);
		
		peerCertChainPtr = peerChain;
	}
	
	/**
	 * Gets the certificate chain of the counterpart
	 * 
	 * @return the certificate chain of the counterpart
	 */
	public List<X509Certificate> getPeerCertificateChain() {
		return peerCertChainPtr;
	}

	/**
	 * Checks whether the peers URI is contained in the peers certificate.
	 * The check is performed via the Subject Alternative Names and uses the
	 * fields in the following order: URI, rfc822Name (E-Mail Address).
	 * 
	 * @param peerUri URI of the counterpart
	 * @return true if the peer certificate contains the peer URI, else false
	 */
	public boolean controlPeerCertificate(String peerUri) {
		if(log.isTraceEnabled())
			log.trace("peerUri=" + peerUri);
		
		if (peerCertChainPtr == null || peerUri == null || "".equals(peerUri))
			return false;
		
		Certificate peerCert = new Certificate(peerCertChainPtr.get(0));
		String altName = peerCert.getSubAltName(Certificate.SAN_URI);
		
		if (peerUri.equalsIgnoreCase(altName))
			return true;

		//strip the protocol part
		String id = peerUri;
		int pos = peerUri.indexOf(':');
		if (pos != -1)
			id = peerUri.substring(pos + 1);

		altName = peerCert.getSubAltName(Certificate.SAN_RFC822NAME);
		if (id.equalsIgnoreCase(altName))
			return true;

		setAuthError("Peer URI " + peerUri + " not found in subject alt names.");
		return false;
	}
}
