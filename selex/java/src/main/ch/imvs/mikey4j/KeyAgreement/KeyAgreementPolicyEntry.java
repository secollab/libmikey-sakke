/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyAgreement;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class for handling Security Policy Types (was Policy_type in libmikey)
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class KeyAgreementPolicyEntry {
	private static Log log = LogFactory.getLog(KeyAgreementPolicyEntry.class);
	
	/** Number of policies */
	public int policy_No;
	/** Type of the security protocol */
	public int prot_type;
	/** Type of the security policy */
	public int policy_type;
	/** The value of the security policy */
	public byte[] value;
	/** The length of the value */
	
	/**
	 * Class constructor
	 */
	public KeyAgreementPolicyEntry() {
		policy_No = 0;
		prot_type = 0;
		policy_type = 0;
		value = null;
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param policy_No Nubmer of policies
	 * @param prot_type Type of the security protocol
	 * @param policy_type Type of the policy
	 * @param value Value of the security policy
	 */
	public KeyAgreementPolicyEntry(int policy_No, int prot_type, int policy_type, byte[] value) {
		if(log.isTraceEnabled())
			log.trace("constructing Policy_type with policy_No=" + policy_No + ", prot_type=" + prot_type + ", policy_type=" + policy_type + ", value=[...]");
		
		this.policy_No = policy_No;
		this.prot_type = prot_type;
		this.policy_type = policy_type;
		this.value = new byte[value.length];
		System.arraycopy(value, 0, this.value, 0, value.length);
	}
}
