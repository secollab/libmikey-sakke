/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyAgreement;

/**
 * Possible types of KeyAgreements
 * 
 * @author Ingo Bauersachs
 */
public enum KeyAgreementType{
	/**
	 * Diffie-Hellman
	 * Corresponds to minisip's ID 0
	 */
	DH  (0),
	
	/**
	 * Pre-Shared Key
	 * Corresponds to minisip's ID 1 
	 */
	PSK  (1),
	
	/**
	 * Public Key
	 * Corresponds to minisip's ID 2
	 */
	PK  (2),
	
	/**
	 * Diffie-Hellman Hash-based Message Authentication Code
	 * Corresponds to minisip's ID 3
	 */
	DHHMAC  (3),
	
	/**
	 * RSA Reverse (http://tools.ietf.org/html/draft-ietf-msec-mikey-rsa-r-07)
	 * Corresponds to minisip's ID 4
	 */
	RSA_R  (4),

	/**
	 * SAKKE (http://tools.ietf.org/html/rfc6509)
	 * Corresponds to minisip-sakke's ID 26
	 */
	SAKKE  (26);


   private KeyAgreementType(final int id) { this.id = id; }
   public final int id;
}
