package ch.imvs.mikey4j.KeyAgreement;

import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;

public class KeyAgreementTEST extends KeyAgreement
{
   public String getFriendlyName() { return "TEST"; }
   public KeyAgreementType type() { return null; }
   public MIKEYMessage createMessage() { return null; }

   public void testSetTgk(byte[] tgk) { setTgk(tgk); }
}

