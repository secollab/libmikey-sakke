package ch.imvs.mikey4j.KeyAgreement;

import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;
import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessageSAKKE;

import com.selex.util.RandomGenerator;

import com.selex.mikeysakke.crypto.ECCSI;
import com.selex.mikeysakke.crypto.SAKKE;
import com.selex.mikeysakke.crypto.SecureRandomGenerator;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.util.OctetString;
import com.selex.log.DefaultLogger;


public class KeyAgreementSAKKE extends KeyAgreement
{
   private final KeyStorage keys;
   private final ECCSI eccsi;
   private final SAKKE sakke;

   private final RandomGenerator rng;

   public KeyAgreementSAKKE(KeyStorage keys, ECCSI eccsi, SAKKE sakke)
   {
      this.keys = keys;
      this.eccsi = eccsi;
      this.sakke = sakke;
      this.rng = new SecureRandomGenerator();
   }

   public KeyStorage getKeyMaterial() { return keys; }
   public ECCSI getSignatureAlgorithm() { return eccsi; }
   public SAKKE getKeyEncapsulationAlgorithm() { return sakke; }

   public RandomGenerator getRng() { return rng; }

   public void setSSV(final OctetString SSV)
   {
      setTgk(SSV.clampedArray());
   }

   protected void setTgk(byte[] tgk)
   {
      DefaultLogger.out.println("SAKKE using TGK: " + OctetString.bytes_to_ascii_hex(tgk));
      super.setTgk(tgk);
   }

   public String getFriendlyName() { return "SAKKE"; }
   public KeyAgreementType type() { return KeyAgreementType.SAKKE; }
   public MIKEYMessage createMessage() { return new MIKEYMessageSAKKE(this); }
}

