/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYCsID;

import java.nio.ByteBuffer;
import java.util.Iterator;
import java.util.ArrayList;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYSrtpCs;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;


/**
 * Class for handling the map of the Crypto Session ID for SRTP
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYCsIdMapSrtp implements MIKEYCsIdMap {
	private static Log log = LogFactory.getLog(MIKEYCsIdMapSrtp.class);
	
	/** ArrayList which will contains the SRTP Crypto Sessions */
	private ArrayList<MIKEYSrtpCs> cs = new ArrayList<MIKEYSrtpCs>();
	
	/**
	 * Class constructor
	 */
	public MIKEYCsIdMapSrtp() {
		log.trace("constructing");
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data received data
	 * @param offset offset
	 * @param length length of the data
	 */
	public MIKEYCsIdMapSrtp(byte[] raw_data, int offset, int length) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYCsIdMapSrtp with raw_data=[...], offset=" + offset + ", length=" + length);
		
        if ((length % 9) != 0)
        	throw new MIKEYException("Invalid length of SRTP_ID map info");
	
	    int nCs = length / 9;
	    int ssrc, roc;
	    int policyNo;

	    ByteBuffer reader = ByteBuffer.wrap(raw_data);
	    reader.position(offset);
	    
	    for(int i = 0; i < nCs; i++) {
	    	policyNo = reader.get(); //1
	    	ssrc = reader.getInt();  //4
	    	roc = reader.getInt();   //4
            addStream(ssrc, roc, policyNo, 0);
	    }
	}
	
	/**
	 * Sets the Synchronization Source (SSRC) of the specified Crypto Session ID
	 * 
	 * @param ssrc The SSRC to be set
	 * @param csId The Crypto Session ID
	 */
	public void setSsrc(int ssrc, int csId) {
		if(log.isTraceEnabled())
			log.trace("ssrc=" + ssrc + ", csId=" + csId);
		
        if (csId > cs.size())
        	return;
	
	    cs.get(csId - 1).ssrc = ssrc;	
	}

	/**
	 * Sets the Rollover Counter (ROC) of the specified Crypto Session ID
	 * 
	 * @param roc The ROC to be set
	 * @param csId The Crypto Session ID
	 */
	public void setRoc(int roc, int csId) {
		if(log.isTraceEnabled())
			log.trace("roc=" + roc + ", csId=" + csId);
		
		if (csId > cs.size())
			return;
		
	    cs.get(csId - 1).roc = roc;	
	}
	
	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
        if (expectedLength < length())
        	throw new MIKEYExceptionMessageLengthException("CsSrtpId is too long");
		
		ByteBuffer writer = ByteBuffer.wrap(raw_data);
		writer.position(offset);
		for(MIKEYSrtpCs sc : cs){
			writer.put((byte)(sc.policyNo & 0xFF)); //1
			writer.putInt(sc.ssrc);					//4
			writer.putInt(sc.roc);					//4
		}
	}
	
	/**
	 * Finds the Crypto Session ID of the Synchronization Source (SSRC)
	 * 
	 * @param ssrc The SSRC of the Crypto Session ID to find
	 * @return The ID of the Crypto session or 0 if no ID with the given SSRC exists
	 */
	public int findCsId(int ssrc) {
		if(log.isTraceEnabled())
			log.trace("ssrc=" + ssrc);
		
		int j = 1;
		for(MIKEYSrtpCs sc : cs){
			if (sc.ssrc == ssrc)
				return j;
			j++;
		}
        return 0;
	}
	
	/**
	 * Finds the number of policies from the Synchronization Source (SSRC)
	 * 
	 * @param ssrc The SSRC of the Crypto Session ID to find
	 * @return The number of policies
	 */
	public int findPolicyNo(int ssrc) {
		if(log.isTraceEnabled())
			log.trace("ssrc=" + ssrc);
		
		for(MIKEYSrtpCs sc : cs){
			if (sc.ssrc == ssrc)
				return sc.policyNo;
		}
        return 0;
	}
	
	/**
	 * Finds the Rollover Counter (ROC) of the Synchronization Source (SSRC)
	 * 
	 * @param ssrc The SSRC of the Crypto Session ID to find
	 * @return The ROC or 0 if no ROC with the given SSRC exists
	 */
	public int findRoc(int ssrc) {
		if(log.isTraceEnabled())
			log.trace("ssrc=" + ssrc);
		
		for(MIKEYSrtpCs sc : cs){
			if (sc.ssrc == ssrc)
				return sc.roc;
		}
        return 0;
	}
	
	@Override
	public int length() {
		log.trace(9 * cs.size());
		
		return 9 * cs.size();
	}
	
	/**
	 * Add a stream to the SRTP Crypto Session
	 * 
	 * @param ssrc Synchronization Source (SSRC)
	 * @param roc Rollover Counter (ROC)
	 * @param policyNo Number of policies
	 * @param csId The Crypto Session ID
	 */
	public void addStream(int ssrc, int roc, int policyNo, int csId) {
		if(log.isTraceEnabled())
			log.trace("ssrc=" + ssrc + ", roc=" + roc + ", policyNo=" + policyNo + ", csId=" + csId);
		
        if (csId == 0) {
            cs.add(new MIKEYSrtpCs(policyNo, ssrc, roc));
            return;
	    }
	
	    if (csId > cs.size())
	    	return;
	
	    cs.get(csId - 1).ssrc = ssrc;
	    cs.get(csId - 1).policyNo = policyNo;
	    cs.get(csId - 1).roc = roc;
	}
	
	@Override
	public String toString() {
		log.trace("");
		
		String output = "";
		int csId = 1;
		
		for (Iterator<MIKEYSrtpCs> iCs = cs.iterator(); iCs.hasNext(); csId++) {
			MIKEYSrtpCs sc = iCs.next();
			output += "csId: <" + csId + ">, policyNo: <" + sc.policyNo + ">, SSRC: <" + sc.ssrc + ">, ROC: <" + sc.roc + ">\n";
		}
		return output;
	}
	
	@Override
	public int getNoCs(){
		return cs.size();
	}
}
