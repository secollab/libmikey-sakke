/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.MIKEYCsID;

/**
 * Definition of the Crypto Session ID handling.
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public interface MIKEYCsIdMap {
	/**
	 * SRTP crypto session map ID.
	 */
	public final static int HDR_CS_ID_MAP_TYPE_SRTP_ID = 0;

	/**
	 * Gets the length of the map of the encoded Crypto Session IDs in bytes.
	 * 
	 * @return byte length of the map of the Crypto Session IDs
	 */
	public abstract int length();
	
	/**
	 * Writes the data of the map of the Crypto Session ID to the MIKEY message
	 * 
	 * @param raw_data the byte array of the MIKEY message
	 * @param offset position to add the data
	 * @param expectedLength expected length of the MIKEY message
	 */
	public abstract void writeData(byte[] raw_data, int offset, int expectedLength);

	/**
	 * Gets the number of crypto sessions.
	 * @return The number of crypto sessions.
	 */
	public abstract int getNoCs();
}
