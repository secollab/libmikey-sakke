package ch.imvs.mikey4j;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import ch.imvs.mikey4j.util.Certificate;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.mikeysakke.crypto.ECCSI;
import com.selex.mikeysakke.crypto.SAKKE;


public abstract class MIKEYCertificatelessConfig implements MIKEYConfig
{
   private final KeyStorage sakkeKeyStore;
   private final ECCSI eccsi;
   private final SAKKE sakke;

   public MIKEYCertificatelessConfig(KeyStorage sakkeKeyStore, ECCSI eccsi, SAKKE sakke)
   {
      this.sakkeKeyStore = sakkeKeyStore;
      this.eccsi = eccsi;
      this.sakke = sakke;
   }

   @Override public KeyStorage getSakkeKeyMaterial() { return sakkeKeyStore; }
   @Override public ECCSI getSakkeSignatureAlgorithm() { return eccsi; }
   @Override public SAKKE getSakkeKeyEncapsulationAlgorithm() { return sakke; }

   @Override
   public boolean isMethodEnabled(KeyAgreementType method)
   {
      return method == KeyAgreementType.PSK
          || method == KeyAgreementType.SAKKE;
   }

   @Override public boolean getCertCheckEnabled() { return false; }
   @Override public List<Certificate> getSigningCertificate() { return null; }
   @Override public boolean verifyCertificate(List<X509Certificate> chain) { return false; }
   @Override public int getMaxTimestampOffsetSeconds() { return 60 * 5; /* 5 minutes */ }
}

