/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import java.io.File;
import java.io.FileNotFoundException;
import java.security.cert.X509Certificate;
import java.util.*;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.util.Certificate;


/**
 * Class for handling the MIKEY configuration based on a PKCS#12 self-signed certificate file.
 * Certificate verification is enabled by default.
 * The incoming certificate from the peer is trusted if it is self-signed.
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYConfigCertificateFromFile extends MIKEYConfigAdapter {
	private static Log log = LogFactory.getLog(MIKEYConfigCertificateFromFile.class);
	
	private boolean checkCert = true;
	private List<Certificate> signingCerts = null;
	private String uri;

	/**
	 * Creates a new config instance based on the supplied certificate file.
	 * 
	 * @param certpath The path to the PKCS#12 certificate. 
	 * @param password The password for the PKCS#12 certificate.
	 */
	public MIKEYConfigCertificateFromFile(String certpath, String password) {
		this(new File(certpath), password);
	}

	/**
	 * Creates a new config instance based on the supplied certificate file.
	 * 
	 * @param certpath The path to the PKCS#12 certificate. 
	 * @param password The password for the PKCS#12 certificate.
	 */
	public MIKEYConfigCertificateFromFile(File certpath, String password) {
		log.trace("constructing MIKEYConfigCertificateFromFile with certpath=" + certpath);

		if (!certpath.exists()){
			log.error("The given path of the certificate does not exist on the filesystem.");
			throw new RuntimeException(new FileNotFoundException());
		}

		signingCerts = new LinkedList<Certificate>();
		signingCerts.add(Certificate.fromPKCS12File(certpath.getAbsolutePath(), password));
	}

	@Override public com.selex.mikeysakke.user.KeyStorage getSakkeKeyMaterial() { return null; }
	@Override public com.selex.mikeysakke.crypto.ECCSI getSakkeSignatureAlgorithm() { return null; }
	@Override public com.selex.mikeysakke.crypto.SAKKE getSakkeKeyEncapsulationAlgorithm() { return null; }

	/* (non-Javadoc)
	 * @see libmikey.MIKEYConfig#getUri()
	 */
	@Override
	public String getUri() {
		return uri;
	}
	
	/**
	 * Sets our own URI.
	 * 
	 * @param uri The URI
	 */
	public void setUri(String uri){
		this.uri = uri;
	}
	
	/* (non-Javadoc)
	 * @see libmikey.MIKEYConfig#getSigningCertificate()
	 */
	@Override
	public List<Certificate> getSigningCertificate(){
		return signingCerts;
	}
	
	/* (non-Javadoc)
	 * @see libmikey.MIKEYConfig#verifyCertificate(java.util.List)
	 */
	@Override
	public boolean verifyCertificate(List<X509Certificate> chain){
		X509Certificate cert = chain.get(0);
		try {
			cert.verify(cert.getPublicKey());
			return super.verifyCertificate(chain);
		}
		catch (Exception e) {
			log.error("certificate verification failed", e);
			return false;
		}
	}

	/* (non-Javadoc)
	 * @see libmikey.MIKEYConfig#getCertCheckEnabled()
	 */
	@Override
	public boolean getCertCheckEnabled() {
		return checkCert;
	}
	
	/**
	 * Sets whether the peers certificate should be verified.
	 * @param status True to enable certificate check, false to disable.
	 */
	public void setCertCheckEnabled(boolean status){
		checkCert = status;
	}

	/**
	 * Not implemented, this configuration is for Certificate use only.
	 * @return always null
	 */
	@Override
	public byte[] getPSK() {
		return null;
	}

	/**
	 * Determines whether the specified KeyAgreement method is enabled.
	 * @see KeyAgreementType
	 * 
	 * @param method The KeyAgreement method whose state is to be determined
	 * @return True when method is one of (KEY_AGREEMENT_TYPE_DH)
	 */
	@Override
	public boolean isMethodEnabled(KeyAgreementType method) {
		return method == KeyAgreementType.DH;
	}
}
