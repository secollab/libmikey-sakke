/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import java.util.ArrayList;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementDH;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementSAKKE;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMap;
import ch.imvs.mikey4j.MIKEYCsID.MIKEYCsIdMapSrtp;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionAuthentication;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageContent;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUnacceptable;
import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionUnimplemented;
import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;
import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessageError;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadDH;
import ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadERR;
import ch.imvs.mikey4j.replaycache.ReplayCache;
import ch.imvs.mikey4j.replaycache.ReplayCacheSingleton;


/**
 * Class for handling the Multimedia Internet KEYing (MIKEY)<br><b>Main class of MIKEY4J</b><br><br>
 *  
 * The following figure shows the key exchange with MIKEY:
 * 
 * <PRE>
 *    Caller            Callee
 *      |                  |
 * get SDP Offer --------| |
 *      |                | |
 *      |                v |
 *      |             set SDP Offer
 *      |                  |
 *      | |---------- get SDP Answer
 *      | |                |
 *      | v                |
 * set SDP Answer          |
 *      |                  |
 * generate TGK       generate TGK
 *      |                  |
 * generate TEK       generate TEK
 * 
 * get SDP Offer:	Initiator creates the first message and sends it to responder
 * 					- MIKEY.initiatorCreate()
 * set SDP Offer:	Responder parses the received message:
 * 					- MIKEY.responderAuthenticate()
 * get SDP Answer:	Responder creates the response message and sends it to initiator
 * 					- MIKEY.responderCreateAnswerMessage()
 * set SDP Answer:	Initiator parses the received answer message:
 *					- MIKEY.initiatorAuthenticate();
 *					- MIKEY.initiatorParse()
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEY {
	/** MIKEY Protocol SRP: 0 */
	public final static int MIKEY_PROTO_SRTP = 0;
	
	/** Prefix of MIKEY messages */
	public final static String MIKEY_MESSAGE_PREFIX = "mikey ";

	private static Log log = LogFactory.getLog(MIKEY.class);
	private String localErrorMessage;
	private ArrayList<Integer> mediaStreamSenders = new ArrayList<Integer>();
	private State state;
	private boolean secured;
	private KeyAgreement ka = null;
	private MIKEYConfig config;
	private MIKEYMessage initiatorMessage;
	private MIKEYMessage responderMessage;
	private final ReplayCache replayCache;

	private enum State {
		STATE_START,
		STATE_INITIATOR,
		STATE_RESPONDER,
		STATE_AUTHENTICATED,
		STATE_ERROR
	}

	/**
	 * Class constructor
	 * 
	 * @param config the config with the user information
	 */
	public MIKEY(MIKEYConfig config) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEY with config=" + config);
		
		this.state = State.STATE_START; 
		this.config = config;
		
		int timeToLive = config.getMaxTimestampOffsetSeconds() + 60; // ttl = timestamp offset + 1 minute
		replayCache = ReplayCacheSingleton.getInstance(timeToLive, timeToLive);
	}

	/**
	 * In the role as responder, authenticates and parses the received message from the initiator
	 * 
	 * @param message The received message (encoded in base64 with mikey-prefix)
	 * @param peerUri The URI of the initiator
	 * @return True if the authentication and the parsing are successful, false if not
	 */
	public boolean responderAuthenticate(String message, String peerUri) {
		if(log.isTraceEnabled())
			log.trace("message=" + message + ", peerUri=" + peerUri);
		
		setState(State.STATE_RESPONDER);
		
		// check if the message is from type MIKEY
		if (message.substring(0, MIKEY_MESSAGE_PREFIX.length()).compareTo(MIKEY_MESSAGE_PREFIX) == 0) {
			String b64Message = message.substring(MIKEY_MESSAGE_PREFIX.length(), message.length());

			if (message == "")
				throwMikeyException("No MIKEY message received");
			else {
				try {
					// parse the received message
					initiatorMessage = MIKEYMessage.parse(b64Message);
					createKeyAgreement(initiatorMessage.keyAgreementType());

					if (replayCache.isReplayed(initiatorMessage)) {
						localErrorMessage = "Replay attack detected. Authentication of initiator message aborted.";
						log.warn(localErrorMessage);
						return false;
					}

					ka.setPeerUri(peerUri);

					// authenticates the message
					if (!initiatorMessage.authenticate(ka))
						throw new MIKEYExceptionAuthentication("Authentication of the MIKEY init message failed: " + ka.authError());

					replayCache.addToCache(initiatorMessage);
					
					if (config.getCertCheckEnabled()) {
						if(ka instanceof KeyAgreementDH){
							KeyAgreementDH kadh = (KeyAgreementDH)ka;
							if(!kadh.controlPeerCertificate(kadh.peerUri()))
								throw new MIKEYExceptionAuthentication("Peer's certificate is not valid for " + kadh.peerUri());
							if(!config.verifyCertificate(kadh.getPeerCertificateChain()))
								throw new MIKEYExceptionAuthentication("Peer's certificate is invalid/not trusted");
						}
					}
					
					secured = true;
					setState(State.STATE_AUTHENTICATED);
				}
				catch(MIKEYException exc) {
					setError(exc);
				}
			}
		}
		else {
			localErrorMessage = "Unknown type of key agreement, not a MIKEY message";
			log.error(localErrorMessage);
			secured = false;
			setState(State.STATE_ERROR);
		}

		return (state == State.STATE_AUTHENTICATED);
	}

	/**
	 * Creates the response message, which will be sent to the initiator
	 * 
	 * @return The Base64 encoded MIKEY message
	 */
	public MIKEYMessage responderCreateAnswerMessage() {
		log.trace("");

		if (ka == null) {
			log.error("Unknown type of key agreement");

			setState(State.STATE_ERROR);
			return null;
		}

		if (initiatorMessage == null) {
			log.error("Uninitialized message, this is a bug");

			setState(State.STATE_ERROR);
			return null;
		}

		// build the response message
		MIKEYMessage responseMessage = null;
		try {
			initiatorMessage.setOffer(ka, config.getMaxTimestampOffsetSeconds());
			addStreamsToKa();
			responseMessage = initiatorMessage.buildResponse(ka);
			if(ka instanceof KeyAgreementDH)
				((KeyAgreementDH) ka).computeTgk();
		}
		catch (MIKEYExceptionUnacceptable exc) {
			setError(exc);
		}
		catch (MIKEYExceptionMessageContent exc) {
			setError(exc);
			responseMessage = exc.errorMessage();
		}
		catch (MIKEYException exc) {
			setError(exc);
		}

		if (responseMessage != null) {
			log.debug("Created response message " + responseMessage);

			return responseMessage;
		}
		else {
			log.error("No response message");
			return null;
		}
	}

	/**
	 * Creates the initiator message, which will be send to the responder
	 * 
	 * @param type The type of the key agreement to create
	 * @param peerUri The URI of the responder 
	 * @return The Bas64 encoded MIKEY message
	 */
	public MIKEYMessage initiatorCreate(KeyAgreementType type, String peerUri) {
		if(log.isTraceEnabled())
			log.trace("type=" + type + ", peerUri=" + peerUri);
		
		setState(State.STATE_INITIATOR);
		
		try {
			// create the key agreement
			createKeyAgreement(type);
			if(ka == null)
				throw new MIKEYException("Can't create key agreement");
			
			ka.setPeerUri(peerUri);
			return ka.createMessage();
		} catch(MIKEYException me) {
			log.error("MIKEYException caught", me);
			setState(State.STATE_ERROR);
		}
		return null;
	}

	/**
	 * Encodes the passed message into a Base 64 string, prepended with the message prefix.
	 * @param msg The message to encode.
	 * @return The message encoded as Base 64, prefixed with MIKEY.
	 */
	public static String encode(MIKEYMessage msg){
		return MIKEY_MESSAGE_PREFIX + msg.base64_encode();
	}

	/**
	 * The initiator authenticates the received message from the responder, but does not parse all payloads.
	 * Use {@link #initiatorParse()} to do this. 
	 * 
	 * @param message The received message
	 * @return True if the authentication and the parsing are successful, false if not
	 */
	public boolean initiatorAuthenticate(String message) {
		if(log.isTraceEnabled())
			log.trace("message=" + message);

		// check if the message is from type MIKEY
		if (message.substring(0, MIKEY_MESSAGE_PREFIX.length()).compareTo(MIKEY_MESSAGE_PREFIX) == 0) {
			String b64Message = message.substring(MIKEY_MESSAGE_PREFIX.length(), message.length());

			if ("".equals(b64Message)) {
				setError("No MIKEY message received");
				return false;
			}
			else {
				try {
					// parses the received message
					responderMessage = MIKEYMessage.parse(b64Message);
					
					if (replayCache.isReplayed(responderMessage)) {
						localErrorMessage = "Replay attack detected. Authentication of responder message aborted.";
						log.warn(localErrorMessage);
						return false;
					}

					//check if this is an error response
					if(responderMessage instanceof MIKEYMessageError){
						int error = ((MIKEYMessageError)responderMessage).getErrorCode();
						setError("Error from responder: " + MIKEYPayloadERR.errorCodeToText(error));
						return false;
					}

					//now do the real authentication
					if (!responderMessage.authenticate(ka))
						throw new MIKEYExceptionAuthentication("Authentication of the response message failed");

					replayCache.addToCache(responderMessage);

					if (config.getCertCheckEnabled()) {
						if(ka instanceof KeyAgreementDH){
							KeyAgreementDH kadh = (KeyAgreementDH) ka;
							if(!kadh.controlPeerCertificate(kadh.peerUri()))
								throw new MIKEYExceptionAuthentication("Peer's certificate is not valid for " + kadh.peerUri());
							if(!config.verifyCertificate(kadh.getPeerCertificateChain()))
								throw new MIKEYExceptionAuthentication("Peer's certificate is invalid/not trusted");
						}
					}

					secured = true;
					setState(State.STATE_AUTHENTICATED);
				}
				catch(MIKEYException exc) {
					setError(exc);
				}
			}
		}
		else{
			localErrorMessage = "Unknown type of key agreement, not a MIKEY message";
			log.error(localErrorMessage);
			setState(State.STATE_ERROR);
		}

		return (state == State.STATE_AUTHENTICATED);
	}

	/**
	 * In the role of the initiator, parse the received message
	 * after authentication ({@link #initiatorAuthenticate(String)}).
	 */
	public void initiatorParse() {
		if (ka == null)
			setError("initiatorParse: Unknown type of key agreement");
		if (responderMessage == null)
			setError("Uninitialized MIKEY responder message, was initiatorAuthenticate called?");

		try {
			responderMessage.parseResponse(ka, config.getMaxTimestampOffsetSeconds());
			if(ka instanceof KeyAgreementDH)
				((KeyAgreementDH)ka).computeTgk();
		}
		catch(MIKEYException exc) {
			setError(exc);
		}
	}

	/**
	 * Adds streams to the key agreement
	 * 
	 * @throws MIKEYException
	 */
	private void addStreamsToKa() throws MIKEYException {
		log.trace("");
		
		this.ka.setCsIdMapType(MIKEYCsIdMap.HDR_CS_ID_MAP_TYPE_SRTP_ID);
		int j = 1;

		for(int ssrc : mediaStreamSenders){
			if (isInitiator()) {
				int policyNo = ka.setDefaultPolicy(MIKEY_PROTO_SRTP);
				ka.<MIKEYCsIdMapSrtp>getCsIdMap().addStream(ssrc, 0, policyNo, 0);
				
				// Placeholder for the receiver to place his SSRC
				ka.<MIKEYCsIdMapSrtp>getCsIdMap().addStream(0, 0, policyNo, 0);
			}
			else {
				ka.<MIKEYCsIdMapSrtp>getCsIdMap().setSsrc(ssrc, 2*j);
				ka.<MIKEYCsIdMapSrtp>getCsIdMap().setRoc(0, 2*j);
			}
			j++;
		}
	}

	/**
	 * Check if an error occurred.
	 * 
	 * @return True if an error exist, false if not
	 */
	public boolean error() {
		if(log.isTraceEnabled())
			log.trace(state == State.STATE_ERROR);
		
		return (state == State.STATE_ERROR);
	}

	/**
	 * Check if MIKEY is secure
	 * 
	 * @return True if secure, false if not
	 */
	public boolean isSecured() {
		if(log.isTraceEnabled())
			log.trace(secured && !error());
		
		return (secured && !error());
	}

	/**
	 * Check if the message is from type initiator
	 * 
	 * @return True if it is an initiator message, false if not
	 */
	public boolean isInitiator() {
		return (state == State.STATE_INITIATOR);
	}

	/**
	 * Gets the key agreement
	 * 
	 * @return The key agreement
	 */
	public KeyAgreement getKeyAgreement() {
		return ka;
	}

	/**
	 * Gets an authentication error
	 * 
	 * @return The authentication error or "" if no error
	 */
	public String authError() {
		if(log.isTraceEnabled())
			log.trace(localErrorMessage == null ? ka.authError() : localErrorMessage);

		return (localErrorMessage == null ? ka.authError() : localErrorMessage);
	}

	/**
	 * Gets the peer URI
	 * 
	 * @return The peer URI or "" if the message is not authenticated
	 */
	public String peerUri() {
		if(log.isTraceEnabled())
			log.trace(state != State.STATE_AUTHENTICATED ? "" : ka.peerUri());
		
		if (state != State.STATE_AUTHENTICATED)
			return "";

		return ka.peerUri();
	}

	private void setError(Exception ex){
		log.error(ex);
		localErrorMessage = ex.getMessage();
		setState(State.STATE_ERROR);
	}

	private void setError(String message){
		log.error(message);
		localErrorMessage = message;
		setState(State.STATE_ERROR);
	}

	private void throwMikeyException(String message){
		MIKEYException ex = new MIKEYException(message);
		localErrorMessage = message;
		log.error(ex);
		throw ex;
	}

	/**
	 * Sets a new state of MIKEY
	 * 
	 * @param newState The new state to be set
	 */
	private void setState(State newState) {
		log.trace("newState=" + newState);
		state = newState;
	}

	/**
	 * Creates a key agreement of the specified type.
	 * 
	 * @param type The type of the key agreement to create
	 * @throws MIKEYException
	 */
	private void createKeyAgreement(KeyAgreementType type) throws MIKEYException {
		log.trace("type=" + type);

		switch(type) {
			case DH: {
				KeyAgreementDH kaDH = new KeyAgreementDH(config.getSigningCertificate());
				if (isInitiator())
					kaDH.setGroup(MIKEYPayloadDH.MIKEYPAYLOAD_DH_GROUP5);

				this.ka = kaDH;

				log.debug("KeyAgreementDH created");
				break;
			}
			case SAKKE:
				this.ka = new KeyAgreementSAKKE(config.getSakkeKeyMaterial(),
														  config.getSakkeSignatureAlgorithm(),
														  config.getSakkeKeyEncapsulationAlgorithm());
				break;
			case PK:
			case RSA_R:
			case PSK:
			case DHHMAC:
				throw new MIKEYExceptionUnimplemented("Not implemented yet in Java");
			default:
				throw new MIKEYExceptionUnimplemented("Unsupported type of KeyAgreement");
		}
		this.ka.setUri(config.getUri());

		if (isInitiator())
			addStreamsToKa();
	}

	/**
	 * Adds the Synchronisation Source (SSRC) of a stream to the key exchange.
	 * @param ssrc The streams ID to add.
	 */
	public void addSender(int ssrc){
		mediaStreamSenders.add(ssrc);
	}
}
