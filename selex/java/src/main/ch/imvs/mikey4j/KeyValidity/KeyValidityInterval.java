/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyValidity;


import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import ch.imvs.mikey4j.MIKEYException.MIKEYExceptionMessageLengthException;
import ch.imvs.mikey4j.util.Conversion;


/**
 * Class for handling a key validity interval.
 * 
 * The Key validity data is not a standalone payload, but a part of either
 * the Key data payload or the DH payload. The Key validity data gives a guideline of when the
 * key should be used.  There are two KV types defined Security Parameter Index /MKI (SPI) or a lifetime range (interval).
 *  
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! VF Length     ! Valid From                                    ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! VT Length     ! Valid To (expires)                            ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 * @see ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadKeyData
 */
public class KeyValidityInterval extends KeyValidity {
	private static Log log = LogFactory.getLog(KeyValidityInterval.class);
	
	/** Valid From: sequence number, index, timestamp, or other start value that the security protocol uses to identify the start position of the key usage */
	private byte[] vf;
	/** Valid To: sequence number, index, timestamp, or other expiration value that the security protocol can use to identify the expiration of the key usage */
	private byte[] vt;
	
	/**
	 * Class constructor
	 */
	public KeyValidityInterval() {
		log.trace("constructing");
		
		vf = null;
		vt = null;
		typeValue = KEYVALIDITY_INTERVAL;
	}
	
	/**
	 * Class constructor
	 * 
	 * @param source source key validity interval for the new instance
	 */
	public KeyValidityInterval(KeyValidityInterval source) {
		if(log.isTraceEnabled())
			log.trace("constructing with source=" + source);
		
		typeValue = KEYVALIDITY_INTERVAL;
		vf = new byte[ source.vf.length ];
		System.arraycopy(source.vf, 0, vf, 0, source.vf.length);
		
		vt = new byte[ source.vt.length ];
		System.arraycopy(source.vt, 0, vt, 0, source.vt.length);
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the counterpart to parse the received data
	 * 
	 * @param raw_data received data
	 * @param offset offset
	 * @param length_limit length limit of the key validity interval data
	 */
	public KeyValidityInterval(byte[] raw_data, int offset, int length_limit) {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", length_limit=" + length_limit);
		
		if (length_limit < 2)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyValidityInterval");
		int vfLength = raw_data[offset + 0];
		
		if (length_limit < 2 + vfLength)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyValidityInterval");
		vf = new byte[ vfLength ];
		System.arraycopy(raw_data, offset + 1, vf, 0, vfLength);
		
		int vtLength = offset + vfLength + 1;
		if (length_limit < 2 + vfLength + vtLength)
			throw new MIKEYExceptionMessageLengthException("Given data is too short to form a KeyValidityInterval");
		
		vt = new byte[ vtLength ];
		System.arraycopy(raw_data, offset + vfLength + 2, vt, 0, vfLength);
	}
	
	/**
	 * Class constructor<br>
	 * It is called from the initiator to initiate a key validity interval
	 * 
	 * @param vf Valid From: sequence number, index, timestamp, or other start value that the security protocol uses to identify the start position of the key usage
	 * @param vt Valid To: sequence number, index, timestamp, or other expiration value that the security protocol can use to identify the expiration of the key usage
	 */
	public KeyValidityInterval(byte[] vf, byte[] vt) {
		if(log.isTraceEnabled())
			log.trace("vf=[...], vt=[...]");
		
		this.vf = new byte[vf.length];
		System.arraycopy(vf, 0, this.vf, 0, vf.length);
		
		this.vt = new byte[vt.length];
		System.arraycopy(vt, 0, this.vt, 0, vt.length);
	}

	@Override
	public int type() {
		log.trace(typeValue);
		
		return typeValue;
	}

	@Override
	public int length() {
		log.trace(vt.length + vf.length + 3);
		
		return vt.length + vf.length + 3;
	}

	@Override
	public void writeData(byte[] raw_data, int offset, int expectedLength) {
		if(log.isTraceEnabled())
			log.trace("raw_data=[...], offset=" + offset + ", expectedLength=" + expectedLength);
		
		raw_data[offset+0] = Conversion.write8bitNBO(vf.length);
		System.arraycopy(vf, 0, raw_data, offset+1, vf.length);	//!!
		raw_data[offset+1+vf.length] = Conversion.write8bitNBO(vt.length);
		System.arraycopy(vt, 0, raw_data, offset+1+vf.length, 2+vf.length);	//!!
	}
	
	@Override
	public String toString() {
		log.trace("");
		
		return "KeyValidityInterval: vf=<" + Conversion.bintohex(vf) + "> vt=<" + Conversion.bintohex(vt) + ">";
	}
}
