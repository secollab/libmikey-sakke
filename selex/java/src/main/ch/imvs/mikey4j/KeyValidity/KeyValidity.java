/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.KeyValidity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


/**
 * Class for handling a general key validity.
 * 
 * The Key validity data is not a standalone payload, but a part of either
 * the Key data payload or the DH payload. The Key validity data gives a guideline of when the
 * key should be used.  There are two KV types defined Security Parameter Index /MKI (SPI) or a lifetime range (interval).<br><br>
 * 
 * Note that for SRTP usage, the key validity period for a TGK/TEK
 * should be specified with either an interval, where the VF/VT
 * Length is equal to 6 bytes (i.e., the size of the index), or with
 * an MKI.  It is RECOMMENDED that if more than one SRTP stream is
 * sharing the same keys and key update/re-keying is desired, this is
 * handled using MKI rather than the From-To method.
 *  
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! VF Length     ! Valid From                                    ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! VT Length     ! Valid To (expires)                            ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 *
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 * @see ch.imvs.mikey4j.MIKEYPayload.MIKEYPayloadKeyData
 */

public class KeyValidity {
	private static Log log = LogFactory.getLog(KeyValidity.class);
	
	/** Key Validity, null: 0 */
	public final static int KEYVALIDITY_NULL = 0;
	/** Key Validity, Security Parameter Index (SPI): 0 */
	public final static int KEYVALIDITY_SPI = 1;
	/** Key Validity, interval: 2 */
	public final static int KEYVALIDITY_INTERVAL = 2;
	
	/**
	 * The type of the key validity
	 */
	protected int typeValue;
	
	/**
	 * Class constructor
	 */
	public KeyValidity() {
		log.trace("constructing");
		
		this.typeValue = KEYVALIDITY_NULL;
	}
	
	/**
	 * Gets the type of the key validity
	 * 
	 * @return type of the key validity
	 */
	public int type() {
		log.trace(typeValue);
		
		return typeValue;
	}
	
	/**
	 * Gets the length of the key validity
	 * 
	 * @return The length of the key validity data
	 */
	public int length() {
		log.trace(0);
		
		return 0;
	}
	
	/**
	 * Writes the data of the key validity to the MIKEY message
	 * 
	 * @param raw_data the byte array of the MIKEY message
	 * @param offset position to add the data
	 * @param expectedLength expected length of the MIKEY message
	 */
	public void writeData(byte[] raw_data, int offset, int expectedLength) {
		log.trace("");
	}
	
	@Override
	public String toString() {
		return "KeyValidity: type=<" + typeValue + "> length=<" + length() + ">";
	}
}
