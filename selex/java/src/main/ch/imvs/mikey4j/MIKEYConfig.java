/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import java.security.cert.X509Certificate;
import java.util.List;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.util.Certificate;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.mikeysakke.crypto.ECCSI;
import com.selex.mikeysakke.crypto.SAKKE;


/**
 * Provides data (certificates, PSK) and callbacks (certificate verification,
 * whether a KeyAgreement may be used) for handling the MIKEY sessions.
 * 
 * @author Ingo Bauersachs
 */
public interface MIKEYConfig {

	/**
	 * Gets our own URI
	 * 
	 * @return The URI
	 */
	public String getUri();

	/**
	 * Return interface to key data retrieved from MIKEY SAKKE KMS.
	 */
	public KeyStorage getSakkeKeyMaterial();

	/**
	 * Return signature implementation used for MIKEY SAKKE.
	 */
	public ECCSI getSakkeSignatureAlgorithm();

	/**
	 * Return key encapsulation implementation used for MIKEY SAKKE.
	 */
	public SAKKE getSakkeKeyEncapsulationAlgorithm();

	/**
	 * Gets the certificate used for signing MIKEY messages. The certificate at
	 * index 0 is used to create the signature and must therefore contain a private key.
	 * If the certificate is self-signed, it is the only one in the list. If it is signed
	 * by a public CA, the list contains all certificates up to the root.
	 *  
	 * @return A list the certificate used to sign the MIKEY messages.
	 */
	public List<Certificate> getSigningCertificate();

	/**
	 * Checks whether the passed certificate is valid (its signature, date range, revocation).
	 * The check if this certificate is valid for a specific DN should NOT be performed here. 
	 * 
	 * @param chain The certificate to validate is the one at index 0, any following certificate
	 *  is assumed to be the issuer of its predecessor
	 * @return True if the certificate can be trusted
	 */
	public boolean verifyCertificate(List<X509Certificate> chain);

	/**
	 * Determines if the certificate verification is enabled.
	 * 
	 * @return True if the certificates need to be validated, false if not
	 */
	public boolean getCertCheckEnabled();
	
	/**
	 * Gets the key for Pre-Shared Key KeyAgreements
	 * 
	 * @return The PSK
	 */
	public byte[] getPSK();

	/**
	 * Determines whether the specified KeyAgreement method is enabled.
	 * 
	 * @param method The KeyAgreement method whose state is to be determined
	 * @return True when the method is enabled, false otherwise
	 */
	public boolean isMethodEnabled(KeyAgreementType method);
	
	/**
	 * Gets the number of seconds the timestamps in a MIKEY session may differ.
	 * 
	 * @return Maximum time offset in seconds
	 */
	public int getMaxTimestampOffsetSeconds();
}
