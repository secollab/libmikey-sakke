/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class for handling SRTP Crypto Sessions
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYSrtpCs {
	private static Log log = LogFactory.getLog(MIKEYSrtpCs.class);
	
	/** The security policy applied for the stream with SSRC_i. The same security policy may apply for all CSs. */
	public int policyNo;
	
	/**
	 * Specifies the SSRC that MUST be used for the i-th SRTP stream. 
	 * Note that it is the sender of the streams that chooses the SSRC. 
	 * Therefore, it is possible that the Initiator of MIKEY cannot fill in all fields.
	 * In this case, SSRCs that are not chosen by the Initiator are set to zero and
	 * the Responder fills in these fields in the response message.
	 * Note that SRTP specifies requirements on the uniqueness of the SSRCs
	 * (to avoid two-time pad problems if the same TEK is used for more than one stream)
	 */
	public int ssrc;
	
	/**
	 * Current rollover counter used in SRTP. If the SRTP session has not started, 
	 * this field is set to 0.  This field is used to enable a member to join and 
	 * synchronize with an already started stream.
	 */
	public int roc;
	
	/**
	 * Class constructor
	 * 
	 * @param policyNo Number of policies
	 * @param ssrc The Synchronization Source (SSRC)
	 * @param roc The Rollover Counter (ROC)
	 */
	public MIKEYSrtpCs(int policyNo, int ssrc, int roc) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYSrtpCs with policyNo=" + policyNo + ", ssrc=" + ssrc + ", roc=" + roc);
		
		this.policyNo = policyNo;
		this.ssrc = ssrc;
		this.roc = roc;
	}
}
