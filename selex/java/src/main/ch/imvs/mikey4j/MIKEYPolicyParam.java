/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Class for handling policy parameters<br>
 * The Policy param part is built up by a set of Type/Length/Value fields. 
 * For each security protocol, a set of possible types/values that can be negotiated is defined.
 * 
 * <PRE>
 *                      1                   2                   3
 *  0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * ! Type          ! Length        ! Value                         ~
 * +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 * </PRE>
 * 
 * @author Markus Zeiter
 * @author Ingo Bauersachs
 */
public class MIKEYPolicyParam {
	private static Log log = LogFactory.getLog(MIKEYPolicyParam.class);
	
	/** specifies the type of the parameter */
	public int type;
	/** specifies the value of the parameter */
	public byte[] value;

	/**
	 * Class constructor
	 * 
	 * @param type The type of the policy parameter
	 * @param value The value of the policy parameter
	 */
	public MIKEYPolicyParam(int type, byte[] value) {
		if(log.isTraceEnabled())
			log.trace("constructing MIKEYPolicyParam with type=" + type + ", value=[...]");
		
		this.type = type;
		this.value = new byte[value.length];
		System.arraycopy(value, 0, this.value, 0, value.length);
	}
}