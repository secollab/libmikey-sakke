/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j;

import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.List;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;


/**
 * Provides some default implementation of the configuration needed for MIKEY.
 * 
 * @author Ingo Bauersachs
 */
public abstract class MIKEYConfigAdapter implements MIKEYConfig {
	/**
	 * Performs only a basic valid from/valid until check.
	 * @see ch.imvs.mikey4j.MIKEYConfig#verifyCertificate(java.util.List)
	 */
	@Override
	public boolean verifyCertificate(List<X509Certificate> chain) {
		X509Certificate cert = chain.get(0);
		Date now = new Date();
		return now.compareTo(cert.getNotBefore()) >= 0 && now.compareTo(cert.getNotAfter()) <= 0;
	}

	/**
	 * @see ch.imvs.mikey4j.MIKEYConfig#getCertCheckEnabled()
	 * @return always true
	 */
	@Override
	public boolean getCertCheckEnabled() {
		return true;
	}

	/**
	 * @see ch.imvs.mikey4j.MIKEYConfig#isMethodEnabled(ch.imvs.mikey4j.KeyAgreement.KeyAgreementType)
	 * @return always false
	 */
	@Override
	public boolean isMethodEnabled(KeyAgreementType method) {
		return false;
	}

	/**
	 * Gets the number of seconds the timestamps in a MIKEY session may differ.
	 * 
	 * @return Always 300 (5 Minutes)
	 */
	@Override
	public int getMaxTimestampOffsetSeconds() {
		return 60 * 5; //5 minutes
	}
}
