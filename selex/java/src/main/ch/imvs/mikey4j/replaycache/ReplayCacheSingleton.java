/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.replaycache;

/**
 * A singleton wrapper for the ReplayCache.
 */
public class ReplayCacheSingleton {

	/** The singleton instance of the replay cache. */
	private static ReplayCache INSTANCE;

	/** Private Constructor. */
	private ReplayCacheSingleton() {
	}

	/**
	 * Returns the wrapped singleton instance of the ReplayCache. The given
	 * parameters are considered only at the first call to create the instance.
	 * Following calls will just return the instance without using the
	 * parameters.
	 * 
	 * @return the singleton instance of the ReplayCache
	 */
	public static ReplayCache getInstance(int timeToLive, int cleanupInterval){
		if (INSTANCE == null){
			INSTANCE = new ReplayCache(timeToLive, cleanupInterval);
		}
		return INSTANCE;
	}
}
