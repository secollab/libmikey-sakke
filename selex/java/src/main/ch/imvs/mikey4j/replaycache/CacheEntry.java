package ch.imvs.mikey4j.replaycache;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Helper class representing an entry within the replay cache.
 * <p>
 * A cache entry consists of a hash value and a time stamp indicating how long
 * the entry is valid.
 * <p>
 * equals and hashCode methods depend only on the hash value (the time stamp is
 * not used).
 */
class CacheEntry implements Serializable {

	private static final long serialVersionUID = 6839035331654777819L;
	
	/** Time in milliseconds indicating how long this entry is valid. */
	private long expireDate;
	/** The hash value. */
	private byte[] hash;

	/**
	 * Construct a new entry for the replay cache.
	 * 
	 * @param hash
	 *            the hash value
	 * @param expirationTime
	 *            the time in milliseconds when this entry expires
	 */
	CacheEntry(byte[] hash, long expirationTime) {
		this.hash = hash;
		this.expireDate = expirationTime;
	}

	/**
	 * Construct a new entry for the replay cache. Use this constructor only for
	 * contains checks, as it sets no expiration time.
	 * 
	 * @param hash
	 *            the hash value
	 */
	CacheEntry(byte[] hash) {
		this.hash = hash;
	}

	/**
	 * @return the expiration time of this cache entry in milliseconds
	 */
	long getExpireDate() {
		return expireDate;
	}

	/**
	 * @return the hash byte[] of this cache entry
	 */
	byte[] getHash() {
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null)
			return false;

		if (!(obj instanceof CacheEntry))
			return false;

		CacheEntry ce = (CacheEntry) obj;
		return Arrays.equals(hash, ce.hash);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(hash);
	}
}