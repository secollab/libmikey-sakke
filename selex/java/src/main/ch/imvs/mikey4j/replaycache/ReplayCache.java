/*
 * MIKEY4J
 * Java implementation of MIKEY (Multimedia Internet KEYing, RFC 3830).
 * 
 * Copyright (C) 2010 the MIKEY4J Team at FHNW
 *   University of Applied Sciences Northwestern Switzerland (FHNW)
 *   School of Engineering
 *   Institute of Mobile and Distributed Systems (IMVS)
 *   http://mikey4j.imvs.ch
 * Copyright (C) 2004-2007 the Minisip Team
 *   Royal Institute of Technology (KTH, Stockholm, Sweden)
 *   http://www.minisip.org
 * 
 * Distributable under LGPL license, see terms of license at gnu.org.
 */
package ch.imvs.mikey4j.replaycache;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.bouncycastle.crypto.digests.SHA1Digest;

import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;

/**
 * A cache for MIKEY messages to prevent replay attacks. Hash values of incoming
 * messages are stored and compared to achieve this.
 * 
 * This cache has no maximal capacity (We don't need that yet). Expired entries
 * are dropped within a clean up process which runs periodically.
 * 
 * For higher safety the cache can persist its entries to a file. This makes it
 * immune against restarts or new instance of the mikey4j library. Just provide a 
 * system property with the key defined in PERSISTENT_CACHE_FILE_KEY constant.
 * 
 * TODO: the file based persistence approach seems not suitable/comfortable for the requirements.
 * Maybe a small, embedded key-value-store or a database would fit better. 
 * Are there better alternatives?
 * 
 * @author Maxim Moschko
 */
public class ReplayCache {
	private static final Log log = LogFactory.getLog(ReplayCache.class);

	private static final int CACHE_SIZE_WARN_LIMIT = 100;

	/**	Property key which defines the file name of the cache file. */
	static final String PERSISTENT_CACHE_FILE_KEY = "mikey4j.replaycache.persistentcachefile";

	private final Set<CacheEntry> cache = new LinkedHashSet<CacheEntry>();

	/** hash function used to hash the messages */
	private final SHA1Digest sha1 = new SHA1Digest();

	/** timer to start periodically the clean up process */
	private final static Timer cleanupTimer = new Timer("MIKEY ReplayCache cleanup timer", true);

	/** mutex for cache modifications */
	private final Object mutex = new Object();

	/** minimum duration in milliseconds an entry must be cached */
	private final int timeToLive;

	/** whether the cache should be persisted into a file or not */
	private boolean persistenceEnabled = false;

	/** file name of the cache file or null if no persistence is wanted */
	private String cacheFile;

	
	
	/**
	 * Constructs an empty replay cache with the specified parameters.
	 * 
	 * @param timeToLive
	 *            the minimum duration in seconds a entry must be cached; must
	 *            be greater than 0
	 * @param cleanupInterval
	 *            the interval in seconds between triggered clean ups; must be
	 *            greater than 0
	 */
	ReplayCache(int timeToLive, int cleanupInterval) {
		if (timeToLive <= 0)
			throw new IllegalArgumentException("timeToLive must be greater than 0");
		if (cleanupInterval <= 0)
			throw new IllegalArgumentException("cleanupIntervall must be greater than 0");

		initAndLoadPersistentCache();

		// init cleanup timer
		this.timeToLive = timeToLive * 1000;
		long cleanupIntervalMS = cleanupInterval * 1000; // in milliseconds

		cleanupTimer.schedule(new TimerTask() {
			@Override
			public void run() {
				dropExpiredEntries();
			}
		}, cleanupIntervalMS, cleanupIntervalMS);
	}

	/**
	 * Adds a message to this replay cache.
	 * 
	 * @param msg
	 *            the message to add
	 * @throws NullPointerException
	 *             if msg argument is null
	 */
	public void addToCache(MIKEYMessage msg) {
		if (msg == null)
			throw new NullPointerException("Message can not be null!");
		
		byte[] hash = computeHash(msg.base64_encode());
		synchronized (mutex) {
			cache.add(new CacheEntry(hash, System.currentTimeMillis() + timeToLive));
			persistCache();
		}

		if (cache.size() > CACHE_SIZE_WARN_LIMIT) {
			log.warn("Cache increases alarmingly. Determine reason or increase warning limit!");
		}
	}

	/**
	 * Check whether this replay cache contains the given message already.
	 * 
	 * @param msg
	 *            the message to search for
	 * @return true if message was already inserted in the replay cache,
	 *         otherwise false
	 * @throws NullPointerException
	 *             if msg argument is null
	 */
	public boolean isReplayed(MIKEYMessage msg) {
		if (msg == null)
			throw new NullPointerException("Message can not be null!");
		
		byte[] hash = computeHash(msg.base64_encode());
		return cache.contains(new CacheEntry(hash));
	}

	/**
	 * Compute a hash for the given string.
	 * 
	 * @param msg
	 *            the string that should be hashed
	 * @return a byte[] containing the hash
	 */
	private byte[] computeHash(String msg) {
		byte[] msgBytes = msg.getBytes();
		sha1.update(msgBytes, 0, msgBytes.length);

		byte[] hash = new byte[sha1.getDigestSize()];
		sha1.doFinal(hash, 0);

		return hash;
	}

	/**
	 * Drops all expired entries in the replay cache.
	 */
	private void dropExpiredEntries() {
		synchronized (mutex) {
			if (cache.isEmpty())
				return;

			long now = System.currentTimeMillis();
			CacheEntry entry;
			for (Iterator<CacheEntry> iter = cache.iterator(); iter.hasNext();) {
				entry = iter.next();
				if (entry.getExpireDate() < now) { // entry expired
					iter.remove();
				}
				else {
					// end cleanup because all remaining entries are still valid
					return;
				}
			}
			persistCache();
		}
	}

	/**
	 * Serialize current cache state in cache file if persistence feature is
	 * enabled.
	 */
	private void persistCache() {
		if(persistenceEnabled) {
			try {
				ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(cacheFile));
				oos.writeObject(cache);
				oos.flush();
				oos.close();
			}
			catch (IOException e) {
				log.error("Could not persist replay cache to file.", e);
			}
		}
	}

	/**
	 * Checks if a cache file was provided and reads in all persisted entries in
	 * the replay cache.
	 * 
	 * Expired entries will be dropped at startup.
	 */
	private void initAndLoadPersistentCache() {
		// check if a cache file was supplied
		cacheFile = System.getProperty(PERSISTENT_CACHE_FILE_KEY);
		if (cacheFile != null) {
			// try to read the persisted cache entries
			try {
				ObjectInputStream ois = new ObjectInputStream(new FileInputStream(cacheFile));
				Object o = ois.readObject();
				ois.close();

				if (o != null) {
					@SuppressWarnings("unchecked")
					Set<CacheEntry> persistedEntries = (Set<CacheEntry>) o;
					cache.addAll(persistedEntries);
					dropExpiredEntries();
				}
			} catch (FileNotFoundException e) {
				log.error("Problems occured during accessing the provided cache file.", e);
			} catch (IOException e) {
				log.error("Problems occured during reading the provided cache file.", e);
			} catch (ClassNotFoundException e) {
				log.error("It seems that the cache file does not contain the serialized collection.", e);
			}
			
			persistenceEnabled = true;
			
		}
		else {
			log.warn("No cache file provided. This could ease replay attacks.");
		}
	}
}
