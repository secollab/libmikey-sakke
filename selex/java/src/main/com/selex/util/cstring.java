package com.selex.util;

import com.selex.log.DefaultLogger;

public final class cstring
{
   public static int strcspn(byte[] s1, int s1_begin, int s1_end,
                             byte[] s2, int s2_begin, int s2_end)
   {
      for (; s1_begin < s1_end; ++s1_begin)
      {
         byte b = s1[s1_begin];
         for (int i = s2_begin; i < s2_end; ++i)
            if (b == s2[i])
               return s1_begin;
      }
      return s1_end;
   }
   public static int strspn(byte[] s1, int s1_begin, int s1_end,
                            byte[] s2, int s2_begin, int s2_end)
   {
      for (; s1_begin < s1_end; ++s1_begin)
      {
         byte b = s1[s1_begin];
         int i = s2_begin;
         for (; i < s2_end; ++i)
            if (b == s2[i])
               break;
         if (i == s2_end)
            return s1_begin;
      }
      return s1_end;
   }
   public static int strcspn(char[] s1, int s1_begin, int s1_end,
                             char[] s2, int s2_begin, int s2_end)
   {
      for (; s1_begin < s1_end; ++s1_begin)
      {
         char b = s1[s1_begin];
         for (int i = s2_begin; i < s2_end; ++i)
            if (b == s2[i])
               return s1_begin;
      }
      return s1_end;
   }
   public static int strspn(char[] s1, int s1_begin, int s1_end,
                            char[] s2, int s2_begin, int s2_end)
   {
      for (; s1_begin < s1_end; ++s1_begin)
      {
         char c = s1[s1_begin];
         int i = s2_begin;
         for (; i < s2_end; ++i)
            if (c == s2[i])
               break;
         if (i == s2_end)
            return s1_begin;
      }
      return s1_end;
   }
   public static int strcspn(byte[] s1, int s1_begin, int s1_end, byte[] s2) { return strcspn(s1, s1_begin, s1_end, s2, 0, s2.length); }
   public static int strcspn(byte[] s1, byte[] s2)                           { return strcspn(s1, 0, s1.length, s2, 0, s2.length); }
   public static int strspn(byte[] s1, int s1_begin, int s1_end, byte[] s2)  { return strspn(s1, s1_begin, s1_end, s2, 0, s2.length); }
   public static int strspn(byte[] s1, byte[] s2)                            { return strspn(s1, 0, s1.length, s2, 0, s2.length); }
   public static int strcspn(char[] s1, int s1_begin, int s1_end, char[] s2) { return strcspn(s1, s1_begin, s1_end, s2, 0, s2.length); }
   public static int strcspn(char[] s1, char[] s2)                           { return strcspn(s1, 0, s1.length, s2, 0, s2.length); }
   public static int strspn(char[] s1, int s1_begin, int s1_end, char[] s2)  { return strspn(s1, s1_begin, s1_end, s2, 0, s2.length); }
   public static int strspn(char[] s1, char[] s2)                            { return strspn(s1, 0, s1.length, s2, 0, s2.length); }

   public static void main(String[] args)
   {
      byte[] A1 = new byte[] {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,13,12,11,10,9,8,7,6,5,4,3,2,1,0};
      byte[] A2 = new byte[] {9,8,7,6};

      int a2_in_a1_start = strcspn(A1, A2);
      int a2_in_a1_end = strspn(A1, a2_in_a1_start, A1.length, A2);

      DefaultLogger.out.println("Span of A2 found at ["+a2_in_a1_start+","+a2_in_a1_end+")");

      a2_in_a1_start = strcspn(A1, a2_in_a1_end, A1.length, A2);
      a2_in_a1_end = strspn(A1, a2_in_a1_start, A1.length, A2);

      DefaultLogger.out.println("         ... and at ["+a2_in_a1_start+","+a2_in_a1_end+")");

      a2_in_a1_start = strcspn(A1, a2_in_a1_end, A1.length, A2);
      a2_in_a1_end = strspn(A1, a2_in_a1_start, A1.length, A2);

      DefaultLogger.out.println("Reached end should be true => " + ((a2_in_a1_start == a2_in_a1_end) && (a2_in_a1_start == A1.length)));

      char[] B1 = "Some 2342356 numbers in a string 98989564 ending just after this 1".toCharArray();
      char[] B2 = "0123456789".toCharArray();

      int b2_in_b1_start = strcspn(B1, B2);
      int b2_in_b1_end = strspn(B1, b2_in_b1_start, B1.length, B2);

      DefaultLogger.out.println("Span of B2 found at ["+b2_in_b1_start+","+b2_in_b1_end+")");

      b2_in_b1_start = strcspn(B1, b2_in_b1_end, B1.length, B2);
      b2_in_b1_end = strspn(B1, b2_in_b1_start, B1.length, B2);

      DefaultLogger.out.println("         ... and at ["+b2_in_b1_start+","+b2_in_b1_end+")");

      b2_in_b1_start = strcspn(B1, b2_in_b1_end, B1.length, B2);
      b2_in_b1_end = strspn(B1, b2_in_b1_start, B1.length, B2);

      DefaultLogger.out.println("         ... and at ["+b2_in_b1_start+","+b2_in_b1_end+")");

      b2_in_b1_start = strcspn(B1, b2_in_b1_end, B1.length, B2);
      b2_in_b1_end = strspn(B1, b2_in_b1_start, B1.length, B2);

      DefaultLogger.out.println("Reached end should be true => " + ((b2_in_b1_start == b2_in_b1_end) && (b2_in_b1_start == B1.length)));
   }
}

