package com.selex.util;

public class TimeMeasure
{
   private long t;

   public TimeMeasure()
   {
      reset();
   }

   public void reset()
   {
      t = System.currentTimeMillis();
   }

   public long getDeltaTimeMillis()
   {
      long ot = t;
      t = System.currentTimeMillis();
      return t - ot;
   }

   public long getCumulativeTimeMillis()
   {
      long now = System.currentTimeMillis();
      return now - t;
   }
}
