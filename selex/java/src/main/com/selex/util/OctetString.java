package com.selex.util;

// !J2ME: import java.util.Arrays;
import javolution.text.CharSet;
import javolution.text.Text;

import com.selex.log.DefaultLogger;

public final class OctetString implements Comparable<OctetString>
{
   public enum ReservationPolicy
   {
      DoubleAsNecessary,
      ForceToSpecified,
      ResizeKeepRight,
   }
   public static final ReservationPolicy DoubleAsNecessary = ReservationPolicy.DoubleAsNecessary;
   public static final ReservationPolicy ForceToSpecified = ReservationPolicy.ForceToSpecified;
   public static final ReservationPolicy ResizeKeepRight = ReservationPolicy.ResizeKeepRight;

   public enum Translation
   {
      Untranslated,
      AsciiBase16,
      //AsciiBase64,
   }
   public static final Translation Untranslated = Translation.Untranslated;
   public static final Translation AsciiBase16  = Translation.AsciiBase16;
   //public Translation AsciiBase64 = Translation.AsciiBase64;

   public OctetString() {}
   public OctetString(OctetString other) { assign(other); }
   public OctetString(String ascii_hex) { assign(ascii_hex, AsciiBase16); }
   public OctetString(String s, Translation t) { assign(s, t); }
   public OctetString(byte[] octets) { assign(octets.length, octets); }
   public OctetString(ByteRange range) { assign(range); }
   public OctetString(int n) { reserve(n, ForceToSpecified); end = n; }
   public OctetString(int n, ReservationPolicy policy) { reserve(n, policy); end = n; }

   public String translate(Translation t)
   {
      if (t == AsciiBase16)
         return toHexString();

      if (t == Untranslated)
         return untranslated();

      return "";
   }
   public String translate() { return translate(AsciiBase16); }
   public String toHexString()
   {
      return bytes_to_ascii_hex(octets, 0, end);
   }
   public static final String bytes_to_ascii_hex(final byte[] octets)
   {
      return bytes_to_ascii_hex(octets, 0, octets.length);
   }
   public static final String bytes_to_ascii_hex(final byte[] octets, int offset, final int length)
   {
      if (length == 0)
         return "";
      int end = offset + length;
      assert(end <= octets.length);
      final StringBuilder hex = new StringBuilder(length << 1);
      int b = octets[offset];
      for (;;)
      {
         hex.append( hexChar((b&0xF0) >> 4) );
         hex.append( hexChar(b&0x0F) );
         if (++offset == end)
            return hex.toString();
         b = octets[offset];
      }
   }
   private static final char hexChar(final int nibble)
   {
      if (nibble > 9)
         return (char) (nibble - 10 + 'a');
      return (char) (nibble + '0');
   }
   public String untranslated()
   {
      try {
         return new String(octets, 0, end, "ISO8859-1");
      } catch (java.io.UnsupportedEncodingException e) {
         throw new RuntimeException("ISO8859-1 unsupported for raw string decode.");
      }
   }
   public String toString()
   {
      return untranslated();
   }

   private static final char throw_not_ascii_hex()
   {
      throw new RuntimeException("Character not in [0-9A-Fa-f] when attempting translation.");
   }
   private static byte from_ascii_hex(char c)
   {
      return (byte)
           ( c >= '0' && c <= '9' ? (c-'0')
           : c >= 'a' && c <= 'f' ? (10+c-'a')
           : c >= 'A' && c <= 'F' ? (10+c-'A')
           : throw_not_ascii_hex() );
   }
   public static int ascii_hex_to_bytes(String s, byte[] result, int offset)
   {
      int in = 0, inend = s.length();
      assert (result.length >= ((inend+1) >> 1));
      if ((inend&1) == 1)
         result[offset++] = from_ascii_hex(s.charAt(in++));
      while (in < inend)
      {
         byte o = (byte) (from_ascii_hex(s.charAt(in++)) << 4);
              o = (byte) (from_ascii_hex(s.charAt(in++)) | o);
         result[offset++] = o;
      }
      return offset;
   }
   public static byte[] ascii_hex_to_bytes(String s)
   {
      byte[] result = new byte[(s.length()+1) >> 1];
      ascii_hex_to_bytes(s, result, 0);
      return result;
   }
   public void deposit_bigendian(byte[] out, int len)
   {
      if (len < end)
         throw new RuntimeException("OctetString too large to deposit into given region");

      if (end < len)
      {
         len -= end; // len ==> prefix-len
         // !J2ME: Arrays.fill(out, 0, len, (byte) 0);
         for (int i = 0; i < len; ++i) out[i] = 0;
         System.arraycopy(octets, 0, out, len, end);
      }
      else
         System.arraycopy(octets, 0, out, 0, len);
   }

   public OctetString clear()
   {
      end = 0;
      return this;
   }

   public boolean empty()
   {
      return end == 0;
   }

   public OctetString assign(String s, Translation t)
   {
      end = 0;
      return concat(s, t);
   }
   public OctetString assign(String s) { return assign(s, AsciiBase16); }
   public OctetString assign(OctetString other) { return assign(other.end, other.octets); }

   public OctetString assign(final int n, final byte[] bytes, final int off)
   {
      reserve(n);
      System.arraycopy(bytes, off, octets, 0, n);
      end = n;
      return this;
   }
   public OctetString assign(final int n, final byte[] bytes)  { return assign(n, bytes, 0); }
   public OctetString assign(final byte[] bytes)               { return assign(bytes.length, bytes, 0); }
   public OctetString assign(final ByteRange r)                { return assign(r.len, r.bytes, r.off); }

   final static CharSet ws = CharSet.valueOf(new char[] {'\t', '\n', '\r', ' '});

   public final static String skipws(final String s)
   {
      Text t = new Text(s);
      return t.replace(ws, Text.EMPTY).toString();
   }

   public OctetString concat(OctetString other)
   {
      return concat(other.size(), other.octets);
   }
   public static byte[] toByteArray(final String s)
   {
      try { return s.getBytes("ISO8859-1"); }

      catch (java.io.UnsupportedEncodingException e)
      { throw new RuntimeException("ISO8859-1 unsupported for raw string decode."); }
   }
   public OctetString concat(String s, Translation t)
   {
      if (t == Untranslated)
      {
         byte[] bytes = toByteArray(s);
         return concat(bytes.length, bytes);
      }
      if (t == AsciiBase16)
      {
         int in = 0, inend = s.length();
         reserve(end + ((inend+1) >> 1));
         if ((inend&1) == 1)
            octets[end++] = from_ascii_hex(s.charAt(in++));
         while (in < inend)
         {
            byte o = (byte) (from_ascii_hex(s.charAt(in++)) << 4);
                 o = (byte) (from_ascii_hex(s.charAt(in++)) | o);
            octets[end++] = o;
         }
      }
      return this;
   }
   public OctetString concat(String s) { return concat(s, AsciiBase16); }
   public OctetString concat(int other_octet_count, byte[] other_octets, int other_offset)
   {
      reserve(end + other_octet_count);
      System.arraycopy(other_octets, other_offset, this.octets, end, other_octet_count);
      end += other_octet_count;
      return this;
   }
   public OctetString concat(int other_octet_count, byte[] other_octets)
   {
      return concat(other_octet_count, other_octets, 0);
   }
   public OctetString concat(byte[] other_octets)
   {
      return concat(other_octets.length, other_octets, 0);
   }
   public OctetString concat(final ByteRange r) { return concat(r.len, r.bytes, r.off); }

   public OctetString concat(int count, byte octet)
   {
      reserve(end + count);
      while (count-- > 0)
         octets[end++] = octet;
      return this;
   }

   public OctetString concat(byte octet)
   {
      reserve(end + 1);
      octets[end++] = octet;
      return this;
   }

   public int compareTo(OctetString other)
   {
      if (this == other)
         return 0;
      return compareTo(other.octets, 0, other.end);
   }
   public int compareTo(ByteRange r)
   {
      return compareTo(r.bytes, r.off, r.len);
   }
   public int compareTo(byte[] other, int off, int len)
   {
      int dl = end - len;
      if (dl != 0)
         return dl;
      for (int i = 0; i != end; ++i, ++off)
      {
         int dx = octets[i] - other[off];
         if (dx != 0)
            return dx;
      }
      return 0;
   }
   public boolean equals(OctetString other) { return compareTo(other) == 0; }

   public byte[] raw() { return octets; }
   public int size() { return end; }
   public int capacity() { return octets.length; }

   // Only call this if there is no interface that can accept raw()
   // and size().  This will likely incur reallocation cost for the
   // clamp, followed by reallocation for any concatenations made
   // subsequently.
   public byte[] clampedArray()
   {
      if (octets.length == end)
         return octets;
      final byte[] newBuffer = new byte[end];
      System.arraycopy(octets, 0, newBuffer, 0, end);
      octets = newBuffer;
      return octets;
   }

   // byte vector implementation
   //
   private byte[] octets = new byte[32];
   private int end = 0;
   public void swap(OctetString other)
   {
      byte[] new_octets = other.octets;
      int new_end = other.end;
      other.octets = this.octets;
      other.end = this.end;
      this.octets = new_octets;
      this.end = new_end;
   }
   public OctetString reserve(final int size)
   {
      if (size <= octets.length)
         return this;
      int newSize = octets.length * 2;
      while (newSize < size)
         newSize *= 2;
      final byte[] newBuffer = new byte[newSize];
      System.arraycopy(octets, 0, newBuffer, 0, octets.length);
      octets = newBuffer;
      return this;
   }
   public OctetString reserve(final int size, final ReservationPolicy policy)
   {
      if (policy == DoubleAsNecessary)
         return reserve(size);
      if (size == octets.length)
         return this;
      // remaining options are force reserve
      final byte[] newBuffer = new byte[size];
      if (end == 0)
      {
         octets = newBuffer;
         return this;
      }
      if (policy == ForceToSpecified)
      {
         if (size < end)
         {
            System.arraycopy(octets, 0, newBuffer, 0, size);
            end = size;
         }
         else
            System.arraycopy(octets, 0, newBuffer, 0, end);
      }
      else if (policy == ResizeKeepRight)
      {
         if (size < end)
            System.arraycopy(octets, end - size, newBuffer, 0, size);
         else
            System.arraycopy(octets, 0, newBuffer, size - end, end);
         end = size;
      }
      octets = newBuffer;
      return this;
   }

   // test
   //
   public static void main(String[] args)
   {
      OctetString o = new OctetString();
      o.reserve(42);
      o.concat("deadbeef");
      o.concat("cafebabe");
      DefaultLogger.out.println(o);
      DefaultLogger.out.println(o.toString());
      DefaultLogger.out.println(o.toHexString());
      DefaultLogger.out.println(o.size());
      DefaultLogger.out.println(o.capacity());

      OctetString s = new OctetString("deadbeefcafebabe");
      DefaultLogger.out.println(s);
      DefaultLogger.out.println(s.toString());
      DefaultLogger.out.println(s.toHexString());
      DefaultLogger.out.println(s.size());
      DefaultLogger.out.println(s.capacity());

      DefaultLogger.out.println(o.compareTo(s));
      DefaultLogger.out.println(s.compareTo(o));

      o.concat((byte)1);

      DefaultLogger.out.println(o);
      DefaultLogger.out.println(o.toString());
      DefaultLogger.out.println(o.toHexString());
      DefaultLogger.out.println(o.size());
      DefaultLogger.out.println(o.capacity());

      DefaultLogger.out.println(o.compareTo(s));
      DefaultLogger.out.println(s.compareTo(o));

      s.concat((byte)2);

      DefaultLogger.out.println(s);
      DefaultLogger.out.println(s.toString());
      DefaultLogger.out.println(s.toHexString());
      DefaultLogger.out.println(s.size());
      DefaultLogger.out.println(s.capacity());

      DefaultLogger.out.println(o.compareTo(s));
      DefaultLogger.out.println(s.compareTo(o));

      s.raw()[s.size()-1] = (byte)1;

      DefaultLogger.out.println(o.compareTo(s));
      DefaultLogger.out.println(s.compareTo(o));

      s.raw()[0] = (byte) 0x04;

      DefaultLogger.out.println(s);
      DefaultLogger.out.println(s.toString());
      DefaultLogger.out.println(s.toHexString());
      DefaultLogger.out.println(s.size());
      DefaultLogger.out.println(s.capacity());

      DefaultLogger.out.println(o.compareTo(s));
      DefaultLogger.out.println(s.compareTo(o));

      // same
      DefaultLogger.out.println(new OctetString( "4adbeefcafebabe01").compareTo(s));
      DefaultLogger.out.println(new OctetString("04adbeefcafebabe01").compareTo(s));

      // numerically equivalent but different as far as octet-string
      // is concerned, leading 3 zero bytes
      DefaultLogger.out.println(new OctetString( "0000004adbeefcafebabe01").compareTo(s));
      DefaultLogger.out.println(new OctetString("00000004adbeefcafebabe01").compareTo(s));

      byte[] t = { (byte)0x04, (byte)0xad, (byte)0xbe, (byte)0xef,
                   (byte)0xca, (byte)0xfe, (byte)0xba, (byte)0xbe,
                   (byte)0x01 };

      DefaultLogger.out.println(new OctetString(t).compareTo(s));
      DefaultLogger.out.println(new OctetString().concat(4,t).concat(5,t,4).compareTo(s));

      DefaultLogger.out.println(new OctetString(s.toHexString()).compareTo(s));
      DefaultLogger.out.println(new OctetString(s.toString(), OctetString.Untranslated).compareTo(s));
      DefaultLogger.out.println(new OctetString(s.toHexString(), OctetString.AsciiBase16).compareTo(s));

      String no_ws = s.toHexString();
      String with_ws = " 04 ad\tbeef\ncafe\r\n ba\t be0 1";
      
      DefaultLogger.out.println("no_ws           = " + no_ws);
      DefaultLogger.out.println("with_ws         = " + with_ws + "\n");
      DefaultLogger.out.println("skipws(with_ws) = " + OctetString.skipws(with_ws) + "\n");

      DefaultLogger.out.println(OctetString.skipws(no_ws) == no_ws); // no copy
      DefaultLogger.out.println(OctetString.skipws(with_ws).compareTo(no_ws) == 0);
      DefaultLogger.out.println(new OctetString(OctetString.skipws(with_ws)).compareTo(s) == 0);
   }
}

