package com.selex.util;

public abstract class RandomGenerator
{
   public abstract void randomize(byte[] octets, int offset, int N);

   public void randomize(byte[] octets) { randomize(octets, 0, octets.length); }

   public abstract int randomInt();
}

