package com.selex.util;

public final class ByteRange
{
   public final byte[] bytes;
   public final int off;
   public final int len;

   public ByteRange(final byte[] bytes)                               { this.bytes = bytes; this.off = 0; this.len = bytes.length; }
   public ByteRange(final byte[] bytes, final int off)                { this.bytes = bytes; this.off = off; this.len = bytes.length - off; }
   public ByteRange(final byte[] bytes, final int off, final int len) { this.bytes = bytes; this.off = off; this.len = len; }
}
