/**
 * Implementation of ModInt using Sun's BigInteger class.
 */
package com.selex.math;

import java.math.BigInteger;

public final class SunModInt
{
   private BigInteger x;

   // constructors
   public SunModInt(byte[] octets)   { x = new BigInteger(octets); }
   public SunModInt(int z)           { x = BigInteger.valueOf(z); }
   public SunModInt(String asciiHex) { x = new BigInteger(asciiHex, 16); }
   public SunModInt(SunModInt other) { x = other.x; /* ref okay since BigInteger is immutable */ }

   // tests
   public int compareTo(int z)           { return (int) x.longValue() - z; }
   public int compareTo(SunModInt other) { return x.compareTo(other.x); }
   public boolean equals(Object obj)     { if (this == obj) return true; if (obj instanceof SunModInt) return x.equals(((SunModInt) obj).x); return false; }
   public boolean isOdd()                { return x.testBit(x.bitLength()-1); }

   // conversion
   public int hashCode()       { return x.hashCode(); }
   public String toString()    { return x.toString(16); }
   public byte[] toByteArray() { return x.toByteArray(); }

   // create new instance as result of operation
   public SunModInt add(SunModInt a, SunModInt mod)             { return new SunModInt(x.add(a.x).mod(mod.x)); }
   public SunModInt exponent(SunModInt exponent, SunModInt mod) { return new SunModInt(x.modPow(exponent.x, mod.x)); }
   public SunModInt gcd(SunModInt a)                            { return new SunModInt(x.gcd(a.x)); }
   public SunModInt invert(SunModInt mod)                       { return new SunModInt(x.modInverse(mod.x)); }
   public SunModInt mod(SunModInt mod)                          { return new SunModInt(x.mod(mod.x)); }
   public SunModInt multiply(SunModInt a, SunModInt mod)        { return new SunModInt(x.multiply(a.x).mod(mod.x)); }
   public SunModInt square(SunModInt mod)                       { return new SunModInt(x.modPow(TWO,mod.x)); }
   public SunModInt subtract(SunModInt a, SunModInt mod)        { return new SunModInt(x.subtract(a.x).mod(mod.x)); }

   // mutate this instance
   public void decrement(int z, SunModInt mod)       { x = x.subtract(BigInteger.valueOf(z)).mod(mod.x); }
   public void decrement(SunModInt a, SunModInt mod) { x = x.subtract(a.x).mod(mod.x); }
   public void divideByTwo(SunModInt mod)            { x = x.divide(TWO).mod(mod.x); }
   public void increment(int z, SunModInt mod)       { x = x.add(BigInteger.valueOf(z)).mod(mod.x); }
   public void increment(SunModInt a, SunModInt mod) { x = x.add(a.x).mod(mod.x); }
   public void multiplyByTwo(SunModInt mod)          { x = x.multiply(TWO).mod(mod.x); }

   // additional implementation-specifics
   private SunModInt(BigInteger x) { this.x = x; }
   private static final BigInteger TWO = BigInteger.valueOf(2);
}

