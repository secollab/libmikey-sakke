package com.selex.math;

import com.selex.util.RandomGenerator;

public final class InplaceModInt extends ModIntValue
{
   // constructors
   public InplaceModInt(byte[] value, ConstantBigInt mod)          { throw new RuntimeException(); }
   public InplaceModInt(int z, ConstantBigInt mod)                 { throw new RuntimeException(); }
   public InplaceModInt(String asciiHex, ConstantBigInt mod)       { throw new RuntimeException(); }
   public InplaceModInt(ConstantBigInt value, ConstantBigInt mod)  { throw new RuntimeException(); }
   public InplaceModInt(ModIntValue other)                         { throw new RuntimeException(); }
   public InplaceModInt(ConstantBigInt mod)                        { throw new RuntimeException(); }

   // assignment
   public InplaceModInt assign(byte[] value)                       { throw new RuntimeException(); }
   public InplaceModInt assign(byte[] value, int off, int len)     { throw new RuntimeException(); }
   public InplaceModInt assign(int z)                              { throw new RuntimeException(); }
   public InplaceModInt assign(String asciiHex)                    { throw new RuntimeException(); }
   public InplaceModInt assign(ConstantBigInt value)               { throw new RuntimeException(); }
   public InplaceModInt assign(ModIntValue other)                  { throw new RuntimeException(); }
   public InplaceModInt makeNaN()                                  { throw new RuntimeException(); }
   public InplaceModInt randomize(RandomGenerator rand)            { throw new RuntimeException(); }

   // swap -- exchange the internal representation with another instance
   // returns 'this' now representing the value of 'other' .
   public InplaceModInt swap(InplaceModInt other)  { throw new RuntimeException(); }

   // move -- destructive; the value represented by this instance becomes NaN after a move operation
   public byte[] moveToByteArray()             { throw new RuntimeException(); }
   public ConstantBigInt moveToConstant()      { throw new RuntimeException(); }

   // all operations below return 'this'

   // additive modifiers
   public InplaceModInt inc()                      { throw new RuntimeException(); }
   public InplaceModInt dec()                      { throw new RuntimeException(); }
   public InplaceModInt add(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt sub(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt add(ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt sub(ConstantBigInt other)  { throw new RuntimeException(); }

   // bitwise modifiers
   public InplaceModInt xor(ModIntValue other)     { throw new RuntimeException(); }

   // multiplicative modifiers
   public InplaceModInt mul2()                     { throw new RuntimeException(); }
   public InplaceModInt mul(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt mod(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt mul(ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt mod(ConstantBigInt other)  { throw new RuntimeException(); }

   // exponentiation
   public InplaceModInt sqr()                      { throw new RuntimeException(); }
   public InplaceModInt exp(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt exp(ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt inv()                      { throw new RuntimeException(); }
   public InplaceModInt gcd(ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt gcd(ConstantBigInt other)  { throw new RuntimeException(); }
}

