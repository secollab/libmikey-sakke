package com.selex.math;

import com.selex.util.ByteRange;

import java.math.BigInteger;

public class SunModIntValue
{
   BigInteger i;
   SunConstantBigInt p;

   // tests
   public int compareTo(int z)                    { return (int) i.longValue() - z; }
   public int compareTo(SunModIntValue other)     { return i.compareTo(other.i); }
   public int compareTo(SunConstantBigInt other)  { return i.compareTo(other.value); }
   public boolean equals(SunModIntValue other)    { if (this == other) return true; return i.equals(other.i); }
   public boolean equals(SunConstantBigInt other) { return i.equals(other.value); }
   public boolean isZero()                        { return i.equals(SunConstantBigInt.ZERO.value); }
   public boolean isOne()                         { return i.equals(SunConstantBigInt.ONE.value); }
   public boolean isOdd()                         { return i.testBit(0); }
   public boolean testBit(int n)                  { return i.testBit(n); }
   public boolean isNaN()                         { return i == null; }

   // conversion
   public int hashCode()         { return i.hashCode(); }
   public String toString()      { return i.toString(16); }
   public byte[] toByteArray()   { return i.toByteArray(); }
   public int bits()             { return i.bitLength(); }
   public int bytes()            { return (i.bitLength()+7)>>3; }
   public ByteRange peekFieldByteRange()
   {
      final int fieldSize = p.bytesInField();
      byte[] flex = i.toByteArray();
      if (flex.length == fieldSize)
         return new ByteRange(flex);
      if (flex.length > fieldSize)
         return new ByteRange(flex, flex.length - fieldSize, fieldSize);
      byte[] padded = new byte[fieldSize];
      System.arraycopy(flex, 0, padded, fieldSize - flex.length, flex.length);
      return new ByteRange(padded);
   }

   // retrieval
   public SunConstantBigInt modulus()  { return p; }

   // addition into given target
   public SunInplaceModInt inc_into(SunInplaceModInt target)                           { target.i = i.add(SunConstantBigInt.ONE.value).mod(target.p.value); return target; }
   public SunInplaceModInt dec_into(SunInplaceModInt target)                           { target.i = i.subtract(SunConstantBigInt.ONE.value).mod(target.p.value); return target; }
   public SunInplaceModInt add_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.add(other.i).mod(target.p.value); return target; }
   public SunInplaceModInt sub_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.subtract(other.i).mod(target.p.value); return target; }
   public SunInplaceModInt add_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.add(other.value).mod(target.p.value); return target; }
   public SunInplaceModInt sub_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.subtract(other.value).mod(target.p.value); return target; }

   // multiplication into given target
   public SunInplaceModInt mul2_into(SunInplaceModInt target)                          { target.i = i.multiply(SunConstantBigInt.TWO.value).mod(target.p.value); return target; }
   public SunInplaceModInt mul_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.multiply(other.i).mod(target.p.value); return target; }
   public SunInplaceModInt mod_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.mod(other.i); return target; }
   public SunInplaceModInt mul_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.multiply(other.value).mod(target.p.value); return target; }
   public SunInplaceModInt mod_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.mod(other.value); return target; }

   // exponentiation into given target
   public SunInplaceModInt sqr_into(SunInplaceModInt target)                           { target.i = i.multiply(i).mod(target.p.value); return target; }
   public SunInplaceModInt exp_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.modPow(other.i, target.p.value); return target; }
   public SunInplaceModInt exp_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.modPow(other.value, target.p.value); return target; }
   public SunInplaceModInt inv_into(SunInplaceModInt target)                           { target.i = i.modInverse(target.p.value); return target; }
   public SunInplaceModInt gcd_into(SunInplaceModInt target, SunModIntValue other)     { target.i = i.gcd(other.i); return target; }
   public SunInplaceModInt gcd_into(SunInplaceModInt target, SunConstantBigInt other)  { target.i = i.gcd(other.value); return target; }

   // additive operations applied to constants storing the result in 'target'
   public static SunInplaceModInt inc_into(SunInplaceModInt target, SunConstantBigInt a)                       { target.i = a.value.add(SunConstantBigInt.ONE.value).mod(target.p.value); return target; }
   public static SunInplaceModInt dec_into(SunInplaceModInt target, SunConstantBigInt a)                       { target.i = a.value.subtract(SunConstantBigInt.ONE.value).mod(target.p.value); return target; }
   public static SunInplaceModInt add_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.add(b.value).mod(target.p.value); return target; }
   public static SunInplaceModInt add_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.add(b.i).mod(target.p.value); return target; }
   public static SunInplaceModInt sub_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.subtract(b.value).mod(target.p.value); return target; }
   public static SunInplaceModInt sub_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.subtract(b.i).mod(target.p.value); return target; }

   // multiplicative operations applied to constants storing the result in 'target'
   public static SunInplaceModInt mul2_into(SunInplaceModInt target, SunConstantBigInt a)                      { target.i = a.value.multiply(SunConstantBigInt.TWO.value).mod(target.p.value); return target; }
   public static SunInplaceModInt mul_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.multiply(b.value).mod(target.p.value); return target; }
   public static SunInplaceModInt mul_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.multiply(b.i).mod(target.p.value); return target; }
   public static SunInplaceModInt mod_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.mod(b.value).mod(target.p.value); return target; }
   public static SunInplaceModInt mod_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.mod(b.i).mod(target.p.value); return target; }

   // exponentiation operations applied to constants storing the result in 'target'
   public static SunInplaceModInt sqr_into(SunInplaceModInt target, SunConstantBigInt a)                       { target.i = a.value.multiply(a.value).mod(target.p.value); return target; }
   public static SunInplaceModInt exp_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.modPow(b.value, target.p.value); return target; }
   public static SunInplaceModInt exp_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.modPow(b.i, target.p.value); return target; }
   public static SunInplaceModInt inv_into(SunInplaceModInt target, SunConstantBigInt a)                       { target.i = a.value.modInverse(target.p.value); return target; }
   public static SunInplaceModInt gcd_into(SunInplaceModInt target, SunConstantBigInt a, SunConstantBigInt b)  { target.i = a.value.gcd(b.value); return target; }
   public static SunInplaceModInt gcd_into(SunInplaceModInt target, SunConstantBigInt a, SunModIntValue b)     { target.i = a.value.gcd(b.i); return target; }
}
