package com.selex.math;

import java.math.BigInteger;

public final class SunConstantBigInt extends InplaceModIntScratchPool
{
   final BigInteger value;
   SunModIntValue minusOne;

   public SunConstantBigInt(final byte[] value)      { this.value = new BigInteger(1, value); }
   public SunConstantBigInt(int z)                   { this.value = BigInteger.valueOf(z); }
   public SunConstantBigInt(int base, int pow)
   {
      if (base != 2)
         throw new RuntimeException("SunConstantBigInt(base, pow) only supports base 2");

      byte[] v = new byte[1 + (pow >> 3)];
      v[0] = (byte) (1 << (pow & 0x7));
      this.value = new BigInteger(1, v);
   }
   public SunConstantBigInt(String asciiHex)         { this.value = new BigInteger(asciiHex, 16); }
   public SunConstantBigInt(SunInplaceModInt value)  { this.value = value.i; }
   public byte[] toByteArray()                       { return value.toByteArray(); }
   public String toString()                          { return value.toString(16); }
   public int bits()                                 { return value.bitLength(); }

   public int bitsInField()                          { return maxInField().i.bitLength(); }
   public int bytesInField()                         { return (maxInField().i.bitLength()+7) >> 3; }
   public SunModIntValue maxInField()                { if (minusOne == null) minusOne = new SunInplaceModInt(this).dec(); return minusOne; }

   public static final SunConstantBigInt ZERO  = new SunConstantBigInt(0);
   public static final SunConstantBigInt ONE   = new SunConstantBigInt(1);
   public static final SunConstantBigInt TWO   = new SunConstantBigInt(2);
   public static final SunConstantBigInt THREE = new SunConstantBigInt(3);
   public static final SunConstantBigInt TEN   = new SunConstantBigInt(10);

   // implementation-specific interface
   SunConstantBigInt(BigInteger value)  { this.value = value; }
}
