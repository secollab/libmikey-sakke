package com.selex.math;

import javolution.util.FastList;
import javolution.util.FastSet;
import javolution.util.FastComparator;
import java.util.NoSuchElementException;

public class InplaceModIntScratchPool
{
   private FastList available;
   private FastSet inUse;

   // Fetch an integer modulo ((ConstantBigInt)this) to be used
   // for scratch operation from a pool managed by this instance.
   // Return to the pool via recycleScratch() to allow reuse or
   // remove from the pool via takeOwnership().
   //
   public final InplaceModInt getScratchInField()
   {
      if (available == null)
      {
         available = FastList.newInstance().setValueComparator(FastComparator.IDENTITY);
         inUse = FastSet.newInstance().setValueComparator(FastComparator.IDENTITY);
      }

      InplaceModInt i;
      try {
         i = (InplaceModInt) available.removeFirst();
      } catch (NoSuchElementException e) {
         i = new InplaceModInt((ConstantBigInt) this);
      }
      inUse.add(i);
      return i;
   }

   public final void recycleScratch(final ModIntValue i)
   {
      if (inUse == null)
         return;
      if (inUse.remove((InplaceModInt) i))
         available.add((InplaceModInt) i);
   }
   
   public final void recycleScratch(final ModIntValue... litter)
   {
      if (inUse == null)
         return;
      for (final ModIntValue i : litter)
         if (inUse.remove((InplaceModInt) i))
            available.add((InplaceModInt) i);
   }

   public final InplaceModInt takeOwnership(InplaceModInt i)
   {
      if (inUse != null)
         inUse.remove(i);
      return i;
   }

   public final int scratchInUse()
   {
      if (inUse == null)
         return 0;
      return inUse.size();
   }

   public final int scratchMaxInUse()
   {
      if (inUse == null)
         return 0;
      return inUse.size() + available.size();
   }
}

