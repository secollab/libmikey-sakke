package com.selex.math;

/**
 * Allows for expressing an immutable value when using InplaceModInt.
 * This is a 'concrete-interface.'  It is expected that only an
 * implementation-specific version InplaceModInt will depend on the
 * details of the corresponding implementation-specific version of
 * ConstantBigInt class.  The methods defined here are the public
 * interface supported for external clients and all throw
 * unconditionally.  Byte code translation should be used to select an
 * appropriate implementation at build time for the appropriate
 * target.
 */
public final class ConstantBigInt extends InplaceModIntScratchPool
{
   public ConstantBigInt(byte[] value)         { throw new RuntimeException(); }
   public ConstantBigInt(int z)                { throw new RuntimeException(); }
   public ConstantBigInt(int base, int pow)    { throw new RuntimeException(); }
   public ConstantBigInt(String asciiHex)      { throw new RuntimeException(); }
   public ConstantBigInt(InplaceModInt value)  { throw new RuntimeException(); }
   public byte[] toByteArray()                 { throw new RuntimeException(); }
   public String toString()                    { throw new RuntimeException(); }
   public int bits()                           { throw new RuntimeException(); }

   public int bitsInField()                    { throw new RuntimeException(); }
   public int bytesInField()                   { throw new RuntimeException(); }
   public ModIntValue maxInField()             { throw new RuntimeException(); }

   public static final ConstantBigInt ZERO  = new ConstantBigInt(0);
   public static final ConstantBigInt ONE   = new ConstantBigInt(1);
   public static final ConstantBigInt TWO   = new ConstantBigInt(2);
   public static final ConstantBigInt THREE = new ConstantBigInt(3);
   public static final ConstantBigInt TEN   = new ConstantBigInt(10);
}

