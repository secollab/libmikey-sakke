package com.selex.math;

import javolution.util.FastSet;
import javolution.util.FastComparator;

public final class ScratchContext
{
   private FastSet locals = FastSet.newInstance().setValueComparator(FastComparator.IDENTITY);
   private InplaceModIntScratchPool pool;

   // potentially pool these objects or somehow guarantee stack-
   // context for them
   //
   public static final ScratchContext forField(ConstantBigInt F)
   {
      return new ScratchContext(F);
   }

   private ScratchContext(InplaceModIntScratchPool pool)
   {
      this.pool = pool;
   }
   public InplaceModInt getInField()
   {
      InplaceModInt i = pool.getScratchInField();
      locals.add(i);
      return i;
   }
   public void recycle()
   {
      for (FastSet.Record r = locals.head(), end = locals.tail(); (r = r.getNext()) != end;)
         pool.recycleScratch((ModIntValue) locals.valueOf(r));
      locals.clear();
   }
   public void recycle(ModIntValue i)
   {
      pool.recycleScratch(i);
      locals.remove(i);
   }
   public void recycle(ModIntValue... litter)
   {
      pool.recycleScratch(litter);
      for (ModIntValue i : litter)
         locals.remove(i);
   }
   public InplaceModInt takeOwnership(InplaceModInt i)
   {
      locals.remove(i);
      return pool.takeOwnership(i);
   }
}
