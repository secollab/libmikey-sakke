package com.selex.math;

import com.selex.util.RandomGenerator;
import java.math.BigInteger;

public final class SunInplaceModInt extends SunModIntValue
{
   // constructors
   public SunInplaceModInt(byte[] value, SunConstantBigInt mod)             { p = mod; assign(value); }
   public SunInplaceModInt(int z, SunConstantBigInt mod)                    { p = mod; assign(z); }
   public SunInplaceModInt(String asciiHex, SunConstantBigInt mod)          { p = mod; assign(asciiHex); }
   public SunInplaceModInt(SunConstantBigInt value, SunConstantBigInt mod)  { p = mod; assign(value); }
   public SunInplaceModInt(SunModIntValue other)                            { i = other.i; p = other.p; }
   public SunInplaceModInt(SunConstantBigInt mod)                           { i = SunConstantBigInt.ZERO.value; p = mod; }

   // assignment
   public SunInplaceModInt assign(byte[] value)                   { i = new BigInteger(1,value).mod(p.value); return this; }
   public SunInplaceModInt assign(byte[] value, int off, int len) { byte[] t = new byte[len]; System.arraycopy(value, off, t, 0, len); return assign(t); }
   public SunInplaceModInt assign(int z)                          { i = BigInteger.valueOf(z).mod(p.value); return this; }
   public SunInplaceModInt assign(String asciiHex)                { i = new BigInteger(asciiHex, 16).mod(p.value); return this; }
   public SunInplaceModInt assign(SunConstantBigInt value)        { i = value.value.mod(p.value); return this; }
   public SunInplaceModInt assign(SunModIntValue other)           { if (this == other) return this; i = other.i; p = other.p; return this; }
   public SunInplaceModInt makeNaN()                              { i = null; return this; }
   public SunInplaceModInt randomize(RandomGenerator rand)
   {
      byte[] a = p.value.toByteArray();
      rand.randomize(a);
      i = new BigInteger(1,a).mod(p.value);
      return this;
   }

   // swap -- exchange the internal representation with another instance
   // returns 'this' now representing the value of 'other' .
   public SunInplaceModInt swap(SunInplaceModInt other) { BigInteger i = this.i; this.i = other.i; other.i = i; return this; }

   // move -- destructive; the value represented by this instance becomes NaN after a move operation
   public byte[] moveToByteArray()             { byte[] rc = i.toByteArray(); i = null; return rc; }
   public SunConstantBigInt moveToConstant()   { SunConstantBigInt rc = new SunConstantBigInt(i); i = null; return rc; }

   // additive modifiers
   public SunInplaceModInt inc()                         { i = i.add(SunConstantBigInt.ONE.value).mod(p.value); return this; }
   public SunInplaceModInt dec()                         { i = i.subtract(SunConstantBigInt.ONE.value).mod(p.value); return this; }
   public SunInplaceModInt add(SunModIntValue other)     { i = i.add(other.i).mod(p.value); return this; }
   public SunInplaceModInt sub(SunModIntValue other)     { i = i.subtract(other.i).mod(p.value); return this; }
   public SunInplaceModInt add(SunConstantBigInt other)  { i = i.add(other.value).mod(p.value); return this; }
   public SunInplaceModInt sub(SunConstantBigInt other)  { i = i.subtract(other.value).mod(p.value); return this; }

   // bitwise modifiers
   public SunInplaceModInt xor(SunModIntValue other)     { i = i.xor(other.i); return this; }

   // multiplicative modifiers
   public SunInplaceModInt mul2()                        { i = i.multiply(SunConstantBigInt.TWO.value).mod(p.value); return this; }
   public SunInplaceModInt mul(SunModIntValue other)     { i = i.multiply(other.i).mod(p.value); return this; }
   public SunInplaceModInt mod(SunModIntValue other)     { i = i.mod(other.i); return this; }
   public SunInplaceModInt mul(SunConstantBigInt other)  { i = i.multiply(other.value).mod(p.value); return this; }
   public SunInplaceModInt mod(SunConstantBigInt other)  { i = i.mod(other.value); return this; }

   // exponentiation
   public SunInplaceModInt sqr()                         { i = i.multiply(i).mod(p.value); return this; }
   public SunInplaceModInt exp(SunModIntValue other)     { i = i.modPow(other.i, p.value); return this; }
   public SunInplaceModInt exp(SunConstantBigInt other)  { i = i.modPow(other.value, p.value); return this; }
   public SunInplaceModInt inv()                         { i = i.modInverse(p.value); return this; }
   public SunInplaceModInt gcd(SunModIntValue other)     { i = i.gcd(other.i); return this; }
   public SunInplaceModInt gcd(SunConstantBigInt other)  { i = i.gcd(other.value); return this; }
}

