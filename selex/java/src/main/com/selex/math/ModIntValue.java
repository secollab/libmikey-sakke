package com.selex.math;

import com.selex.util.ByteRange;

public class ModIntValue
{
   // tests
   public int compareTo(int z)                 { throw new RuntimeException(); }
   public int compareTo(ModIntValue other)     { throw new RuntimeException(); }
   public int compareTo(ConstantBigInt other)  { throw new RuntimeException(); }
   public boolean equals(ModIntValue other)    { throw new RuntimeException(); }
   public boolean equals(ConstantBigInt other) { throw new RuntimeException(); }
   public boolean isZero()                     { throw new RuntimeException(); }
   public boolean isOne()                      { throw new RuntimeException(); }
   public boolean isOdd()                      { throw new RuntimeException(); }
   public boolean testBit(int n)               { throw new RuntimeException(); }
   public boolean isNaN()                      { throw new RuntimeException(); }

   // conversion
   public int hashCode()                  { throw new RuntimeException(); }
   public String toString()               { throw new RuntimeException(); }
   public byte[] toByteArray()            { throw new RuntimeException(); }
   public int bits()                      { throw new RuntimeException(); }
   public int bytes()                     { throw new RuntimeException(); }
   public ByteRange peekFieldByteRange()  { throw new RuntimeException(); }

   // retrieval
   public ConstantBigInt modulus()  { throw new RuntimeException(); }

   // all operations below return 'target'

   // additive operations applied to 'this' storing the result in 'target'
   public InplaceModInt inc_into(InplaceModInt target)                        { throw new RuntimeException(); }
   public InplaceModInt dec_into(InplaceModInt target)                        { throw new RuntimeException(); }
   public InplaceModInt add_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt sub_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt add_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt sub_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }

   // multiplicative operations applied to 'this' storing the result in 'target'
   public InplaceModInt mul2_into(InplaceModInt target)                       { throw new RuntimeException(); }
   public InplaceModInt mul_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt mod_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt mul_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt mod_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }

   // exponentiation operations applied to 'this' storing the result in 'target'
   public InplaceModInt sqr_into(InplaceModInt target)                        { throw new RuntimeException(); }
   public InplaceModInt exp_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt exp_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }
   public InplaceModInt inv_into(InplaceModInt target)                        { throw new RuntimeException(); }
   public InplaceModInt gcd_into(InplaceModInt target, ModIntValue other)     { throw new RuntimeException(); }
   public InplaceModInt gcd_into(InplaceModInt target, ConstantBigInt other)  { throw new RuntimeException(); }

   // additive operations applied to constants storing the result in 'target'
   public static InplaceModInt inc_into(InplaceModInt target, ConstantBigInt a)                    { throw new RuntimeException(); }
   public static InplaceModInt dec_into(InplaceModInt target, ConstantBigInt a)                    { throw new RuntimeException(); }
   public static InplaceModInt add_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt add_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }
   public static InplaceModInt sub_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt sub_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }

   // multiplicative operations applied to constants storing the result in 'target'
   public static InplaceModInt mul2_into(InplaceModInt target, ConstantBigInt a)                   { throw new RuntimeException(); }
   public static InplaceModInt mul_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt mul_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }
   public static InplaceModInt mod_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt mod_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }

   // exponentiation operations applied to constants storing the result in 'target'
   public static InplaceModInt sqr_into(InplaceModInt target, ConstantBigInt a)                    { throw new RuntimeException(); }
   public static InplaceModInt exp_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt exp_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }
   public static InplaceModInt inv_into(InplaceModInt target, ConstantBigInt a)                    { throw new RuntimeException(); }
   public static InplaceModInt gcd_into(InplaceModInt target, ConstantBigInt a, ConstantBigInt b)  { throw new RuntimeException(); }
   public static InplaceModInt gcd_into(InplaceModInt target, ConstantBigInt a, ModIntValue b)     { throw new RuntimeException(); }
}
