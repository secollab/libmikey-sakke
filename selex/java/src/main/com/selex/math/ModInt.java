package com.selex.math;

/**
 * Modular arithmetic integer interface supporting some in-place
 * update operations.  Note: All functions throw. Replace references
 * to this class with a target-specific implementation via
 * translation.
 */
public final class ModInt
{
   // constructors
   public ModInt(byte[] octets)                        { throw new RuntimeException(); }
   public ModInt(int z)                                { throw new RuntimeException(); }
   public ModInt(String asciiHex)                      { throw new RuntimeException(); }
   public ModInt(ModInt other)                         { throw new RuntimeException(); }

   // tests
   public int compareTo(int z)                         { throw new RuntimeException(); }
   public int compareTo(ModInt other)                  { throw new RuntimeException(); }
   public boolean equals(Object obj)                   { throw new RuntimeException(); }
   public boolean isOdd()                              { throw new RuntimeException(); }

   // conversion
   public int hashCode()                               { throw new RuntimeException(); }
   public String toString()                            { throw new RuntimeException(); }
   public byte[] toByteArray()                         { throw new RuntimeException(); }

   // create new instance as result of operation
   public ModInt add(ModInt a, ModInt mod)             { throw new RuntimeException(); }
   public ModInt exponent(ModInt exponent, ModInt mod) { throw new RuntimeException(); }
   public ModInt gcd(ModInt a)                         { throw new RuntimeException(); }
   public ModInt invert(ModInt mod)                    { throw new RuntimeException(); }
   public ModInt mod(ModInt mod)                       { throw new RuntimeException(); }
   public ModInt multiply(ModInt a, ModInt mod)        { throw new RuntimeException(); }
   public ModInt square(ModInt mod)                    { throw new RuntimeException(); }
   public ModInt subtract(ModInt a, ModInt mod)        { throw new RuntimeException(); }

   // mutate this instance
   public void decrement(int z, ModInt mod)            { throw new RuntimeException(); }
   public void decrement(ModInt a, ModInt mod)         { throw new RuntimeException(); }
   public void divideByTwo(ModInt mod)                 { throw new RuntimeException(); }
   public void increment(int z, ModInt mod)            { throw new RuntimeException(); }
   public void increment(ModInt a, ModInt mod)         { throw new RuntimeException(); }
   public void multiplyByTwo(ModInt mod)               { throw new RuntimeException(); }
}

