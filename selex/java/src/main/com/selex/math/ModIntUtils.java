package com.selex.math;

public final class ModIntUtils
{
   static final ModInt ZERO  = new ModInt(0);
   static final ModInt ONE   = new ModInt(1);
   static final ModInt TWO   = new ModInt(2);
   static final ModInt THREE = new ModInt(3);
   static final ModInt TEN   = new ModInt(10);

   public static boolean isZero(ModInt i)
   {
      return i.equals(ZERO);
   }

   public static int bitLength(ModInt i)
   {
      return bitLength(i.toByteArray());
   }

   public static int bitLength(byte[] v)
   {
      final int n = v.length;
      int base = n;
      for (int i = 0; i != n; ++i)
      {
         byte f = v[i];
         if (f == 0)
         {
            --base;
            continue;
         }
         if ((f&0x80) != 0) return base << 3;
         --base;
         if ((f&0x40) != 0) return base + 7;
         if ((f&0x20) != 0) return base + 6;
         if ((f&0x10) != 0) return base + 5;
         if ((f&0x08) != 0) return base + 4;
         if ((f&0x04) != 0) return base + 3;
         if ((f&0x02) != 0) return base + 2;
         if ((f&0x01) != 0) return base + 1;
      }
      return 1;
   }

   public static int byteLength(byte[] v)
   {
      final int n = v.length;
      for (int i = 0; i != n; ++i)
         if (v[i] != 0)
            return n - i;
      return 1;
   }
}
