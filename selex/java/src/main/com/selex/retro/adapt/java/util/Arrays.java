package com.selex.retro.adapt.java.util;

public final class Arrays
{
   public static byte[] copyOf(byte[] a, int len)
   {
      byte[] rc = new byte[len];
      System.arraycopy(a, 0, rc, 0, len);
      return rc;
   }
}

