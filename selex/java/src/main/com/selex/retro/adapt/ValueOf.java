package com.selex.retro.adapt;

public final class ValueOf
{
   public static final Boolean valueOf(boolean b) { return new Boolean(b); }
   public static final Integer valueOf(int i) { return new Integer(i); }
   public static final Byte valueOf(byte b) { return new Byte(b); }
}
