package com.selex.retro.subst.java.lang;

import java.io.Serializable;
import java.lang.Comparable;

/**
 * Simple substitution for Java 5 Enum.
 * valueOf is not implemented.
 */
public abstract class Enum implements Comparable, Serializable
{
   private final String name;
   private final int ordinal;

   protected Enum(String name, int ordinal)
   {
      this.name = name;
      this.ordinal = ordinal;
   }
   public final String name()
   {
      return name;
   }
   public final int ordinal()
   {
      return ordinal;
   }
   public String toString()
   {
      return name;
   }
   public final boolean equals(Object other)
   {
      return this == other;
   }
   public final int compareTo(Object other)
   {
      if (this.getClass() == other.getClass())
      {
         return this.ordinal - ((Enum) other).ordinal;
      }
      throw new ClassCastException();
   }
   public final int hashCode()
   {
      return System.identityHashCode(this);
   }
   public final Class getDeclaringClass()
   {
      return this.getClass();
   }
   public static Enum valueOf(Class enumType, String name)
   {
      throw new RuntimeException("Enum.valueOf not implemented.");
   }
}

