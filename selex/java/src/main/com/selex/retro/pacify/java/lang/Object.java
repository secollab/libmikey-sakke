package com.selex.retro.pacify.java.lang;

public class Object
{
   public static java.lang.Object clone(java.lang.Object o) throws RuntimeException
   {
      // This breaks the guarantee that Enum.values() will return a
      // copy of the synthetic $VALUES array; this would allow clients
      // to modify the definition of an Enum at runtime.  It is
      // thought that the majority of cases for this clone() would be
      // in synthetic SwitchMap code generated for switch statements
      // where such a corruption is impossible within source owing to
      // the code being compiler-generated.  This gives a minor
      // performance advantage in that context and is simpler than the
      // alternatively arraycopy.
      if (o instanceof Enum[])
         return o;
      throw new RuntimeException("No java.lang.Object.clone() on J2ME");
   }
}

