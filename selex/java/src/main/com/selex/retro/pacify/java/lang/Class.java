package com.selex.retro.pacify.java.lang;

public class Class
{
   public static java.lang.Class getComponentType(java.lang.Class c) throws RuntimeException
   {
      // XXX: Warning: This prevents code generated with Java 6 from
      // XXX: recovering the element type of arrays when built for
      // XXX: Java ME.  This is quite often unnecessary but users of
      // XXX: this library should be wary of it.
      // XXX:
      // TODO: This is not ideal.  The ideal solution would be to
      // TODO: remove unnecessary references to this (such as compiler
      // TODO: generated calls in enum code generation) using BCEL's
      // TODO: InstructionFinder:  See 
      // TODO:    http://opensource.adobe.com/svn/opensource/flex/sdk/sandbox/asc-redux/build/java/src/Downgrader.java
      // TODO: for example usage.
      // TODO: This could then be removed such that a verification
      // TODO: error would occur at build time -- alternatively this
      // TODO: could throw at runtime -- obviously the former is
      // TODO: preferred.
      //
      // null is a documented valid result for this function meaning
      // "c is not an array"  --- of course, in reality it might be!
      // 
      return null;
   }
   public static boolean desiredAssertionStatus(java.lang.Class c)
   {
      return false;
   }
   public static java.lang.ClassLoader getClassLoader(java.lang.Class c)
   {
      throw new RuntimeException("java.lang.Class.getClassLoader() not supported on J2ME");
   }
}

