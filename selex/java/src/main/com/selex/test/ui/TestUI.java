package com.selex.test.ui;

interface TestUI
{
   enum UserInputType
   {
      Text, Password, Integer, Boolean, URL
   }

   /**
    * Request user input with the given prompt on startup.
    */
   void addParameter(String prompt, UserInputType type, String defaultText);

   /**
    * Return whether this application has a standard console.
    */
   boolean hasConsole();

   /**
    * Implement the test.
    */
   int test(String[] args);

}
