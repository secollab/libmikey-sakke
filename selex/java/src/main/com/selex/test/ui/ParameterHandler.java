package com.selex.test.ui;

interface ParameterHandler
{
   void handle(String prompt, TestUI.UserInputType type, String defaultText);
}
