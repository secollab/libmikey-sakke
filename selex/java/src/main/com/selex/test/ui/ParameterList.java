package com.selex.test.ui;

import java.util.ArrayList;

public class ParameterList
{
   private static class Entry
   {
      String prompt; TestUI.UserInputType type; String defaultText;

      Entry(String prompt, TestUI.UserInputType type, String defaultText)
      {
         this.prompt = prompt;
         this.type = type;
         this.defaultText = defaultText;
      }
   }
   private ArrayList<Entry> list = new ArrayList<Entry>();

   void addParameter(String prompt, TestUI.UserInputType type, String defaultText)
   {
      list.add(new Entry(prompt, type, defaultText));
   }

   void forEach(ParameterHandler handler)
   {
      for (Entry e : list)
         handler.handle(e.prompt, e.type, e.defaultText);
   }
}
