package com.selex.log;

import org.apache.commons.logging.Log;
import java.io.PrintStream;
import java.io.OutputStream;

public class DefaultLogger implements Log
{
   public static PrintStream err = System.err;
   public static PrintStream out = System.out;
   public static PrintStream info = System.out;
   public static PrintStream verb = System.out;

   public static final PrintStream nowhere =
      new PrintStream(new OutputStream()
      {
         public void write(byte[] b, int off, int len) { }
         public void write(int b) { }
         public void flush() { }
      });

   public boolean isDebugEnabled() { return verb != nowhere; }
   public boolean isErrorEnabled() { return err != nowhere; }
   public boolean isFatalEnabled() { return err != nowhere; }
   public boolean isInfoEnabled()  { return info != nowhere; }
   public boolean isTraceEnabled() { return verb != nowhere; }
   public boolean isWarnEnabled()  { return out != nowhere; }

   public void trace(Object message)               { log(verb, message); }
   public void trace(Object message, Throwable t)  { log(verb, message, t); }
   public void debug(Object message)               { log(verb, message); }    
   public void debug(Object message, Throwable t)  { log(verb, message, t); } 
   public void info(Object message)                { log(info, message); }    
   public void info(Object message, Throwable t)   { log(info, message, t); } 
   public void warn(Object message)                { log(out, message); }    
   public void warn(Object message, Throwable t)   { log(out, message, t); } 
   public void error(Object message)               { log(err, message); }    
   public void error(Object message, Throwable t)  { log(err, message, t); } 
   public void fatal(Object message)               { log(err, message); }    
   public void fatal(Object message, Throwable t)  { log(err, message, t); } 

   private static void log(PrintStream out, Object o)
   {
      if (out == nowhere)
         return;
      logTrusted(out, o.toString());
   }
   private static void log(PrintStream out, Object o, Throwable t)
   {
      if (out == nowhere)
         return;
      logTrusted(out, o.toString() + ": " + t.toString());
   }
   private static void logTrusted(PrintStream out, String s)
   {
      out.println(s);
   }
}

