package com.selex.mikeysakke.kms;

import com.selex.mikeysakke.user.KeyStorage;

import com.selex.net.ssl.CertificateUtils;
import com.selex.net.HttpsConnector;
import com.selex.net.HttpsConnection;
import com.selex.util.OctetString;

import org.w3.util.URLUTF8Encoder;

import java.io.InputStream;
import java.io.IOException;

import org.json.me.JSONObject;
import org.json.me.JSONArray;
import org.json.me.JSONException;

import com.selex.log.DefaultLogger;

public class DefaultClient implements Client
{
   public DefaultClient(KeyStorage ks)
   {
      this.ks = ks;
   }

   private HttpsConnection c = null;

   public void Cancel()
   {
      synchronized (this)
      {
         if (c == null)
            return;
         try { c.close(); } catch (Exception e) {}
         c = null;
      }
   }

   public void FetchKeyMaterial(final String kmsServer,
                                final SSLVerificationPolicy sslVerificationPolicy,
                                final String username,
                                final String password,
                                final String identifier,
                                final String uriSuffix,
                                final CompletionHandler handler)
   {
      if (sslVerificationPolicy == DontVerifySSLCertificate)
      {
         CertificateUtils.disableHostnameVerification();
         CertificateUtils.trustAllCertificates();
      }

      if (kmsServer.isEmpty() || username.isEmpty() || password.isEmpty())
      {
         handler.OnComplete(identifier, ErrorCode.InvalidArgument, "(kmsServer, username, password) must be set ("+kmsServer+", "+username+", set="+!password.isEmpty()+")");
         return;
      }

      String url_encoded_id = URLUTF8Encoder.encode(identifier);

      String url = "https://"
      //           + username + ":" + password + "@"
                 + kmsServer
                 + "/secure/key?id="
                 + url_encoded_id
                 ;

      if (uriSuffix != null)
         url += uriSuffix;

      try
      {
         HttpsConnection c;

         synchronized (this)
         {
            c = this.c = HttpsConnector.open(url, username, password);
         }

         if (c.getResponseCode()/100 == 2)
         {
            InputStream is = c.openDataInputStream();

            // Get the length and process the data
            int len = (int) c.getLength();
            if (len > 0)
            {
               JSONObject keyData;

               byte[] data = new byte[len];
               int actual = is.read(data);

               if (actual < len)
               {
                  OctetString s = new OctetString(len);
                  s.concat(actual, data);
                  len -= actual;
                  while ((actual = is.read(data)) > 0)
                  {
                     s.concat(actual, data);
                     len -= actual;
                  }
                  keyData = new JSONObject(s.toString());
               }
               else
               {
                  keyData = new JSONObject(new String(data));
               }

               JSONObject privateData = keyData.getJSONObject("private");
               JSONObject publicData = keyData.getJSONObject("public");
               JSONArray communityData = keyData.getJSONArray("community");
               String error = keyData.optString("error");

               if (!error.isEmpty())
               {
                  handler.OnComplete(identifier, ErrorCode.OperationNotPermitted, error);
                  return;
               }

               if (privateData != null)
               {
                  ks.StorePrivateKey(identifier, "SSK", new OctetString(privateData.getString("userSecretSigningKey")));
                  ks.StorePrivateKey(identifier, "RSK", new OctetString(privateData.getString("receiverSecretKey")));
               }
               if (publicData != null)
               {
                  ks.StorePublicKey(identifier, "PVT", new OctetString(publicData.getString("userPublicValidationToken")));
               }
               if (communityData != null)
               for (int i = 0, end = communityData.length(); i < end; ++i)
               {
                  JSONObject community = communityData.getJSONObject(i);

                  String communityId = community.getString("kmsIdentifier");
            
                  ks.AddCommunity(communityId);

                  ks.StorePublicKey(communityId, "KPAK", new OctetString(community.getString("kmsPublicAuthenticationKey")));
                  ks.StorePublicKey(communityId, "Z",    new OctetString(community.getString("kmsPublicKey")));
                  ks.StorePublicParameter(communityId, "SakkeSet", community.getString("sakkeParameterSetIndex"));
               }
            }
            else throw new IOException("No content in HTTP response");
         }
         else throw new IOException("Response code "+c.getResponseCode());
      }
      catch (JSONException e)
      {
         ks.RevokeKeys(identifier);
         handler.OnComplete(identifier, ErrorCode.BadMessage, e.toString());
         return;
      }
      catch (Exception e)
      {
         handler.OnComplete(identifier, CertificateUtils.isCertificateException(e)
                                      ? ErrorCode.CertificateVerificationFailure
                                      : ErrorCode.ConnectionReset
                                      , e.toString());
         return;
      }
      finally
      {
         Cancel();
      }

      handler.OnComplete(identifier, ErrorCode.Success, "");
   }

   private KeyStorage ks;
}

