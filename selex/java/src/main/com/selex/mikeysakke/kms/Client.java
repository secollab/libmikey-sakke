package com.selex.mikeysakke.kms;

public interface Client
{
   enum ErrorCode
   {
      Success,
      ConnectionReset,
      BadMessage,
      OperationNotPermitted,
      InvalidArgument,
      CertificateVerificationFailure,
   }

   interface CompletionHandler
   {
      void OnComplete(String identifier, ErrorCode errorCode, String errorText);
   }

   enum SSLVerificationPolicy
   {
      DontVerifySSLCertificate,
      VerifySSLCertificate,
   }
   static final SSLVerificationPolicy DontVerifySSLCertificate = SSLVerificationPolicy.DontVerifySSLCertificate;
   static final SSLVerificationPolicy VerifySSLCertificate     = SSLVerificationPolicy.VerifySSLCertificate;

   void FetchKeyMaterial(String kmsServer,
                         SSLVerificationPolicy sslVerificationPolicy,
                         String username,
                         String password,
                         String identifier,
                         String uriSuffix,
                         CompletionHandler completionHandler);
}

