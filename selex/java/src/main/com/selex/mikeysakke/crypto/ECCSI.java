package com.selex.mikeysakke.crypto;

import com.selex.util.OctetString;
import com.selex.util.RandomGenerator;
import com.selex.mikeysakke.user.KeyStorage;


public interface ECCSI
{
   public boolean ValidateSigningKeysAndCacheHS(String identifier,
                                                String community,
                                                KeyStorage keys);

   public OctetString Sign(byte[] msg, int offset, int len,
                           String identifier,
                           String community,
                           RandomGenerator random,
                           KeyStorage keys);

   public boolean Verify(byte[] msg, int msgOffset, int msgLen,
                         byte[] sign, int signOffset, int signLen,
                         String identifier,
                         String community,
                         KeyStorage keys);
}

