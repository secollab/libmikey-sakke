package com.selex.mikeysakke.crypto;

import com.selex.util.OctetString;

// this may be switched at build time for a secure implementation
public final class CryptoUtils
{
   public static void Erase(byte[] octets, int offset, int N)
   {
      for (int i = offset, end = N - offset; i < end; ++i)
         octets[i] = 0;
   }
   public static void Erase(byte[] octets)
   {
      Erase(octets, 0, octets.length);
   }
   public static void Erase(OctetString octets)
   {
      Erase(octets.raw(), 0, octets.size());
   }
}

