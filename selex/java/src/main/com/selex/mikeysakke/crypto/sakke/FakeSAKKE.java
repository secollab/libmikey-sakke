package com.selex.mikeysakke.crypto.sakke;
import com.selex.mikeysakke.crypto.SAKKE;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.util.OctetString;
import com.selex.util.RandomGenerator;


public class FakeSAKKE implements SAKKE
{
   public boolean ValidateReceiverSecretKey(final String identifier,
                                            final String community,
                                            final KeyStorage keys)
   {
      return true;
   }

   private static final OctetString FakeSSV = new OctetString(OctetString.skipws
            ("12345678 9ABCDEF0 12345678 9ABCDEF0"));

   private static final OctetString FakeSED = new OctetString(
             "FAKE SAKKE ENCAPSULATED DATA", OctetString.Untranslated);

   public OctetString GenerateSharedSecretAndSED(final OctetString SED_out,
                                                 final String identifier,
                                                 final String community,
                                                 final RandomGenerator randomSource,
                                                 final KeyStorage keys)
   {
      SED_out.assign(FakeSED);

      return new OctetString(FakeSSV);
   }

   public OctetString ExtractSharedSecret(final OctetString SED,
                                          final String identifier,
                                          final String community,
                                          final KeyStorage keys)
   {
      if (SED.equals(FakeSED))
         return new OctetString(FakeSSV);

      return new OctetString();
   }
}

