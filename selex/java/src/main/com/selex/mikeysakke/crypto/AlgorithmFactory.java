package com.selex.mikeysakke.crypto;

import com.selex.mikeysakke.crypto.ECCSI;
import com.selex.mikeysakke.crypto.SAKKE;

import com.selex.mikeysakke.crypto.eccsi.*;
import com.selex.mikeysakke.crypto.sakke.*;


public final class AlgorithmFactory
{
   /**
    * All computation is performed locally -- *really* slow on handsets
    */
   public static final String LOCAL = "local";

   /**
    * Costly computation is offloaded to a remote trusted third party
    * -- NOT IMPLEMENTED YET.   Need a URI scheme for specifying
    * remote; for example: remote:https:server:port
    */
   public static final String REMOTE = "remote";

   /**
    * No computation is done, hardcoded values are used.
    */
   public static final String FAKE = "fake";


   public static ECCSI getSignatureAlgorithm(String impl)
   {
      if (impl.equals(LOCAL))
         return new LocalECCSI();
      throw new RuntimeException("Signature algorithm implementation '"+impl+"' not supported.");
   }

   public static SAKKE getKeyEncapsulationAlgorithm(String impl)
   {
      if (impl.equals(LOCAL))
         return new LocalSAKKE();
      else if (impl.equals(FAKE))
         return new FakeSAKKE();
      throw new RuntimeException("Key encapsulation algorithm implementation '"+impl+"' not supported.");
   }

}

