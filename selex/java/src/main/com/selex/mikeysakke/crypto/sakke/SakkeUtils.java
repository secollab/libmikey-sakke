package com.selex.mikeysakke.crypto.sakke;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.util.OctetString;
import com.selex.mikeysakke.crypto.hash.SHA256Digest;
import com.selex.mikeysakke.crypto.ecc.PrimeCurveAffineFp;
import com.selex.mikeysakke.crypto.SakkeParameterSet;

import com.selex.mikeysakke.user.LongTaskAbortThread;

import com.selex.math.ConstantBigInt;
import com.selex.math.ModIntValue;
import com.selex.math.InplaceModInt;
import com.selex.math.ScratchContext;

import com.selex.log.DefaultLogger;


public class SakkeUtils
{
   static final void HashToIntegerRangeSHA256(final InplaceModInt v, // intrinsically mod n
                                              final byte[] s,
                                              final int s_off,
                                              final int s_len)
   {
      SHA256Digest d = new SHA256Digest();

      // RFC6508 5.1

      // 1) A = hashfn(s)
      //
      // 2) h = zero initialized string of hashlen bits
      //
      // scratch = h || A used for h, v_i, h_i, A, in algorithm below.
      // A is at offset 32.  h is at offset 0 and is implicitly zero
      // initialized.
      //
      byte[] scratch = new byte[32+32];
      d.digest(s, s_off, s_len).sync(scratch, 32);

      // 3) l = ceiling(lg(n)/hashlen)
      int l = (v.modulus().bits()+255) >> 8;

      // 4) For i in [1, l] do
      //
      //    a) Let h_i = hashfn(h_(i - 1))
      //
      //    b) Let v_i = hashfn(h_i || A), where || denotes concatenation
      //
      // 5) Let v' = v_1 || ...  || v_l
      //
      byte[] vprime = new byte[32 * l];
      int vprime_off = 0;
      for (; l != 0; --l)
      {
         d.digest(scratch, 0, 32).sync(scratch);
         d.digest(scratch).sync(vprime, vprime_off);
         vprime_off += 32;
      }

      // 6) v = v' mod n
      //
      v.assign(vprime); // intrinsically mod n
   }


   // XXX: In some cases scratch usage in the following PF_p_*
   // XXX: functions can be reduced if the input arguments do not
   // XXX: overlap the result argument; the result argument may be
   // XXX: used as scratch.  This is not true of ALL functions below.

   // Square an element A of PF_p, placing the result in R.  R and A
   // may point to the same storage.
   //
   static final void PF_p_sqr(final InplaceModInt R1, final InplaceModInt R2,
                              final ModIntValue A1, final ModIntValue A2)
   {
      final ConstantBigInt p = R1.modulus();

      InplaceModInt Ta1 = p.getScratchInField().assign(A1);
      InplaceModInt Ta2 = p.getScratchInField().assign(A2);
      InplaceModInt Tb1 = p.getScratchInField();
      InplaceModInt Tb2 = p.getScratchInField();

      Ta1.add_into(Tb1, Ta2);
      Ta1.sub_into(Tb2, Ta2);

      Tb1.mul_into(R1, Tb2);

      Ta1.mul_into(R2, Ta2);

      R2.mul2();

      p.recycleScratch(Ta1, Ta2, Tb1, Tb2);
   }

   // Multiply two elements, A and B, of PF_p, placing the result in R.
   // Any of R, A or B may share the same memory (though if A and B share
   // the same memory it is more efficient to call PF_p_sqr() above.
   //
   static final void PF_p_mul(final InplaceModInt R1, final InplaceModInt R2,
                              final ModIntValue A1, final ModIntValue A2,
                              final ModIntValue B1, final ModIntValue B2)
   {
      final ConstantBigInt p = R1.modulus();

      InplaceModInt Ta1 = p.getScratchInField().assign(A1);
      InplaceModInt Ta2 = p.getScratchInField().assign(A2);
      InplaceModInt Tb1 = p.getScratchInField().assign(B1);
      InplaceModInt Tb2 = p.getScratchInField().assign(B2);
      
      InplaceModInt phi = p.getScratchInField();

      Ta1.mul_into(R1, Tb1);
      R1.sub(Ta2.mul_into(phi, Tb2));

      Ta1.mul_into(R2, Tb2);
      R2.add(Ta2.mul_into(phi, Tb1));

      p.recycleScratch(Ta1, Ta2, Tb1, Tb2, phi);
   }

   // Raise an element A of PF_p to power n, storing the result in R.
   // R and A may share the same storage.
   //
   static final void PF_p_pow(final InplaceModInt R1, final InplaceModInt R2,
                              final ModIntValue A1, final ModIntValue A2,
                              final ModIntValue n)
   {
      if (n.isZero())
         throw new RuntimeException("PF_p_pow raise to power 0 not implemented.");

      final ConstantBigInt p = R1.modulus();

      InplaceModInt acc1 = p.getScratchInField().assign(A1);
      InplaceModInt acc2 = p.getScratchInField().assign(A2);

      for (int N = n.bits()-2; N >= 0; --N)
      {
         PF_p_sqr(acc1, acc2, acc1, acc2);
         if (n.testBit(N))
            PF_p_mul(acc1, acc2, acc1, acc2, A1, A2);
      }
      R1.assign(acc1);
      R2.assign(acc2);
   }
   //=============================================================================


   static final SakkeParameterSet GetParamSet(String community, KeyStorage keys)
   {
      if (!keys.GetPublicParameter(community, "SakkeSet").equals("1"))
         throw new RuntimeException("Only SAKKE parameter set 1 is supported.");

      return SakkeParameterSet.SAKKE_PARAM_SET_1;
   }

   
   //=============================================================================
   // RFC2508 3.2. The Tate-Lichtenbaum Pairing
   // Ref also: MIKEY-SAKKE on Android 3.2.2
   //
   // Transcribed from the reference implementation
   //
   static final void ComputePairing(final SakkeParameterSet params,
                                    final InplaceModInt w,
                                    final ModIntValue Rx, final ModIntValue Ry,
                                    final ModIntValue Qx, final ModIntValue Qy)
   {
      final PrimeCurveAffineFp E = params.E;

      final ConstantBigInt p = E.field_order();
      final ConstantBigInt q = E.point_order();

      final ModIntValue q_minus_one = q.maxInField();
      
      final InplaceModInt v1 = p.getScratchInField(), v2 = p.getScratchInField();
      final InplaceModInt T1 = p.getScratchInField(), T2 = p.getScratchInField();
      final InplaceModInt Cx = p.getScratchInField().assign(Rx);
      final InplaceModInt Cy = p.getScratchInField().assign(Ry);

      final InplaceModInt lambda = p.getScratchInField();
      final InplaceModInt lambda_sq = p.getScratchInField();
      final InplaceModInt EAT1 = p.getScratchInField();
      final InplaceModInt EARx = p.getScratchInField();
      final InplaceModInt EARy = p.getScratchInField();

      // allowed t not-in Fp?
      final InplaceModInt t = p.getScratchInField();

      v1.assign(ConstantBigInt.ONE);
      v2.assign(ConstantBigInt.ZERO);

      final LongTaskAbortThread.AbortFlag abort = LongTaskAbortThread.getCurrentThreadAbortFlag();

      for (int N = q_minus_one.bits()-2; N >= 0; --N)
      {
         if (abort != null && abort.isSet())
         {
            w.assign(0);
            return;
         }

         PF_p_sqr(v1, v2, v1, v2);

         Cx.sqr_into(T1).dec().mul(ConstantBigInt.THREE);
         Qx.add_into(t, Cx);
         T1.mul(t);
         Cy.sqr_into(t).mul2();
         T1.sub(t);

         Cy.mul2_into(T2).mul(Qy);

         PF_p_mul(v1, v2, v1, v2, T1, T2);

         // inline doubling of EC point
         // (it is known the C is not at infinity)
         {
            Cx.sqr_into(lambda).dec().mul(ConstantBigInt.THREE);
            Cy.mul2_into(EAT1);
            lambda.mul(EAT1.inv());

            lambda.sqr_into(lambda_sq);

            Cx.mul2_into(EAT1);
            lambda_sq.sub_into(EARx, EAT1);

            EAT1.sub_into(EARy, lambda_sq).add(Cx).mul(lambda);
            EARy.sub(Cy);

            Cx.swap(EARx);
            Cy.swap(EARy);
         }

         if (q_minus_one.testBit(N))
         {
            Qx.add_into(T1, Rx).mul(Cy);
            Qx.add_into(t, Cx);
            T1.sub(t.mul(Ry));
            
            Cx.sub_into(T2, Rx).mul(Qy);

            PF_p_mul(v1, v2, v1, v2, T1, T2);

            // inline addition of EC point R to C
            // (it is known that neither R nor C are at infinity)
            {
               Ry.sub_into(lambda, Cy);
               Rx.sub_into(EAT1, Cx);
               lambda.mul(EAT1.inv());

               lambda.sqr_into(lambda_sq);

               lambda_sq.sub_into(EARx, Cx).sub(Rx);

               Rx.sub_into(EARy, lambda_sq).add(Cx.mul2()).mul(lambda);
               EARy.sub(Cy);

               Cx.swap(EARx);
               Cy.swap(EARy);
            }
         }
      }

      PF_p_sqr(v1, v2, v1, v2);
      PF_p_sqr(v1, v2, v1, v2);

      v1.inv_into(w).mul(v2);

      q.recycleScratch(q_minus_one);
      p.recycleScratch(v1, T1, Cx, Cy, t);
      p.recycleScratch(lambda, lambda_sq, EAT1, EARx, EARy);
   }



   public static void main(String[] args)
   {
      testHashToIntegerRangeSHA256();
   }

   public static void testHashToIntegerRangeSHA256()
   {
      OctetString M = new OctetString(OctetString.skipws
         ("12345678 9ABCDEF0 12345678 9ABCDEF0"+
          "32303131 2D303200 74656C3A 2B343437"+
          "37303039 30303132 3300             "));

      OctetString q = new OctetString(OctetString.skipws
         ("265EAEC7 C2958FF6 99718466 36B4195E"+
          "905B0338 672D2098 6FA6B8D6 2CF8068B"+
          "BD02AAC9 F8BF03C6 C8A1CC35 4C69672C"+
          "39E46CE7 FDF22286 4D5B49FD 2999A9B4"+
          "389B1921 CC9AD335 144AB173 595A0738"+
          "6DABFD2A 0C614AA0 A9F3CF14 870F026A"+
          "A7E535AB D5A5C7C7 FF38FA08 E2615F6C"+
          "203177C4 2B1EB3A1 D99B601E BFAA17FB"));

      InplaceModInt r = new InplaceModInt(new ConstantBigInt(q.clampedArray()));
      HashToIntegerRangeSHA256(r, M.raw(), 0, M.size());

      DefaultLogger.info.println("HTIR: M:  " + M.toHexString());
      DefaultLogger.info.println("HTIR: q:  " + q.toHexString());
      DefaultLogger.out.println("HTIR: r:  " + r);
   }
}


