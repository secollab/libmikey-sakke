package com.selex.mikeysakke.crypto.sakke;

import com.selex.mikeysakke.crypto.SAKKE;
import com.selex.mikeysakke.crypto.SakkeParameterSet;

import com.selex.mikeysakke.user.KeyStorage;
import com.selex.util.OctetString;
import com.selex.util.ByteRange;
import com.selex.mikeysakke.crypto.hash.SHA256Digest;
import com.selex.mikeysakke.crypto.ecc.PrimeCurveAffineFp;

import com.selex.math.ConstantBigInt;
import com.selex.math.ModIntValue;
import com.selex.math.InplaceModInt;
import com.selex.math.ScratchContext;
import com.selex.util.RandomGenerator;

import com.selex.log.DefaultLogger;


public class LocalSAKKE extends SakkeUtils implements SAKKE
{
   public boolean ValidateReceiverSecretKey(final String identifier,
                                            final String community,
                                            final KeyStorage keys)
   {
      final SakkeParameterSet params = GetParamSet(community, keys);

      final PrimeCurveAffineFp E = params.E;

      final ScratchContext p = ScratchContext.forField(E.field_order());

      try { // use scratch

      // RFC6508 6.1.2
      //
      // Upon receipt of key material, each user MUST verify its RSK.
      // For Identifier 'a', RSKs from KMS_T are verified by checking
      // that the following equation holds: < [a]P + Z, K_(a,T) > = g,
      // where 'a' is interpreted as an integer.
      //
      // Note that P is already loaded as the base-point of E.
      //
      final InplaceModInt Rx = p.getInField();
      final InplaceModInt Ry = p.getInField();

      final InplaceModInt Qx = p.getInField();
      final InplaceModInt Qy = p.getInField();

      final InplaceModInt Zx = p.getInField();
      final InplaceModInt Zy = p.getInField();

      if (!E.decode_point(keys.GetPublicKey(community, "Z"), Zx, Zy)) // XXX: could cache this
         throw new RuntimeException("Z is not on curve");

      // XXX: field size sufficient for all 'identifier'?
      // Note: pairing result 'w' used as temporary for 'a'
      final InplaceModInt w = p.getInField().assign(OctetString.toByteArray(identifier));
      
      E.mul_base_into(Rx, Ry, w);
      E.add_point_trusted_distinct(Rx, Ry, Zx, Zy);

      if (!E.decode_point(keys.GetPrivateKey(identifier, "RSK"), Qx, Qy))
         throw new RuntimeException("RSK is not on curve");

      ComputePairing(params, w, Rx, Ry, Qx, Qy);

      if (w.isZero())
         throw new RuntimeException("T-L pairing aborted");
      
      if (w.equals(params.g))
         return true;
      }
      catch (RuntimeException e) 
      {
         DefaultLogger.err.println("Exception verifying SAKKE encryption keys: "+e);
      }
      finally { p.recycle(); }

      DefaultLogger.err.println("Failed to verify SAKKE encryption keys.  Revoking keys for '" + identifier + "'");
      keys.RevokeKeys(identifier);
      return false;
   }


   public OctetString GenerateSharedSecretAndSED(final OctetString SED_out,
                                                 final String identifier,
                                                 final String community,
                                                 final RandomGenerator randomSource,
                                                 final KeyStorage keys)
   {
      final SakkeParameterSet params = GetParamSet(community, keys);

      final PrimeCurveAffineFp E = params.E;

      final ConstantBigInt p = E.field_order();
      final ConstantBigInt q = E.point_order();

      final ScratchContext P = ScratchContext.forField(p);
      final ScratchContext Q = ScratchContext.forField(q);

      try { // use scratch

      // RFC6508 6.2.1
      //
      // 1) Select random ephemeral integer for SSV in [0,2^n)
      //
      final OctetString SSV = new OctetString(params.n >> 3);
      randomSource.randomize(SSV.raw(), 0, SSV.size());

      // 2) Compute r = HashToIntegerRangeSHA256( SSV || b, q )
      //
      final OctetString SSV_b = new OctetString(SSV); SSV_b.concat(identifier, OctetString.Untranslated);
      final InplaceModInt r = Q.getInField();
      HashToIntegerRangeSHA256(r, SSV_b.raw(), 0, SSV_b.size()); 

      // 3) Compute R_(b,S) = [r]([b]P + Z_S) in E(F_p)
      //
      // TODO: Consider vector multiplication as per C++ impl.  For
      // TODO: now though, just do it the slow way.
      //
      // Rewrite to use OpenSSL vector scale and sum (EC_POINTs_mul).
      // Note that P has been set as the base-point in E_j so only Z and
      // its scalar are placed in the vector.  Note also that the storage
      // used for Z is reused for the result Rb.
      //
      // 3') Compute R_(b,S) = [r][b]P + [r]Z_S   // XXX: not here
      //
      final InplaceModInt Zx = P.getInField();
      final InplaceModInt Zy = P.getInField();

      if (!E.decode_point(keys.GetPublicKey(community, "Z"), Zx, Zy)) // XXX: could cache this
         throw new RuntimeException("Z is not on curve");

      // 3) Compute R_(b,S) = [r]([b]P + Z_S) in E(F_p)
      //
      // P already loaded as base point in E
      //
      final InplaceModInt Rx = P.getInField();
      final InplaceModInt Ry = P.getInField();
      final InplaceModInt b = P.getInField().assign(OctetString.toByteArray(identifier));

      E.mul_base_into(Rx, Ry, b);
      E.add_point_trusted_distinct(Rx, Ry, Zx, Zy);
      E.mul_into_trusted(Rx, Ry, Rx, Ry, r);

      // SED starts with R_(b,S)
      E.encode_point(Rx, Ry).swap(SED_out);

      // 4) Compute the HINT, H;
      //
      // 4.a) Compute g^r.
      //
      OctetString g_pow_r_octets;
      if (r.isZero())
         g_pow_r_octets = new OctetString();
      else
      {
         // reuse Zx, Zy as not needed again
         final InplaceModInt g1 = Zx.assign(ConstantBigInt.ONE);
         final InplaceModInt g2 = Zy.assign(params.g);

         PF_p_pow(g1, g2, g1, g2, r);

         // Form representation of PF_p (x_1, x_2) in F_p (x_2/x_1 mod p)
         //
         // scrunch g1 for result
         //
         g1.inv().mul(g2);

         g_pow_r_octets = new OctetString(g1.peekFieldByteRange());
      }

      // 4.b) Compute H := SSV XOR HashToIntegerRange( g^r, 2^n );
      //
      // Fp element r from above reused here.
      //
      InplaceModInt ssv = r.assign(SSV.raw());
      InplaceModInt H = params.two_pow_n.getScratchInField();
      HashToIntegerRangeSHA256(H,
                               g_pow_r_octets.raw(),
                               0,
                               g_pow_r_octets.size());
      H.xor(ssv);

      // 5) Form the SED ( R_(b,S), H )
      //
      SED_out.concat(H.peekFieldByteRange());

      params.two_pow_n.recycleScratch(H);

      // 6) Output SSV
      return SSV;

      }
      catch (RuntimeException e) 
      {
         DefaultLogger.err.println("Exception generating SAKKE SSV and SED: "+e);
         return new OctetString();
      }
      finally { P.recycle(); Q.recycle(); }
   }

   public OctetString ExtractSharedSecret(final OctetString SED,
                                          final String identifier,
                                          final String community,
                                          final KeyStorage keys)
   {
      final SakkeParameterSet params = GetParamSet(community, keys);

      final PrimeCurveAffineFp E = params.E;

      final ConstantBigInt p = E.field_order();
      final ConstantBigInt q = E.point_order();

      final ScratchContext P = ScratchContext.forField(p);
      final ScratchContext Q = ScratchContext.forField(q);
      final ScratchContext FH = ScratchContext.forField(params.two_pow_n);

      try { // use scratch

      final int L = p.bytesInField();

      final int Rb_octet_count = 2 * L + 1;

      if (SED.size() < Rb_octet_count)
         throw new RuntimeException("SED size ("+SED.size()+") insufficient for expectation ("+Rb_octet_count+")");

      // RFC6508 6.2.2
      //
      // 1) Parse the Encapsulated Data ( R_(b,S), H ), and extract
      //    R_(b,S) and H;
      //
      final InplaceModInt Rx = P.getInField();
      final InplaceModInt Ry = P.getInField();
      final InplaceModInt H = FH.getInField();

      if (!E.decode_point(SED.raw(), 0, Rb_octet_count, Rx, Ry))
         throw new RuntimeException("R_(b,S) in SED is not on curve");

      H.assign(SED.raw(), Rb_octet_count, SED.size() - Rb_octet_count);

      // 2) Compute w := < R_(b,S), K_(b,S) >
      //
      final InplaceModInt Qx = P.getInField();
      final InplaceModInt Qy = P.getInField();
      if (!E.decode_point(keys.GetPrivateKey(identifier, "RSK"), Qx, Qy))
         throw new RuntimeException("RSK is not on curve");

      final InplaceModInt w = P.getInField();

      ComputePairing(params, w, Rx, Ry, Qx, Qy);

      if (w.isZero())
         throw new RuntimeException("T-L pairing aborted");
      
      final ByteRange w_octets = w.peekFieldByteRange();

      // 3) Compute SSV := H XOR HashToIntegerRange( w, 2^n, Hash );
      //
      final InplaceModInt SSV = FH.getInField();
      HashToIntegerRangeSHA256(SSV,
                               w_octets.bytes,
                               w_octets.off,
                               w_octets.len);
      SSV.xor(H);

      // 4) Compute r = HashToIntegerRangeSHA256( SSV || b, q, Hash )
      //
      final OctetString SSV_b = new OctetString(SSV.peekFieldByteRange()); SSV_b.concat(identifier, OctetString.Untranslated);

      final InplaceModInt r = Q.getInField();
      HashToIntegerRangeSHA256(r, SSV_b.raw(), 0, SSV_b.size()); 

      // 5) Compute TEST = [r]([b]P + Z_S)
      //
      final InplaceModInt Zx = Qx; // reuse Qx for storage
      final InplaceModInt Zy = Qy; // reuse Qy for storage

      if (!E.decode_point(keys.GetPublicKey(community, "Z"), Zx, Zy)) // XXX: could cache this
         throw new RuntimeException("Z is not on curve");

      // reuse 'w' for 'b's storage
      final InplaceModInt b = w.assign(OctetString.toByteArray(identifier));

      E.mul_base_into(Rx, Ry, b);
      E.add_point_trusted_distinct(Rx, Ry, Zx, Zy);
      E.mul_into_trusted(Rx, Ry, Rx, Ry, r);

      // test computed point against Rb provided in SED
      if (E.encode_point(Rx, Ry).compareTo(SED.raw(), 0, Rb_octet_count) == 0)
         return SSV_b.reserve(SSV.bytes(), OctetString.ForceToSpecified);

      }
      catch (RuntimeException e) 
      {
         DefaultLogger.err.println("Exception extracting SAKKE SSV: "+e);
      }
      finally { P.recycle(); Q.recycle(); FH.recycle(); }

      DefaultLogger.err.println("Failed to verify SAKKE SSV.");
      return new OctetString();
   }
}

