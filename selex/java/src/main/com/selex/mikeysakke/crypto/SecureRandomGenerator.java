package com.selex.mikeysakke.crypto;

import java.lang.ThreadLocal;
import java.security.SecureRandom;

import com.selex.util.RandomGenerator;
import com.selex.util.OctetString; // for test

public class SecureRandomGenerator extends RandomGenerator
{
   private static final ThreadLocal<SecureRandom> rng = new ThreadLocal<SecureRandom>() {
      @Override protected SecureRandom initialValue() {
         return new SecureRandom();
      }
   };

   public void randomize(final byte[] octets)
   {
      rng.get().nextBytes(octets);
   }

   public void randomize(final byte[] octets, final int offset, final int N)
   {
      final int end = N + offset;

      assert (end <= octets.length);

      SecureRandom rng = SecureRandomGenerator.rng.get();

      if (offset == 0 && N == octets.length)
      {
         rng.nextBytes(octets);
         return;
      }
      
      for (int i = offset; i < end;)
      {
         int rnd = rng.nextInt();
         octets[i++] = (byte) rnd;
         if (i == end) return;
         octets[i++] = (byte) (rnd >>= 8);
         if (i == end) return;
         octets[i++] = (byte) (rnd >>= 8);
         if (i == end) return;
         octets[i++] = (byte) (rnd >>= 8);
      }
   }

   public int randomInt() { return SecureRandomGenerator.rng.get().nextInt(); }


   public static void main(String[] args)
   {
      byte[] b = new byte[257];
      SecureRandomGenerator srg = new SecureRandomGenerator();
      System.out.println(new OctetString(b).toHexString());
      srg.randomize(b);
      System.out.println(new OctetString(b).toHexString());
      srg.randomize(b, 0, b.length);
      System.out.println(new OctetString(b).toHexString());
      srg.randomize(b, 10, 30);
      System.out.println(new OctetString(b).toHexString());
      srg.randomize(b, 0, 30);
      System.out.println(new OctetString(b).toHexString());
      srg.randomize(b, b.length-30, 30);
      System.out.println(new OctetString(b).toHexString());
   }
}

