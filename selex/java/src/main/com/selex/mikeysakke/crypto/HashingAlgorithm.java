package com.selex.mikeysakke.crypto;

public enum HashingAlgorithm
{
   SHA1    (  1, 20),
   SHA256  (256, 32);

   HashingAlgorithm(final int code, final int octets) 
   {
      this.code = code;
      this.octets = octets;
   }
   public final int code;
   public final int octets;
}

