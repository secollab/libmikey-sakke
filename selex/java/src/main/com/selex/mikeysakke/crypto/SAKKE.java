package com.selex.mikeysakke.crypto;

import com.selex.util.OctetString;
import com.selex.util.RandomGenerator;
import com.selex.mikeysakke.user.KeyStorage;


public interface SAKKE
{
   public boolean ValidateReceiverSecretKey(final String identifier,
                                            final String community,
                                            final KeyStorage keys);

   public OctetString GenerateSharedSecretAndSED(final OctetString SED_out,
                                                 final String identifier,
                                                 final String community,
                                                 final RandomGenerator randomSource,
                                                 final KeyStorage keys);

   public OctetString ExtractSharedSecret(final OctetString SED,
                                          final String identifier,
                                          final String community,
                                          final KeyStorage keys);
}
