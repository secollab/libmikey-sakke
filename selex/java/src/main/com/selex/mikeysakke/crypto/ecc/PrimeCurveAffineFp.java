package com.selex.mikeysakke.crypto.ecc;

import com.selex.math.ScratchContext;
import com.selex.math.ConstantBigInt;
import com.selex.math.ModIntValue;
import com.selex.math.InplaceModInt;
import com.selex.util.OctetString;

import java.math.BigInteger;

import com.selex.log.DefaultLogger;

public class PrimeCurveAffineFp
{
   ConstantBigInt p, three_p, a, h, b, r;
   ModIntValue Gx, Gy;
   byte[] seed;

   // use as placeholder if either x or y is only result required from a point operation
   // -- the value of this should be considered permanently undefined.
   public InplaceModInt ignore;

   private static final BigInteger THREE = BigInteger.valueOf(3);

   public PrimeCurveAffineFp(ConstantBigInt p, ConstantBigInt a, ConstantBigInt h, ConstantBigInt b, ConstantBigInt r, ConstantBigInt Gx, ConstantBigInt Gy, byte[] seed)
   {
      this.p = p;
      this.three_p = new ConstantBigInt(new BigInteger(1,p.toByteArray()).multiply(THREE).toByteArray());
      this.a = a;
      this.h = h;
      this.b = b;
      this.r = r;
      this.Gx = new InplaceModInt(Gx, p);
      this.Gy = new InplaceModInt(Gy, p);

      this.ignore = new InplaceModInt(p);

      // pre-create a number of integers for commonly used fields.
      {
         ScratchContext P = ScratchContext.forField(p);
         for (int i = 0; i != 16; ++i) P.getInField();
         P.recycle();
      }
      {
         ScratchContext R = ScratchContext.forField(r);
         for (int i = 0; i != 16; ++i) R.getInField();
         R.recycle();
      }
      {
         ScratchContext P3 = ScratchContext.forField(three_p);
         for (int i = 0; i != 2; ++i) P3.getInField();
         P3.recycle();
      }
      p.maxInField(); // pre-cache p-1 
   }

   public ConstantBigInt field_order() { return p; }
   public ConstantBigInt cofactor()    { return h; }
   public ConstantBigInt point_order() { return r; }
   public ModIntValue    base_x()      { return Gx; }
   public ModIntValue    base_y()      { return Gy; }

   public OctetString base_point_octets()
   {
      return encode_point(Gx, Gy);
   }
   public OctetString encode_point(final ModIntValue x, final ModIntValue y)
   {
      return new OctetString()
         .reserve(1 + 2 * p.bytesInField())
         .concat((byte) 4)
         .concat(x.peekFieldByteRange())
         .concat(y.peekFieldByteRange())
         ;
   }
   public boolean decode_point(OctetString x9, InplaceModInt xout, InplaceModInt yout)
   {
      return decode_point(x9.raw(), 0, x9.size(), xout, yout);
   }
   public boolean decode_point(byte[] raw, int off, int len, InplaceModInt xout, InplaceModInt yout)
   {
      int fieldSize = p.bytesInField();
      if (len != (1 + fieldSize + fieldSize))
      {
         DefaultLogger.err.println("X9.62 length unexpected " + len + " != (" + (1 + fieldSize + fieldSize) + ")");
         return false;
      }
      if (raw[off] != (byte) 4)
      {
         DefaultLogger.err.println("Expected X9.62 uncompressed point (first byte 0x04)");
         return false;
      }
      xout.assign(raw, off + 1, fieldSize);
      yout.assign(raw, off + 1 + fieldSize, fieldSize);
      return is_on_curve(xout, yout);
   }

   public boolean is_on_curve(final ModIntValue x, final ModIntValue y)
   {
      final InplaceModInt ysq = y.sqr_into(p.getScratchInField());
      final InplaceModInt rhs = x.exp_into(p.getScratchInField(), ConstantBigInt.THREE);
      final InplaceModInt ax = x.mul_into(p.getScratchInField(), a);
      rhs.add(ax).add(b);
      final boolean rc = ysq.equals(rhs);
      p.recycleScratch(ysq, rhs, ax);
      return rc;
   }

   /**
    * Add base-point to itself n-1 times storing the resulting point
    * in (xout, yout). 
    */
   public void mul_base_into(final InplaceModInt xout, final InplaceModInt yout, final ModIntValue n)
   {
      mul_into_trusted(xout, yout, Gx, Gy, n);
   }

   public void mul_into(final InplaceModInt xout, final InplaceModInt yout,
                        final ModIntValue Px, final ModIntValue Py, final ModIntValue n)
   {
      if (!is_on_curve(Px, Py))
         throw new RuntimeException("Point not on curve");

      mul_into_trusted(xout, yout, Px, Py, n);
   }

   /**
    * Add the given (trusted) point to itself n-1 times storing the
    * resulting point in (xout, yout).  (Px, Py) is trusted to be on
    * the curve.
    *
    * FIXME: This is the simplest implementation (double-and-add), it
    * is by no means optimal.  TODO: Replace with a windowed,
    * non-adjacent form version.  This would require some
    * pre-computation specific to the point being multiplied which
    * should be cached for faster reentry.  See:
    * http://en.wikipedia.org/wiki/Elliptic_curve_point_multiplication#wNAF_Method
    */
   public void mul_into_trusted(final InplaceModInt xout, final InplaceModInt yout,
                                final ModIntValue Px, final ModIntValue Py, final ModIntValue n)
   {
      ModIntValue posPx, posPy;
      {
         if (xout == Px) posPx = p.getScratchInField().assign(Px);
         else xout.assign(posPx = Px);

         if (yout == Py) posPy = p.getScratchInField().assign(Py);
         else yout.assign(posPy = Py);
      }
      InplaceModInt negPy = ModIntValue.sub_into(p.getScratchInField(), ConstantBigInt.ZERO, posPy);
      InplaceModInt h = ModIntValue.mul_into(three_p.getScratchInField(), ConstantBigInt.THREE, n);

      for (int i = h.bits() - 2; i > 0; --i)
      {
         double_point_trusted(xout, yout);

         boolean ht = h.testBit(i);
         boolean nt = n.testBit(i);

         if (ht != nt)
            add_point_trusted_distinct(xout, yout, posPx, ht ? posPy : negPy);
      }

      p.recycleScratch(negPy, h);
      if (xout == Px)
         p.recycleScratch(posPx);
      if (yout == Py)
         p.recycleScratch(posPy);
   }

   public void add_point_trusted_distinct(final InplaceModInt xinout, final InplaceModInt yinout,
                                          final ModIntValue xaddend, final ModIntValue yaddend)
   {
      final ModIntValue xin = p.getScratchInField().swap(xinout);
      final ModIntValue yin = p.getScratchInField().swap(yinout);
      add_point_into_trusted_distinct(xinout, yinout, xin, yin, xaddend, yaddend);
      p.recycleScratch(xin, yin);
   }

   // val(ax,ay) != val(bx.by)
   // ax != xout != bx
   // ay != yout != by
   public void add_point_into_trusted_distinct(final InplaceModInt xout, final InplaceModInt yout,
                                               final ModIntValue ax, final ModIntValue ay,
                                               final ModIntValue bx, final ModIntValue by)
   {
      // g = (by - ay) / (bx - ax) (mod p)
      //
      // xout is used as inline scratch for (bx - ax)^-1
      //
      final ModIntValue
         g = by.sub_into(p.getScratchInField(), ay)
            .mul(bx.sub_into(xout, ax).inv());

      // xout = g^2 - ax - bx
      // yout = g(ax - xout) - ay
      //
      g.sqr_into(xout).sub(ax).sub(bx);
      ax.sub_into(yout, xout).mul(g).sub(ay);

      p.recycleScratch(g);
   }

   public void double_point_trusted(final InplaceModInt xinout, final InplaceModInt yinout)
   {
      final ModIntValue xin = p.getScratchInField().swap(xinout);
      final ModIntValue yin = p.getScratchInField().swap(yinout);
      double_point_into_trusted(xinout, yinout, xin, yin);
      p.recycleScratch(xin, yin);
   }

   public void double_point_into_trusted(final InplaceModInt xout, final InplaceModInt yout,
                                         final ModIntValue xin, final ModIntValue yin)
   {
      // g = (3x^2 + a) / 2y
      //
      // yout is used as inline scratch for (2y)^-1
      //
      final ModIntValue
         g = xin.sqr_into(p.getScratchInField())
            .mul(ConstantBigInt.THREE)
            .add(this.a)
            .mul(yin.mul_into(yout, ConstantBigInt.TWO).inv());

      // xout = g^2 - 2x
      // yout = g(x - xout) - y
      //
      // yout is used as inline scratch for 2x in xout result
      //
      g.sqr_into(xout).sub(xin.mul_into(yout, ConstantBigInt.TWO));
      xin.sub_into(yout, xout).mul(g).sub(yin);

      p.recycleScratch(g);
   }
}

