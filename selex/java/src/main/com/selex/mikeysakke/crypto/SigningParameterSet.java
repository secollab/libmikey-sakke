package com.selex.mikeysakke.crypto;

import com.selex.util.OctetString;

import com.selex.mikeysakke.crypto.ecc.PrimeCurveAffineFp;
import com.selex.math.ConstantBigInt;


public class SigningParameterSet
{
   public final PrimeCurveAffineFp curve;
   public final HashingAlgorithm hash;

   SigningParameterSet(String nist, HashingAlgorithm hash)
   {
      if (!nist.equals("P-256"))
         throw new RuntimeException("Only NIST P-256 supported for signing");

      curve = new PrimeCurveAffineFp(
            /* p    */ new ConstantBigInt("ffffffff00000001000000000000000000000000ffffffffffffffffffffffff"),
            /* a    */ new ConstantBigInt("ffffffff00000001000000000000000000000000fffffffffffffffffffffffc"),
            /* h    */ new ConstantBigInt(1),
            /* b    */ new ConstantBigInt("5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b"),
            /* r    */ new ConstantBigInt("ffffffff00000000ffffffffffffffffbce6faada7179e84f3b9cac2fc632551"),
            /* G.x  */ new ConstantBigInt("6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296"),
            /* G.y  */ new ConstantBigInt("4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5"),
            /* seed */ new OctetString("c49d360886e704936a6678e1139d26b7819f7e90").clampedArray());

      this.hash = hash;
   }

   public static final SigningParameterSet
      ECCSI_6509_PARAM_SET = new SigningParameterSet("P-256", HashingAlgorithm.SHA256);
}

