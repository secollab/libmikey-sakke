package com.selex.mikeysakke.util;

import org.apache.commons.net.ntp.TimeStamp;
import com.selex.util.OctetString;
import com.selex.log.DefaultLogger;
import java.util.Date;
import java.util.Calendar;
import com.selex.util.cstring;

import com.selex.mikeysakke.user.SakkeIdentifierScheme;
import com.selex.mikeysakke.test.MIKEYSAKKETest;


public class MIKEYSAKKEUtils
{
   public static final byte[] formatIdDateStamp6509(final TimeStamp t)
   {
      // YYYY-MM\0
      byte[] rc = new byte[4+1+2+1];
      return formatIdDateStamp6509(t, rc, 0);
   }

   public static final byte[] formatIdDateStamp6509(final TimeStamp t, final byte[] out, int off)
   {
      Calendar c = Calendar.getInstance();

      if (MIKEYSAKKETest.enable_sakke_test_vectors)
         c.setTime(MIKEYSAKKETest.TestDateFrom6509);
      else
         c.setTime(t.getDate());

      formatAsciiDec(out, off, 4, c.get(Calendar.YEAR));
      off += 4; out[off++] = (byte) '-';
      formatAsciiDec(out, off, 2, c.get(Calendar.MONTH)+1);
      off += 2; out[off] = 0;
      return out;
   }

   public static final void formatAsciiDec(final byte[] out, int off, int len, final int value)
   {
      final String s = Integer.toString(value);
      final int s_len = s.length();
      if (s_len > len)
         throw new RuntimeException("formatDec() value out of range");
      for (; len > s_len; --len)
         out[off++] = (byte) '0';
      for (int i = 0; i < s_len; ++i)
         out[off++] = (byte) s.charAt(i);
   }

   public static final char[] e164 = "+0123456789".toCharArray();

   public static final String canonicalizeUri(String uri)
   {
      if (MIKEYSAKKETest.enable_sakke_test_vectors)
         return MIKEYSAKKETest.TestURI;

      final char[] curi = uri.toCharArray();
      int span_end = 0;

      // TODO: proper parsing here;
      // XXX: for now just find a span of E164 characters preceding an @.
      for (;;)
      {
         final int span_begin = cstring.strcspn(curi, span_end, curi.length, e164);
         if (span_begin == curi.length) // no span found
            return uri;
         span_end = cstring.strspn(curi, span_begin, curi.length, e164);
         if (span_end == curi.length) // no @ found
            return uri;
         if (curi[span_end] == '@')
            return "tel:" + new String(curi, span_begin, span_end - span_begin);
      }
   }

   public static SakkeIdentifierScheme fromUri(String uri, int offset)
   {
      if (MIKEYSAKKETest.enable_sakke_test_vectors)
         return SakkeIdentifierScheme.TelURIWithMonthlyKeys;

      if (uri.startsWith("tel:", offset))
         return SakkeIdentifierScheme.TelURIWithMonthlyKeys;
      if (uri.startsWith("sip:", offset) || uri.startsWith("sips:", offset))
         return SakkeIdentifierScheme.PrivateEndPointAddressWithMonthlyKeys;

      throw new RuntimeException("No SAKKE Identifier Scheme for URI: "+uri);
   }

   public static SakkeIdentifierScheme fromSakkeIdentifier(String identifier)
   {
      int n = identifier.indexOf(0);
      if (n == -1)
         throw new RuntimeException("Sakke identifier did not have an embedded NUL prefixing the URI");
      return fromUri(identifier, n+1);
   }


   public static void main(String[] args)
   {
      // now
      TimeStamp t = new TimeStamp(new Date());

      OctetString o = new OctetString(formatIdDateStamp6509(t));
      DefaultLogger.out.println(o.toHexString());
      DefaultLogger.out.println(o);

      t = new TimeStamp(MIKEYSAKKETest.TestDateFrom6509);

      formatIdDateStamp6509(t, o.raw(), 0);
      DefaultLogger.out.println(o.toHexString());
      DefaultLogger.out.println(o);

      o.assign("Data Stamp ID used in RFC 6509 is [YYYY!MM!]", OctetString.Untranslated);

      formatIdDateStamp6509(t, o.raw(), o.size() - 9);
      DefaultLogger.out.println(o.toHexString());
      DefaultLogger.out.println(o);

      DefaultLogger.out.println(canonicalizeUri("sip:bob@test"));
      DefaultLogger.out.println(canonicalizeUri("sip:b123ob@test"));
      DefaultLogger.out.println(canonicalizeUri("sip:123ob@test"));
      DefaultLogger.out.println(canonicalizeUri("sip:bob123@test"));
      DefaultLogger.out.println(canonicalizeUri("sip:+447700900123@test"));
      DefaultLogger.out.println(canonicalizeUri("sip:1111111@test"));
   }
}

