package com.selex.mikeysakke.test;

import com.selex.util.RandomGenerator;
import com.selex.util.OctetString;

import java.util.Date;


public final class MIKEYSAKKETest
{
   // Control flag used by MIKEY stack to determine whether to
   // substitute the values here for the generated values.
   public static boolean enable_sakke_test_vectors = false;

   // Sat Feb 05 2011 16:50:22 GMT+0000 (GMT Standard Time)
   public static final Date TestDateFrom6509 = new Date(1296924622000L);

   // User URI used for both Alice and Bob across 6507, 6508 and 6509
   public static final String TestURI = "tel:+447700900123";

   // Fixed 'random' functions providing reference values from spec
   // Conveniently accessible to other test harnesses.
   //
   public static final RandomGenerator EphemeralFrom6507 = new RandomGenerator()
   {
      public void randomize(byte[] octets, int offset, int len)
      {
         assert (offset == 0);
         new OctetString("34567").deposit_bigendian(octets, len);
      }
      public int randomInt() { return 0x00034567; }
   };
   public static final RandomGenerator SSVFrom6508 = new RandomGenerator()
   {
      public void randomize(byte[] p, int offset, int len)
      {
         assert (offset == 0);
         new OctetString(OctetString.skipws
            ("12345678 9ABCDEF0 12345678 9ABCDEF0")).deposit_bigendian(p,len);
      }
      public int randomInt() { return 0x12345678; }
   };
}

