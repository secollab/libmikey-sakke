package com.selex.mikeysakke.user;

import com.selex.util.OctetString;
import java.util.Map;
import java.util.Collection;
import java.util.HashMap;
import java.util.ArrayList;

public class RuntimeKeyStorage implements KeyStorage
{
   private static final String stringFallback = "";
   private static final OctetString octectStringFallback = new OctetString();

   public OctetString GetPrivateKey(String identifier, String key)
   {  
      return FetchOrDefault(privateKeys, identifier, key, octectStringFallback);
   }
   public OctetString GetPublicKey(String identifier, String key)
   {
      return FetchOrDefault(publicKeys, identifier, key, octectStringFallback);
   }
   public String GetPublicParameter(String identifier, String key)
   {
      return FetchOrDefault(publicParameters, identifier, key, stringFallback);
   }
   public void StorePrivateKey(String identifier, String key, OctetString value)
   {
      Store(privateKeys, identifier, key, value);
   }
   public void StorePublicKey(String identifier, String key, OctetString value)
   {
      Store(publicKeys, identifier, key, value);
   }
   public void StorePublicParameter(String identifier, String key, String value)
   {
      Store(publicParameters, identifier, key, value);
   }
   public void AddCommunity(String community)
   {
      communities.add(community);
   }
   public Collection<String> GetCommunityIdentifiers()
   {
      return communities;
   }
   public void RevokeKeys(String identifier)
   {
      privateKeys.remove(identifier);
      publicKeys.remove(identifier);
      publicParameters.remove(identifier);
      communities.remove(identifier);
   }
   public void Purge()
   {
      privateKeys.clear();
      publicKeys.clear();
      publicParameters.clear();
      communities.clear();
   }

   private <P,S,V> void Store(Map<P, Map<S, V>> map, P p, S s, V v)
   {
      Map<S, V> sec = map.get(p);
      if (sec == null) {
         sec = new HashMap<S, V>();
         map.put(p, sec);
      }
      sec.put(s, v);
   }
   private <P,S,R> R FetchOrDefault(Map<P, Map<S, R>> map, P p, S s, R fallback)
   {
      Map<S, R> sec = map.get(p);
      if (sec != null)
      {
         R r = sec.get(s);
         if (r != null)
            return r;
      }
      if (fallback instanceof OctetString)
         return (R) new OctetString();
      return fallback;
   }
   private Map<String, Map<String, String>>        publicParameters = new HashMap<String, Map<String, String>>();
   private Map<String, Map<String, OctetString>>   publicKeys = new HashMap<String, Map<String, OctetString>>();
   private Map<String, Map<String, OctetString>>   privateKeys = new HashMap<String, Map<String, OctetString>>();
   private Collection<String>                      communities = new ArrayList<String>();
}

