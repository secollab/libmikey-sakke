package com.selex.mikeysakke.user;

public abstract class LongTaskAbortThread extends Thread
{
   public static final class AbortFlag
   {
      private boolean abort = false;
      public boolean isSet() { return abort; }
      public void set() { abort = true; }
   }
   private final AbortFlag abortFlag = new AbortFlag();

   public final AbortFlag getAbortFlag()
   {
      return abortFlag;
   }
   public static AbortFlag getCurrentThreadAbortFlag()
   {
      if (Thread.currentThread() instanceof LongTaskAbortThread)
         return ((LongTaskAbortThread) Thread.currentThread()).abortFlag;
      return null;
   }
}
