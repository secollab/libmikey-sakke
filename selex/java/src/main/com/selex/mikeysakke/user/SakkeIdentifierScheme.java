package com.selex.mikeysakke.user;

public enum SakkeIdentifierScheme
{
   UndefinedSakkeIdentifierScheme (0),
   TelURIWithMonthlyKeys (1),
   PrivateEndPointAddressWithMonthlyKeys (240);

   private SakkeIdentifierScheme(int code) { this.code = code; }

   public final int code;
}

