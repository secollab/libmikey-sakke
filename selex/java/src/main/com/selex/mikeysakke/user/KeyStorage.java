package com.selex.mikeysakke.user;

import com.selex.util.OctetString;
import java.util.Collection;

/**
 * Provides access to and update of a set of identity-based keys.
 * XXX: consider dropping the tier of this by removing the identifier
 * XXX: parameter from all functions.  An application would then map
 * XXX: and identity-specific object via this interface to that
 * XXX: identities keys.  See mskms/key-storage.alternate.h
 */
public interface KeyStorage
{
   OctetString GetPrivateKey(String identifier, String key);
   OctetString GetPublicKey(String identifier, String key);
   String GetPublicParameter(String identifier, String param);
   void StorePrivateKey(String identifier, String key, OctetString value);
   void StorePublicKey(String identifier, String key, OctetString value);
   void StorePublicParameter(String identifier, String param, String value);

   /**
    * Add a user community.  Key material for these are accessed in
    * the same way as user key material.  This provides a means to
    * accumulate known communities and support enumeration of them via
    * GetCommunityIdentifiers().
    */
   void AddCommunity(String community);
   Collection<String> GetCommunityIdentifiers();

   void RevokeKeys(String identifier);
   void Purge();
}

