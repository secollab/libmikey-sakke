package com.selex.net;

import java.io.InputStream;

public interface HttpsConnection
{
   // request params
   void setRequestProperty(String property, String value) throws java.io.IOException;;

   // make request and access response
   InputStream openDataInputStream() throws java.io.IOException;

   // response meta-data
   int getResponseCode() throws java.io.IOException;
   long getLength();

   void close() throws java.io.IOException;;
}

