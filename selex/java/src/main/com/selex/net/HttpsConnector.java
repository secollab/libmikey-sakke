package com.selex.net;

import com.sun.j2me.examples.netclient.BasicAuth;

public class HttpsConnector
{
   public static HttpsConnection open(String url) throws java.io.IOException
   {
      return new HttpsConnectionImpl(url);
   }
   public static HttpsConnection open(String url, String username, String password) throws java.io.IOException
   {
      HttpsConnection c = new HttpsConnectionImpl(url);
      c.setRequestProperty("Authorization", "Basic " + BasicAuth.encode(username, password));
      return c;
   }
}

