package com.selex.math;

import com.selex.util.OctetString;

public final class ByteArrayConstantBigInt extends InplaceModIntScratchPool
{
   final byte[] value;
   final int bitLength;

   ByteArrayModIntValue minusOne;
   int fieldBitLength;
   int fieldByteLength;
   int msbMask = -1;

   public ByteArrayConstantBigInt(final byte[] value)
   {
      this.bitLength = ModIntUtils.bitLength(value);
      final int bytes = (this.bitLength+7)>>3; 
      this.value = new byte[bytes];
      System.arraycopy(value, value.length - bytes, this.value, 0, bytes);
   }
   public ByteArrayConstantBigInt(int z)
   {
      if (z >= 0 && z < 256)
      {
         this.value = new byte[1];
         this.value[0] = (byte) z;
         this.bitLength = ModIntUtils.bitLength(this.value);
      }
      else throw new RuntimeException("ByteArrayConstantBigInt currently only supports single unsigned byte.");
   }
   public ByteArrayConstantBigInt(int base, int pow)
   {
      if (base != 2)
         throw new RuntimeException("ByteArrayConstantBigInt(base, pow) only supports base 2");

      this.value = new byte[1 + (pow >> 3)];
      this.value[0] = (byte) (1 << (pow & 0x7));
      this.bitLength = pow + 1;
   }
   public ByteArrayConstantBigInt(String asciiHex)
   {
      this.value = OctetString.ascii_hex_to_bytes(asciiHex);
      this.bitLength = ModIntUtils.bitLength(this.value);
   }
   public ByteArrayConstantBigInt(InplaceModInt value)
   {
      this(value.toByteArray());
   }
   public byte[] toByteArray()
   {
      byte[] rc = new byte[value.length];
      System.arraycopy(value, 0, rc, 0, value.length);
      return rc;
   }
   public String toString()
   {
      return OctetString.bytes_to_ascii_hex(value);
   }
   public int bits()                         { return bitLength; }

   public int bitsInField()                  { if (minusOne == null) maxInField(); return fieldBitLength; }
   public int bytesInField()                 { if (minusOne == null) maxInField(); return fieldByteLength; }
   public ByteArrayModIntValue maxInField()  { if (minusOne == null) minusOne = new ByteArrayInplaceModInt(this, -1); return minusOne; }

   public static final ByteArrayConstantBigInt ZERO  = new ByteArrayConstantBigInt(0);
   public static final ByteArrayConstantBigInt ONE   = new ByteArrayConstantBigInt(1);
   public static final ByteArrayConstantBigInt TWO   = new ByteArrayConstantBigInt(2);
   public static final ByteArrayConstantBigInt THREE = new ByteArrayConstantBigInt(3);
   public static final ByteArrayConstantBigInt TEN   = new ByteArrayConstantBigInt(10);

   // implementation-specific interface
   ByteArrayConstantBigInt(final byte[] value, final int no_copy_tag)
   {
      this.value = value; 
      this.bitLength = ModIntUtils.bitLength(value);
   }
   int msb_mask()
   {
      if (msbMask == -1)
      {
         int msbpow = (bitsInField()-1) & 0x7;
         if (msbpow == 7)
            msbMask = 0xFF;
         else
            msbMask = (1 << (msbpow+1)) - 1;
      }
      return msbMask;
   }
}
