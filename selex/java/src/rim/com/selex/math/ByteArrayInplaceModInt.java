package com.selex.math;

import net.rim.device.api.crypto.CryptoByteArrayArithmetic;
import com.selex.util.OctetString;
import com.selex.util.RandomGenerator;

public final class ByteArrayInplaceModInt extends ByteArrayModIntValue
{
   // constructors
   public ByteArrayInplaceModInt(byte[] value, ByteArrayConstantBigInt mod)
   {
      this.mod = mod;
      this.value = new byte[mod.value.length];
      CryptoByteArrayArithmetic.mod(value, mod.value, this.value);
   }
   public ByteArrayInplaceModInt(int z, ByteArrayConstantBigInt mod)
   {
      this.mod = mod;
      this.value = new byte[mod.value.length];
      if (z == 0)
         return;
      if (z > 0 && z < 256)
         this.value[this.value.length-1] = (byte) z;
      else throw new RuntimeException("ByteArrayInplaceModInt(int) currently only supports single unsigned byte.");
   }
   public ByteArrayInplaceModInt(String asciiHex, ByteArrayConstantBigInt mod)
   {
      this(OctetString.ascii_hex_to_bytes(asciiHex), mod);
   }
   public ByteArrayInplaceModInt(ByteArrayConstantBigInt value, ByteArrayConstantBigInt mod)
   {
      this(value.value, mod);
   }
   public ByteArrayInplaceModInt(ByteArrayModIntValue other)
   {
      this.value = new byte[other.value.length];
      System.arraycopy(other.value, 0, this.value, 0, other.value.length);
      this.mod = other.mod;
   }
   public ByteArrayInplaceModInt(ByteArrayConstantBigInt mod)
   {
      this(0, mod);
   }

   // assignment
   public ByteArrayInplaceModInt assign(byte[] value)
   {
      return assign(value, 0, value.length);
   }
   public ByteArrayInplaceModInt assign(byte[] value, final int in_off, final int in_len)
   {
      CryptoByteArrayArithmetic.mod(value, in_off, in_len, mod.value, 0, mod.value.length, this.value, 0, this.value.length);
      return this;
   }
   public ByteArrayInplaceModInt assign(int z)
   {
      if (z >= 0 && z < 256)
      {
         byte[] a = this.value;
         final int last = a.length - 1;
         for (int i = 0; i != last; ++i)
            a[i] = 0;
         a[last] = (byte) z;
      }
      else throw new RuntimeException("ByteArrayInplaceModInt::assign currently only supports single unsigned byte.");

      return this;
   }
   public ByteArrayInplaceModInt assign(String asciiHex)                { return assign(OctetString.ascii_hex_to_bytes(asciiHex)); }
   public ByteArrayInplaceModInt assign(ByteArrayConstantBigInt value)  { return assign(value.value); }
   public ByteArrayInplaceModInt assign(ByteArrayModIntValue other)     { if (other == this) return this; return assign(other.value); }
   public ByteArrayInplaceModInt makeNaN()                              { value = null; return this; }
   public ByteArrayInplaceModInt randomize(RandomGenerator rand)
   {
      rand.randomize(value);
      CryptoByteArrayArithmetic.mod(value, mod.value, value);
      return this;
   }

   // swap -- exchange the internal representation with another instance
   // returns 'this' now representing the value of 'other' .
   public ByteArrayInplaceModInt swap(ByteArrayInplaceModInt other) { byte[] value = this.value; this.value = other.value; other.value = value; return this; }

   // move -- destructive; the value represented by this instance becomes NaN after a move operation
   public byte[] moveToByteArray()                  { byte[] rc = value; value = null; return rc; }
   public ByteArrayConstantBigInt moveToConstant()  { ByteArrayConstantBigInt rc = new ByteArrayConstantBigInt(value,0); value = null; return rc; }

   // additive modifiers
   public ByteArrayInplaceModInt inc()                               { CryptoByteArrayArithmetic.increment(value, mod.value, value); return this; }
   public ByteArrayInplaceModInt dec()                               { CryptoByteArrayArithmetic.decrement(value, mod.value, value); return this; }
   public ByteArrayInplaceModInt add(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.add(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt sub(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.subtract(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt add(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.add(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt sub(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.subtract(value, other.value, mod.value, value); return this; }

   // bitwise modifiers
   public ByteArrayInplaceModInt xor(ByteArrayModIntValue other)
   {
      byte[] t = this.value, o = other.value;
      int ti, oi;
      if (t.length > o.length)
      {
         ti = t.length - o.length;
         oi = 0;
      }
      else
      {
         ti = 0;
         oi = o.length - t.length;
      }
      for (final int e = t.length; ti != e; ++ti, ++oi)
         t[ti] ^= o[oi];
      return this;
   }

   // multiplicative modifiers
   public ByteArrayInplaceModInt mul2()                              { CryptoByteArrayArithmetic.multiplyByTwo(value, mod.value, value); return this; }
   public ByteArrayInplaceModInt mul(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.multiply(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt mod(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.mod(value, other.value, value); return this; }
   public ByteArrayInplaceModInt mul(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.multiply(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt mod(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.mod(value, other.value, value); return this; }

   // exponentiation
   public ByteArrayInplaceModInt sqr()                               { CryptoByteArrayArithmetic.square(value, mod.value, value); return this; }
   public ByteArrayInplaceModInt exp(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.exponent(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt exp(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.exponent(value, other.value, mod.value, value); return this; }
   public ByteArrayInplaceModInt inv()                               { CryptoByteArrayArithmetic.invert(value, mod.value, value); return this; }
   public ByteArrayInplaceModInt gcd(ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.gcd(value, other.value, value); return this; }
   public ByteArrayInplaceModInt gcd(ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.gcd(value, other.value, value); return this; }

   // implementation-specific interface
   ByteArrayInplaceModInt(ByteArrayConstantBigInt mod, int minus_one_tag)
   {
      this.mod = mod;
      value = new byte[mod.value.length];
      CryptoByteArrayArithmetic.decrement(mod.value, mod.value, value);
      mod.fieldBitLength = CryptoByteArrayArithmetic.getNumBits(value);
      mod.fieldByteLength = (mod.fieldBitLength + 7) >> 3;
   }
}

