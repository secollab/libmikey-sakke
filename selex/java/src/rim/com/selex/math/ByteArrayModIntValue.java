package com.selex.math;

import net.rim.device.api.crypto.CryptoByteArrayArithmetic;
import com.selex.util.OctetString;
import com.selex.util.ByteRange;

public class ByteArrayModIntValue
{
   byte[] value;
   ByteArrayConstantBigInt mod;

   // tests
   public int compareTo(int z)                           { throw new RuntimeException("ByteArrayInplaceModInt::compareTo(int) currently not supported (isZero/isOne are)."); }
   public int compareTo(ByteArrayModIntValue other)      { return CryptoByteArrayArithmetic.compare(value, other.value); }
   public int compareTo(ByteArrayConstantBigInt other)   { return CryptoByteArrayArithmetic.compare(value, other.value); }
   public boolean equals(ByteArrayModIntValue other)     { if (this == other) return true; return CryptoByteArrayArithmetic.compare(value, other.value) == 0; }
   public boolean equals(ByteArrayConstantBigInt other)  { return CryptoByteArrayArithmetic.compare(value, other.value) == 0; }
   public boolean isZero()                               { return CryptoByteArrayArithmetic.isZero(value); }
   public boolean isOne()                                { return CryptoByteArrayArithmetic.isOne(value); }
   public boolean isOdd()                                { return (value[value.length-1] & 1) == 1; }
   public boolean isNaN()                                { return value == null; }
   public boolean testBit(int n)
   {
      final int off = value.length - 1 - (n >> 3);
      if (off < 0)
         return false;
      final int mask = 1 << (n & 0x7);
      return (value[off] & mask) != 0;
   }

   // conversion
   public int hashCode()
   {
      int rc = 7;
      for (int i = 0, e = value.length; i < e; i += 2)
         rc = rc * -13 + value[i];
      return rc;
   }
   public String toString()
   {
      if (value.length == mod.bytesInField())
         return OctetString.bytes_to_ascii_hex(value);
      return OctetString.bytes_to_ascii_hex(value, 1, value.length - 1);
   }
   public byte[] toByteArray()
   {
      byte[] rc = new byte[mod.bytesInField()];
      System.arraycopy(value, value.length - rc.length, rc, 0, rc.length);
      return rc;
   }
   public ByteRange peekFieldByteRange()
   {
      if (value.length == mod.bytesInField())
         return new ByteRange(value);
      return new ByteRange(value, 1, value.length - 1);
   }

   public int bits()             { return CryptoByteArrayArithmetic.getNumBits(value); }
   public int bytes()            { return (CryptoByteArrayArithmetic.getNumBits(value)+7) >> 3; }
   // public int bytes()         { return ModIntUtils.byteLength(value); } // may be faster?

   // retrieval
   public ByteArrayConstantBigInt modulus()  { return mod; }

   // addition into given target
   public ByteArrayInplaceModInt inc_into(ByteArrayInplaceModInt target)                                 { CryptoByteArrayArithmetic.increment(value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt dec_into(ByteArrayInplaceModInt target)                                 { CryptoByteArrayArithmetic.decrement(value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt add_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.add(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt sub_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.subtract(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt add_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.add(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt sub_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.subtract(value, other.value, target.mod.value, target.value); return target; }

   // multiplication into given target
   public ByteArrayInplaceModInt mul2_into(ByteArrayInplaceModInt target)                                { CryptoByteArrayArithmetic.multiplyByTwo(value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt mul_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.multiply(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt mod_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.mod(value, other.value, target.value); return target; }
   public ByteArrayInplaceModInt mul_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.multiply(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt mod_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.mod(value, other.value, target.value); return target; }

   // exponentiation into given target
   public ByteArrayInplaceModInt sqr_into(ByteArrayInplaceModInt target)                                 { CryptoByteArrayArithmetic.square(value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt exp_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.exponent(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt exp_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.exponent(value, other.value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt inv_into(ByteArrayInplaceModInt target)                                 { CryptoByteArrayArithmetic.invert(value, target.mod.value, target.value); return target; }
   public ByteArrayInplaceModInt gcd_into(ByteArrayInplaceModInt target, ByteArrayModIntValue other)     { CryptoByteArrayArithmetic.gcd(value, other.value, target.value); return target; }
   public ByteArrayInplaceModInt gcd_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt other)  { CryptoByteArrayArithmetic.gcd(value, other.value, target.value); return target; }

   // additive operations applied to constants storing the result in 'target'
   public static ByteArrayInplaceModInt inc_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a)                             { CryptoByteArrayArithmetic.increment(a.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt dec_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a)                             { CryptoByteArrayArithmetic.decrement(a.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt add_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.add(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt add_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.add(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt sub_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.subtract(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt sub_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.subtract(a.value, b.value, target.mod.value, target.value); return target; }

   // multiplicative operations applied to constants storing the result in 'target'
   public static ByteArrayInplaceModInt mul2_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a)                            { CryptoByteArrayArithmetic.multiplyByTwo(a.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt mul_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.multiply(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt mul_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.multiply(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt mod_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.mod(a.value, b.value, target.value); return target; }
   public static ByteArrayInplaceModInt mod_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.mod(a.value, b.value, target.value); return target; }

   // exponentiation operations applied to constants storing the result in 'target'
   public static ByteArrayInplaceModInt sqr_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a)                             { CryptoByteArrayArithmetic.square(a.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt exp_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.exponent(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt exp_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.exponent(a.value, b.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt inv_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a)                             { CryptoByteArrayArithmetic.invert(a.value, target.mod.value, target.value); return target; }
   public static ByteArrayInplaceModInt gcd_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayConstantBigInt b)  { CryptoByteArrayArithmetic.gcd(a.value, b.value, target.value); return target; }
   public static ByteArrayInplaceModInt gcd_into(ByteArrayInplaceModInt target, ByteArrayConstantBigInt a, ByteArrayModIntValue b)     { CryptoByteArrayArithmetic.gcd(a.value, b.value, target.value); return target; }
}
