package com.selex.adapt;

import java.nio.ByteBuffer;

public class ByteBufferExtras
{
   private static boolean TestBigEndian()
   {
      final byte[] be = new byte[] { 0x12, 0x34, 0x56, 0x78 };
      final byte[] test = new byte[be.length];
      ByteBuffer w = ByteBuffer.wrap(test);
      w.putInt(0x12345678);
      for (int i = 0, e = be.length; i < e; ++i)
         if (be[i] != test[i])
            return false;
      return true;
   }

   private static final boolean PlatformIsBigEndian = TestBigEndian();

   public static final ByteBuffer putLong(final ByteBuffer b, final long l)
   {
      if (PlatformIsBigEndian)
      {
         b.putInt((int) (l >>> 32));
         b.putInt((int) (l & 0xFFFFFFFFL));
      }
      else
      {
         b.putInt((int) (l & 0xFFFFFFFFL));
         b.putInt((int) (l >>> 32));
      }
      return b;
   }
   public static final long getLong(final ByteBuffer b)
   {
      long rc;
      if (PlatformIsBigEndian)
      {
         rc = (b.getInt() & 0xFFFFFFFFL) << 32;
         rc |= b.getInt() & 0xFFFFFFFFL;
      }
      else
      {
         rc = b.getInt() & 0xFFFFFFFFL;
         rc |= (b.getInt() & 0xFFFFFFFFL) << 32;
      }
      return rc;
   }
}
