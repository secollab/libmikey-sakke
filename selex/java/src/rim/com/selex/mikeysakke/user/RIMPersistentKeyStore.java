package com.selex.mikeysakke.user;

// Legacy Java containers supporting persistence on Blackberry
// platform and the RIM secure Hashtable for private keys. 
import java.util.Hashtable;
import java.util.Vector;
import net.rim.device.api.util.ContentProtectedHashtable;

// Java 6 containers for compliance with interface
import java.util.Collection;
import java.util.ArrayList;

import com.selex.util.OctetString;

import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;
import net.rim.device.api.util.Persistable;

public final class RIMPersistentKeyStore implements KeyStorage
{
   private static RIMPersistentKeyStore instance;
   private Hashtable keyMaterial;
   private Hashtable publicParameters;
   private Hashtable publicKeys;
   private ContentProtectedHashtable privateKeys;
   private Vector communities;
   private PersistentObject po;
   private static final String UNIQUE_PERSISTANCE_PATH = "com.selex_es.mikeysakke.user.RIMPersistentKeyStore";

   private void ResetKeyMaterial()
   {
      keyMaterial = new Hashtable();
      keyMaterial.put("publicParameters", publicParameters = new Hashtable());
      keyMaterial.put("publicKeys", publicKeys = new Hashtable());
      keyMaterial.put("privateKeys", privateKeys = new ContentProtectedHashtable());
      keyMaterial.put("communities", communities = new Vector());
   }

   private RIMPersistentKeyStore()
   {
      po = PersistentStore.getPersistentObject(UNIQUE_PERSISTANCE_PATH.hashCode());
      if (po.getContents() != null && po.getContents() instanceof Hashtable)
      {
         keyMaterial = (Hashtable) po.getContents();
         publicParameters = (Hashtable) keyMaterial.get("publicParameters");
         publicKeys = (Hashtable) keyMaterial.get("publicKeys");
         privateKeys = (ContentProtectedHashtable) keyMaterial.get("privateKeys");
         communities = (Vector) keyMaterial.get("communities");
      }
      else ResetKeyMaterial();
   }

   public static final synchronized RIMPersistentKeyStore instance()
   {
      if (instance == null)
         instance = new RIMPersistentKeyStore();
      return instance;
   }

   public OctetString GetPrivateKey(String identifier, String key)
   {  
      return FetchKeyOrDefault(privateKeys, identifier, key);
   }
   public OctetString GetPublicKey(String identifier, String key)
   {
      return FetchKeyOrDefault(publicKeys, identifier, key);
   }
   public String GetPublicParameter(String identifier, String key)
   {
      return FetchParamOrDefault(publicParameters, identifier, key);
   }
   public void StorePrivateKey(String identifier, String key, OctetString value)
   {
      Store(privateKeys, identifier, key, value);
   }
   public void StorePublicKey(String identifier, String key, OctetString value)
   {
      Store(publicKeys, identifier, key, value);
   }
   public void StorePublicParameter(String identifier, String key, String value)
   {
      Store(publicParameters, identifier, key, value);
   }
   public void AddCommunity(String community)
   {
      communities.addElement(community);
   }
   public Collection GetCommunityIdentifiers()
   {
      ArrayList r = new ArrayList();
      for (int i = 0, e = communities.size(); i != e; ++i)
         r.add((String) communities.elementAt(i));
      return r;
   }

   public synchronized void commit()
   {
      synchronized (po)
      {
         po.setContents(keyMaterial);
         po.commit();
      }
   }

   public synchronized void reset()
   {
      synchronized (po)
      {
         ResetKeyMaterial();
         po.setContents(keyMaterial);
         po.commit();
      }
   }

   public void RevokeKeys(String identifier)
   {
      privateKeys.remove(identifier);
      publicKeys.remove(identifier);
      publicParameters.remove(identifier);
      communities.removeElement(identifier);
      commit();
   }

   public void Purge()
   {
      reset();
   }

   private static void Store(Hashtable map, String p, String s, OctetString v)
   {
      Store(map, p, s, v.toString());
   }
   private static void Store(Hashtable map, String p, String s, String v)
   {
      Hashtable sec = (Hashtable) map.get(p);
      if (sec == null) {
         sec = new Hashtable();
         map.put(p, sec);
      }
      sec.put(s, v);
   }
   private static void Store(ContentProtectedHashtable map, String p, String s, String v)
   {
      ContentProtectedHashtable sec = (ContentProtectedHashtable) map.get(p);
      if (sec == null) {
         sec = new ContentProtectedHashtable();
         map.put(p, sec);
      }
      sec.put(s, v);
   }
   private static OctetString FetchKeyOrDefault(Hashtable map, String p, String s)
   {
      return new OctetString(FetchParamOrDefault(map, p, s), OctetString.Untranslated);
   }
   private static String FetchParamOrDefault(Hashtable map, String p, String s)
   {
      Hashtable sec = (Hashtable) map.get(p);
      if (sec != null)
      {
         String r = (String) sec.get(s);
         if (r != null)
            return r;
      }
      return "";
   }
}

