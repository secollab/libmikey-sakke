package com.selex.mikeysakke.crypto.hash;

import com.selex.util.ByteRange;
import com.selex.util.OctetString;
import java.io.UnsupportedEncodingException;

public class SHA256Digest
{
   private net.rim.device.api.crypto.SHA256Digest sha256
     = new net.rim.device.api.crypto.SHA256Digest();

   public SHA256Digest digest(byte[] octets, int offset, int length)
   {
      sha256.update(octets, offset, length);
      return this;
   }

   public SHA256Digest digest(ByteRange r)
   {
      sha256.update(r.bytes, r.off, r.len);
      return this;
   }

   public SHA256Digest digest(byte[] octets)
   {
      sha256.update(octets, 0, octets.length);
      return this;
   }

   public SHA256Digest digest(OctetString octets)
   {
      sha256.update(octets.raw(), 0, octets.size());
      return this;
   }
   
   public SHA256Digest digest(String s)
   {
      try
      {
         return digest(s.getBytes("ISO8859-1"));
      }
      catch (UnsupportedEncodingException e) { throw new RuntimeException(e.toString()); }
   }

   public SHA256Digest sync(byte[] result, int off)
   {
      assert(result.length - off >= 32);
      sha256.getDigest(result, off, /*resetDigest=*/true);
      return this;
   }
   public SHA256Digest sync(byte[] result) { return sync(result, 0); }
}
