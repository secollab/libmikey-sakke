package com.selex.mikeysakke.crypto;

import com.selex.util.RandomGenerator;
import net.rim.device.api.crypto.RandomSource;

public class SecureRandomGenerator extends RandomGenerator
{
   public void randomize(final byte[] octets)
   {
      RandomSource.getBytes(octets);
   }

   public void randomize(final byte[] octets, final int offset, final int N)
   {
      RandomSource.getBytes(octets, offset, N);
   }

   public int randomInt()
   {
      return RandomSource.getInt();
   }
}

