package com.selex.test.ui;

import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;

import java.io.PrintStream;
import java.io.OutputStream;
import java.util.ArrayList;

import javolution.text.TypeFormat;

import com.selex.log.DefaultLogger;
import com.selex.util.OctetString;

public abstract class SimpleUI extends MIDlet implements Runnable, TestUI, CommandListener
{
   private Form form = null;
   private PrintStream stdout = DefaultLogger.out;
   private PrintStream stderr = DefaultLogger.err;
   private Thread thread = null;

   private void setup()
   {
      DefaultLogger.out = new PrintStream(new OutputStream()
         {
            public void write(byte[] b, int off, int len)
            {
               buf.concat(len, b, off);
               flush();
            }
            public void write(int b)
            {
               buf.concat((byte) b);
               if (b == 10)
                  flush();
            }
            public void flush()
            {
               //Display.getDisplay(SimpleUI.this).setCurrentItem(form.get(
                  form.append(buf.toString())
               //))
               ;
               buf.clear();
            }
            OctetString buf = new OctetString();
         });
      DefaultLogger.err = DefaultLogger.out;
      if (com.selex.util.PlatformUtils.isCLDC())
      {
         DefaultLogger.info = DefaultLogger.nowhere;
         DefaultLogger.verb = DefaultLogger.nowhere;
      }
      else
      {
         DefaultLogger.info = DefaultLogger.out;
         DefaultLogger.verb = DefaultLogger.out;
      }

      form = new Form("Test UI");

      Display.getDisplay(this).setCurrent(form);
   }

   public void startApp()
   {
      if (form == null)
         setup();

      if (params.isEmpty())
         runTest();

      Display.getDisplay(this).setCurrent(params.get(0));
   }

   private void runTest()
   {
      try
      {
         if (thread == null)
         {
            thread = new Thread(this);
            thread.start();
         }
      }
      catch (Exception e)
      {
         form.append("Thread start failure: " + e);
      }
   }

   public boolean hasConsole() { return false; }

   static final Command NextParam = new Command("Next", Command.OK, 1);

   ArrayList<Displayable> params = new ArrayList<Displayable>();
   ArrayList<String> args = new ArrayList<String>();

   public void addParameter(String prompt, UserInputType type, String defaultText)
   {
      Displayable d;
      if (type == UserInputType.Boolean)
      {
         ChoiceGroup cg = new ChoiceGroup(null, ChoiceGroup.EXCLUSIVE,
              new String[] { "No", "Yes" }, null);
         cg.setSelectedIndex(TypeFormat.parseBoolean(defaultText)?1:0, true);
         d = new Form(prompt, new Item[] {cg});
      }
      else
         d = new TextBox(prompt, defaultText, 1024, 
              type == UserInputType.Password? TextField.PASSWORD :
              type == UserInputType.Integer? TextField.NUMERIC :
              type == UserInputType.URL? TextField.URL : TextField.ANY);
      d.addCommand(NextParam);
      d.setCommandListener(this);
      params.add(d);
   }

   public void commandAction(Command command, Displayable displayable)
   {
      if (command == NextParam && !params.isEmpty())
      {
         Displayable d = params.get(0);
         if (d instanceof TextBox)
            args.add(((TextBox) d).getString());
         else if (d instanceof Form)
            args.add(((ChoiceGroup) ((Form) d).get(0)).getSelectedIndex()==1? "true" : "false");
         params.remove(0);
         if (params.isEmpty())
         {
            Display.getDisplay(this).setCurrent(form);
            runTest();
         }
         else
            Display.getDisplay(this).setCurrent(params.get(0));
      }
   }

   public void run()
   {
      try
      {
         int rc = test(args.toArray(new String[args.size()]));
         form.append("Program completed with return code " + rc);
      }
      catch (Exception e)
      {
         form.append("Oops: " + e);
      }
      thread = null;
   }

   public void pauseApp()
   {
      form.deleteAll();
      thread = null;
   }

   public void destroyApp(boolean unconditional)
   {
      thread = null;
      notifyDestroyed();
      DefaultLogger.out = stdout;
      DefaultLogger.err = stderr;
   }
}

