package com.selex.net.ssl;

import com.selex.log.DefaultLogger;

public class CertificateUtils
{
   public static void trustAllCertificates()
   {
      DefaultLogger.err.println("Bypassing SSL signature verification not supported in J2ME.  Use keytool to import the certificates for this required sites.");
   }
   public static void disableHostnameVerification()
   {
      DefaultLogger.err.println("Disabling SSL hostname verification not supported in J2ME.  Use keytool to import the certificates for the required sites.");
   }
   public static boolean isCertificateException(Exception e)
   {
      return e instanceof javax.microedition.pki.CertificateException;
   }
}

