package com.selex.net;

import java.io.InputStream;
import javax.microedition.io.Connector;
import javax.microedition.io.HttpsConnection;

public class HttpsConnectionImpl implements com.selex.net.HttpsConnection
{
   private HttpsConnection c;

   public HttpsConnectionImpl(String url) throws java.io.IOException
   {
      c = (HttpsConnection) Connector.open(url);
   }

   // request params
   public void setRequestProperty(String property, String value) throws java.io.IOException
   {
      c.setRequestProperty(property, value);
   }

   // make request and access response
   public InputStream openDataInputStream() throws java.io.IOException
   {
      return c.openDataInputStream();
   }

   // response meta-data
   public int getResponseCode() throws java.io.IOException
   {
      return c.getResponseCode();
   }
   public long getLength()
   {
      return c.getLength();
   }

   public void close() throws java.io.IOException
   {
      c.close();
   }
}

