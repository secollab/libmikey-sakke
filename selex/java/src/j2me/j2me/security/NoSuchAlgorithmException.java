package j2me.security;

public class NoSuchAlgorithmException extends RuntimeException
{
   public NoSuchAlgorithmException(String s) { super(s); }
}

