package j2me.security.cert;

import java.io.InputStream;

public class CertificateFactory
{
   public static CertificateFactory getInstance(String s) { throw new RuntimeException("java.security.cert.CertificateFactory not supported."); }
   public Certificate generateCertificate(InputStream in) { return null; }

   private CertificateFactory() {}
}
