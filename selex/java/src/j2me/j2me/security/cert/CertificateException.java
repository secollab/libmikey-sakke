package j2me.security.cert;

public class CertificateException extends RuntimeException
{
   public CertificateException(String s) { super(s); }
}

