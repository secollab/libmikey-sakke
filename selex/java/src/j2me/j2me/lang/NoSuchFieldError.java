package j2me.lang;

public class NoSuchFieldError extends RuntimeException
{
   public NoSuchFieldError(String s) { super(s); }
}

