package ch.imvs.mikey4j.MIKEYPayload;

import ch.imvs.mikey4j.MIKEYException.MIKEYException;
import java.security.cert.X509Certificate;

public class MIKEYPayloadCERT extends MIKEYPayload
{
   public final static int MIKEYPAYLOAD_CERT_PAYLOAD_TYPE = 7;

   public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3 = 0;
   public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3URL = 1;
   public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3SIGN = 2;
   public final static int MIKEYPAYLOAD_CERT_TYPE_X509V3ENCR = 3;

   public MIKEYPayloadCERT(int type, X509Certificate cert) { certData(); }
   public MIKEYPayloadCERT(byte[] bytes, int offset, int len) { certData(); }
   public void writeData(byte[] raw_data, int offset, int expectedLength) throws MIKEYException
   {
      throw new MIKEYException("MIKEYPayloadCERT -- Certificate-based key exchange not supported on J2ME");
   }
   public byte[] certData()
   {
      throw new RuntimeException("MIKEYPayloadCERT -- Certificate-based key exchange not supported on J2ME");
   }
   public int length() { return 4; }
}
