package ch.imvs.mikey4j.MIKEYMessage;

import ch.imvs.mikey4j.KeyAgreement.KeyAgreement;
import ch.imvs.mikey4j.KeyAgreement.KeyAgreementType;
import ch.imvs.mikey4j.MIKEYException.MIKEYException;

public class MIKEYMessageDH extends MIKEYMessage
{
   public MIKEYMessageDH()
   {
      throw new MIKEYException("DH: Certificate-based key-exchange not supported on J2ME");
   }

   public void parseResponse(KeyAgreement ka, int maxOffsetSeconds) {}
   public void setOffer(KeyAgreement ka, int maxOffsetSeconds) {}
   public MIKEYMessage buildResponse(KeyAgreement ka) { return null; }
   public boolean authenticate(KeyAgreement ka) { return false; }
   public KeyAgreementType keyAgreementType() { return KeyAgreementType.DH; }
}

