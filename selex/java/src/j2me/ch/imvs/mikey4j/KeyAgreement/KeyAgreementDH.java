package ch.imvs.mikey4j.KeyAgreement;

import ch.imvs.mikey4j.MIKEYMessage.MIKEYMessage;
import ch.imvs.mikey4j.util.Certificate;
import java.security.cert.X509Certificate;
import java.util.List;

public class KeyAgreementDH extends KeyAgreement
{
   public KeyAgreementDH(List<Certificate> signingCertificate)
   {
      throw new RuntimeException("Certificate-based key agreement not supported on J2ME");
   }
   public String getFriendlyName()     { return "DH-Not-Supported"; }
   public KeyAgreementType type()      { return KeyAgreementType.DH; }
   public MIKEYMessage createMessage() { return null; }
   public boolean controlPeerCertificate(String peerUri) { return false; }
   public List<X509Certificate> getPeerCertificateChain() { return null; }
   public List<Certificate> certificateChain() { return null; }
   public void computeTgk() {}
   public void setGroup(int g) {}

   private KeyAgreementDH() {}
}

