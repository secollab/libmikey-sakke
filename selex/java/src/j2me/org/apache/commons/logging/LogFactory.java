/*
 * Minimal compatibility interface for
 * org.apache.commons.logging.LogFactory
 * necessary to build MIKEY4J for j2me.
 */ 

package org.apache.commons.logging;

import com.selex.log.DefaultLogger;

public class LogFactory
{
   static final DefaultLogger defaultLogger = new DefaultLogger();

   public static Log getLog(Class c)
   {
      return defaultLogger;
   }
}

