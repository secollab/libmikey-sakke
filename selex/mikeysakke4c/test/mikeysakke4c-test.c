#include <mikeysakke4c.h>
#include <stdio.h>

void fdumphex(FILE*, void*, size_t);

int main(int argc, char** argv)
{
   char const* kms_server = "10.50.74.101:7070";
   char const* user_uri = "tel:+44770090012";
   char const* kms_username = "jim";
   char const* kms_password = "jimpass";
   char const* keystore_uri = "runtime:empty"; // user, try keydir:./some/path

   // iterate over arguments
   {
      int i = 0;
      if (argc > ++i && *argv[i]) kms_server = argv[i];
      if (argc > ++i && *argv[i]) user_uri = argv[i];
      if (argc > ++i && *argv[i]) kms_username = argv[i];
      if (argc > ++i && *argv[i]) kms_password = argv[i];
      if (argc > ++i && *argv[i]) keystore_uri = argv[i];
   }


   // test KMS client
   {
      mikey_sakke_key_material* keys = mikey_sakke_alloc_key_material(keystore_uri);

      if (keys == 0)
      {
         fprintf(stderr, "Failed to create key store: %s\n", mikey_sakke_get_last_error());
         return 1;
      }

      bool keys_retrieved_and_validated =
         mikey_sakke_fetch_and_validate_keys(kms_server,
                                             /*verify_ssl_certificate=*/false,
                                             kms_username,
                                             kms_password,
                                             user_uri,
                                             keys);

      if (!keys_retrieved_and_validated)
         fprintf(stderr, "Failed KMS sync: %s\n", mikey_sakke_get_last_error());

      mikey_sakke_free_key_material(keys);
   }


   // Note that for checking against spec; bob happens to have same
   // phone number as alice.
   char const alice_uri[] = "tel:+447700900123";
   char const bob_uri[] = "tel:+447700900123";

   mikey_sakke_key_material* alice_keys = mikey_sakke_alloc_key_material("test:alice");
   mikey_sakke_key_material* bob_keys = mikey_sakke_alloc_key_material("test:bob");

   // cause any URI, date stamp, or random number required internally
   // to use the 6507, 6508 and 6509 test vectors.  The date (2011-02)
   // is the only significant override for the purpose of this test.
   // The 'test:...' key material generated above for Alice and Bob is
   // keyed on that date.
   mikey_sakke_always_overwrite_with_test_vectors(true);

   // run validation of Alice's signing keys in order to cache HS
   // required for signing in the test below.
   if (!mikey_sakke_validate_signing_keys(alice_uri, alice_keys))
      fprintf(stderr, "Alice signing keys validation failed: %s\n", mikey_sakke_get_last_error());

   mikey_sakke_user* alice = mikey_sakke_alloc_user(alice_uri, alice_keys);
   mikey_sakke_user* bob = mikey_sakke_alloc_user(bob_uri, bob_keys);

   // Alice starts a call...
   mikey_sakke_call* alice_outgoing = mikey_sakke_alloc_call(alice);
   mikey_sakke_add_sender_stream(alice_outgoing, 0xcafebabe);

   // ...creating a MIKEY SAKKE OFFER for Bob
   // (this alone is sufficient to protect the call at Alice's UA)
   mikey_key_mgmt_string init = mikey_sakke_uac_init(alice_outgoing, bob_uri);
   fprintf(stderr, "Alice sends '%s'\n", init.ptr);

   // Bob receives the call...
   mikey_sakke_call* bob_incoming = mikey_sakke_alloc_call(bob);
   mikey_sakke_add_sender_stream(bob_incoming, 0xdeadbeef);

   // Bob authenticates the MIKEY SAKKE OFFER from Alice
   bool init_auth = mikey_sakke_uas_auth(bob_incoming, init, alice_uri);
   fprintf(stderr, "Bob authenticates and decrypts - result = %d\n", init_auth);

   fprintf(stderr, "Bob considers the call %s\n", (mikey_sakke_call_is_secured(bob_incoming)? "secure" : "NOT SECURE"));

   // Bob responds in order to update Alice's CSID map with his SRTP
   // SSRC (0xdeadbeef).  This is not protected information necessary
   // for key exchange.  It is simply to ensure that Alice knows what
   // CSID index to use to generate her SRTP traffic decryption keys.
   mikey_key_mgmt_string resp = mikey_sakke_uas_resp(bob_incoming);
   fprintf(stderr, "Bob send CSID map update to Alice '%s'\n", resp.ptr);

   // Alice updates her CSID with the HDR-only MIKEY SAKKE ANSWER from Bob
   bool resp_auth = mikey_sakke_uac_auth(alice_outgoing, resp);

   fprintf(stderr, "Alice updates local CSID map - result = %d\n", resp_auth);

   fprintf(stderr, "Alice considers the call %s\n", (mikey_sakke_call_is_secured(alice_outgoing)? "secure" : "NOT SECURE"));


   uint16_t tek_len, salt_len, tag_len;

   // Alice keys her SRTP streams
   mikey_sakke_query_crypto_params(alice_outgoing, &tek_len, &salt_len, &tag_len);
   uint8_t alice_tx_csid = mikey_sakke_get_csid_for_stream(alice_outgoing, 0xcafebabe);
   uint8_t alice_rx_csid = mikey_sakke_get_csid_for_stream(alice_outgoing, 0xdeadbeef);
   
   uint8_t* srtp_key_pack = (uint8_t*) malloc(tek_len + salt_len);

   fprintf(stderr, "Alice encrypting SRTP CS %d with: ", alice_tx_csid);
   mikey_sakke_gen_tek(alice_outgoing, alice_tx_csid, srtp_key_pack, tek_len);
   mikey_sakke_gen_salt(alice_outgoing, alice_tx_csid, srtp_key_pack + tek_len, salt_len);
   fdumphex(stderr, srtp_key_pack, tek_len + salt_len);
   fputs("\n", stderr);

   fprintf(stderr, "Alice decrypting SRTP CS %d with: ", alice_rx_csid);
   mikey_sakke_gen_tek(alice_outgoing, alice_rx_csid, srtp_key_pack, tek_len);
   mikey_sakke_gen_salt(alice_outgoing, alice_rx_csid, srtp_key_pack + tek_len, salt_len);
   fdumphex(stderr, srtp_key_pack, tek_len + salt_len);
   fputs("\n", stderr);

   free(srtp_key_pack);


   // Bob keys his SRTP streams
   mikey_sakke_query_crypto_params(bob_incoming, &tek_len, &salt_len, &tag_len);
   uint8_t bob_tx_csid = mikey_sakke_get_csid_for_stream(bob_incoming, 0xdeadbeef);
   uint8_t bob_rx_csid = mikey_sakke_get_csid_for_stream(bob_incoming, 0xcafebabe);

   srtp_key_pack = (uint8_t*) malloc(tek_len + salt_len);

   fprintf(stderr, "Bob encrypting SRTP CS %d with: ", bob_tx_csid);
   mikey_sakke_gen_tek(bob_incoming, bob_tx_csid, srtp_key_pack, tek_len);
   mikey_sakke_gen_salt(bob_incoming, bob_tx_csid, srtp_key_pack + tek_len, salt_len);
   fdumphex(stderr, srtp_key_pack, tek_len + salt_len);
   fputs("\n", stderr);

   fprintf(stderr, "Bob decrypting SRTP CS %d with: ", bob_rx_csid);
   mikey_sakke_gen_tek(bob_incoming, bob_rx_csid, srtp_key_pack, tek_len);
   mikey_sakke_gen_salt(bob_incoming, bob_rx_csid, srtp_key_pack + tek_len, salt_len);
   fdumphex(stderr, srtp_key_pack, tek_len + salt_len);
   fputs("\n", stderr);

   free(srtp_key_pack);
   

   mikey_sakke_free_key_mgmt_string(resp);
   mikey_sakke_free_key_mgmt_string(init);
   mikey_sakke_free_call(bob_incoming);
   mikey_sakke_free_call(alice_outgoing);
   mikey_sakke_free_user(bob);
   mikey_sakke_free_user(alice);
   mikey_sakke_free_key_material(bob_keys);
   mikey_sakke_free_key_material(alice_keys);
}


void fdumphex(FILE* f, void* d, size_t n)
{
   uint8_t const* i = (uint8_t*) d, *e = d + n;
   for (; i < e; ++i)
      fprintf(f, "%02x", *i);
}

