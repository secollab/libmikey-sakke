#include <mikeysakke4c.h>

#include <mskms/client.h>
#include <mskms/flat-file-key-storage.inl>

#include <mscrypto/eccsi.h>

#include <libmikey/Mikey.h>
#include <libmikey/KeyAgreementSAKKE.h>

#include <util/printable.inl>
#include <boost/algorithm/string.hpp>

#if !MIKEYSAKKE4C_SINGLE_THREAD
#include <pthread.h>
#endif


using namespace MikeySakkeKMS;
using namespace MikeySakkeCrypto;


namespace {


struct null_deleter { void operator() (void const*) const {} };

inline KeyStoragePtr       from_c(mikey_sakke_key_material* p) { return KeyStoragePtr(reinterpret_cast<KeyStorage*>(p), null_deleter()); }
inline MRef<IMikeyConfig*> from_c(mikey_sakke_user* p) { return MRef<IMikeyConfig*>(reinterpret_cast<IMikeyConfig*>(p)); }
inline MRef<Mikey*>        from_c(mikey_sakke_call* p) { return MRef<Mikey*>(reinterpret_cast<Mikey*>(p)); }

inline mikey_sakke_key_material* to_c(KeyStorage* p) { return reinterpret_cast<mikey_sakke_key_material*>(p); }
inline mikey_sakke_user*         to_c(MRef<IMikeyConfig*> const& p) { return reinterpret_cast<mikey_sakke_user*>(*p); }
inline mikey_sakke_call*         to_c(MRef<Mikey*> const& p) { return reinterpret_cast<mikey_sakke_call*>(*p); }

inline mikey_key_mgmt_string to_key_mgmt_string(std::string const& s)
{
   mikey_key_mgmt_string rc = { (char*) std::malloc(s.length()+1), s.length() };
   std::memcpy(rc.ptr, s.data(), rc.len);
   rc.ptr[rc.len] = 0;
   return rc;
}
inline std::string from_key_mgmt_string(mikey_key_mgmt_string const& s)
{
   return std::string(s.ptr, s.ptr + s.len);
}


#if !MIKEYSAKKE4C_SINGLE_THREAD
pthread_key_t last_error_tss_key;
pthread_once_t last_error_tss_once = PTHREAD_ONCE_INIT;
void free_thread_specific_string(std::string* s)
{
   delete s;
}
void make_last_error_tss_key()
{
   pthread_key_create(&last_error_tss_key, (void(*)(void*)) free_thread_specific_string);
}
std::string& last_error()
{
   pthread_once(&last_error_tss_once, make_last_error_tss_key);
   if (void* p = pthread_getspecific(last_error_tss_key))
      return *reinterpret_cast<std::string*>(p);

   std::string* s = new std::string;
   pthread_setspecific(last_error_tss_key, s);
   return *s;
}
#else
std::string& last_error() { static std::string e; return e; }
#endif


void set_alice_and_bob_community_params(KeyStorage* keys)
{
   std::string community = "aliceandbob.co.uk";

   OctetString communityPublicAuthenticationKey = OctetString::skipws
      ("04                                 "
       "50D4670B DE75244F 28D2838A 0D25558A"
       "7A72686D 4522D4C8 273FB644 2AEBFA93"
       "DBDD3755 1AFD263B 5DFD617F 3960C65A"
       "8C298850 FF99F203 66DCE7D4 367217F4");

   OctetString communityPublicKey = OctetString::skipws
      ("04                                 "
       "5958EF1B 1679BF09 9B3A030D F255AA6A"
       "23C1D8F1 43D4D23F 753E69BD 27A832F3"
       "8CB4AD53 DDEF4260 B0FE8BB4 5C4C1FF5"
       "10EFFE30 0367A37B 61F701D9 14AEF097"
       "24825FA0 707D61A6 DFF4FBD7 273566CD"
       "DE352A0B 04B7C16A 78309BE6 40697DE7"
       "47613A5F C195E8B9 F328852A 579DB8F9"
       "9B1D0034 479EA9C5 595F47C4 B2F54FF2"
       "                                   "
       "1508D375 14DCF7A8 E143A605 8C09A6BF"
       "2C9858CA 37C25806 5AE6BF75 32BC8B5B"
       "63383866 E0753C5A C0E72709 F8445F2E"
       "6178E065 857E0EDA 10F68206 B63505ED"
       "87E534FB 2831FF95 7FB7DC61 9DAE6130"
       "1EEACC2F DA3680EA 4999258A 833CEA8F"
       "C67C6D19 487FB449 059F26CC 8AAB655A"
       "B58B7CC7 96E24E9A 39409575 4F5F8BAE");
   
   keys->AddCommunity(community);
   keys->StorePublicKey(community, "KPAK", communityPublicAuthenticationKey);
   keys->StorePublicKey(community, "Z", communityPublicKey);
   keys->StorePublicParameter(community, "SakkeSet", "1");
}


OctetString::Translation const raw = OctetString::Untranslated;

KeyStorage* make_alice_key_store()
{
   KeyStorage* keys = new RuntimeKeyStorage;

   std::string alice_uri = "tel:+447700900123";
   OctetString alice_id;
   alice_id.concat("2011-02",raw).concat(0).concat(alice_uri,raw).concat(0);

   keys->StorePrivateKey(alice_id, "SSK", OctetString::skipws
      ("23F374AE 1F4033F3 E9DBDDAA EF20F4CF"
       "0B86BBD5 A138A5AE 9E7E006B 34489A0D"));

   keys->StorePublicKey(alice_id, "PVT", OctetString::skipws
      ("04                                 "
       "758A1427 79BE89E8 29E71984 CB40EF75"
       "8CC4AD77 5FC5B9A3 E1C8ED52 F6FA36D9"
       "A79D2476 92F4EDA3 A6BDAB77 D6AA6474"
       "A464AE49 34663C52 65BA7018 BA091F79"));

   set_alice_and_bob_community_params(keys);

   return keys;
}

KeyStorage* make_bob_key_store()
{
   KeyStorage* keys = new RuntimeKeyStorage;

   std::string bob_uri = "tel:+447700900123";
   OctetString bob_id;
   bob_id.concat("2011-02",raw).concat(0).concat(bob_uri,raw).concat(0);

   keys->StorePrivateKey(bob_id, "RSK", OctetString::skipws
      ("04                                 "
       "93AF67E5 007BA6E6 A80DA793 DA300FA4"
       "B52D0A74 E25E6E7B 2B3D6EE9 D18A9B5C"
       "5023597B D82D8062 D3401956 3BA1D25C"
       "0DC56B7B 979D74AA 50F29FBF 11CC2C93"
       "F5DFCA61 5E609279 F6175CEA DB00B58C"
       "6BEE1E7A 2A47C4F0 C456F052 59A6FA94"
       "A634A40D AE1DF593 D4FECF68 8D5FC678"
       "BE7EFC6D F3D68353 25B83B2C 6E69036B"
       "                                   "
       "155F0A27 241094B0 4BFB0BDF AC6C670A"
       "65C325D3 9A069F03 659D44CA 27D3BE8D"
       "F311172B 55416018 1CBE94A2 A783320C"
       "ED590BC4 2644702C F371271E 496BF20F"
       "588B78A1 BC01ECBB 6559934B DD2FB65D"
       "2884318A 33D1A42A DF5E33CC 5800280B"
       "28356497 F87135BA B9612A17 26042440"
       "9AC15FEE 996B744C 33215123 5DECB0F5"));

   set_alice_and_bob_community_params(keys);

   return keys;
}


} // anon


mikey_sakke_key_material* mikey_sakke_alloc_key_material(char const* keystore_uri)
{
   using boost::starts_with;
   using boost::equals;

   if (starts_with(keystore_uri, "keydir:"))
      return to_c(new FlatFileKeyStorage(keystore_uri + 7));
   if (equals(keystore_uri, "runtime:empty"))
      return to_c(new RuntimeKeyStorage);
   if (starts_with(keystore_uri, "test:"))
   {
      if (equals(keystore_uri + 5, "alice"))
         return to_c(make_alice_key_store());
      if (equals(keystore_uri + 5, "bob"))
         return to_c(make_bob_key_store());
   }
   std::string& e = last_error();
   e = "Unsupported keystore URI '"; e += keystore_uri; e += "'";
   return 0;
}

void mikey_sakke_purge_key_material(mikey_sakke_key_material* keys)
{
   if (keys) from_c(keys)->Purge();
}

void mikey_sakke_free_key_material(mikey_sakke_key_material* keys)
{
   if (keys) delete from_c(keys).get();
}


char const* mikey_sakke_get_last_error()
{
   std::string& e = last_error();
   if (e.empty())
      return "No error";
   return e.c_str();
}


bool mikey_sakke_fetch_and_validate_keys(char const* kms_server,
                                         bool verify_ssl_certificate,
                                         char const* kms_username,
                                         char const* kms_password,
                                         char const* user_uri,
                                         mikey_sakke_key_material* keystore)
{
   struct Handler
   {
      static void HandleFetchResult(KeyStoragePtr keys,
                                    std::string const& identifier,
                                    sys::error_code error,
                                    std::string const& error_text,
                                    std::string* error_msg)
      {
         if (error)
         {
            (*error_msg) = "Failed to fetch keys for identifier '" + make_printable(identifier) + "': " + error.message() + ": " + error_text;
            return;
         }

         std::string details;
         if (!KeyAgreementSAKKE::ValidateKeyMaterial(keys, identifier, &details))
            (*error_msg) = "Key material invalid for '" + make_printable(identifier) + "', keys revoked: " + details;
      }
   };

   KeyStoragePtr keys = from_c(keystore);

   Client client(keys);

   std::string& error_msg = last_error();

   error_msg.clear();

   using namespace std::placeholders;

   std::string sakke_id = KeyAgreementSAKKE::ToSakkeIdentifier(user_uri);

   client.FetchKeyMaterial(kms_server,
                           verify_ssl_certificate
                            ? Client::VerifySSLCertificate
                            : Client::DontVerifySSLCertificate,
                           kms_username,
                           kms_password,
                           sakke_id,
                           std::bind(Handler::HandleFetchResult, keys, _1, _2, _3, &error_msg));

   return error_msg.empty();
}

bool mikey_sakke_validate_signing_keys(char const* user_uri, mikey_sakke_key_material* keystore)
{
   std::string identifier = KeyAgreementSAKKE::ToSakkeIdentifier(user_uri);
   KeyStoragePtr keys = from_c(keystore);

   std::vector<std::string> communities = keys->GetCommunityIdentifiers();

   std::string& e = last_error(); e.clear();

   // FIXME: currently assuming only first community in use
   if (communities.empty())
   {
      e = "Signing parameters incomplete for '" + make_printable(identifier) + "': No communities defined.";
      return false;
   }

   e.clear();
   std::string details;
   if (!ValidateSigningKeysAndCacheHS(identifier, communities[0], keys))
      e = "Signing keys invalid for '" + make_printable(identifier) + "', keys revoked.";

   return e.empty();
}

mikey_sakke_user* mikey_sakke_alloc_user(char const* uri, mikey_sakke_key_material* keys)
{
   struct MikeyUserConfig : public IMikeyConfig
   {
      MikeyUserConfig(char const* uri, mikey_sakke_key_material* keys) : uri(uri) , keys(from_c(keys)) {}

      KeyAccessPtr getKeys() const { return keys; }

      const std::string getUri() const { return uri; }

      MRef<SipSim*> getSim() const { return 0; }
      MRef<CertificateChain*> getPeerCertificate() const { return 0; }
      size_t getPskLength() const { return 0; }
      const byte_t* getPsk() const { return 0; }

      bool isMethodEnabled(int method) const { return method == KEY_AGREEMENT_TYPE_SAKKE; }

      bool isCertCheckEnabled() const { return false; }

      std::string uri;
      MikeySakkeKMS::KeyAccessPtr keys;
   };
   MRef<IMikeyConfig*> rc(new MikeyUserConfig(uri, keys));
   rc->incRefCount();
   return to_c(rc);
}

void mikey_sakke_free_user(mikey_sakke_user* user)
{
   if (user) from_c(user)->decRefCount();
}


mikey_sakke_call* mikey_sakke_alloc_call(mikey_sakke_user* user)
{
   MRef<Mikey*> rc(new Mikey(from_c(user)));
   rc->incRefCount();
   return to_c(rc);
}

void mikey_sakke_free_call(mikey_sakke_call* call)
{
   if (call) from_c(call)->decRefCount();
}


void mikey_sakke_free_key_mgmt_string(mikey_key_mgmt_string s)
{
   free(s.ptr);
}


void mikey_sakke_add_sender_stream(mikey_sakke_call* call, uint32_t ssrc)
{
   from_c(call)->addSender(ssrc);
}

mikey_key_mgmt_string mikey_sakke_uac_init(mikey_sakke_call* call, char const* to_uri)
{
   return to_key_mgmt_string(from_c(call)->initiatorCreate(KEY_AGREEMENT_TYPE_SAKKE, to_uri));
}

bool mikey_sakke_uas_auth(mikey_sakke_call* call, mikey_key_mgmt_string init, char const* from_uri)
{
   MRef<Mikey*> mikey = from_c(call);
   if (mikey->responderAuthenticate(from_key_mgmt_string(init), from_uri))
   {
      mikey->setMikeyOffer();
      return true;
   }
   std::string& e = last_error();
   e = "Responder authentication failed from '"; e += from_uri; e += +"'.";
   return false;
}

mikey_key_mgmt_string mikey_sakke_uas_resp(mikey_sakke_call* call)
{
   MRef<Mikey*> mikey = from_c(call);
   std::string resp_payload = mikey->responderParse();
   if (mikey->error())
   {
      mikey_key_mgmt_string error = { 0, 0 };
      return error;
   }
   return to_key_mgmt_string("mikey "+resp_payload);
}

bool mikey_sakke_uac_auth(mikey_sakke_call* call, mikey_key_mgmt_string resp)
{
   MRef<Mikey*> mikey = from_c(call);
   if (mikey->initiatorAuthenticate(from_key_mgmt_string(resp)))
   {
      last_error() = mikey->initiatorParse();
      return last_error().empty();
   }
   last_error() = "Initiator authentication failed.";
   return false;
}


bool mikey_sakke_call_is_secured(mikey_sakke_call* call)
{
   return from_c(call)->isSecured();
}


uint8_t mikey_sakke_get_csid_for_stream(mikey_sakke_call* call, uint32_t ssrc)
{
   MRef<MikeyCsIdMap*> csIdMap = from_c(call)->getKeyAgreement()->csIdMap();
   if (MikeyCsIdMapSrtp* srtpMap = dynamic_cast<MikeyCsIdMapSrtp*>(*csIdMap))
      return srtpMap->findCsId(ssrc);
   return 0;
}

bool mikey_sakke_query_crypto_params(mikey_sakke_call*, uint16_t* tek_len, uint16_t* salt_len, uint16_t* tag_len)
{
   // XXX: currently only support AES_CM_128_HMAC_SHA1_80
   *tek_len = 16;
   *salt_len = 14;
   *tag_len = 10;
   return true;
}

void mikey_sakke_gen_tek(mikey_sakke_call* call, uint8_t csid, uint8_t* tek, uint16_t tek_len)
{
   from_c(call)->getKeyAgreement()->genTek(csid, tek, tek_len);
}

void mikey_sakke_gen_salt(mikey_sakke_call* call, uint8_t csid, uint8_t* salt, uint16_t salt_len)
{
   from_c(call)->getKeyAgreement()->genSalt(csid, salt, salt_len);
}


void mikey_sakke_always_overwrite_with_test_vectors(bool b)
{
   KeyAgreementSAKKE::EnableGlobalTestVectors(b);
}

namespace MikeySakkeCrypto {
   extern bool SAKKE_generate_fake;
}

void mikey_sakke_use_fake_sed(bool b)
{
   MikeySakkeCrypto::SAKKE_generate_fake = b;
}

