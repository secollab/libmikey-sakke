#ifndef MIKEYSAKKE4C_H
#define MIKEYSAKKE4C_H

#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>

#if __cplusplus
extern "C" {
#endif

typedef struct mikey_sakke_key_material mikey_sakke_key_material;
typedef struct mikey_sakke_user mikey_sakke_user;
typedef struct mikey_sakke_call mikey_sakke_call;
typedef struct mikey_key_mgmt_string { char* ptr; size_t len; } mikey_key_mgmt_string;

// keystore uri example: keydir:/var/run/some-key-dir
mikey_sakke_key_material*  mikey_sakke_alloc_key_material(char const* keystore_uri);
void                       mikey_sakke_purge_key_material(mikey_sakke_key_material*);
void                       mikey_sakke_free_key_material(mikey_sakke_key_material*);

// returns the most recent MIKEY SAKKE error string for the calling
// thread.  The returned pointer is valid until another function
// defined in this header is invoked in the calling thread.
char const*                mikey_sakke_get_last_error();

bool                       mikey_sakke_fetch_and_validate_keys(char const* kms_server,
                                                               bool verify_ssl_certificate,
                                                               char const* kms_username,
                                                               char const* kms_password,
                                                               char const* user_uri,
                                                               mikey_sakke_key_material* keystore);

bool                       mikey_sakke_validate_signing_keys(char const* user_uri, mikey_sakke_key_material* keystore);
bool                       mikey_sakke_validate_encapsulation_keys(char const* user_uri, mikey_sakke_key_material* keystore);

mikey_sakke_user*          mikey_sakke_alloc_user(char const* uri, mikey_sakke_key_material*);
void                       mikey_sakke_free_user(mikey_sakke_user*);

mikey_sakke_call*          mikey_sakke_alloc_call(mikey_sakke_user*);
void                       mikey_sakke_free_call(mikey_sakke_call*);

void                       mikey_sakke_free_key_mgmt_string(mikey_key_mgmt_string);

void                       mikey_sakke_add_sender_stream(mikey_sakke_call*, uint32_t ssrc);
mikey_key_mgmt_string      mikey_sakke_uac_init(mikey_sakke_call*, char const* to_uri);
bool                       mikey_sakke_uas_auth(mikey_sakke_call*, mikey_key_mgmt_string init, char const* from_uri);
mikey_key_mgmt_string      mikey_sakke_uas_resp(mikey_sakke_call*);
bool                       mikey_sakke_uac_auth(mikey_sakke_call*, mikey_key_mgmt_string resp);

bool                       mikey_sakke_call_is_secured(mikey_sakke_call*);

uint8_t                    mikey_sakke_get_csid_for_stream(mikey_sakke_call*, uint32_t ssrc);
bool                       mikey_sakke_query_crypto_params(mikey_sakke_call*, uint16_t* tek_len, uint16_t* salt_len, uint16_t* tag_len);
void                       mikey_sakke_gen_tek(mikey_sakke_call*, uint8_t csid, uint8_t* tek, uint16_t tek_len);
void                       mikey_sakke_gen_salt(mikey_sakke_call*, uint8_t csid, uint8_t* salt, uint16_t salt_len);

void                       mikey_sakke_always_overwrite_with_test_vectors(bool);
void                       mikey_sakke_use_fake_sed(bool);

#if __cplusplus
}
#endif

#endif//MIKEYSAKKE4C_H

