#ifndef UTILITIES_CONVENIENCE_CPTR_H
#define UTILITIES_CONVENIENCE_CPTR_H

namespace convenience
{
   template <typename T> struct cptr_deref;
   template <typename T> struct cptr_pointee_traits;

   template <typename T>
   class cptr_base
   {
   public:

      typedef cptr_pointee_traits<T> traits;
      typedef typename traits::pointer pointer;
      typedef typename traits::free_result (*deallocate_t) (typename traits::free_pointer);

      cptr_base(pointer p) : p(p) {}
      #if USING_CPLUSPLUS0X
      cptr_base(cptr_base&& old) : p(old.p) { old.release(); }
      #endif

      pointer operator->() const { return p; }
      operator pointer() const { return p; }
      typename cptr_deref<pointer>::result operator*() const { return cptr_deref<pointer>::apply(p); }
      pointer release() { pointer rc = p; p = 0; return rc; }

   protected: // update ops

      pointer& ref() { return p; }
      pointer set(pointer p) { pointer rc = this->p; this->p = p; return rc; }

   public: // move emulation

      #if !USING_CPLUSPLUS0X
      struct reference_holder { cptr_base* ref; };
      cptr_base(reference_holder const& old) : p(old.ref->p) { old.ref->release(); }
      reference_holder move() const { reference_holder rc = {const_cast<cptr_base*>(this)}; return rc; }
      #endif

   private:

      pointer p;

   private:

      #if USING_CPLUSPLUS0X
      cptr_base(cptr_base const&) = delete;
      cptr_base& operator=(cptr_base const&) = delete;
      #else
      public:  cptr_base(cptr_base const&);  // link error if used
      private: cptr_base& operator=(cptr_base const&);
      #endif
   };

   #if USING_CPLUSPLUS0X
   #define IF_CXX11(x) x
   #define NOT_CXX11(x)
   #else
   #define IF_CXX11(x)
   #define NOT_CXX11(x) x
   #define cptr cptr_ct
   #endif

   template <typename T, typename cptr_pointee_traits<T>::free_result (*deallocate) (typename cptr_pointee_traits<T>::free_pointer) IF_CXX11(= nullptr)>
   class cptr : public cptr_base<T>
   {
   public:
      cptr(typename cptr::pointer p = 0) : cptr_base<T>(p) {}
      ~cptr() { if (typename cptr::pointer p = *this) deallocate(p); }
      IF_CXX11( cptr(cptr&&) = default; )

      typename cptr::pointer* zero_ref() { if (typename cptr::pointer p = *this) { deallocate(p); this->set(0); } return &this->ref(); }

   protected:
      NOT_CXX11( typedef cptr_ct cptr_type; )
   };

   #if USING_CPLUSPLUS0X
   #define cptr_rt_specialization cptr<T, nullptr>
   #else
   #undef cptr
   #define cptr cptr_rt
   #define cptr_rt_specialization cptr_rt
   #endif

   template <typename T>
   class cptr_rt_specialization : public cptr_base<T>
   {
   public:
      cptr(typename cptr::pointer p, typename cptr::deallocate_t deallocate) : cptr_base<T>(p), deallocate(deallocate) {}
      cptr(typename cptr::deallocate_t deallocate) : cptr_base<T>(0), deallocate(deallocate) {}
      ~cptr() { if (typename cptr::pointer p = *this) deallocate(p); }
      IF_CXX11( cptr(cptr&&) = default; )

      typename cptr::pointer* zero_ref() { if (typename cptr::pointer p = *this) { deallocate(p); this->set(0); } return &this->ref(); }

   protected:
      NOT_CXX11( typedef cptr_rt cptr_type; )

   private:
      typename cptr::deallocate_t deallocate;
   };


   #if !USING_CPLUSPLUS0X
   #undef cptr
   // Begin workaround for non C++11 compilers.  For C++11 the above
   // two class templates cptr_ct, and cptr_rt are implemented by the
   // cptr template.  The compile time deallocating cptr is
   // implemented as the generic case with the 'deallocate' template
   // parameter defaulting to 'nullptr'.  A specialization of cptr for
   // 'deallocate' being 'nullptr' implements the runtime variant.
   // The two distinct classes and the horrendous meta programming
   // noise below are completely unnecessary in C++11.
   // 
   // Note that this implementation will work equally well on C++11
   // compilers, just that it is unnecessary noise and complexity.
   //
   template <typename T>
   typename cptr_pointee_traits<T>::free_result defer_deallocate(typename cptr_pointee_traits<T>::free_pointer);

   template <bool B, typename T, typename F> struct select { typedef T result; };
   template <typename T, typename F> struct select<false, T, F> { typedef F result; };

   template <typename T
            ,typename cptr_pointee_traits<T>::free_result (*a) (typename cptr_pointee_traits<T>::free_pointer)
            ,typename cptr_pointee_traits<T>::free_result (*b) (typename cptr_pointee_traits<T>::free_pointer)
            > struct check_equal { static const bool value = false; };
   template <typename T
            ,typename cptr_pointee_traits<T>::free_result (*a) (typename cptr_pointee_traits<T>::free_pointer)
            > struct check_equal<T,a,a> { static const bool value = true; };

   template <typename T, typename cptr_pointee_traits<T>::free_result (*deallocate) (typename cptr_pointee_traits<T>::free_pointer) = defer_deallocate<T> >
   struct cptr : select<check_equal<T,deallocate,defer_deallocate<T> >::value, cptr_rt<T>, cptr_ct<T, deallocate> >::result
   {
      template <typename P> cptr(P p) : cptr::cptr_type(p) {}
      template <typename P, typename F> cptr(P p, F f) : cptr::cptr_type(p, f) {}
   };
   //
   // End of non-C++11 workaround
   #endif


   template <typename T> struct cptr_deref<T*>
   {
      typedef T& result;
      static result apply(T* p) { return *p; }
   };
   template <> struct cptr_deref<void*>
   {
      typedef struct error {} result;
      static result apply(void*) { return error(); }
   };

   template <typename T> struct cptr_pointee_traits
   {
      typedef T* pointer;
      typedef T* free_pointer;
      typedef void free_result;
   };
   template <typename T, typename U> struct cptr_pointee_traits<T(U)>
   {
      typedef T* pointer;
      typedef U* free_pointer;
      typedef void free_result;
   };
   template <typename T> struct cptr_pointee_traits<T()>
   {
      typedef T* pointer;
      typedef void* free_pointer;
      typedef void free_result;
   };
   template <typename T, typename U> struct cptr_pointee_traits<T(U())>
   {
      typedef T* pointer;
      typedef void* free_pointer;
      typedef U free_result;
   };
   template <typename T, typename U, typename V> struct cptr_pointee_traits<T(U(V))>
   {
      typedef T* pointer;
      typedef V* free_pointer;
      typedef U free_result;
   };


}

#if !USING_CPLUSPLUS0X
namespace std
{
   template <typename T, typename convenience::cptr_pointee_traits<T>::free_result
                    (*D)(typename convenience::cptr_pointee_traits<T>::free_pointer)>
   typename convenience::cptr<T,D>::reference_holder
   move(convenience::cptr<T,D> const& p) { return p.move(); }
}
#endif

#endif//UTILITIES_CONVENIENCE_CPTR_H
