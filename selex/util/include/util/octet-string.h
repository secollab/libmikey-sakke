#ifndef UTIL_OCTET_STRING_H
#define UTIL_OCTET_STRING_H

#include <util/std.h>
#include <vector>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <cstring>


struct OctetString
{
   enum Translation
   {
      Untranslated,
      AsciiBase16,
      //AsciiBase64,
   };

   OctetString() {}
   OctetString(std::string const& ascii_hex) { assign(ascii_hex, AsciiBase16); }
   OctetString(std::string const& s, Translation t) { assign(s, t); }
   OctetString(size_t n, uint8_t const* p) : octets(p,p+n) {}
   OctetString(size_t n) : octets(n) {}

   std::string translate(Translation t = AsciiBase16) const
   {
      if (t == AsciiBase16)
      {
         std::ostringstream oss;
         oss << *this;
         return oss.str();
      }

      if (t == Untranslated)
         return untranslated();

      return std::string();
   }
   std::string untranslated() const
   {
      return std::string(reinterpret_cast<char const*>(raw()), size());
   }
   operator std::string() const
   {
      return untranslated();
   }
   friend std::ostream& operator<< (std::ostream& out, OctetString const& v)
   {
      using namespace std;
      ios::fmtflags fmt(out.flags());
      out << hex << setfill('0');
      for (uint8_t const* p = v.raw(), *e = p + v.size(); p < e; ++p)
         out << setw(2) << +*p;
      out.flags(fmt);
      return out;
   }

   static void throw_not_ascii_hex()
   {
      throw std::invalid_argument("Character not in [0-9A-Fa-f] when attempting translation.");
   }
   static uint8_t from_ascii_hex(char c)
   {
      return c >= '0' && c <= '9' ? (c-'0')
           : c >= 'a' && c <= 'f' ? (10+c-'a')
           : c >= 'A' && c <= 'F' ? (10+c-'A')
           : (throw_not_ascii_hex(), 15);
   }

   void deposit_bigendian(uint8_t* out, size_t len)
   {
      size_t this_len = size();
      if (len < this_len)
         throw std::range_error("OctetString to large to deposit into given region");

      if (this_len < len)
      {
         len -= this_len; // len ==> prefix-len
         std::memset(out, 0, len);
         std::memcpy(out + len, raw(), this_len);
      }
      else
         std::memcpy(out, raw(), this_len);
   }

   OctetString& clear()
   {
      octets.clear();
      return *this;
   }

   bool empty() const
   {
      return octets.empty();
   }

   OctetString& assign(std::string const& s, Translation t = AsciiBase16)
   {
      octets.clear();
      return concat(s, t);
   }

   OctetString& assign(size_t n, uint8_t* p)
   {
      octets.assign(p, p+n);
      return *this;
   }

   static std::string skipws(std::string s)
   {
      size_t o = s.find_first_of("\t\r\n ");
      if (o == std::string::npos)
         return s;
      for (size_t i = o+1, e = s.size(); i != e; ++i)
      {
         switch (s[i])
         {
            case '\t': case '\r': case '\n': case ' ':
               continue;
            default:
               s[o++] = s[i];
         }
      }
      s.resize(o);
      return s;
   }

   OctetString& concat(OctetString const& other)
   {
      octets.insert(octets.end(), other.octets.begin(), other.octets.end());
      return *this;
   }
   OctetString& concat(std::string const& s, Translation t = AsciiBase16)
   {
      if (t == Untranslated)
      {
         octets.insert(octets.end(), s.begin(), s.end());
      }
      else if (t == AsciiBase16)
      {
         size_t b = octets.size();
         size_t n = s.length();
         octets.resize(b + (n+1) / 2);
         std::string::const_iterator in = s.begin(), end = s.end();
         std::vector<uint8_t>::iterator out = octets.begin() + b;
         if (n&1)
            *out++ = from_ascii_hex(*in++);
         while (in < end)
         {
            uint8_t o = from_ascii_hex(*in++) << 4;
            o |= from_ascii_hex(*in++);
            *out++ = o;
         }
      }
      return *this;
   }
   OctetString& concat(size_t octet_count, uint8_t const* octets)
   {
      this->octets.insert(this->octets.end(), octets, octets + octet_count);
      return *this;
   }
   OctetString& concat(uint8_t octet)
   {
      octets.push_back(octet);
      return *this;
   }

   bool operator== (OctetString const& other) const
   {
      return octets == other.octets;
   }

   uint8_t const* raw() const { if (octets.empty()) return 0; return &octets.at(0); }
   uint8_t* raw() { if (octets.empty()) return 0; return &octets.at(0); }
   size_t size() const { return octets.size(); }

   void swap(OctetString& other)
   {
      octets.swap(other.octets);
   }

   std::vector<uint8_t> octets;
};


#endif//UTIL_OCTET_STRING_H

