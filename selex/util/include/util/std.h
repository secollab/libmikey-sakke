#ifndef MSKMS_CONFIG_H
#define MSKMS_CONFIG_H

#include <string>

#if __ANDROID__
typedef unsigned long long uint64_t;
#endif

#if __cplusplus >= 201103L

#include <memory>
#include <functional>
#include <cstdint>

#elif _CPPLIB_VER // dinkumware

#include <boost/function.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
namespace std {
   using boost::function;
   using boost::bind;
   using boost::shared_ptr;
   using boost::enable_shared_from_this;
   namespace placeholders {
      using ::_1; using ::_2; using ::_3;
      using ::_4; using ::_5; using ::_6;
      using ::_7; using ::_8; using ::_9;
   }
}

#else // XXX: workaround for non C++11 compilers that provide TR1

#include <tr1/memory>
#include <tr1/functional>
#include <stdint.h>
namespace std { using namespace tr1;  }

#endif

#endif//MSKMS_CLIENT_CONFIG_H

