#if 0
ROOT=$(dirname $0)/../../../
${CROSS_PREFIX}g++ -D TEST_BIGINT_SSL $(sh $ROOT/build/test-flags.sh $0) $@
exit $?
#endif

#include <util/bigint-ssl.h>
#if !BIGINT_SINGLE_THREAD
#include <pthread.h>
#endif

#if TEST_BIGINT_SSL
#include <iostream>
#define TRACE(x) std::cerr << x << "\n"
#else
#define TRACE(x)
#endif

#if !BIGINT_SINGLE_THREAD
static pthread_key_t ssl_ctx_key;
static pthread_once_t ssl_ctx_key_once = PTHREAD_ONCE_INIT;
static void free_thread_specific_ctx(bigint_ssl_scratch* scratch)
{
   delete scratch;
}
static void ssl_ctx_make_key()
{
   pthread_key_create(&ssl_ctx_key, (void(*)(void*)) free_thread_specific_ctx);
}
#endif

bigint_ssl_scratch& bigint_ssl_scratch::get()
{
 #if BIGINT_SINGLE_THREAD
   static bigint_ssl_scratch s;
   return s;
 #else
   pthread_once(&ssl_ctx_key_once, ssl_ctx_make_key);
   if (void* p = pthread_getspecific(ssl_ctx_key))
      return *reinterpret_cast<bigint_ssl_scratch*>(p);

   bigint_ssl_scratch* scratch = new bigint_ssl_scratch;
   pthread_setspecific(ssl_ctx_key, scratch);
   return *scratch;
 #endif
}

bigint_ssl_scratch::bigint_ssl_scratch()
 : ctx(BN_CTX_new())
{
   TRACE("Create TSS context");
}
bigint_ssl_scratch::~bigint_ssl_scratch()
{
   BN_CTX_free(ctx);
   TRACE("Free TSS context");
}



bigint& to_bigint(bigint& out, BIGNUM const* in)
{
   bn_check_top(in);

   // Optimistically hope that component word size is
   // compatible between OpenSSL and GMP ...
   if ((sizeof in->d[0] * 8 == GMP_NUMB_BITS) && (BN_BITS2 == GMP_NUMB_BITS))
   {
      mpz_ptr mpz = out.get_mpz_t();

      if (!_mpz_realloc(mpz, in->top))
         throw std::runtime_error("Could not reallocate mpz to hold given BIGNUM.");

      std::memcpy(mpz->_mp_d, in->d, in->top * sizeof in->d[0]);
      mpz->_mp_size = in->top;
      if (in->neg)
         mpz->_mp_size = -mpz->_mp_size;

      return out;
   }

   TRACE("Warning: using slow octet conversion for BIGNUM -> mpz");

   // ... if word sizes differ go via octet representation (slower)
   //
   uint8_t octets[BN_num_bytes(in)];
   BN_bn2bin(in, octets);
   mpz_import(out.get_mpz_t(), sizeof octets, 1, 1, 1, 0, octets);
   
   // XXX: erase 'octets' securely?  Maybe pass user param.
   return out;
}


BIGNUM* to_BIGNUM(BIGNUM* out, bigint const& in)
{
   // Optimistically hope that component word size is
   // compatible between OpenSSL and GMP ...
   if ((sizeof out->d[0] * 8 == GMP_NUMB_BITS) && (BN_BITS2 == GMP_NUMB_BITS))
   {
      mpz_srcptr mpz = in.get_mpz_t();

      bool neg;
      int size = mpz->_mp_size;
      if (size < 0)
      {
         neg = true;
         size = -size;
      }
      else
         neg = false;

      BN_zero(out); // may speed up expand
      if (bn_expand2(out, size) == 0)
         throw std::runtime_error("Could not expand BIGNUM to hold given mpz.");

      out->top = size;
      std::memcpy(out->d, mpz->_mp_d, size * sizeof out->d[0]);
      bn_correct_top(out);
      out->neg = neg;
      return out;
   }

   TRACE("Warning: using slow octet conversion for mpz -> BIGNUM");

   // ... if word sizes differ go via octet representation (slower)
   //
   size_t octet_count = (bits(in)+7) >> 3;
   uint8_t octets[octet_count];
   mpz_export(octets, 0, 1, 1, 1, 0, in.get_mpz_t());
   BN_bin2bn(octets, octet_count, out);

   // XXX: erase 'octets' securely?  Maybe pass user param.
   return out;
}


#if TEST_BIGINT_SSL
#include <iostream>
#include <gmp-impl.h>
void* test_thread(void*)
{
   BN_CTX* ctx = bigint_ssl_scratch::get();
   ctx = bigint_ssl_scratch::get();
   (void) ctx;
   return 0;
}
int main()
{
   mpz_t xxx;
   mpz_init(xxx);

   BN_CTX* ctx = bigint_ssl_scratch::get();
   ctx = bigint_ssl_scratch::get();
   (void) ctx;

   std::cout << BYTES_PER_MP_LIMB << std::endl;

   pthread_t t1; pthread_create(&t1, 0, test_thread, 0);
   pthread_t t2; pthread_create(&t2, 0, test_thread, 0);

   pthread_join(t1, 0);
   pthread_join(t2, 0);

   OctetString xs("1234567890abcdef123456");
   OctetString ys("f0123456789abcdef01234");

   bigint x = as_bigint(xs);
   bigint y = as_bigint(ys);

   bigint z = x + y;

   std::cout << "x: " << x << "\n";
   std::cout << "y: " << y << "\n";
   std::cout << "z: " << z << "\n";
   std::cout << "x: " << as<OctetString>(x) << "\n";
   std::cout << "y: " << as<OctetString>(y) << "\n";
   std::cout << "z: " << as<OctetString>(z) << "\n";

   bigint_ssl xbn_froms = as_bigint_ssl(xs);
   bigint_ssl ybn_froms = as_bigint_ssl(ys);

   std::cout << "xbn_froms: " << xbn_froms << "\n";
   std::cout << "ybn_froms: " << ybn_froms << "\n";

   bigint_ssl xbn_frombigint = as_bigint_ssl(x);
   bigint_ssl ybn_frombigint = as_bigint_ssl(y);

   std::cout << "xbn_frombigint: " << xbn_frombigint << "\n";
   std::cout << "ybn_frombigint: " << ybn_frombigint << "\n";

   bigint_ssl zbn;

   BN_add(zbn, xbn_frombigint, ybn_frombigint);

   std::cout << "zbn: " << zbn << "\n";

   std::cout << "xbn: " << as<OctetString>(xbn_froms) << "\n";
   std::cout << "ybn: " << as<OctetString>(ybn_froms) << "\n";
   std::cout << "zbn: " << as<OctetString>(zbn) << "\n";

   bigint x2 = as_bigint(xbn_froms);
   bigint y2 = as_bigint(ybn_froms);
   bigint z2 = as_bigint(zbn);

   std::cout << "x2: " << as<OctetString>(x2) << "\n";
   std::cout << "y2: " << as<OctetString>(y2) << "\n";
   std::cout << "z2: " << as<OctetString>(z2) << "\n";

}
#endif // TEST_BIGINT_SSL
