#if 0
set -x
TARGET_SYSTEM=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)
${CROSS_PREFIX}g++ $0 -o ${CROSS_PREFIX}${0%.cpp} ${CXXFLAGS} -O4 -s -g0 -L $GMP/$TARGET_SYSTEM/.libs -I $GMP/$TARGET_SYSTEM -I $GMP -Wl,-Bstatic -lgmpxx -lgmp -Wl,-Bdynamic -I$(dirname $0)/../include
exit $?
#endif

#include <gmpxx.h>

#include <util/bigint.h>

#include <iostream>
#include <iomanip>

int main()
{
   size_t n = 0;
   mpz_class x;
   bigint y(x);
   y += 15;

   mpz_class a("0f30 4020", 16);
   std::ios::fmtflags f = std::cout.flags();
   std::cout << std::hex << a << "\n";
   std::cout.flags(f);
   std::cout << bits(a) << "\n";
   std::cout << as<OctetString>(a) << "\n";
   std::cout << as<OctetString>(a).size() << "\n";

   a += bigint("70000000", 16);
   f = std::cout.flags();
   std::cout << std::hex << a << "\n";
   std::cout.flags(f);
   std::cout << bits(a) << "\n";
   std::cout << as<OctetString>(a) << "\n";
   std::cout << as<OctetString>(a).size() << "\n";

   a *= 7;
   f = std::cout.flags();
   std::cout << std::hex << a << "\n";
   std::cout.flags(f);
   std::cout << bits(a) << "\n";
   std::cout << as<OctetString>(a) << "\n";
   std::cout << as<OctetString>(a).size() << "\n";

   try {
      std::cout << as<OctetString>(a, 4) << "\n";
   } catch (std::range_error& e) {
      std::cout << "Caught range_error: " << e.what() << "\n";
   }
   std::cout << as<OctetString>(a, 6) << "\n";
   std::cout << "112233445566\n";

   OctetString s = as<OctetString>(a, 6);
   s.concat(s);
   std::cout << s << "\n";
   bigint i = as_bigint(s);
   std::cout << i << "\n";
   std::cout << as<OctetString>(i) << "\n";
   std::cout << as<OctetString>(i,12) << "\n";

   n = bits(y);

   std::cout << x << "\n";
   std::cout << y << "\n";
   std::cout << n << "\n";
}
