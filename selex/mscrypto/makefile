mscrypto/ := $(dir $(lastword $(MAKEFILE_LIST)))

include $(mscrypto/)../build/makefile.head

$(call requires-env,GMP BOOST OPENSSL)

target := $(if $(static),archive,$(if $(linux)$(msw),shared-object,archive))

$(target) := mscrypto
$(target).sources := \
	src/mscrypto/eccsi.cpp \
	src/mscrypto/sakke.cpp \
	src/mscrypto/ecc/curve-openssl.cpp \
	src/mscrypto/ecc/curve-gmp.cpp \
	src/mscrypto/parameter-set.cpp \
	../util/src/util/bigint-ssl.cpp \
	../util/src/util/bigint.cpp \
	../util/src/util/time-measure.cpp \

program := mscrypto-test
program.sources := \
	test/mscrypto-test.cpp

CXXFLAGS += $(if $(msw),,-fPIC)
CPPFLAGS += \
	-Wp,-isystem,$(BOOST) \
	-Wp,-isystem,$(GMP)/$(gcc.target) -Wp,-isystem,$(GMP) \
	-Wp,-isystem,$(OPENSSL)/include \
	$(if $(msw),-DBIGINT_SINGLE_THREAD) \
	-I$(basedir)/../util/include \
	-I$(basedir)/../mskms/client/include \

install-include: $(basedir)/../util/include

LDFLAGS += \
	-L$(GMP)/$(gcc.target)/.libs \
	$(if $(msw),-L$(GMP)/.libs -L$(OPENSSL)) \
	-L$(OPENSSL)/$(gcc.target) \
	-L$(BOOST)/$(CROSS_PREFIX)stage/lib \

mscrypto.LDLIBS := \
	$(if $(darwin),,-Wl,-Bstatic) \
		-lgmpxx -lgmp -lboost_date_time \
	$(if $(darwin),,-Wl,-Bdynamic) \
	-lcrypto \
	$(if $(android)$(msw)$(darwin)$(qnx),,-lrt -ldl) \
	$(if $(msw),-lwinmm -lgdi32) \

program.CPPFLAGS += -Dmscrypto_test=main
program.LDLIBS += -lmscrypto
program.LDFLAGS += \
	$(if $(android)$(qnx), -Wl$(,)-rpath-link$(,)$(OPENSSL)/$(gcc.target))

# If building the library as a shared object then the dependencies are
# statically packed into the library, if building the library as an
# object archive then the dependencies need to be specified to link
# the test program.
ifeq ($(target),shared-object)
shared-object.LDLIBS += $(mscrypto.LDLIBS)
else
program.LDLIBS += $(mscrypto.LDLIBS)
endif

include $(mscrypto/)../build/makefile.tail

