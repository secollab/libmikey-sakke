#ifndef MSCRYPTO_ECC_FWD_H
#define MSCRYPTO_ECC_FWD_H

#include <util/std.h>
#include <util/bigint.h> // FIXME: bigint is not opaque so can't be fwd'd

class bigint_ssl;

namespace MikeySakkeCrypto {
namespace ECC {

template <typename BigInt> class PrimeCurve;
template <typename BigInt> class Point;

typedef PrimeCurve<bigint>     PrimeCurveAffine;
typedef PrimeCurve<bigint_ssl> PrimeCurveJacobian;

typedef std::shared_ptr<const PrimeCurveJacobian> PrimeCurveJacobianPtr;
typedef std::shared_ptr<const PrimeCurveAffine>   PrimeCurveAffinePtr;

}} // MikeySakkeCrypto::ECC

#endif//MSCRYPTO_ECC_FWD_H

