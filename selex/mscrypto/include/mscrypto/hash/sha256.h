#ifndef MSCRYPTO_HASH_SHA256_H
#define MSCRYPTO_HASH_SHA256_H

#include <util/octet-string.h>
#include <mscrypto/erase.h>
#include <openssl/sha.h>
#include <cassert>

namespace MikeySakkeCrypto {

class InplaceSHA256Digest
{
public:

   InplaceSHA256Digest(OctetString& result)
      : result(result.raw())
      , insync(true)
   {
      assert(result.size() >= 32);
      SHA256_Init(&ctx);
   }
   InplaceSHA256Digest(uint8_t result[32])
      : result(result)
      , insync(true)
   {
      SHA256_Init(&ctx);
   }
   ~InplaceSHA256Digest()
   {
      if (!is_complete())
         complete();
   }

public:

   InplaceSHA256Digest& digest(uint8_t const* octets, size_t N)
   {
      assert(!is_complete());
      insync = false;
      SHA256_Update(&ctx, octets, N);
      return *this;
   }
   InplaceSHA256Digest& digest(OctetString const& octets)
   {
      assert(!is_complete());
      insync = false;
      SHA256_Update(&ctx, octets.raw(), octets.size());
      return *this;
   }
   InplaceSHA256Digest& digest(std::string const& s)
   {
      assert(!is_complete());
      insync = false;
      SHA256_Update(&ctx, s.c_str(), s.size());
      return *this;
   }
   InplaceSHA256Digest& sync()
   {
      assert(!is_complete());
      if (!insync)
      {
         SHA256_Final(result, &ctx);
         SHA256_Init(&ctx);
         insync = true;
      }
      return *this;
   }
   void complete()
   {
      assert(!is_complete());
      if (!insync)
      {
         SHA256_Final(result, &ctx);
         insync = true;
      }
      Erase(reinterpret_cast<uint8_t*>(&ctx), sizeof ctx);
      result = 0;
   }
   bool is_synchronized() const
   {
      return insync;
   }
   bool is_complete() const
   {
      return result == 0;
   }

private:

   SHA256_CTX ctx;
   uint8_t* result;
   bool insync;
};

struct OctetStringHolder
{
   OctetString octets;
   OctetStringHolder(size_t octets) : octets(octets) {}
};

class SHA256Digest : private OctetStringHolder, public InplaceSHA256Digest
{
public:
   SHA256Digest() : OctetStringHolder(32), InplaceSHA256Digest(octets.raw())
   {}
   ~SHA256Digest()
   {
      assert(is_complete());
   }
   
   OctetString const& str() const
   {
      assert(is_synchronized());
      return octets;
   }

   operator OctetString const& ()
   {
      return str();
   }
};

} // MikeySakkeCrypto

#endif//MSCRYPTO_HASH_SHA256_H

