#ifndef MSCRYPTO_ERASE_H
#define MSCRYPTO_ERASE_H

#include <util/octet-string.h>
#include <openssl/crypto.h>

namespace MikeySakkeCrypto {

inline void Erase(uint8_t* octets, size_t N)
{
   OPENSSL_cleanse(octets, N);
}
inline void Erase(OctetString& octets)
{
   Erase(octets.raw(), octets.size());
}

} // MikeySakkeCrypto

#endif//MSCRYPTO_ERASE_H

