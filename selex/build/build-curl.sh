cd $CURL

MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

mkdir -p $MACH
cd $MACH

if [ -z "$NO_CONFIGURE" ]
then
WITHOUT=$(../configure  --help | grep -- '--without' | awk '{print $1}' |
   grep -vE '(ssl|PACKAGE)')
DISABLE=$(../configure --help | grep -- '--disable.*support' | awk '{print $1}' |
   grep -vE '(http|file|ftp|proxy|cookies)')

if [ -n "$OPENSSL" ]
then
   mkdir -p openssl
   ln -sfn ${OPENSSL}/include openssl/include
   ln -sfn ${OPENSSL}/${MACH} openssl/lib

   export OPENSSL_CFLAGS=-I$PWD/openssl/include
   export OPENSSL_LIBS="-L$PWD/openssl/lib -lssl -lcrypto"
fi

[[ $MACH = *x86_64*gnu* ]] && LDFLAGS="$LDFLAGS -ldl"

echo -e '#include <openssl/ssl.h>\nint main(){return 0;}' | ${CC-${CROSS_PREFIX}gcc} -xc - $CPPFLAGS $CFLAGS $LDFLAGS $OPENSSL_CFLAGS $OPENSSL_LIBS -o /dev/null ||
   { echo >&2 "Set OPENSSL to root dir of openssl source."; exit 20; }

CPPFLAGS="$CPPFLAGS $OPENSSL_CFLAGS" \
LDFLAGS="$LDFLAGS $OPENSSL_LIBS" \
RANLIB=true \
../configure --host $MACH ${WITHOUT} ${DISABLE} --with-ssl=$PWD/openssl --disable-manual --enable-shared=no || exit $?
fi

make "$@"

