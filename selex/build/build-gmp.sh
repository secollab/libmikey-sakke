[ -n "$GMP" ] || { echo >&2 "GMP not defined."; exit 20; }

cd $GMP || exit $?

MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

mkdir -p $MACH
cd $MACH

if [ -z "$NO_CONFIGURE" ]; then

# fix-up compile in ARM thumb mode and with bionic
#
(cd .. && patch -f -p1 2>/dev/null <<'EOP')
--- gmp-5.0.4/longlong.h
+++ gmp/longlong.h
@@ -423,7 +423,7 @@
 	     "rIJ" ((USItype) (bl)))
 #endif
 
-#if defined (__arm__) && W_TYPE_SIZE == 32
+#if defined (__arm__) && W_TYPE_SIZE == 32 && !defined(__thumb__)
 #define add_ssaaaa(sh, sl, ah, al, bh, bl) \
   __asm__ ("adds\t%1, %4, %5\n\tadc\t%0, %2, %3"			\
 	   : "=r" (sh), "=&r" (sl)					\
@@ -468,7 +468,7 @@
 	       : "=r" (sh), "=&r" (sl)					\
 	       : "r" (ah), "rI" (bh), "r" (al), "rI" (bl) __CLOBBER_CC);\
     } while (0)
-#if 1 || defined (__arm_m__)	/* `M' series has widening multiply support */
+#if !defined (__thumb__)
 #define umul_ppmm(xh, xl, a, b) \
   __asm__ ("umull %0,%1,%2,%3" : "=&r" (xl), "=&r" (xh) : "r" (a), "r" (b))
 #define UMUL_TIME 5
--- gmp-5.0.4/cxx/ismpf.cc
+++ gmp/cxx/ismpf.cc
@@ -45,7 +45,11 @@
   bool ok = false;
 
   // C decimal point, as expected by mpf_set_str
+#if __ANDROID__ // bionic has a fake clocale
+  const char *lconv_point = ".";
+#else
   const char *lconv_point = localeconv()->decimal_point;
+#endif
 
   // C++ decimal point
 #if HAVE_STD__LOCALE
EOP

if [[ $MACH = *darwin* ]]; then
   # Fix up ARM asm for iPhone
   mkdir -p mpn/arm
   cp -afv ../mpn/arm/invert_limb.asm mpn/arm
   sed -i.bak 's/.section .rodata/.section	__TEXT,__const/' mpn/arm/invert_limb.asm

elif [[ $MACH = *arm* ]]; then
   # Force full ARM mode where thumb may be chosen by default (e.g.
   # android).  Dispatching to gmp allocation function SIGILL's in
   # thumb mode.
   CFLAGS="$CFLAGS -marm"
   CXXFLAGS="$CXXFLAGS -marm"
fi


# the intent is to link gmpxx and gmp directly into mscrypto, make
# sure it's objects are built position-independent.
CFLAGS="$CFLAGS -fPIC -O3" CXXFLAGS="$CXXFLAGS -Wp,-include,stddef.h,-include,stdarg.h -fPIC -O3" \
../configure --host $MACH --enable-cxx --enable-shared=no || exit $?

fi

make "$@"

