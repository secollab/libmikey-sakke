# echo standalone compile and link flags for single file tests

# assumes $1 is the main source file; implying the executable output
# prefixed by $CROSS_PREFIX to allow for one-off cross-compilation
#
echo -o$(dirname $1)/$CROSS_PREFIX$(basename ${1%.*}) $1

GCC_VER=$(${CC-${CROSS_PREFIX}gcc} -dumpversion)
GCC_MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

ROOT=$(dirname $(dirname $0))
echo -I$ROOT/mscrypto/include
echo -I$ROOT/mskms/client/include
echo -I$ROOT/util/include
echo -L$ROOT/mscrypto/output-gcc-$GCC_VER-$GCC_MACH/lib
echo -L$ROOT/mskms/client/output-gcc-$GCC_VER-$GCC_MACH/lib
echo $CFLAGS $CXXFLAGS $LDFLAGS
echo -W -Wall -Wextra
echo -ggdb -O0

echo -L$OPENSSL/$GCC_MACH
echo -I$OPENSSL/include
echo -I$GMP/$GCC_MACH
echo -I$GMP
echo -L$GMP/$GCC_MACH/.libs
echo -I$CURL
echo -I$BOOST
echo -L$BOOST/${CROSS_PREFIX}stage/lib
echo -lssl -lcrypto -lgmpxx -lgmp -pthread

