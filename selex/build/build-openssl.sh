[ -n "$OPENSSL" ] || { echo >&2 "OPENSSL not defined."; exit 20; }

[ $OPENSSL != builtin ] || { echo >&2 "Using built-in OPENSSL."; exit 0; }

BUILD_SCRIPTS_DIR="$(cd $(dirname $0) && pwd)"

cd $OPENSSL || exit $?

MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine) || exit $?

mkdir -p $MACH
cd $MACH || exit $?

if [ -z "$NO_CONFIGURE" ]; then

case $MACH in
  arm-*qnx*)       OPENSSL_TARGET=QNX6 ;;
  i486-*qnx*)      OPENSSL_TARGET=QNX6-i386 ;;
  *android*)       OPENSSL_TARGET=android ;;
  x86_64*linux*)   OPENSSL_TARGET=linux-x86_64 ;;
  i?86*linux*)     OPENSSL_TARGET=linux-generic32 ;;
  x86_64*darwin*)  OPENSSL_TARGET=darwin64-x86_64-cc ;;
  i?86*darwin*)    OPENSSL_TARGET=darwin-i386-cc ;;
  arm*darwin*)     OPENSSL_TARGET=iphoneos-cross ;;
  *)               OPENSSL_TARGET=OPENSSL_TARGET_UNKNOWN ;;
esac

# OpenSSL has non-standard configure scheme.  To support multiple
# build targets use lndir to link the non-output paths from the source
# directory.  Note: all output paths are machine tuples with
# hyphenated parts; no file in the top level openssl dir has hyphens
# so *-* is good enough to prevent linking in any output dirs.
#
find .. -mindepth 1 -maxdepth 1 -type d -not -name '*-*' -exec lndir2 -f '{}' ';'
find .. -mindepth 1 -maxdepth 1 -type f -not -name '*-*' -exec ln -sfn '{}' . ';'

# substitute syslog.h for QNX and link CRT start routines for Android
case $MACH in
  *qnx*)       cp -vf "$BUILD_SCRIPTS_DIR/platform/qnx/include/syslog.h" include ;;
  *android*)   ln -vs "$ANDROID_SYSROOT/usr/lib/crt"* . ;;
esac

sed -i.bak '/QNX6"/  s/TERMIOS:/TERMIOS -fPIC:/' ./Configure

CC=${CC-${CROSS_PREFIX}gcc} \
CFLAGS="$CFLAGS -fPIC -O3" CXXFLAGS="$CXXFLAGS -fPIC -O3" \
./Configure $OPENSSL_TARGET no-shared || exit $?

fi

make -C crypto "$@" && make -C ssl "$@"

