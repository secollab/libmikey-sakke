[ -n "$OPENCOREAMR" ] || { echo >&2 "OPENCOREAMR not defined."; exit 20; }

cd $OPENCOREAMR || exit $?

MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

mkdir -p $MACH
cd $MACH

if [ -z "$NO_CONFIGURE" ]; then

CFLAGS="$CFLAGS -fPIC -O3" CXXFLAGS="$CXXFLAGS -fPIC -O3" \
../configure --host $MACH --enable-shared=no || exit $?

if [[ $MACH = *qnx* ]]; then
sed -i.bak 's/ -std=c99/ /' */Makefile
fi
sed -i.bak '/AR/ !s/\<ar\>/$(AR)/' */Makefile

fi

make -C amrnb "$@" || exit $?

ln -sfn . $OPENCOREAMR/amrnb/opencore-amrnb

