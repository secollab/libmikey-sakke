#!/usr/bin/env bash

function log()
{
   echo >&2 "[33;1m$(date): [36m$@[0m"
}

[ -n "$NO_CONFIGURE" ] && NO_CLEAN=1
[ -n "$FORCE_BOOTSTRAP" ] && [ -n "$NO_CONFIGURE" ] && {
   echo >&2 "Conflicting options FORCE_BOOTSTRAP and NO_CONFIGURE."
   exit 20; }
[ -n "$FORCE_BOOTSTRAP" ] && [ -n "$NO_CLEAN" ] && {
   echo >&2 "Conflicting options FORCE_BOOTSTRAP and NO_CLEAN."
   exit 20; }

# when installing, don't overwrite target files if source has not
# changed.  Also, log the copy.
export CPPROG="cp -uv"
export INSTALL="install -c -p -v"

COMPILER_VER=gcc-$(${CC-${CROSS_PREFIX}gcc} -dumpversion)
TARGET_MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)
OUTPUT_SPEC=$COMPILER_VER-$TARGET_MACH
MINISIP_ROOT=${MINISIP-$(readlink -f $(dirname $0))}
BUILD_DIR="${TARGET_MACH}${BUILD_NAME+-$BUILD_NAME}"

if [[ $TARGET_MACH = *i?86* || $TARGET_MACH = *x86* ]]
then BUILD_DESKTOP=true
else BUILD_DESKTOP=false
fi

JOBS=1

while [ $# -gt 0 ]; do ARG=$1; shift; case $ARG in
   -j*)     JOBS=${ARG#-j} ;;
   -k)      KEEP_GOING=-k ;;
   install) if [[ $TARGET_MACH = *android* ]]
            then ANDROID_INSTALL=1;
            else LOCAL_INSTALL=1;
            fi ;;
esac; done


export OPENSSL_CFLAGS="${OPENSSL:+-I${OPENSSL}/include}"
export OPENSSL_LIBS="${OPENSSL:+-L${OPENSSL}/${TARGET_MACH}} -lssl -lcrypto"


if [ -n "$OPENCOREAMR" ]
then
   export OPENCOREAMR_CFLAGS="-I$OPENCOREAMR/amrnb"
   export OPENCOREAMR_LIBS="-L$OPENCOREAMR/$TARGET_MACH/amrnb/.libs"
fi

# add libmskms and libmscrypto
export LIBMIKEYSAKKE_ROOT="${LIBMIKEYSAKKE-$(readlink -f ${LIBMIKEYSAKKE_ROOT-${MINISIP_ROOT}/../libmikey-sakke/selex})}"
export LIBMIKEYSAKKE_CPPFLAGS="-I$LIBMIKEYSAKKE_ROOT/mskms/client/include -I$LIBMIKEYSAKKE_ROOT/mscrypto/include -I$LIBMIKEYSAKKE_ROOT/util/include"
export LIBMIKEYSAKKE_CXXFLAGS="-L$LIBMIKEYSAKKE_ROOT/mskms/client/output-${OUTPUT_SPEC}/lib -L$LIBMIKEYSAKKE_ROOT/mscrypto/output-${OUTPUT_SPEC}/lib"
export LIBMIKEYSAKKE_CXXFLAGS="$LIBMIKEYSAKKE_CXXFLAGS -Wl,-rpath,$LIBMIKEYSAKKE_ROOT/mskms/client/output-${OUTPUT_SPEC}/lib -Wl,-rpath,$LIBMIKEYSAKKE_ROOT/mscrypto/output-${OUTPUT_SPEC}/lib"
export LIBMIKEYSAKKE_CXXFLAGS="$LIBMIKEYSAKKE_CXXFLAGS -lmskms-client -lmscrypto"

export LIBMIKEYSAKKE_CPPFLAGS="$LIBMIKEYSAKKE_CPPFLAGS -I$BOOST"

export LIBMIKEYSAKKE_CPPFLAGS="$LIBMIKEYSAKKE_CPPFLAGS -I$GMP/$TARGET_MACH -I$GMP"

# since we override CPPFLAGS and LDFLAGS include the above manually
export CPPFLAGS="$CPPFLAGS $OPENSSL_CFLAGS $OPENCOREAMR_CFLAGS $LIBMIKEYSAKKE_CPPFLAGS -ggdb"
export CXXFLAGS="$CXXFLAGS $LIBMIKEYSAKKE_CXXFLAGS -ggdb"
export LDFLAGS="$LDFLAGS $OPENSSL_LIBS $OPENCOREAMR_LIBS"


if [[ $TARGET_MACH = *darwin* ]]
then
   CPPFLAGS="$CPPFLAGS -DDARWIN -D_XOPEN_SOURCE"
fi


# FIXME: this is only appropriate for our test app really; though the
# FIXME: library could be installed here a la QT for Android (lighthouse)
[ -z "$INSTALL_PREFIX" ] && if $BUILD_DESKTOP
then
   INSTALL_PREFIX=$HOME/minisip
else
   INSTALL_PREFIX=/data/data/se.kth.minisip.android/files
fi


# Minisip library flags
for l in \
   mutil \
   mnetutil \
   mcrypto \
   mstun \
   mikey \
   msip \
   minisip \
   ; \
do
   L=$(echo $l | tr '[a-z]' '[A-Z]')
   d="${MINISIP_ROOT}/lib$l"
   
   # to disambiguate the minisip library from the program,
   # dependent projects use LIBMINISIP as a variable prefix rather
   # than simply MINISIP
   #
   if [ $L = MINISIP ]; then L=LIBMINISIP; fi

   export ${L}_CFLAGS="-I$d/$BUILD_DIR/include -I$d/include"
   export ${L}_LIBS="-L$d/$BUILD_DIR/.libs -l$l"
   export ACLOCAL_FLAGS="$ACLOCAL_FLAGS -I$d/m4"
   export PKG_CONFIG_PATH="$d/$BUILD_DIR:$PKG_CONFIG_PATH"
done

if [[ $TARGET_MACH = *android* ]]; then

export PKG_CONFIG=${CROSS_PREFIX}pkg-config

else

# we fully specify <LIB>_CFLAGS and <LIB>_LIBS above voiding the need
# for pkg-config.  However, despite the documentation to the contrary,
# none of the above values are checked unless pkg-config can be
# executed; at which points its results are overwritten by the flags
# -- bizarre.  Tell configure to use the program 'true' in place of
# 'pkg-config' so that the variables are used as expected.
export PKG_CONFIG=true

fi # !android

# add install prefix to the bottom to allow for pre-installed stuff at
# that prefix to be used as last resort
export CPPFLAGS="$CPPFLAGS -I$INSTALL_PREFIX/include $ADD_CPPFLAGS"
export LDFLAGS="$LDFLAGS -L$INSTALL_PREFIX/lib $ADD_LDFLAGS"


# comment out by prefixing with underscore.
# alternatively list modules to skip in SKIP
# or list modules in ONLY to build a subset.


AVAILABLE_MODULES="\
   libmutil \
   libmnetutil \
   libmcrypto \
   libmstun \
   libmikey \
   libmsip \
   libminisip \
   $EXTRA \
"

notfound=0

# validate filter to catch typos that may cause full rebuilds
#
for m in $SKIP $ONLY
do
   if [[ $AVAILABLE_MODULES != *" $m "* ]]
   then
      echo >&2 "Error: module '$m' is not known; typo?"
      notfound=$((notfound+1))
   fi
done
[ $notfound -eq 0 ] || exit $notfound

# pad to allow for word boundary checks below
ONLY=${ONLY:+ $ONLY }
SKIP=${SKIP:+ $SKIP }


[ -n "$NO_LIBS" ] ||
for dir in $AVAILABLE_MODULES
do
(
   if [[ $dir = _* ]] || [[ $SKIP = *" $dir "* ]] || ( [ -n "$ONLY" ] && [[ "$ONLY" != *" $dir "* ]] ); then
      log Skipping $dir.
      continue
   fi
   mkdir $MINISIP_ROOT/$dir/$BUILD_DIR -p
   cd $MINISIP_ROOT/$dir/$BUILD_DIR || exit $?

   [ -n "$NO_CLEAN" ] || {
      log Cleaning $dir...
      make distclean

      [ -z "$FORCE_BOOTSTRAP" ] || (

         log Bootstrapping $dir...

         pwd
         cd ../
         pwd

         ./bootstrap || exit $?
      )
   }

   [ -n "$NO_CONFIGURE" ] || {
      log Configuring in $dir...

      if [ $dir = minisip ] && ${LD-${CROSS_PREFIX}ld} -shared -o/dev/null -lX11
      then
         LDFLAGS="$LDFLAGS -lX11"
      fi

      ../configure \
         --host=$TARGET_MACH \
         --oldincludedir=/dev/null \
         --prefix=$INSTALL_PREFIX \
         ${EXEC_PREFIX+--exec-prefix=$EXEC_PREFIX} \
         --disable-sctp \
         --disable-ipv6 \
         ${TEXT_ONLY+--disable-gtk} \
         ${TEXT_ONLY---enable-textui} \
         --enable-debug \
         ${CONFIGURE_OPTS} \
         || exit $?
   }
   
   [ -n "$NO_MAKE" ] || {
      log Making in $dir...
      make -j$JOBS $KEEP_GOING ${LOCAL_INSTALL:+INSTALL="$INSTALL" install} || exit $? 
   }

) || exit $?
done


if [ -z "$NO_MAKE" ] && [ -z "$NO_ANDROID_APPS" ] && ! $BUILD_DESKTOP
then
   if [ -n "$NO_GUI" ]
   then
      log Building Android library and test program...
      (
         cd $MINISIP_ROOT/libminisip/source/android || exit $?

         [ -n "$NO_CLEAN" ] || make -C MinisipTest clean

         make -C MinisipTest -j$JOBS ${ANDROID_INSTALL:+install} || exit $?
      )
   else 
      log Building Android library, test program and Android GUI...
      (
         cd $MINISIP_ROOT/minisip/minisip/gui/android || exit $?

         [ -n "$NO_CLEAN" ] || make -C AndroidMikeySakke clean

         make -C AndroidMikeySakke -j$JOBS ${ANDROID_INSTALL:+install} || exit $?
      )
   fi
fi

log Done.

