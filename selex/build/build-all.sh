build_scripts=$(dirname $0)
for i in gmp openssl curl boost libmikeysakke opencore-amr pjsip
do
   echo >&2 "Building '$i'..."
   $build_scripts/build-$i.sh $@ || { rc=$?; echo >&2 "Failed to build '$i'."; exit $rc; }
done
