[ -n "$MINISIP" ]       || { echo >&2 "MINISIP not defined - necessary for libmikey."; exit 20; }
[ -n "$LIBMIKEYSAKKE" ] || { echo >&2 "LIBMIKEYSAKKE not defined - necessary for SAKKE mode."; exit 20; }
[ -n "$BOOST" ]         || { echo >&2 "BOOST not defined - necessary for SAKKE mode."; exit 20; }

TARGET_MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

export BUILD_NAME=for-libmikeysakke

# build archives of Selex SAKKE crypto and KMS libraries
#
${MAKE-make} -C "$LIBMIKEYSAKKE" "$@" static=1 || exit $?
${MAKE-make} -C "$LIBMIKEYSAKKE" install DESTDIR=$LIBMIKEYSAKKE PREFIX=/ EXEC_PREFIX=/$TARGET_MACH "$@" static=1 || exit $?

# link libdl and librt on plain glibc linux
[[ $TARGET_MACH = *linux* ]] && ADD_LDFLAGS="-ldl -lrt"
[[ $TARGET_MACH = *android* ]] && ADD_LDFLAGS=

# selective build of libmikey (with SAKKE mode) from minisip
# (NOTE: since we create static object archives of the mscrypto and
# mskms libraries above, we need to pass through their link time
# dependencies)
#
EXEC_PREFIX="$LIBMIKEYSAKKE/$TARGET_MACH" \
INSTALL_PREFIX="$LIBMIKEYSAKKE" \
CONFIGURE_OPTS="--enable-shared=no" \
LOCAL_INSTALL=1 \
ADD_LDFLAGS="-L$LIBMIKEYSAKKE/$TARGET_MACH/lib -L${GMP}/${TARGET_MACH}/.libs -Wl,-lgmpxx -Wl,-lgmp $ADD_LDFLAGS" \
ONLY="${ONLY-libmutil libmcrypto libmnetutil libmikey}" \
NO_ANDROID_APPS=1 \
$(dirname $0)/build-minisip.sh "$@" || exit $?

# flatten archives into libmikeysakke.a
(
   cd $LIBMIKEYSAKKE/$TARGET_MACH/lib || exit $?
   rm -rf libmikeysakke.a libmikeysakke4c.a o && mkdir -p o && cd o || exit $?
   for a in ../*.a; do ${AR-ar} x "$a"; done
   ${AR-ar} scrv ../libmikeysakke.a *.o
)

# build Selex MIKEY SAKKE C API and install alongside C++
# implementation
#
${MAKE-make} -C "$LIBMIKEYSAKKE/mikeysakke4c" "$@" static=1 || exit $?
${MAKE-make} -C "$LIBMIKEYSAKKE/mikeysakke4c" install DESTDIR=$LIBMIKEYSAKKE PREFIX=/ EXEC_PREFIX=/$TARGET_MACH "$@" static=1 || exit $?

