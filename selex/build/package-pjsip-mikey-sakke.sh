CROSS_PREFIX=${CROSS_PREFIX:-$TARGET_SYSTEM-}
ARCH=$(${CC:-${CROSS_PREFIX}gcc} -dumpmachine)
ARCH=${ARCH:-${CROSS_PREFIX%-}}

TARGET=${PJSIP}-${ARCH}
TARGET_INC=$TARGET/include
TARGET_LIB=$TARGET/lib

rm -rf "$TARGET"

mkdir -p "$TARGET_INC" "$TARGET_LIB"

TARGET_INC="$(cd "$TARGET_INC" && pwd)"
TARGET_LIB="$(cd "$TARGET_LIB" && pwd)"

for i in \
${PJSIP}/${ARCH}/pjsip/include \
${PJSIP}/${ARCH}/pjlib/include \
${PJSIP}/${ARCH}/pjlib-util/include \
${PJSIP}/${ARCH}/pjnath/include \
${PJSIP}/${ARCH}/pjmedia/include \
;
do
   (cd $i && find . -not -type d | tar c -h -T-) | (cd $TARGET_INC && tar xv) || exit $?
done

for i in \
${PJSIP}/${ARCH}/pjsip/lib \
${PJSIP}/${ARCH}/pjlib/lib \
${PJSIP}/${ARCH}/pjlib-util/lib \
${PJSIP}/${ARCH}/pjnath/lib \
${PJSIP}/${ARCH}/pjmedia/lib \
${PJSIP}/${ARCH}/third_party/lib \
${OPENCOREAMR}/${ARCH}/amrnb/.libs \
${OPENSSL}/${ARCH} \
${GMP}/${ARCH}/.libs \
${CURL}/${ARCH}/lib/.libs \
${BOOST}/${ARCH}-stage/lib \
;
do
   (cd $i && find . -maxdepth 1 -name '*.a' | tar c -h -T-) | (cd $TARGET_LIB && tar xv) || exit $?
done
(cd $TARGET_LIB && find . -name "*-$ARCH*" | while IFS= read line; do mv -v "$line" "${line//-$ARCH/}"; done)

cp -af ${LIBMIKEYSAKKE}/include/mikeysakke4c.h "$TARGET_INC" || exit $?
cp -af ${LIBMIKEYSAKKE}/${ARCH}/lib/libmikeysakke* "$TARGET_LIB" || exit $?

[ -n "$NO_TAR" ] || (cd "$(dirname "$TARGET")" && echo "Creating '$TARGET.tar.bz2'... (set NO_TAR=1 to skip this)" && tar jcf "$TARGET".tar.bz2 "$(basename "$TARGET")")
[ -n "$NO_ZIP" ] || (cd "$(dirname "$TARGET")" && echo "Creating '$TARGET.zip'... (set NO_ZIP=1 to skip this)" && rm -f "$TARGET".zip && zip -rq "$TARGET".zip "$(basename "$TARGET")")

