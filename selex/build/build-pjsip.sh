[ -n "$PJSIP" ] || { echo >&2 "PJSIP not defined."; exit 20; }
[ -n "$OPENSSL" ] || { echo >&2 "OPENSSL not defined."; exit 20; }
[ -n "$OPENCOREAMR" ] || { echo >&2 "OPENCOREAMR not defined."; exit 20; }
[ -n "$LIBMIKEYSAKKE" ] || { echo >&2 "LIBMIKEYSAKKE not defined."; exit 20; }
[ -n "$CURL" ] || { echo >&2 "CURL not defined."; exit 20; }
[ -n "$BOOST" ] || { echo >&2 "BOOST not defined."; exit 20; }
[ -n "$GMP" ] || { echo >&2 "GMP not defined."; exit 20; }

cd $PJSIP || exit $?

MACH=$(${CC-${CROSS_PREFIX}gcc} -dumpmachine)

mkdir -p $MACH
cd $MACH

for i in ../pj* ../third* ../build
do
   lndir2 -f $i
done
ln -sfn ../Makefile
ln -sfn ../version.mak

if [ -z "$NO_CONFIGURE" ]; then

for i in ../pj* ../third* ../build
do
   lndir2 -f $i
done
ln -sfn ../Makefile
ln -sfn ../version.mak

CPPFLAGS="$CPPFLAGS -I$OPENSSL/include -I$OPENCOREAMR/amrnb"

# copy sample site config and use preprocessor
# options below to select the section to use.
cat pjlib/include/pj/config_site_sample.h \
  > pjlib/include/pj/config_site.h

if [[ $MACH = *qnx* ]] # assume BB10
then
   # manually specify flags for BB10
   CPPFLAGS="$CPPFLAGS -DPJMEDIA_AUDIO_DEV_HAS_BB10 -DPJMEDIA_AUDIO_DEV_HAS_PORTAUDIO=0"
   LDEXTRA="-laudio_manager -lasound"
elif [[ $MACH = *android* ]]
then
   CPPFLAGS="$CPPFLAGS -DPJ_CONFIG_ANDROID"
   LDEXTRA="-L${ANDROID_LIBCXX}/libs/${ANDROID_LIBCXX_ARCH} -lgnustl_shared"
elif [[ $MACH = *darwin* ]]
then
   if ${CC-${CROSS_PREFIX}gcc} -v 2>&1 | grep -q iPhone
   then
      CPPFLAGS="$CPPFLAGS -DPJ_CONFIG_IPHONE"
   fi
   LDEXTRA="-lobjc"
elif [[ $MACH = *x86_64*gnu* ]]
then
   LDEXTRA="-ldl"
fi

ac_cv_host=$MACH \
CPPFLAGS="$CPPFLAGS" \
CFLAGS="$CFLAGS -fPIC -O3 $CPPFLAGS" \
CXXFLAGS="$CXXFLAGS -fPIC -O3 $CPPFLAGS" \
LDFLAGS="-L$OPENSSL/$MACH -L$OPENCOREAMR/$MACH/amrnb/.libs $LDEXTRA" \
AR="${AR-ar} scrv" \
RANLIB=true \
../aconfigure --host $MACH --with-ssl --with-opencore-amr --with-libmikeysakke=$LIBMIKEYSAKKE --disable-oss || exit $?

make dep $@ || exit $?

fi

# pjsip's dependency encoding is broken preventing multiprocess builds
# from working out of the box.  There appear to be 6 artifacts which
# fail at link time due to mis-encoded dependencies -- in all cases
# running again solves the problem.  As a workaround here, if a -jN
# option is found in the arglist we loop until success or loop count
# exceeds 6.  To prevent this loop (when developing and expecting
# errors for example) spell '-jN' as two arguments '-j' 'N'.

loop_make=false

args=("$@")
until $loop_make || [ $# -eq 0 ]; do
   case $1 in
      -j)   shift; continue ;;
      -j*)  loop_make=true; break ;;
   esac
   shift
done

if $loop_make; then
   rc=0
   i=0; until make "${args[@]}"; do
      rc=$?
      [ $((++i)) -lt 6 ] || exit $rc
   done
else
   make "${args[@]}"
fi

