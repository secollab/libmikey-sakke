[ -n "$GDB" ] || { echo >&2 Define GDB to be root of gdb source.; exit 20; }
cd "$GDB" || exit $?

export ANDROID_HOST_TUPLE=$(android-cross @ANDROID_HOST_TUPLE)
export NDK_PREFIX=$(dirname $(dirname $(android-cross which ${ANDROID_HOST_TUPLE}-gdb)))

# want access to android tooling but not to make them the primary tooling
export PATH=$(android-cross @PATH)

mkdir -p $ANDROID_HOST_TUPLE
cd $ANDROID_HOST_TUPLE || exit $?

[ -n "$NO_CONFIGURE" ] || ../configure --prefix=$NDK_PREFIX --target=$ANDROID_HOST_TUPLE

make $@
make $@ install

