#/source/with/bash/or/zsh
#
# This script (which supports sourcing in order to update the calling
# environment) fetches, extracts and sets up the necessary environment
# variables to point at the dependencies required for libmikey-sakke
# and it's demonstrator programs.
#
# A variable will not be considered if it is already set in the
# calling environment.  No fetching will be done if the expected
# archive exists and no extracting will be done if the expected
# directory exists.
#
# Pass a directory as the first argument to override the default
# BASEDIR below.  The default for BASEDIR will be read from
# $HOME/.libmikey-sakke.basedir if the script has been run before.
#
# Pass --unset to unset all variables that would be set by this script.
# Pass -f to force creation of BASEDIR if it does not exist.
# Pass -u or --update to update any git/svn checkouts after first use
#
if [ -r "$HOME/.libmikey-sakke.basedir" ]
then BASEDIR="$(cat "$HOME/.libmikey-sakke.basedir")"
else BASEDIR="$HOME/src"
fi

DEPSET='

   # MIKEY-SAKKE library source
   #
   LIBMIKEYSAKKE  git       https://bitbucket.org/secollab/libmikey-sakke.git   /selex

   # Core MIKEY-SAKKE crypto and KMS client dependencies
   #
   GMP            5.0.5     ftp://ftp.gmplib.org/pub/gmp-$ver/gmp-$ver.tar.bz2
   OPENSSL        1.0.1c    http://www.openssl.org/source/openssl-$ver.tar.gz
   CURL           7.28.1    http://curl.haxx.se/download/curl-$ver.tar.bz2
   BOOST          1_49_0    http://sourceforge.net/projects/boost/files/boost/${ver//_/.}/boost_$ver.tar.bz2/download

   # Composite libmikey-sakke dependencies (libmikey)
   #
   MINISIP        git       https://bitbucket.org/secollab/minisip-mikey-sakke.git

   # Demonstrator programs (minisip also contains demo programs but is
   # fetched as a library dependency (libmikey) above)
   #
   OPENCOREAMR    0.1.3     http://sourceforge.net/projects/opencore-amr/files/opencore-amr/opencore-amr-0.1.3.tar.gz/download
   PJSIP          git       https://bitbucket.org/secollab/pjsip-mikey-sakke.git

'

unset UNSET FORCE UPDATE ENUM ENUM_TARGETS

ARGS=("$@")
while [ $# -gt 0 ]; do ARG=$1; shift
case $ARG in
   --unset)            UNSET=1 ;;
   -f)                 FORCE=1 ;;
   -u|--update)        UPDATE=1 ;;
   -e|--enum)          ENUM=1 ;;
   -t|--enum-targets)  ENUM_TARGETS=1 ;;
   *)                  BASEDIR="$ARG" ;;
esac
done
set -- ${ARGS[@]}
unset ARGS


this="${BASH_SOURCE[0]}"
this="${this:-$0}"


# shell feature test
(
   declare x="1.2.3";
   [ "${x//./_}" = "1_2_3" ] || {
      echo >&2 'Shell must support ${VAR//pat/sub} substitution.'
      exit 21
   }
) || return $?


failures=0
failure_text=()

fetch_cmd=()


if [[ "$OS" = *Windows* ]]
then NIL=NUL
else NIL=/dev/null
fi


function fetch_url() # url targetfile
{
   [ ${#fetch_cmd[@]} -gt 0 ] || {
      if wget --version 2>$NIL >$NIL; then
         fetch_cmd=(wget -O)
      elif curl --version 2>$NIL >$NIL; then
         fetch_cmd=(curl -L --proto-redir =http,https --max-redirs 5 -o)
      else
         echo >&2 "No suitable program found to fetch remote file."
         return 20
      fi
   }

   echo >&2 "Fetching '$1' to '$PWD/$2'..."
   ${fetch_cmd[@]} "$2" "$1"
}


function fetch_extract_identify() # lib ver url
{
   cd "$BASEDIR" 2>$NIL || {
      if [ "$FORCE" = 1 ]; then
         mkdir -p "$BASEDIR" && cd "$BASEDIR" || return 2
      else
         return 1
      fi
   }

   # cache BASEDIR
   if ! [ -r "$HOME/.libmikey-sakke.basedir" ]; then
      echo > "$HOME/.libmikey-sakke.basedir" "$PWD"
   fi

   declare lib=$1 ver=$2 url="$3" subdir="$4"

   declare packname="${url%/download}" vcs=false
   declare packname="$(basename "$packname")"

   declare extract_cmd
   extract_cmd=()

   case $packname in
      *.tar.bz2) extract_cmd=(tar jxvf)     dirname=${packname%.tar.bz2} ;;
      *.tar.gz)  extract_cmd=(tar zxvf)     dirname=${packname%.tar.gz} ;;
      *.zip)     extract_cmd=(unzip)        dirname=${packname%.zip} ;;
      *) case $ver in
         git)    extract_cmd=(git clone)    dirname=${packname%.git} vcs=true ;;
         svn)    extract_cmd=(svn checkout) dirname=${packname} vcs=true ;;
         esac ;;
   esac

   [ -x "$dirname" ] || {

      [ "${#extract_cmd[@]}" -gt 0 ] || return 3
      
      if $vcs
      then
         [ -r "$packname" ] || ${extract_cmd[@]} 1>&2 "$url" || return 4
      else
         [ -r "$packname" ] || fetch_url 1>&2 "$url" "$packname" || return 5
         ${extract_cmd[@]} 1>&2 "$packname" || return 6
      fi
   }

   echo "$lib=$PWD/$dirname$subdir"

   return 0
}

function failure_message() # code lib ver url
{
   declare code=$1 lib=$2 ver=$3 url=$4

   case $code in
      0) echo "No error" ;;
      1) echo "Base directory '$BASEDIR' does not exist, pass -f to create." ;;
      2) echo "Could not create/enter directory '$BASEDIR'" ;;
      3) echo "Could not determine type or resulting directory from '$url'" ;;
      4) echo "Failed to checkout '$url'" ;;
      5) echo "Failed to fetch '$url'" ;;
      6) echo "Failed to extract '$(basename "$url")'" ;;
      *) echo "Unknown error processing $lib $ver $url" ;;
   esac
}

function leave() # code
{
   return $1
}


while read lib ver url subdir
do
   [ -n "$lib" -a "$lib" != '#' ] || continue

   if [ "$UNSET" = 1 ]
   then
      [ -n "$ENUM" ] || echo "unset $lib"
      unset $lib
      continue
   fi

   # if var already set then skip this assignment
   eval '[ -z "${'"$lib"'}" ]' || { export $lib; continue; }

   eval url="$url" # expand $ver

   if assigment=$(fetch_extract_identify $lib $ver $url $subdir)
   then
      [ -n "$ENUM" ] || echo "export $assigment"
      export "$assigment"
   else
      failure_code=$?
      failures=$((failures+1))
      failure_text=("${failure_text[@]}" "$(failure_message $failure_code $lib $ver $url)")
      [ $failure_code -lt 3 ] || break
   fi
done < <(echo "$DEPSET")

for e in "${failure_text[@]}"
do
   echo >&2 "ERROR: $e"
done

AJB_TOOLS="$BASEDIR/ajb-tools"

if ! [ -x "$AJB_TOOLS" ]
then
   svn checkout https://subversion.assembla.com/svn/ajb-tools/trunk "$AJB_TOOLS" 1>&2 || return $?
fi
mkdir -p "$HOME/bin"
[ -x "$HOME/bin/lndir2" ] || "$AJB_TOOLS/lndir/make-install-bin.sh" || return $?
for s in \
   android/android-cross/android-cross \
   blackberry/bb10-cross/bb10-cross \
   iphone/iphone-cross/iphone-cross \
   ;
do
   ln -sfn "$AJB_TOOLS/$s" "$HOME/bin/$(basename $s)"
done
which pkg-config >$NIL 2>$NIL && for m in \
   arm-unknown-nto-qnx8.0.0eabi \
   arm-linux-androideabi \
   arm-apple-darwin10 \
   i486-pc-nto-qnx8.0.0eabi \
   ;
do
   $AJB_TOOLS/build/make-target-pkg-config/make-target-pkg-config $m
done
export MAKE=make-3.82.90-ajb2
case "$(uname -a)" in
   Darwin*)          MAKE=x86_64-apple-darwin11.4.0-$MAKE ;;
   Linux*x86_64*)    MAKE=x86_64-unknown-linux-gnu-$MAKE ;;
   Linux*i?86*)      MAKE=i486-unknown-linux-gnu-$MAKE ;;
   MINGW*i?86*)      MAKE=win32-$MAKE.exe ;;
   MINGW*x86_64*)    MAKE=win64-$MAKE.exe ;;
   *)                unset MAKE ;;
esac
if [ -n "$MAKE" ] && ! [ -x "$HOME/bin/$MAKE" ]
then
   fetch_url https://bitbucket.org/abutcher/gmake-ajb/downloads/$MAKE "$HOME/bin/$MAKE"
   chmod +x "$HOME/bin/$MAKE"
fi
[[ "$PATH" = *"$HOME/bin"* ]] || export PATH="$HOME/bin:$PATH"


if [ "$UPDATE" = 1 ]
then
   while read lib ver url subdir
   do
      [ -n "$lib" -a "$lib" != '#' ] || continue

      eval echo >&2 -n '"[1mUpdating '\'$lib\'' in '\'\$$lib\''[0m... "'
      case $ver in
         git)  echo >&2; eval '(cd $'$lib'; git pull --rebase 1>&2)' ;;
         svn)  echo >&2; eval '(cd $'$lib'; svn update 1>&2)' ;;
         *)    echo >&2 'Not a VCS source, skipped.' ;;
      esac || eval echo >&2 '"[31;1mFAILED to update '\'$lib\'' in '\'\$$lib\''[0m"'

   done < <(echo "$DEPSET")

   # test whether this script was updated before doing the rest
   #
   if diff -q "$LIBMIKEYSAKKE/build/mikeysakke.env" "$this"
   then
      echo >&2 "[1mUpdating '$AJB_TOOLS'[0m..."
      r1=$(svn info "$AJB_TOOLS" | sed -n '/Last Changed Rev/ s/[^0-9]*//p')
      svn up -q "$AJB_TOOLS" 1>&2 || echo >&2 "[31;1mFAILED to update '$AJB_TOOLS'[0m"
      r2=$(svn info "$AJB_TOOLS" | sed -n '/Last Changed Rev/ s/[^0-9]*//p')
      if [ "$r1" != "$r2" ]
      then
         # rebuild and install lndir
         "$AJB_TOOLS/lndir/make-install-bin.sh"
      fi
      unset r1 r2
      echo >&2 "[1mDone.[0m"
   else
      echo >&2 "[1mUpdating this script '$this'[0m..."
      cp -afv "$LIBMIKEYSAKKE/build/mikeysakke.env" "$this"
      echo >&2
      echo >&2 "[35;1mRe-running with updated script and args $@[0m..."
      . "$this" "$@"
   fi
fi

function enum_targets() # dir
{
   declare find_opts find_expr
   if find -E . -maxdepth 0 >$NIL 2>$NIL; then
      find_opts=(-E)
   else
      find_opts=()
   fi
   if find . -regextype posix-extended -maxdepth 0 >$NIL 2>$NIL; then
      find_expr=(-regextype posix-extended)
   else
      find_expr=()
   fi
   find ${find_opts[@]} "$1" ${find_expr[@]} -maxdepth 3 -type d -regex '.*(arm|i.86|mips|x86_64)[^-]*-[^-]*-[^/]*'
}

if [ "$ENUM" = 1 ] || [ "$ENUM_TARGETS" = 1 ]
then
   while read lib ver url subdir
   do
      [ -n "$lib" -a "$lib" != '#' ] || continue
      if [ "$ENUM" = 1 ]
      then
         eval echo "\$$lib"
      fi
      if [ "$ENUM_TARGETS" = 1 ]
      then
         eval enum_targets "\$$lib"
      fi
   done < <(echo "$DEPSET")
fi

unset UNSET FORCE UPDATE ENUM ENUM_TARGETS BASEDIR DEPSET

leave $failures

