
SELEX MIKEY-SAKKE Key Management Service
------------------------------------------------
Contact: Adam Butcher <adam.butcher@selexelsag.com>
         Nick Jones <nick.jones@selexelsag.com>


INSTALLATION
===========================================================

The script 'install-deps.pl' alongside this file will fetch (via CPAN
and git), build and install all dependencies automatically.  Consult
the 'install-deps.pl' script for more details.

It has been tested on GNU with various versions of Perl and git.  It
has been tested with Strawberry Perl 5.12.3 and 5.16.1 on Windows XP
(32-bit) and Windows 7 (64-bit) and msys git 1.7.11.


USAGE
===========================================================

After installing the necessary dependencies simply do:

  ./mskms.pl

to run the server.  Or, if Hypnotoad is in a different location to
that stated in the hashbang line, do:

  /path/to/hypnotoad -f ./mskms.pl

On Windows, where Hypnotoad is unsupported, run through Morbo instead:

  morbo mskms.pl

Connect to https://hostname:7777 to login, add/remove users and URI
mappings and query status and key material.

The SQLite database ./mskms.db will be created on first execution with
some demonstration users and a default administrator account with
username 'admin', password 'admin'.  This can be used to set up the
system proper.


MSKMS REST API v1
===========================================================

TODO: Document new REST API.


LEGACY API
===========================================================

The legacy service path /secure/key is provided for compatibility with
prior demonstrator endpoints.

/secure/key?id=MIKEY-SAKKE-IDENTIFIER

   Access the key material for the identifier MIKEY-SAKKE-IDENTIFIER.
   This identifier is of the form:

      YYYY-MM%00SOME-URL-ENCODED-URI%00

   where YYYY is the year, MM is the month and SOME-URL-ENCODED-URI is
   a registered URI.

   If the URI belongs to a user different than the currently
   authenticated user, then only public key material is returned.

   If the URI belongs to the currently authenticated user, then
   user-specific private key material is returned in addition to the
   public material.

   KMS public community data is always returned.

   Note: the /secure/key path supports Basic www-authentication as
   well as browser-based interactive session authentication.  This
   allows a MIKEY-SAKKE user agent to be provided with credentials to
   directly access the key material for a user with one request.

   The data is returned as JSON content.

