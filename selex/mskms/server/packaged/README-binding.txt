To bind the SIP server to a specific IP address (necessary for well-
defined behaviour if you have multiple addresses) simply pass the IP
address as a command line parameter.  If launching from the desktop
file manager, create an empty file having the required IP address as
its name and drag it onto the cmd file icon.  An binding file for
172.30.232.40 is provided as an example.
