#!/usr/bin/env perl

use 5.010;
use strict;
use warnings;

use File::Basename;
use Cwd 'abs_path';
use CPAN;
use File::Find;

my $is_windows = $ENV{OS} && $ENV{OS} =~ /Windows/;

our @deps = qw(
     Crypt::Blowfish
     Crypt::CBC
     Crypt::DES_EDE3
     Crypt::Rijndael
     Data::Compare
     Encoding::BER
     Digest::SHA
     Digest::SHA1
     UUID::Tiny
     Math::GMPz
     Net::SSLeay
     IO::Socket::SSL
     DBD::SQLite
     Mojolicious
     Net::SIP
  );

push @deps, 'Net::Interface' if not $is_windows;

open SHELL, "|cpan";
printf SHELL "notest install $_\n" for @deps;
close SHELL;
wait;
for (@deps)
{
   printf "Checking $_...\n";
   eval("use $_; 1;") or
   do { die "Failed to install $_: $@" }
}

printf "CPAN packages installed.\n";

CPAN::HandleConfig->load;

my $this_dir = abs_path(dirname($0));
my $build_dir = $CPAN::Config->{build_dir};
my $make = $CPAN::Config->{make} || 'make';
my $make_install_make = $CPAN::Config->{make_install_make_command} || $make;

# Fetch, patch, build and install Crypt::ECDSA (Math::GMPz version)
#
my $ecdsa_dir = "$build_dir/Crypt-ECDSA-ajb1";
system(qq|git clone https://bitbucket.org/abutcher/crypt-ecdsa-gmpz.git $ecdsa_dir|) if not -x $ecdsa_dir;
chdir $ecdsa_dir;
system(qq|git pull --rebase|);
system(qq|perl Makefile.PL|) == 0 or die "Failed to generate makefile";
system(qq|$make|) == 0 or die "Failed to build.";
system(qq|$make_install_make install|) == 0 or die "Failed to install.";

