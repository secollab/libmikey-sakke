cd /d "%~dp0"

:: expect this to fail due to winres bug
perl -MCPAN -e "CPAN::Shell->notest('install', 'PAR::Packer')"

:: fix winres bug and install manually
@for /f "usebackq" %%i in (`perl -MCPAN -e "CPAN::HandleConfig->load; print $CPAN::Config->{build_dir}"`) do set "BUILD_DIR=%%i"
@for /f "usebackq" %%i in (`perl -MCPAN -e "CPAN::HandleConfig->load; print $CPAN::Config->{make} || 'make'"`) do set "MAKE=%%i"
@for /d %%i in (%BUILD_DIR%\PAR-Packer*) do @set "BUILD_DIR=%%i"
pushd %BUILD_DIR%
%MAKE% && goto :skip_patch
sed -i.bak "/^VS_VERSION_INFO/,/^END/ d" myldr/winres/pp.rc
%MAKE% 
%MAKE% install
:skip_patch
popd

@for %%i in (perl.exe) do set "PERLBIN=%%~dp$PATH:i"
set "PERLCBIN=%PERLBIN%..\..\c\bin\"

if not exist "packaged/bin" mkdir "packaged/bin"
if not exist "packaged/lib" mkdir "packaged/lib"

copy /Y "%PERLBIN%libstdc++-6.dll" "packaged/lib"
copy /Y "%PERLCBIN%libeay32_.dll"  "packaged/lib"
copy /Y "%PERLCBIN%ssleay32_.dll"  "packaged/lib"
copy /Y "%PERLCBIN%zlib1_.dll"     "packaged/lib"

pp -n -c ^
   -o packaged/bin/mskms.exe mskms.pl      ^
   -a templates;script/templates           ^
   -a public;script/public                 ^
   -I lib                                  ^
   -a %PERLBIN%/../site/lib/Mojo/entities.txt;Mojo/entities.txt ^
   -a %PERLBIN%/../site/lib/Mojo/IOLoop/server.crt;Mojo/IOLoop/server.crt ^
   -a %PERLBIN%/../site/lib/Mojo/IOLoop/server.key;Mojo/IOLoop/server.key ^
   -M DBD::SQLite                          ^
   -M Mojolicious::Plugin::AuthHelper      ^
   -M Mojolicious::Plugin::BasicAuth       ^
   -M Mojolicious::Plugin::Charset         ^
   -M Mojolicious::Plugin::Config          ^
   -M Mojolicious::Plugin::DefaultHelpers  ^
   -M Mojolicious::Plugin::EPLRenderer     ^
   -M Mojolicious::Plugin::EPRenderer      ^
   -M Mojolicious::Plugin::HeaderCondition ^
   -M Mojolicious::Plugin::JSONConfig      ^
   -M Mojolicious::Plugin::Mount           ^
   -M Mojolicious::Plugin::PODRenderer     ^
   -M Mojolicious::Plugin::PoweredBy       ^
   -M Mojolicious::Plugin::RequestTimer    ^
   -M Mojolicious::Plugin::TagHelpers      ^
   -M Mojolicious::Command::daemon         ^

