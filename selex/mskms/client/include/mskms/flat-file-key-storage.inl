#ifndef MSKMS_FLAT_FILE_KEY_STORAGE_INL
#define MSKMS_FLAT_FILE_KEY_STORAGE_INL

#define BOOST_FILESYSTEM_VERSION 2 // for Android's libbionic C library (no wide character string)

#include <mskms/key-storage.h>
#include <util/octet-string.h>
#include <boost/filesystem.hpp>
#include <boost/filesystem/fstream.hpp>
#include <mskms/runtime-key-storage.inl>

namespace MikeySakkeKMS {

namespace fs = boost::filesystem;
namespace sys = boost::system;

/**
 * Development implementation for key storage in a local file
 * directory structure.
 */
class FlatFileKeyStorage : public RuntimeKeyStorage
{
public:
   FlatFileKeyStorage(fs::path const& rootpath = "./key-data")
      : rootpath(rootpath)
      , saveEnabled(false)
   {
      // Defer setting saveEnabled until after initial load to prevent
      // unnecessary writes on startup; the Store function would
      // otherwise attempt to write the data back from the file we
      // just read it from.
      LoadKeys("private-keys", &KeyStorage::StorePrivateKey);
      LoadKeys("public-keys", &KeyStorage::StorePublicKey);
      LoadKeys("public-parameters", &KeyStorage::StorePublicParameter);
      sys::error_code ignore;
      for (fs::directory_iterator it(rootpath/"communities", ignore), end;
           it != end; ++it)
      {
         AddCommunity(it->path().filename());
      }
      saveEnabled = true;
   }
   void StorePrivateKey(std::string const& identifier, std::string const& key, OctetString const& value)
   {
      RuntimeKeyStorage::StorePrivateKey(identifier, key, value);
      SaveKey("private-keys", identifier, key, value);
   }
   void StorePublicKey(std::string const& identifier, std::string const& key, OctetString const& value)
   {
      RuntimeKeyStorage::StorePublicKey(identifier, key, value);
      SaveKey("public-keys", identifier, key, value);
   }
   void StorePublicParameter(std::string const& identifier, std::string const& key, std::string const& value)
   {
      RuntimeKeyStorage::StorePublicParameter(identifier, key, value);
      SaveKey("public-parameters", identifier, key, value);
   }
   void AddCommunity(std::string const& community)
   {
      RuntimeKeyStorage::AddCommunity(community);
      if (!saveEnabled)
         return;
      fs::path communities(rootpath/"communities");
      fs::create_directories(communities);
      fs::ofstream file(communities/community);
   }
   void RevokeKeys(std::string const& identifier)
   {
      RuntimeKeyStorage::RevokeKeys(identifier);
      char const* categories[] = {
         "private-keys", "public-keys", "public-parameters", "communities"
      };
      for (char const
           ** cat = categories,
           ** catend = categories + sizeof categories / sizeof *categories;
           cat != catend;
           ++cat)
      {
         fs::remove_all(rootpath/(*cat)/fnencode(identifier));
      }
   }
   void Purge()
   {
      RuntimeKeyStorage::Purge();
      fs::remove_all(rootpath);
   }

private:
   template <typename StorageType>
   void LoadKeys(std::string const& category,
                 void (KeyStorage::* Store) (std::string const& id,
                                             std::string const& key,
                                             StorageType const& val))
   {
      sys::error_code ignore;
      for (fs::directory_iterator id(rootpath/category, ignore), idend;
           id != idend; ++id)
      {
         for (fs::directory_iterator key(id->path(), ignore), keyend;
              key != keyend; ++key)
         {
            fs::ifstream file(*key);
            std::string content;
            std::getline(file, content);
            if (!content.empty())
               (this->*Store) (fndecode(id->path().filename()),
                               key->path().filename(),
                               content);
         }
      }
   }
   template <typename StorageType>
   void SaveKey(std::string const& category,
                std::string const& identifier,
                std::string const& key,
                StorageType const& content)
   {
      if (!saveEnabled)
         return;

      fs::path const& dir = rootpath/category/fnencode(identifier);
      fs::create_directories(dir);
      fs::ofstream file(dir/key);
      file << content;
   }
   static std::string fnencode(std::string const& id)
   {
      std::string rc;
      char esc[] = "%xx";
      for (char const* it = id.c_str(), *end = it + id.size();
           it != end; ++it)
      {
         if (*it <= '+' || (*it > '9' && *it < '@'))
         {
            std::sprintf(esc+1, "%02x", *it);
            rc += esc;
         }
         else
            rc += *it;
      }
      return rc;
   }
   static std::string fndecode(std::string const& id)
   {
      std::string rc;
      for (char const* it = id.c_str(), *end = it + id.size();
           it != end; ++it)
      {
         if (*it == '%' && (end - it) > 2)
         {
            int c;
            std::sscanf(it+1, "%02x", &c);
            rc += (char)c;
            it += 2;
         }
         else
         {
            rc += *it;
         }
      }
      return rc;
   }

   fs::path rootpath;
   bool saveEnabled;
};

} // MikeySakkeKMS

#endif//MSKMS_FLAT_FILE_KEY_STORAGE_INL

