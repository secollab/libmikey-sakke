#ifndef MSKMS_CLIENT_H
#define MSKMS_CLIENT_H

#include <mskms/client-fwd.h>
#include <boost/system/error_code.hpp>


namespace MikeySakkeKMS {


namespace sys = boost::system;


class Client : public std::enable_shared_from_this<Client>
{
public:

   typedef std::function
           <void (std::string const& identifier, sys::error_code, std::string const& error_text)>
           CompletionHandler;

public:

   enum SSLVerificationPolicy
   {
      DontVerifySSLCertificate,
      VerifySSLCertificate,
   };

   Client(KeyStoragePtr const&);

   void FetchKeyMaterial(std::string const& kmsServer,
                         SSLVerificationPolicy,
                         std::string const& username,
                         std::string const& password,
                         std::string const& identifier,
                         CompletionHandler);

private:

   KeyStoragePtr ks;
};


} // MikeySakkeKMS

#endif//MSKMS_CLIENT_H

