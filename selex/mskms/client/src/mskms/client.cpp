#include <mskms/client.h>
#include <curl/curl.h>
#include <json/json.h>
#include <mskms/key-storage.h>
#include <convenience/cptr.h>

#include <iostream>

#include <util/octet-string.h>

namespace MikeySakkeKMS {

Client::Client(KeyStoragePtr const& ks)
   : ks(ks)
{
   // TODO: HACK: use std::call_once with std::once_flag
   // TODO: ideally inside a ((constructor)) function
   static bool done = false;
   if (!done) {
      done = true;
      curl_global_init(CURL_GLOBAL_ALL);
   }
}


namespace
{
   std::string display_string(std::string s)
   {
      for (std::string::iterator it = s.begin(), end = s.end(); it != end; ++it)
         if (*it == '\0')
            *it = ' ';
      return s;
   }
}


void Client::FetchKeyMaterial(std::string const& kmsServer,
                              SSLVerificationPolicy verifyPolicy,
                              std::string const& username,
                              std::string const& password,
                              std::string const& identifier,
                              CompletionHandler handler)
{
   using convenience::cptr;
   using namespace boost::system;
   using namespace sys::errc;

   if (kmsServer.empty() || username.empty() || password.empty())
      return handler(identifier, make_error_code(invalid_argument), "(server, username, password) must be provided");

   cptr<CURL,curl_easy_cleanup> curl = curl_easy_init(); 

   if (!curl)
      return handler(identifier, make_error_code(function_not_supported), "libcurl failed to initialize");

   std::string url_encoded_id;
   {
      cptr<char(),curl_free> escaped =
         curl_easy_escape(curl, identifier.c_str(), identifier.length());

      url_encoded_id.assign(escaped);
   }

   std::string url = "https://"
                   + username + ":" + password + "@"
                   + kmsServer
                   + "/secure/key?id="
                   + url_encoded_id
                   ;

   std::cerr << "FETCHING: " << url << "\n";

   struct CurlHandler
   {
      static int AppendToString(char const* data,
                                size_t size, size_t count,
                                std::string* s)
      {
         size *= count;
         s->append(data, size);
         return size;
      }
   };
   
   std::string json;

   char errorBuffer[CURL_ERROR_SIZE];
   curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorBuffer);  

   curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
   curl_easy_setopt(curl, CURLOPT_HEADER, 0);
   curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1);
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, CurlHandler::AppendToString);
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, &json);

   if (verifyPolicy == DontVerifySSLCertificate)
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, false);

   CURLcode rc = curl_easy_perform(curl);

   if (rc != CURLE_OK)
   {
      std::cerr << errorBuffer << "\n";
      return handler(identifier, make_error_code(connection_reset), errorBuffer);
   }

   std::cerr << "JSON: " << json << "\n";

   Json::Reader r;
   Json::Value keyData;
   if (!r.parse(json, keyData, false))
      return handler(identifier, make_error_code(bad_message), "failed to parse JSON");

   Json::Value privateData = keyData["private"];
   Json::Value publicData = keyData["public"];
   Json::Value communityData = keyData["community"];
   Json::Value error = keyData["error"];

   if (!error.empty())
      return handler(identifier, make_error_code(operation_not_permitted), display_string(error.asString()));

   try
   {
      if (!privateData.empty())
      {
         ks->StorePrivateKey(identifier, "SSK", privateData["userSecretSigningKey"].asString());
         ks->StorePrivateKey(identifier, "RSK", privateData["receiverSecretKey"].asString());
      }
      if (!publicData.empty())
      {
         ks->StorePublicKey(identifier, "PVT", publicData["userPublicValidationToken"].asString());
      }
      for (Json::ValueIterator
           it = communityData.begin(), end = communityData.end();
           it != end;
           ++it)
      {
         Json::Value const& community = *it;

         std::string communityId = community["kmsIdentifier"].asString();
   
         ks->AddCommunity(communityId);

         ks->StorePublicKey(communityId, "KPAK", community["kmsPublicAuthenticationKey"].asString());
         ks->StorePublicKey(communityId, "Z",    community["kmsPublicKey"].asString());
         ks->StorePublicParameter(communityId, "SakkeSet", community["sakkeParameterSetIndex"].asString());
      }
   }
   catch (std::invalid_argument& e)
   {
      ks->RevokeKeys(identifier);
      handler(identifier, make_error_code(invalid_argument), e.what());
   }

   handler(identifier, make_error_code(success), std::string());
}

} // MikeySakkeKMS

