// this program computes the pairing Tate-Lichtenbaum Pairing <R,Q> as used in the SAKKE scheme
// NOTE: here we use the specific values for p,q and G as given in Pairing Based IBE

#include <stdio.h>
#include <math.h>
#include <gmp.h>
#include <jni.h>
#include <string.h>
#include <openssl/bn.h>
#include <openssl/ec.h>
#include <ec_lcl.h>
#include <openssl/sha.h>


// CONSTANTS
mpz_t p; // 1024-bit prime p as defined in Pairing Based IBE
mpz_t q; // prime q dividing p+1 as defined in Pairing Based IBE
mpz_t qminusone; // q-1

// Fp2mult variables
mpz_t Fp2tempa[2], Fp2tempb[2];
void initFp2()
{
	mpz_init(Fp2tempa[0]); mpz_init(Fp2tempa[1]);
	mpz_init(Fp2tempb[0]); mpz_init(Fp2tempb[1]);
}
void clearFp2()
{
	mpz_clear(Fp2tempa[0]); mpz_clear(Fp2tempa[1]);
	mpz_clear(Fp2tempb[0]); mpz_clear(Fp2tempb[1]);
}
// function to multiply two elements PF_p[i], c = a.b
void Fp2mult(mpz_t c[2], mpz_t a[2], mpz_t b[2])
{
	mpz_set(Fp2tempa[0],a[0]); mpz_set(Fp2tempa[1],a[1]);
	mpz_set(Fp2tempb[0],b[0]); mpz_set(Fp2tempb[1],b[1]);

	mpz_mul(c[0],Fp2tempa[0],Fp2tempb[0]);
	mpz_submul(c[0],Fp2tempa[1],Fp2tempb[1]);
	mpz_mod(c[0],c[0],p);

	mpz_mul(c[1],Fp2tempa[0],Fp2tempb[1]);
	mpz_addmul(c[1],Fp2tempa[1],Fp2tempb[0]);
	mpz_mod(c[1],c[1],p);
}
// function to multiply two elements PF_p[i], c = a.b
void Fp2sqr(mpz_t c[2], mpz_t a[2])
{
	mpz_set(Fp2tempa[0],a[0]); mpz_set(Fp2tempa[1],a[1]);

	mpz_add(Fp2tempb[0],Fp2tempa[0],Fp2tempa[1]);
	mpz_sub(Fp2tempb[1],Fp2tempa[0],Fp2tempa[1]);
	mpz_mul(c[0],Fp2tempb[0],Fp2tempb[1]);
	mpz_mod(c[0],c[0],p);

	mpz_mul(c[1],Fp2tempa[0],Fp2tempa[1]);
	mpz_mul_ui(c[1],c[1],2);
	mpz_mod(c[1],c[1],p);
}

// function to return number of bits in an mpz_t, eg bits(4=0b100) = 3
unsigned int bits(mpz_t n)
{
	mpz_t m; mpz_init_set(m,n);
	unsigned int i=0;
	while(mpz_cmp_ui(m,0))
	{
		mpz_fdiv_q_2exp(m,m,1);
		i++;
	}
	mpz_clear(m);
	return i;
}

// ELLIPTIC ARITHMETIC FUNCTIONS
mpz_t lambda, lambda2, EAT1, EAT, EAR[2];
void initEAV()
{
	mpz_init(lambda); mpz_init(lambda2); mpz_init(EAT1); mpz_init(EAT); mpz_init(EAR[0]); mpz_init(EAR[1]);
}
void clearEAV()
{
	mpz_clear(lambda); mpz_clear(lambda2); mpz_clear(EAT1); mpz_clear(EAT); mpz_clear(EAR[0]); mpz_clear(EAR[1]);
}

// function to double a point (R=[2]P) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_double(mpz_t R[2], mpz_t P[2])
{
	if(mpz_cmp(P[0],p)==0) // if P = infinity
	{
		mpz_set(EAR[0],p);
		mpz_set(EAR[1],p);
	}
	else // if P != infinity
	{
		mpz_powm_ui(lambda,P[0],2,p);
		mpz_sub_ui(lambda,lambda,1);
		mpz_mul_ui(lambda,lambda,3);
		mpz_mul_2exp(EAT1,P[1],1);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_mul_2exp(EAT1,P[0],1);
		mpz_sub(EAR[0],lambda2,EAT1);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

		mpz_sub(EAR[1],EAT1,lambda2);
		mpz_add(EAR[1],EAR[1],P[0]);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py
	}
	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to add two points (R=P+Q) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_addition(mpz_t R[2], mpz_t P[2], mpz_t Q[2])
{
	// set EAT = -Q[1] mod p
	mpz_set(EAT,Q[1]);
	mpz_neg(EAT,EAT);
	mpz_mod(EAT,EAT,p);

	if(mpz_cmp(P[0],p)==0) // if P = point at infinity
	{
		mpz_set(EAR[0],Q[0]);
		mpz_set(EAR[1],Q[1]);
	}
	else if(mpz_cmp(Q[0],p)==0) // if Q = point at infinity
	{
		mpz_set(EAR[0],P[0]);
		mpz_set(EAR[1],P[1]);
	}
	else if(mpz_cmp(P[0],Q[0])==0 && mpz_cmp(P[1],EAT)==0) // if P = -Q
	{
		mpz_set(EAR[0],p);
		mpz_set(EAR[1],p);
	}
	else if(mpz_cmp(P[0],Q[0])) // if Px != Qx
	{
		mpz_sub(lambda,Q[1],P[1]);
		mpz_sub(EAT1,Q[0],P[0]);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (Qy-Py)/(Qx-Px)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_sub(EAR[0],lambda2,P[0]);
		mpz_sub(EAR[0],EAR[0],Q[0]);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - Px - Qx

		mpz_sub(EAR[1],Q[0],lambda2);
		mpz_addmul_ui(EAR[1],P[0],2);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = ((2*Px + Py) - lambda^2)*lambda - Py
	}
	else // if Px = Qx but P != -Q
	{
		mpz_powm_ui(lambda,P[0],2,p);
		mpz_sub_ui(lambda,lambda,1);
		mpz_mul_ui(lambda,lambda,3);
		mpz_mul_2exp(EAT1,P[1],1);
		mpz_invert(EAT1,EAT1,p);
		mpz_mul(lambda,lambda,EAT1);
		mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

		mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

		mpz_mul_2exp(EAT1,P[0],1);
		mpz_sub(EAR[0],lambda2,EAT1);
		mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

		mpz_sub(EAR[1],EAT1,lambda2);
		mpz_add(EAR[1],EAR[1],P[0]);
		mpz_mul(EAR[1],EAR[1],lambda);
		mpz_mod(EAR[1],EAR[1],p);
		mpz_sub(EAR[1],EAR[1],P[1]);
		mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py
	}
	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to double a point (R=[2]P) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_double_specific(mpz_t R[2], mpz_t P[2])
{
	mpz_powm_ui(lambda,P[0],2,p);
	mpz_sub_ui(lambda,lambda,1);
	mpz_mul_ui(lambda,lambda,3);
	mpz_mul_2exp(EAT1,P[1],1);
	mpz_invert(EAT1,EAT1,p);
	mpz_mul(lambda,lambda,EAT1);
	mpz_mod(lambda,lambda,p); // lambda = (3.Px^2 - 3)/(2.Py)

	mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

	mpz_mul_2exp(EAT1,P[0],1);
	mpz_sub(EAR[0],lambda2,EAT1);
	mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - 2*Px

	mpz_sub(EAR[1],EAT1,lambda2);
	mpz_add(EAR[1],EAR[1],P[0]);
	mpz_mul(EAR[1],EAR[1],lambda);
	mpz_mod(EAR[1],EAR[1],p);
	mpz_sub(EAR[1],EAR[1],P[1]);
	mpz_mod(EAR[1],EAR[1],p); // Ry = (3*Px - lambda^2)*lambda - Py

	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to add two points (R=P+Q) on the elliptic curve E : y^2 = x^3 - 3*x
// see Silverman, The Arithmetic of Elliptic Curves, 2.3
void elliptic_addition_specific(mpz_t R[2], mpz_t P[2], mpz_t Q[2])
{
	mpz_sub(lambda,Q[1],P[1]);
	mpz_sub(EAT1,Q[0],P[0]);
	mpz_invert(EAT1,EAT1,p);
	mpz_mul(lambda,lambda,EAT1);
	mpz_mod(lambda,lambda,p); // lambda = (Qy-Py)/(Qx-Px)

	mpz_powm_ui(lambda2,lambda,2,p); // lambda2 = lambda^2

	mpz_sub(EAR[0],lambda2,P[0]);
	mpz_sub(EAR[0],EAR[0],Q[0]);
	mpz_mod(EAR[0],EAR[0],p); // Rx = lambda^2 - Px - Qx

	mpz_sub(EAR[1],Q[0],lambda2);
	mpz_addmul_ui(EAR[1],P[0],2);
	mpz_mul(EAR[1],EAR[1],lambda);
	mpz_mod(EAR[1],EAR[1],p);
	mpz_sub(EAR[1],EAR[1],P[1]);
	mpz_mod(EAR[1],EAR[1],p); // Ry = ((2*Px + Py) - lambda^2)*lambda - Py

	mpz_set(R[0],EAR[0]);
	mpz_set(R[1],EAR[1]);
}

// function to multiply a point on the curve by an mpz_t integer, R=[n]Q
void elliptic_mult(mpz_t R[2], mpz_t n, mpz_t Q[2])
{
	unsigned int m=bits(n)-1;
	unsigned int i;

	mpz_set(R[0],Q[0]);
	mpz_set(R[1],Q[1]);

	if(mpz_cmp_ui(n,0))
	{
		for(i=1;i<=m;i++)
		{
			elliptic_double(R,R);
			if(mpz_tstbit(n,m-i)==1) elliptic_addition(R,R,Q);
		}
	}
	else
	{
		mpz_set(R[0],p);
		mpz_set(R[1],p);
	}
}

//// function to raise elt a of FP_p (where a = B/A is F_p rep of A + iB) to power n
void Fp2power(mpz_t c, mpz_t a, mpz_t n)
{
	mpz_init_set_str(p,"997ABB1F 0A563FDA 65C61198 DAD0657A 416C0CE1 9CB48261 BE9AE358 B3E01A2E F40AAB27 E2FC0F1B 228730D5 31A59CB0 E791B39F F7C88A19 356D27F4 A666A6D0 E26C6487 326B4CD4 512AC5CD 65681CE1 B6AFF4A8 31852A82 A7CF3C52 1C3C09AA 9F94D6AF 56971F1F FCE3E823 89857DB0 80C5DF10 AC7ACE87 666D807A FEA85FEB",16);
	mpz_t temp; mpz_init(temp);
	mpz_t A[2]; mpz_init_set_ui(A[0],1); mpz_init_set(A[1],a);
	mpz_t ans[2]; mpz_init_set_ui(ans[0],1); mpz_init_set(ans[1],a);

	unsigned int m=bits(n)-1;
	unsigned int i;

	if(mpz_cmp_ui(n,0))
	{
		for(i=1;i<=m;i++)
		{
			if(mpz_tstbit(n,m-i)==0) Fp2sqr(ans,ans);
			else
			{
				Fp2sqr(ans,ans);
				Fp2mult(ans,ans,A);
			}
		}
		mpz_invert(c,ans[0],p);
		mpz_mul(c,c,ans[1]);
		mpz_mod(c,c,p); // x = vy/vx where v = vx + i.vy
	}
	else mpz_set_ui(c,0);
	mpz_clear(temp);
	mpz_clear(ans[0]); mpz_clear(ans[1]);
	mpz_clear(A[0]); mpz_clear(A[1]);
}

jstring Java_dja_alice_AliceBlue_power(JNIEnv * env, jobject thiz, jstring rin){
	const char * rs;
	rs = (*env)->GetStringUTFChars(env,rin,NULL);

	mpz_t g; mpz_init_set_str(g,"66FC2A432B6EA392148F15867D623068C6A87BD1FB94C41E27FABE658E015A87371E94744C96FEDA449AE9563F8BC446CBFDA85D5D00EF577072DA8F541721BEEE0FAED1828EAB90B99DFB0138C7843355DF0460B4A9FD74B4F1A32BCAFA1FFAD682C033A7942BCCE3720F20B9B7B0403C8CAE87B7A0042ACDE0FAB36461EA46",16);
	mpz_t r; mpz_init_set_str(r,rs,16);
	(*env)->ReleaseStringUTFChars(env,rin,rs);
	mpz_t gr; mpz_init(gr);

	initEAV();
	Fp2power(gr,g,r);
	clearEAV();

	char * ret = calloc(1,mpz_sizeinbase(gr,16));
	gmp_sprintf(ret,"%Zx",gr);

	return (*env)->NewStringUTF(env,ret);
}

//jstring Java_dja_alice_Alice_calc(JNIEnv * env, jobject thiz, jstring jr){
//	// INITIALISE VARIABLES
//	initEAV();
//	initFp2();
//
//	// set value of p and q
//	mpz_init_set_str(p,"997ABB1F 0A563FDA 65C61198 DAD0657A 416C0CE1 9CB48261 BE9AE358 B3E01A2E F40AAB27 E2FC0F1B 228730D5 31A59CB0 E791B39F F7C88A19 356D27F4 A666A6D0 E26C6487 326B4CD4 512AC5CD 65681CE1 B6AFF4A8 31852A82 A7CF3C52 1C3C09AA 9F94D6AF 56971F1F FCE3E823 89857DB0 80C5DF10 AC7ACE87 666D807A FEA85FEB",16);
//	mpz_init_set_str(q,"265EAEC7 C2958FF6 99718466 36B4195E 905B0338 672D2098 6FA6B8D6 2CF8068B BD02AAC9 F8BF03C6 C8A1CC35 4C69672C 39E46CE7 FDF22286 4D5B49FD 2999A9B4 389B1921 CC9AD335 144AB173 595A0738 6DABFD2A 0C614AA0 A9F3CF14 870F026A A7E535AB D5A5C7C7 FF38FA08 E2615F6C 203177C4 2B1EB3A1 D99B601E BFAA17FB",16);
//	// set qminusone
//	mpz_init(qminusone); mpz_sub_ui(qminusone,q,1);
//
//	const char * cr;
//	cr = (*env)->GetStringUTFChars(env,jr,NULL);
//
//
//	mpz_t K[2];
//	mpz_init_set_str(K[0],"0AB631BD 80052A89 5AE44295 753842D5 0B64798C 723D55FC AC4048DC A7BF61C6 F42C26D1 C82A8558 C5C19D50 98F9F706 037793B9 FE2062C3 7BFFDFC6 D5E4308B BD2C3FFB 24767D86 11C08A17 5DE13AE8 E4DB0F77 536877B5 A3262AAD AFD007B7 F07F9A1E 4263A1EC F73E050C CEE68AF6 741EE7BB CF0FB40E C31C2E10 6488EE82",16);
//	mpz_init_set_str(K[1],"8614FA9A F6EAC87A 9FE1C996 13235038 A0473076 B5C0A0EB 1BB321AB AD030905 7A491EB0 4A2A98D1 EA1E57D8 DE924B0B 41642FFE 62642FCA 794F6DFF CCB03B2E E2ACCCFA 469951D2 4778E032 125CE424 628A54FA F11DABCD CD23B72D D76401B5 62A655FB 9B56581A 46CEAC6E 0C0A5E2A F23D30B3 1BC1D6B4 E0068701 646EDDE8",16);
//
//	mpz_t R[2];
//	mpz_init_set_str(R[0],cra,16);
//	mpz_init_set_str(R[1],crb,16);
//
//	// dummy variable to hold value of computed pairing
//	mpz_t w; mpz_init(w);
//
//	// COMPUTE PAIRING
//
//	TLpairing(w,R,K);
//
//
//	char * ret = malloc(mpz_sizeinbase(w,16));
//	gmp_sprintf(ret,"%Zx",w);
//
//	(*env)->ReleaseStringUTFChars(env,jra,cra);
//	(*env)->ReleaseStringUTFChars(env,jrb,crb);
//
//	// CLEAR VARIABLES
//	clearEAV();
//	clearFp2();
//
//	return (*env)->NewStringUTF(env,ret);
//}

jbyteArray Java_dja_alice_AliceBlue_muladdmul(JNIEnv * env, jobject thiz, jstring rin,jstring bin, jstring zin){
	const char * rs = (*env)->GetStringUTFChars(env,rin,NULL);
	BIGNUM * r = BN_new(); BN_hex2bn(&r, rs);
	(*env)->ReleaseStringUTFChars(env,rin,rs);

	const char * bs = (*env)->GetStringUTFChars(env,bin,NULL);
	BIGNUM * b = BN_new(); BN_hex2bn(&b, bs);
	(*env)->ReleaseStringUTFChars(env,bin,bs);

	const char * zs = (*env)->GetStringUTFChars(env,zin,NULL);

	BIGNUM * primeP = BN_new(); BN_hex2bn(&primeP, "997ABB1F0A563FDA65C61198DAD0657A416C0CE19CB48261BE9AE358B3E01A2EF40AAB27E2FC0F1B228730D531A59CB0E791B39FF7C88A19356D27F4A666A6D0E26C6487326B4CD4512AC5CD65681CE1B6AFF4A831852A82A7CF3C521C3C09AA9F94D6AF56971F1FFCE3E82389857DB080C5DF10AC7ACE87666D807AFEA85FEB");
	BIGNUM * primeQ = BN_new(); BN_hex2bn(&primeQ, "265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB");

	BIGNUM * primeQminusONE = BN_new();
	BN_copy(primeQminusONE,primeQ);
	BN_sub_word(primeQminusONE,1);

	BIGNUM * coeffA = BN_new(); BN_copy(coeffA, primeP); BN_sub_word(coeffA,3);
	BIGNUM * coeffB = BN_new(); BN_zero(coeffB);

	BN_CTX * CTX = BN_CTX_new();
	EC_GROUP * curve = EC_GROUP_new_curve_GFp(primeP, coeffA, coeffB, CTX);

	EC_POINT * P = EC_POINT_new(curve);
	EC_POINT_hex2point(curve, "0453FC09EE332C29AD0A7990053ED9B52A2B1A2FD60AEC69C698B2F204B6FF7CBFB5EDB6C0F6CE2308AB10DB9030B09E1043D5F22CDB9DFA55718BD9E7406CE8909760AF765DD5BCCB337C86548B72F2E1A702C3397A60DE74A7C1514DBA66910DD5CFB4CC80728D87EE9163A5B63F73EC80EC46C4967E0979880DC8ABEAE638950A8249063F6009F1F9F1F0533634A135D3E82016029906963D778D821E141178F5EA69F4654EC2B9E7F7F5E5F0DE55F66B598CCF9A140B2E416CFF0CA9E032B970DAE117AD547C6CCAD696B5B7652FE0AC6F1E80164AA989492D979FC5A4D5F213515AD7E9CB99A980BDAD5AD5BB4636ADB9B5706A67DCDE75573FD71BEF16D7",P,CTX);

	EC_POINT * Z = EC_POINT_new(curve);
	EC_POINT_hex2point(curve, zs,Z, CTX);
	(*env)->ReleaseStringUTFChars(env,zin,zs);

	EC_POINT * rbPZ = EC_POINT_new(curve);
//	EC_POINT * bP = EC_POINT_new(curve);
//	EC_POINT * bPZ = EC_POINT_new(curve);
//
//	EC_POINT_mul(curve, bP, NULL, P, b, CTX);
//	EC_POINT_add(curve,bPZ,bP,Z,CTX);
//	EC_POINT_mul(curve, rbPZ, NULL, bPZ, r, CTX);

	EC_POINT * ECpoints[2];
	ECpoints[0] = EC_POINT_new(curve); EC_POINT_copy(ECpoints[0], P);
	ECpoints[1] = EC_POINT_new(curve); EC_POINT_copy(ECpoints[1], Z);
	BIGNUM * scalars[2];
	scalars[0] = BN_new(); BN_mod_mul(scalars[0],r,b,primeQ,CTX);
	scalars[1] = BN_new(); BN_copy(scalars[1],r);

	EC_POINTs_mul(curve, rbPZ, NULL, 2, (const EC_POINT **) ECpoints,(const BIGNUM **)scalars, CTX);

	unsigned char * R = malloc(257);
	EC_POINT_point2oct(curve,rbPZ,4,R,257,CTX);

	jbyteArray jR = (*env)->NewByteArray(env,257);
	(*env)->SetByteArrayRegion(env,jR,0,257,R);

	return jR;
}

jbyteArray Java_dja_alice_AliceBlue_eccsisign(JNIEnv * env,jobject thiz, jbyteArray SSKin, jbyteArray PVTin, jbyteArray HSin, jbyteArray sakkepayloadin){

	int ml = 8;

	jboolean * HS = (*env)->GetBooleanArrayElements(env,HSin,NULL);//32
	jboolean * sakkepayload = (*env)->GetBooleanArrayElements(env,sakkepayloadin,NULL);//277
	jboolean * SSK = (*env)->GetBooleanArrayElements(env,SSKin,NULL);//32
	jboolean * PVT = (*env)->GetBooleanArrayElements(env,PVTin,NULL);//65

	BIGNUM * signq = BN_new(); BN_hex2bn(&signq, "FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551");

	BIGNUM * test = BN_new(); BN_zero(test);
	BIGNUM * ZERO = BN_new(); BN_zero(ZERO);

	BIGNUM * qm1 = BN_new(); BN_hex2bn(&qm1, "FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632550");
	BN_CTX * CTX = BN_CTX_new();
	BIGNUM * j = BN_new(); //BN_hex2bn(&j, "34567");

	EC_GROUP * NISTp256 = EC_GROUP_new_by_curve_name(NID_X9_62_prime256v1);
	EC_POINT * J = EC_POINT_new(NISTp256);
	BIGNUM * Jx = BN_new(); BIGNUM * Jy = BN_new();

	unsigned char * tempr;
	unsigned char * r = calloc(32,sizeof(unsigned char));
	int diff = 0;
	int i = 0;
	unsigned char * HSrM = calloc(32+32+ml,sizeof(unsigned char));
	int numbytes = 0;
	BIGNUM * HEbn = BN_new();
	BIGNUM * SSKbn = BN_new();


	while(BN_cmp(test,ZERO)==0) {

		BN_rand_range(j,qm1); BN_add_word(j,1);

		EC_POINT_mul(NISTp256, J, j, NULL, NULL, CTX);

		EC_POINT_get_affine_coordinates_GFp(NISTp256, J, Jx, Jy, CTX);

		numbytes = BN_num_bytes(Jx);
		tempr = calloc(numbytes,sizeof(unsigned char));

		BN_bn2bin(Jx,tempr);
		diff = 32-numbytes;
		for (i=diff ; i<32 ; i++) {
			r[i] = tempr[i-diff];
		}

		for (i=0 ; i<32 ; i++) {
			HSrM[i] = HS[i];
			HSrM[32+i] = r[i];
		}
		for (i=0 ; i<ml ; i++) {
			HSrM[64+i] = sakkepayload[i];
		}

		unsigned char * HE = calloc(32,sizeof(unsigned char));
		HE = SHA256(HSrM,32+32+ml,NULL);

		BN_bin2bn(HE,32,HEbn);
		BN_bin2bn(SSK,32,SSKbn);

		BN_mod_mul(test, Jx, SSKbn, signq, CTX);
		BN_mod_add(test, test, HEbn, signq, CTX);


	}




	BIGNUM * sdash = BN_new();
	BN_mod_inverse(sdash, test, signq, CTX);
	BN_mod_mul(sdash, sdash, j, signq, CTX);

	BN_zero(j);

	unsigned char * s = calloc(32,sizeof(unsigned char));
	numbytes = BN_num_bytes(sdash);
	unsigned char * temps = calloc(numbytes,sizeof(unsigned char));
	BN_bn2bin(sdash,temps);
	diff = 32-numbytes;
	for (i=diff ; i<32 ; i++) {
		s[i] = temps[i-diff];
	}


	unsigned char * signature = calloc(32+32+65,sizeof(unsigned char));
	for(i=0 ; i<32 ; i++) {
		signature[i] = r[i];
		signature[32+i] = s[i];
	}
	for(i=0 ; i<65 ; i++) {
		signature[64+i] = PVT[i];
	}

	//	char * ret = calloc(2*(32+32+65),sizeof(char));
	//	for(i=0;i<32+32+65;i++) sprintf(ret+strlen(ret),"%02x",signature[i]);
	//	return (*env)->NewStringUTF(env,ret);

	jbyteArray jsignature = (*env)->NewByteArray(env,32+32+65);
	(*env)->SetByteArrayRegion(env,jsignature,0,32+32+65,signature);

	return jsignature;
}

jboolean Java_dja_alice_AliceBlue_eccsiverify(JNIEnv * env,jobject thiz,jbyteArray KPAKin, jbyteArray IDin, jbyteArray signaturein, jbyteArray sakkepayloadin) {

	int ml = 8;

	jboolean * signature = (*env)->GetBooleanArrayElements(env,signaturein,NULL);

	jboolean * KPAK = (*env)->GetBooleanArrayElements(env,KPAKin,NULL);
	jboolean * ID = (*env)->GetBooleanArrayElements(env,IDin,NULL);
	jboolean * sakkepayload = (*env)->GetBooleanArrayElements(env,sakkepayloadin,NULL);

	unsigned char * r = calloc(32,sizeof(unsigned char));
	unsigned char * s = calloc(32,sizeof(unsigned char));

	int i = 0;

	unsigned char PVToncurve = 0;
	unsigned char Jxrnonzero = 0;

	BIGNUM * signq = BN_new(); BN_hex2bn(&signq, "FFFFFFFF00000000FFFFFFFFFFFFFFFFBCE6FAADA7179E84F3B9CAC2FC632551");
	BN_CTX * CTX = BN_CTX_new();

	BIGNUM * ZERO = BN_new(); BN_zero(ZERO);

	for(i=0 ; i<32 ; i++) {
		r[i] = signature[i];
		s[i] = signature[i+32];
	}

	unsigned char * PVT = calloc(65,sizeof(unsigned char));
	for(i=0 ; i<65 ; i++) PVT[i] = signature[64+i];

	EC_GROUP * NISTp256 = EC_GROUP_new_by_curve_name(NID_X9_62_prime256v1);
	EC_POINT * PVTecp = EC_POINT_new(NISTp256);
	EC_POINT_oct2point(NISTp256, PVTecp, PVT, 65, CTX);

	if(EC_POINT_is_on_curve(NISTp256,PVTecp,CTX)==0) {
		PVToncurve = 0; // point not on curve
	}
	else {
		PVToncurve = 1; // point on curve
	}

	unsigned char * G = calloc(65,sizeof(unsigned char));
	EC_POINT_point2oct(NISTp256,NISTp256->generator,4,G,65,CTX);

//	char * t = calloc(65,sizeof(char));
//	for(i=0;i<65;i++) sprintf(t+strlen(t),"%02X",G[i]);
//	return (*env)->NewStringUTF(env,t);

	unsigned char * GKPAKIDPVT = calloc(65+65+26+65,sizeof(unsigned char));
	for(i=0 ; i<65 ; i++) {
		GKPAKIDPVT[i] = G[i];
		GKPAKIDPVT[65+i] = KPAK[i];
		GKPAKIDPVT[65+65+26+i] = PVT[i];
	}
	for(i=0 ; i<26 ; i++) {
		GKPAKIDPVT[130+i] = ID[i];
	}

//	char * ret = calloc(65+65+65+26,sizeof(char));
//	sprintf(ret+strlen(ret),"\n\n");
//	for(i=0;i<65+65+65+26;i++) sprintf(ret+strlen(ret),"%02x",GKPAKIDPVT[i]);
//	return (*env)->NewStringUTF(env,ret);

	unsigned char * HS = calloc(32,sizeof(unsigned char));
	SHA256(GKPAKIDPVT,65+65+26+65,HS);

	unsigned char * HSrM = calloc(32+32+ml,sizeof(unsigned char));
	for (i=0 ; i<32 ; i++) {
		HSrM[i] = HS[i];
		HSrM[32+i] = r[i];
	}
	for (i=0 ; i<ml ; i++) {
		HSrM[64+i] = sakkepayload[i];
	}

	unsigned char * HE = calloc(32,sizeof(unsigned char));
	SHA256(HSrM,32+32+ml,HE);

	BIGNUM * sbn = BN_bin2bn(s,32,NULL);
	BIGNUM * rbn = BN_bin2bn(r,32,NULL);
	BIGNUM * HSbn = BN_bin2bn(HS,32,NULL);
	BIGNUM * HEbn = BN_bin2bn(HE,32,NULL);

//	return (*env)->NewStringUTF(env,BN_bn2hex(HEbn));

	BIGNUM * sHEbn = BN_new(); BN_mod_mul(sHEbn,sbn,HEbn,signq,CTX);
	BIGNUM * srHSbn = BN_new(); BN_mod_mul(srHSbn,sbn,rbn,signq,CTX); BN_mod_mul(srHSbn,srHSbn,HSbn,signq,CTX);
	BIGNUM * srbn = BN_new(); BN_mod_mul(srbn,sbn,rbn,signq,CTX);

	EC_POINT * KPAKecp = EC_POINT_new(NISTp256);
	EC_POINT_oct2point(NISTp256, KPAKecp, KPAK, 65, CTX);

	EC_POINT * J = EC_POINT_new(NISTp256);

	EC_POINT * ECpoints[2];
	ECpoints[0] = EC_POINT_new(NISTp256); EC_POINT_copy(ECpoints[0], PVTecp);
	ECpoints[1] = EC_POINT_new(NISTp256); EC_POINT_copy(ECpoints[1], KPAKecp);
	BIGNUM * scalars[2];
	scalars[0] = BN_new(); BN_copy(scalars[0],srHSbn);
	scalars[1] = BN_new(); BN_copy(scalars[1],srbn);

	EC_POINTs_mul(NISTp256, J, sHEbn, 2, (const EC_POINT **) ECpoints,(const BIGNUM **)scalars, CTX);

//	char * ret = calloc(2*(32+32+65),sizeof(char));
//	for(i=0;i<32+32+65;i++) sprintf(ret+strlen(ret),"%02x",signature[i]);
//	return (*env)->NewStringUTF(env,ret);

	BIGNUM * Jx = BN_new(); BIGNUM * Jy = BN_new();
	EC_POINT_get_affine_coordinates_GFp(NISTp256, J, Jx, Jy, CTX);

//	return (*env)->NewStringUTF(env,EC_POINT_point2hex(NISTp256, J, 4, CTX));

//	return (*env)->NewStringUTF(env,BN_bn2hex(Jx));

	if(BN_cmp(Jx,rbn)!=0 || BN_cmp(Jx,ZERO)==0) {
		Jxrnonzero = 0;
	}
	else {
		Jxrnonzero = 1;
	}

	return Jxrnonzero & PVToncurve;
}
