LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE    := pbgmp
LOCAL_SRC_FILES := libgmp.a
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include

include $(PREBUILT_STATIC_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := pbssl
LOCAL_SRC_FILES := devicecrypto.so
LOCAL_EXPORT_C_INCLUDES := $(LOCAL_PATH)/include $(LOCAL_PATH)/include/opensssl

include $(PREBUILT_SHARED_LIBRARY)

include $(CLEAR_VARS)

LOCAL_MODULE    := mygmp
LOCAL_SRC_FILES := alice.c
LOCAL_STATIC_LIBRARIES := pbssl pbgmp

include $(BUILD_SHARED_LIBRARY)