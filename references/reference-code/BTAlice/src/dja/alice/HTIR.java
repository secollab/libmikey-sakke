package dja.alice;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import android.util.Log;


public class HTIR {

	public static void main(String[] args) {
		try{
			BigInteger b = htir("123456789ABCDEF0123456789ABCDEF0323031302D30370074656C3A2B34343132333435363738393000",
					new BigInteger("265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB",16));
			System.out.println(b.toString(16));
			}
		catch (Exception e){e.printStackTrace();}

	}

	public static BigInteger htir(String octet,BigInteger n){
		MessageDigest md = null;
		while(md==null){
			try {md = MessageDigest.getInstance("SHA-256");} 
			catch (NoSuchAlgorithmException e) {Log.e("SHA", "No such alg");}
		}
		String A = hxs(octet,md);		
		
		int q = n.bitLength()/256;
		int r = n.bitLength()%256;
		if(r>0) q++;
		int l=q;
		l++;
		
		String[] h = new String[l]; 
		String[] v = new String[l];
		String V = "";
		
		byte[] zeros = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
		h[1]=new BigInteger(md.digest(zeros)).toString(16);
		
		v[1] = hxs(h[1].concat(A),md);
		V=V.concat(v[1]);
		
		for(int i=2;i<l;i++){
			h[i] = hxs(h[i-1],md);
			v[i] = hxs(h[i].concat(A),md);
			V=V.concat(v[i]);
		}
		
		return new BigInteger(V,16).mod(n);
	}
	
	//hxs returns the hex string representation of a hash of a hex string
	private static String hxs(String in, MessageDigest md){		
		byte[] bytes = (new BigInteger(in,16)).toByteArray();
		if(bytes.length==65&&bytes[0]==0){bytes = bodge(bytes);}
		
		return new BigInteger(1,md.digest(bytes)).toString(16);
	}
	
	//bodge strips a leading zero in the byte array returned by biginteger constructor
	private static byte[] bodge(byte[] in){
		byte [] ret = new byte[64];
		for(int i=0;i<64;i++){
			ret[i]=in[i+1];
		}
		return ret;
	}
}
