/*
 * SAKKEpairing-projective.c
 *
 *  Created on: 8 Jul 2011
 *      Author: alan
 */

#include <openssl/ec.h>
#include <stdio.h>
#include <time.h>
#include <ec_lcl.h>
#include <jni.h>
#include <string.h>

// constants
BIGNUM * primeP;
BIGNUM * primeQ;
BIGNUM * primeQminusONE;
BIGNUM * coeffA;
BIGNUM * coeffB;
EC_GROUP * curve;
BIGNUM * ZERO;
BIGNUM * ONE;
BIGNUM * TWO;
BIGNUM * THREE;
BIGNUM * CONSTANT;

// temp variables for mod_inv()
BIGNUM * a;
BIGNUM * b;
BIGNUM * c;
BIGNUM * quot;
BIGNUM * P;
BIGNUM * Q;
BIGNUM * R;
BIGNUM * S;
BIGNUM * R_temp;
BIGNUM * S_temp;

// temp variables
BN_CTX * CTX;
BIGNUM * tempA[2], * tempB[2];
BIGNUM * TEMP;
BIGNUM * RET[2];
BIGNUM * lambda, * nu;
BIGNUM * temp1, * temp2, * temp3;
BIGNUM * CpX, * CpY, * CpZ;
BIGNUM * RpX, * RpY, * RpZ;

// function to multiply two elements PF_p[i], c = a.b
void Fp2mult(BIGNUM * c[2], BIGNUM * a[2], BIGNUM * b[2])
{
	// USES TEMP GLOBAL VARIABLES RET[2], TEMP, CTX

	BN_mul(RET[0],a[0],b[0],CTX);
	BN_mul(TEMP,a[1],b[1],CTX);
	BN_mod_sub(RET[0],RET[0],TEMP,primeP,CTX);

	BN_mul(RET[1],a[0],b[1],CTX);
	BN_mul(TEMP,a[1],b[0],CTX);
	BN_mod_add(RET[1],RET[1],TEMP,primeP,CTX);

	BN_copy(c[0],RET[0]); BN_copy(c[1],RET[1]);
}

// PAIRING: function to compute the T-L pairing <R,Q> of two points R,Q of E(F_p)[q]
// NOTE: R,Q are not the point at infinity
BIGNUM * TLpairing(EC_POINT * R, EC_POINT * Q) {

	BIGNUM * t = BN_new();
	BIGNUM * tempX = BN_new(), * tempY = BN_new();
	BIGNUM * temp1 = BN_new(), * temp2 = BN_new();

	BIGNUM * arrayQx = BN_new();
	BIGNUM * arrayQy = BN_new();
	EC_POINT_get_affine_coordinates_GFp(curve, Q, arrayQx, arrayQy, CTX);

//	EC_POINT * R = EC_POINT_new(curve);
//	EC_POINT_set_affine_coordinates_GFp(curve, R, arrayR[0], arrayR[1], CTX);
//	EC_POINT * Q = EC_POINT_new(curve);
//	EC_POINT_set_affine_coordinates_GFp(curve, Q, arrayQx, arrayQy, CTX);

	// Initialise variables
	// element of PF_p[q], v = (F_p)*
	BIGNUM * v[2]; v[0] = BN_new(); v[1] = BN_new();
	BN_one(v[0]); BN_zero(v[1]);

	EC_POINT * C = EC_POINT_new(curve);
	EC_POINT_copy(C,R);

	int length = BN_num_bits(primeQminusONE);

	int i;

	for (i=length-2 ; i>=0 ; i--) {

//		gradient of line through C, C, [-2]C and accumulate line evaluated at [i]Q into v

		ec_GFp_simple_get_Jprojective_coordinates_GFp(curve, C, CpX, CpY, CpZ, CTX);

		BN_mod_sqr(temp1,CpZ,primeP,CTX);
		BN_mod_mul(tempA[0],arrayQx,temp1,primeP,CTX);
		BN_mod_add(tempA[0], tempA[0], CpX, primeP, CTX);
		BN_mod_add(temp2,CpX,temp1,primeP,CTX);
		BN_mod_mul(tempA[0],tempA[0],temp2,primeP,CTX);
		BN_mod_sub(temp2,CpX,temp1,primeP,CTX);
		BN_mod_mul(tempA[0],tempA[0],temp2,primeP,CTX);
		BN_mul_word(tempA[0],3);
		BN_mod_sqr(temp2,CpY,primeP,CTX);
		BN_add(temp2,temp2,temp2);
		BN_mod_sub(tempA[0],tempA[0],temp2,primeP,CTX);

		BN_mod_mul(tempA[1],temp1,CpZ,primeP,CTX);
		BN_mod_mul(tempA[1],tempA[1],CpY,primeP,CTX);
		BN_add(tempA[1],tempA[1],tempA[1]);
		BN_mod_mul(tempA[1],tempA[1],arrayQy,primeP,CTX);

		Fp2mult(v,v,v);
		Fp2mult(v,v,tempA);

		EC_POINT_dbl(curve, C, C, CTX);

		if(BN_is_bit_set(primeQminusONE, i)) {


//			gradient of line through C, R, -C-R and accumulate line evaluated at [i]Q into v

			ec_GFp_simple_get_Jprojective_coordinates_GFp(curve, C, CpX, CpY, CpZ, CTX);
			ec_GFp_simple_get_Jprojective_coordinates_GFp(curve, R, RpX, RpY, RpZ, CTX);

			BN_mod_sqr(temp1,RpZ,primeP,CTX);
			BN_mod_sqr(temp2,CpZ,primeP,CTX);

			BN_mod_mul(tempX,arrayQx,temp1,primeP,CTX);
			BN_mod_add(tempX,tempX,RpX,primeP,CTX);
			BN_mod_mul(tempX,tempX,RpZ,primeP,CTX);
			BN_mod_mul(tempX,tempX,CpY,primeP,CTX);

			BN_mod_mul(tempY,arrayQx,temp2,primeP,CTX);
			BN_mod_add(tempY,tempY,CpX,primeP,CTX);
			BN_mod_mul(tempY,tempY,CpZ,primeP,CTX);
			BN_mod_mul(tempY,tempY,RpY,primeP,CTX);

			BN_mod_sub(tempA[0],tempX,tempY,primeP,CTX);

			BN_mod_mul(tempX,temp1,CpX,primeP,CTX);
			BN_mod_mul(tempY,temp2,RpX,primeP,CTX);
			BN_mod_sub(tempA[1],tempX,tempY,primeP,CTX);
			BN_mod_mul(tempA[1],tempA[1],CpZ,primeP,CTX);
			BN_mod_mul(tempA[1],tempA[1],RpZ,primeP,CTX);
			BN_mod_mul(tempA[1],tempA[1],arrayQy,primeP,CTX);

			Fp2mult(v,v,tempA);

			EC_POINT_add(curve, C, C, R, CTX);

		}

	}

	//	v = v^c where c = (primeP + 1)/primeQ = 4

	Fp2mult(v,v,v);
	Fp2mult(v,v,v);

	BN_mod_inverse(temp1,v[0],primeP,CTX);

	BN_mod_mul(t,temp1,v[1],primeP,CTX);

	return t;
}

////////////////////////////////////////////////////////////

jstring Java_dja_bob_Bob_pair(JNIEnv * env, jobject thiz){
	char * ret = calloc(1,1000);

	CONSTANT = BN_new(); BN_hex2bn(&CONSTANT, "668544E0F5A9C0259A39EE67252F9A85BE93F31E634B7D9E41651CA74C1FE5D10BF554D81D03F0E4DD78CF2ACE5A634F186E4C60083775E6CA92D80B5999592F1D939B78CD94B32BAED53A329A97E31E49500B57CE7AD57D5830C3ADE3C3F655606B2950A968E0E0031C17DC767A824F7F3A20EF5385317899927F850157A015");

	CTX = BN_CTX_new();
	TEMP = BN_new();
	temp1 = BN_new(); temp2 = BN_new(); temp3 = BN_new();
	tempA[0] = BN_new(); tempA[1] = BN_new();
	tempB[0] = BN_new(); tempB[1] = BN_new();
	lambda = BN_new(); nu = BN_new();
	RET[0] = BN_new(); RET[1] = BN_new();

	a = BN_new(); b = BN_new(); c = BN_new();
	quot = BN_new();
	P = BN_new(); Q = BN_new(); R = BN_new(); S = BN_new();
	R_temp = BN_new(); S_temp = BN_new();

	CpX = BN_new(); CpY = BN_new(); CpZ = BN_new();
	RpX = BN_new(); RpY = BN_new(); RpZ = BN_new();

	primeP = BN_new(); BN_hex2bn(&primeP, "997ABB1F0A563FDA65C61198DAD0657A416C0CE19CB48261BE9AE358B3E01A2EF40AAB27E2FC0F1B228730D531A59CB0E791B39FF7C88A19356D27F4A666A6D0E26C6487326B4CD4512AC5CD65681CE1B6AFF4A831852A82A7CF3C521C3C09AA9F94D6AF56971F1FFCE3E82389857DB080C5DF10AC7ACE87666D807AFEA85FEB");
	primeQ = BN_new(); BN_hex2bn(&primeQ, "265EAEC7C2958FF69971846636B4195E905B0338672D20986FA6B8D62CF8068BBD02AAC9F8BF03C6C8A1CC354C69672C39E46CE7FDF222864D5B49FD2999A9B4389B1921CC9AD335144AB173595A07386DABFD2A0C614AA0A9F3CF14870F026AA7E535ABD5A5C7C7FF38FA08E2615F6C203177C42B1EB3A1D99B601EBFAA17FB");

	primeQminusONE = BN_new();
	BN_copy(primeQminusONE,primeQ);
	BN_sub_word(primeQminusONE,1);

	coeffA = BN_new(); BN_copy(coeffA, primeP); BN_sub_word(coeffA,3);
	coeffB = BN_new(); BN_zero(coeffB);

	curve = EC_GROUP_new_curve_GFp(primeP, coeffA, coeffB, CTX);

	ZERO = BN_new(); BN_zero(ZERO);
	ONE = BN_new(); BN_one(ONE);
	TWO = BN_new(); BN_copy(TWO,ONE); BN_add_word(TWO,1);
	THREE = BN_new(); BN_copy(THREE,TWO); BN_add_word(THREE,1);

	EC_POINT * generatorG = EC_POINT_new(curve);
	BIGNUM * x = BN_new(); BN_hex2bn(&x, "53FC09EE332C29AD0A7990053ED9B52A2B1A2FD60AEC69C698B2F204B6FF7CBFB5EDB6C0F6CE2308AB10DB9030B09E1043D5F22CDB9DFA55718BD9E7406CE8909760AF765DD5BCCB337C86548B72F2E1A702C3397A60DE74A7C1514DBA66910DD5CFB4CC80728D87EE9163A5B63F73EC80EC46C4967E0979880DC8ABEAE63895");
	BIGNUM * y = BN_new(); BN_hex2bn(&y, "0A8249063F6009F1F9F1F0533634A135D3E82016029906963D778D821E141178F5EA69F4654EC2B9E7F7F5E5F0DE55F66B598CCF9A140B2E416CFF0CA9E032B970DAE117AD547C6CCAD696B5B7652FE0AC6F1E80164AA989492D979FC5A4D5F213515AD7E9CB99A980BDAD5AD5BB4636ADB9B5706A67DCDE75573FD71BEF16D7");
	EC_POINT_set_affine_coordinates_GFp(curve, generatorG, x, y, CTX);

	BIGNUM * answer = BN_new();

	// COMPUTE PAIRING

	sprintf(ret+strlen(ret),"x(G) = %s\n",BN_bn2hex(x));
	sprintf(ret+strlen(ret),"y(G) = %s\n",BN_bn2hex(y));

	answer = TLpairing(generatorG,generatorG);

	// check g = <G,G> has been correctly computed
	// set value of gss, the correct value of <G,G> as taken from Pairing Based IBE
	sprintf(ret+strlen(ret),"g = <G,G> = %s\n",BN_bn2hex(answer));

	BIGNUM * GSS = BN_new(); BN_hex2bn(&GSS, "66FC2A432B6EA392148F15867D623068C6A87BD1FB94C41E27FABE658E015A87371E94744C96FEDA449AE9563F8BC446CBFDA85D5D00EF577072DA8F541721BEEE0FAED1828EAB90B99DFB0138C7843355DF0460B4A9FD74B4F1A32BCAFA1FFAD682C033A7942BCCE3720F20B9B7B0403C8CAE87B7A0042ACDE0FAB36461EA46");
	if(BN_cmp(answer,GSS)==0) sprintf(ret+strlen(ret),"The above value computed for g = <G,G> is correct\n");
	else sprintf(ret+strlen(ret),"The above value computed for g = <G,G> is INCORRECT\n");

	// TIMING

	clock_t t0,t1;
	unsigned int time=0;
	double timing;
	int i,N,top;
	N=5;
	top=1<<N;

	t0=clock();
	for(i=0 ; i<top ; i++)
	{
		answer = TLpairing(generatorG,generatorG);
	}
	t1=clock();

	time = t1 - t0;
	timing=(double)(time)/CLOCKS_PER_SEC;
	sprintf(ret+strlen(ret),"Time for 2^%d = %d pairings = %f\n",N,top,timing);
	sprintf(ret+strlen(ret),"Average time for 1 pairing = %f\n",(double)timing/((double)top));

//	// EM+A START
//
//	EC_POINT * R = EC_POINT_new(curve);
//	EC_POINT_copy(R,generatorG);
//
//	N=5;
//	top=1<<N;
//
//	t0=clock();
//	for(i=0 ; i<top ; i++)
//	{
//		EC_POINT_mul(curve, R, NULL, generatorG, primeQminusONE, CTX);
////		EC_POINT_dbl(curve,generatorG,generatorG,CTX);
////		EC_POINT_add(curve,R,R,generatorG,CTX);
//	}
//	t1=clock();
//
//	time = t1 - t0;
//	timing=(double)(time)/CLOCKS_PER_SEC;
//	sprintf(ret+strlen(ret),"Time for 2^%d = %d EM = %f\n",N,top,timing);
//	sprintf(ret+strlen(ret),"Average time for 1 EM = %f\n",(double)timing/((double)top));
//
//	// EM END

	return (*env)->NewStringUTF(env,ret);
}
